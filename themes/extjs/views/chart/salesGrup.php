<script type="text/javascript">
    window.onload = function () {
        var data = '<?php echo $data; ?>';
        var json = JSON.parse(data);
        var chart = new CanvasJS.Chart("chartContainer",
            {
                backgroundColor: null,
                title: {
                    text: "Sales Group <?php echo $chart_title?>"
                },
                legend:{
                    verticalAlign: "bottom",
                    horizontalAlign: "center"
                },
                data: [
                    {
                        indexLabelFontSize: 20,
                        indexLabelFontFamily: "Monospace",
                        indexLabelFontColor: "darkgrey",
                        indexLabelLineColor: "darkgrey",
                        indexLabelPlacement: "outside",
                        type: "pie",
                        showInLegend: true,
                        toolTipContent: "{y} - <strong>#percent%</strong> of <?php echo $total?>",
                        dataPoints: json
                    }
                ]
            });
        chart.render();
    }
</script>