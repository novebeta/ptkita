<h1>Sales Return Details by Group</h1>
<h2>GROUP : <?= $grup ?></h2>
<h3>FROM : <?= $start ?></h3>
<h3>TO : <?= $to ?></h3>
<h3>BRANCH : <?= $store ?></h3>
<?
$this->pageTitle = 'Sales Return Details by Group';
$this->widget('ext.groupgridview.GroupGridView', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'mergeColumns' => is_report_excel() ? array() : array('doc_ref', 'nama_customer', 'no_customer', 'nama_customer', 'tgl'),
    'columns' => array(
        array(
            'header' => 'Date',
            'name' => 'tgl'
        ),
        array(
            'header' => 'No. Natasha Receipt',
            'name' => 'doc_ref'
        ),
        array(
            'header' => 'Customer Number',
            'name' => 'no_customer'
        ),
        array(
            'header' => 'Customer Name',
            'name' => 'nama_customer'
        ),
        array(
            'header' => 'Item Code',
            'name' => 'kode_barang',
            'footer' => "Total"
        ),
        array(
            'header' => 'Quantity',
            'name' => 'qty',
            'value' => function ($data) {
                return format_number_report($data['qty']);
            },
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => format_number_report($qty)
        ),
        array(
            'header' => 'Price',
            'name' => 'price',
            'value' => function ($data) {
                return format_number_report($data['price'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => format_number_report($price, 2)
        ),
        array(
            'header' => 'Bruto',
            'name' => 'bruto',
            'value' => function ($data) {
                return format_number_report($data['bruto'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => format_number_report($bruto, 2)
        ),
        array(
            'header' => 'Disc Amount',
            'name' => 'discrp',
            'value' => function ($data) {
                return format_number_report($data['discrp'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => format_number_report($discrp, 2)
        ),
        array(
            'header' => 'VAT',
            'name' => 'vatrp',
            'value' => function ($data) {
                return format_number_report($data['vatrp'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => format_number_report($vatrp, 2)
        ),
        array(
            'header' => 'Total',
            'name' => 'total',
            'value' => function ($data) {
                return format_number_report($data['total'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => format_number_report($total, 2)
        )
    ),
));
?>