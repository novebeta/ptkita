<h1>Customers Birthday List</h1>
<h3>FROM : <?= $from ?></h3>
<h3>TO : <?= $to ?></h3>
<h3>BRANCH : <?= $store ?></h3>
<?
$this->pageTitle = 'Customers Birthday List';
$this->widget('ext.groupgridview.GroupGridView', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'columns' => array(
        array(
            'header' => 'No. Customers',
            'name' => 'no_customer'
        ),
        array(
            'header' => 'Customers Name',
            'name' => 'nama_customer'
        ),
        array(
            'header' => 'Birth Place',
            'name' => 'tempat_lahir'
        ),
        array(
            'header' => 'BirthDay',
            'name' => 'tgl_lahir'
        ),
        array(
            'header' => 'Email',
            'name' => 'email'
        ),
        array(
            'header' => 'Phone',
            'name' => 'telp',
            'htmlOptions' => array('class' => 'text')
        ),
        array(
            'header' => 'First',
            'name' => 'awal'
        ),
        array(
            'header' => 'Last',
            'name' => 'akhir'
        ),
        array(
            'header' => 'Origin Branch',
            'name' => 'store'
        ),
        array(
            'header' => 'Address',
            'name' => 'alamat'
        ),
        array(
            'header' => 'Sub District',
            'name' => 'nama_kecamatan'
        ),
        array(
            'header' => 'City',
            'name' => 'nama_kota'
        ),
        array(
            'header' => 'Province',
            'name' => 'nama_provinsi'
        ),
        array(
            'header' => 'Country',
            'name' => 'nama_negara'
        ),
        array(
            'header' => 'Status',
            'name' => 'nama_status'
        ),
    )
));
?>