<h1>Account Payable</h1>
<h2><?= $supplier ?></h2>
<h3>FROM : <?= $from ?></h3>
<h3>TO : <?= $to ?></h3>
<h3>BRANCH : <?= $store ?></h3>
<?
$this->pageTitle = 'Account Payable';
$this->widget('ext.groupgridview.GroupGridView', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'columns' => array(
        array(
            'header' => 'Date',
            'name' => 'tgl'
        ),
        array(
            'header' => 'No. Natasha Receipt',
            'name' => 'doc_ref'
        ),
        array(
            'header' => 'Type',
            'name' => 'note'
        ),
        array(
            'header' => 'Receipt',
            'name' => 'no_faktur'
        ),
        array(
            'header' => 'Debt',
            'name' => 'hutang',
            'value' => function ($data) {
                return format_number_report($data['hutang'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'Payment',
            'name' => 'payment',
            'value' => function ($data) {
                return format_number_report($data['payment'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'Balance',
            'name' => 'saldo',
            'value' => function ($data) {
                return format_number_report($data['saldo'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        )
    )
));
?>