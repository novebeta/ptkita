<h1>General Ledger</h1>
<h2><?= $account_code ?> - <?= $account_name ?></h2>
<h3>FROM : <?= $from ?></h3>
<h3>TO : <?= $to ?></h3>
<h3>BRANCH : <?= $store ?></h3>
<?
$this->pageTitle = 'General Ledger';
$this->widget('ext.groupgridview.GroupGridView', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'columns' => array(
        array(
            'header' => 'Date',
            'name' => 'tgl'
        ),
        array(
            'header' => 'Description',
            'name' => 'memo_'
        ),
        array(
            'header' => 'Debit',
            'name' => 'Debit',
//            'value' => function ($data) {
//                    return number_format($data['Debit'], 2);
//                },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'Credit',
            'name' => 'Credit',
//            'value' => function ($data) {
//                    return number_format($data['Credit'], 2);
//                },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'Balance',
            'name' => 'Balance',
            'value' => function ($data) {
                return format_number_report($data['Balance'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        )
    )
));
?>