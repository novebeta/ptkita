<h1>CATEGORY CUSTOMERS</h1>
<h3>CATEGORY : <?= $kat ?></h3>
<h3>DATE : <?= sql2long_date($tgl) ?></h3>
<h3>BRANCH : <?= $store ?></h3>
<?
$this->pageTitle = 'BirthDay Customers';
$this->widget('ext.groupgridview.GroupGridView', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'columns' => array(
        array(
            'header' => 'No. Customers',
            'name' => 'no_customer'
        ),
        array(
            'header' => 'Customers Name',
            'name' => 'nama_customer'
        ),
        array(
            'header' => 'Birth Place',
            'name' => 'tempat_lahir'
        ),
        array(
            'header' => 'BirthDay',
            'name' => 'tgl_lahir'
        ),
        array(
            'header' => 'Email',
            'name' => 'email'
        ),
        array(
            'header' => 'Phone',
            'name' => 'telp',
            'htmlOptions' => array('class' => 'text')
        ),
        array(
            'header' => 'First',
            'name' => 'awal'
        ),
        array(
            'header' => 'Last',
            'name' => 'akhir'
        ),
        array(
            'header' => 'Origin Branch',
            'name' => 'store'
        ),
        array(
            'header' => 'Address',
            'name' => 'alamat'
        )
    )
));
?>