<h1>Sales Summary</h1>
<h3>FROM : <?= $start ?></h3>
<h3>TO : <?= $to ?></h3>
<h3>BRANCH : <?= $store ?></h3>
<?
$this->pageTitle = 'Sales Summary';
$this->widget('ext.groupgridview.GroupGridView', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'mergeColumns' => array('doc_ref', 'tgl'),
    'columns' => array(
        array(
            'header' => 'No. Natasha Receipt',
            'name' => 'doc_ref',
            'footer' => "Total :"
        ),
        array(
            'header' => 'Comment',
            'name' => 'ketdisc'
        ),
        array(
            'header' => 'Date',
            'name' => 'tgl'
        ),
        array(
            'header' => 'Customer Number',
            'name' => 'no_customer'
        ),
        array(
            'header' => 'Customer Name',
            'name' => 'nama_customer'
        ),
        array(
            'header' => 'Total Transaction',
            'name' => 'total',
            'value' => function ($data) {
                return format_number_report($data['total'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => format_number_report($total, 2)
        ),
        array(
            'header' => 'Payment Method',
            'name' => 'nama_bank'
        ),
        array(
            'header' => 'Collected',
            'name' => 'amount',
            'value' => function ($data) {
                return format_number_report($data['amount'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => format_number_report($amount, 2)
        ),
        array(
            'header' => 'Changed',
            'name' => 'kembali',
            'value' => function ($data) {
                return format_number_report($data['kembali'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => format_number_report($kembali, 2)
        ),
    ),
));
?>