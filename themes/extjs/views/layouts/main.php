<html>
<head>
    <meta charset="UTF-8">
    <link rel="shortcut icon" href="<?php echo bu(); ?>/images/icon-natasha.gif"/>
    <title><?php echo CHtml::encode(Yii::app()->name); ?></title>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/resources/css/ext-all.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/resources/css/xtheme-gray.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/grid/grid-examples.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/css/RowEditor.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/css/extjs.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/shared/icons/silk.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/css/aspnet/aspnet.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/css/silk_v013/silk013.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/form/combos.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/fileuploadfield/css/fileuploadfield.css"/>
    <style>
        #drop {
            border: 2px dashed #BBBBBB;
            border-radius: 5px;
            color: #BBBBBB;
            font: 20pt bold, "Vollkorn";
            padding: 25px;
            text-align: center;
        }

        * {
            font-size: 12px;
            font-family: Candara;
        }

        .container {
            display: table;
        }

        .search-item-table {
            display: table-row;
            color: #8B1D51;
        }

        .cell4 {
            display: table-cell;
            /*border: solid;*/
            /*border-width: thin;*/
            padding-left: 5px;
            padding-right: 5px;
        }

        .custom-sales-details .x-grid-row-selected .x-grid-cell-first {
            padding-left: 5px;
        }
        @media screen and (-webkit-min-device-pixel-ratio: 0) {
            .x-grid3-cell, /* Normal grid cell */
            .x-grid3-gcell { /* Grouped grid cell (esp. in head)*/
                box-sizing: border-box;
            }
        }
    </style>
</head>
<body>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/adapter/ext/ext-base.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/ext-all.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/lib.min.js"></script>
<script>
    var LOGOUT = false;
    DATE_NOW = Date.parseDate('<?=date("Y-m-d H:i:s")?>', 'Y-m-d H:i:s');
    BASE_URL = '<?=bu()===""?"/":bu();?>';
    SYSTEM_TITLE = '<?= app()->params['system_title']; ?>';
    SYSTEM_SUBTITLE = '<?= app()->params['system_subtitle']; ?>';
    SYSTEM_LOGO = '<img src="<?=bu(); ?>/images/logo.png" alt=""/>';
    SALES_TYPE = '<?=Users::is_audit();?>';
    HEADOFFICE = <?if(defined('HEADOFFICE')){    echo HEADOFFICE ? 'true' : 'false';    }else{    echo 'false';    }?>;
    SYSTEM_BANK_CASH = '<?=Bank::get_bank_cash_id();?>';
    SYSTEM_PETTY_CASH = '<?=Bank::get_petty_cash_id();?>';
    SYSTEM_BANK_CASH_WHILE = '<?=Bank::get_bank_cash_while_id();?>';
    PORT_CLOSED = true;
//    Ext.chart.Chart.CHART_URL = '<?//=bu(); ?>///js/ext340/resources/charts.swf';
    VALID_CARD = <?=VALID_CARD;?>;
    INTERVALSYNC = <?=INTERVALSYNC;?>;
    ENABLESYNC = <?=ENABLESYNC? 'true' : 'false';?>;
    STORE = '<?=STOREID;?>';
    if(!notReady()){
        var fs = require('fs');
        var data = fs.readFileSync('./config.json'),myObj;
        try {
            myObj = JSON.parse(data);
            PRINTER_RECEIPT = myObj.PRINTER_RECEIPT;
            PRINTER_CARD = myObj.PRINTER_CARD ;
            COM_POSIFLEX = myObj.COM_POSIFLEX ;
        }
        catch (err) {
            console.log('There has been an error parsing your JSON.')
            console.log(err);
        }
    }
    SALES_OVERRIDE = '<?
    $id = Yii::app()->user->getId();
    $user = Users::model()->findByPk($id);
    echo Users::get_override($user->user_id,$user->password) ? 1 : 0;
    ?>';
    function nwis_round_up(e) {
        return round(Math.round(round(e / 0.05, 2)) * 0.05, 2);
    }
    function goodbye(e) {
        if (!LOGOUT) {
            if (!e) e = window.event;
            e.cancelBubble = true;
            e.returnValue = 'You sure you want to leave?'; //This is displayed on the dialog
            if (e.stopPropagation) {
                e.stopPropagation();
                e.preventDefault();
            }
        }
    }
    window.onbeforeunload = goodbye;
    if (!notReady()) {
        var gui = require('nw.gui');
        var win = gui.Window.get();
        win.maximize();
        win.on('new-win-policy', function (frame, url, policy) {
            policy.forceNewPopup();
        });
    }
</script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/TableGrid.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/GroupSummary.js"></script>
<?
$dir = array('/js/view/');
foreach ($dir as $path) {
    $templatePath = dirname(Yii::app()->basePath) . $path;
    $files = scandir($templatePath);
    foreach ($files as $file) {
        if (is_file($templatePath . '/' . $file)) {
            ?>
            <script type="text/javascript" src="<?php echo(bu() . $path . $file); ?>"></script>
        <?
        }
    }
}
?>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/mainpanel.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/app.js"></script>
<?php echo $content; ?>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/sha512.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/fileuploadfield/FileUploadField.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/cc.js"></script>

<script type="text/javascript"></script>
<iframe id="myFrame" name="myFrame" style="border:none"></iframe>
</body>
</html>
