jun.ajaxCounter = 0;
//jun.globalStore = new Ext.ux.state.LocalStorageProvider({prefix: 'nscc-'});
jun.TreeUi = Ext.extend(Ext.tree.TreePanel, {
    title: "Menu",
    useArrows: !0,
    region: "west",
    split: !0,
    autoScroll: !0,
    rootVisible: !1,
    floatable: !1,
    containerScroll: !0,
    width: 240,
    initComponent: function () {
        this.root = {
            text: "Menu"
        };
        jun.TreeUi.superclass.initComponent.call(this);
//        this.on('load', function () {
//
//        });
    }
});
jun.sidebar = new jun.TreeUi({
    dataUrl: "site/tree"
});
jun.sidebar.on("click", function (a, b) {
    a.isLeaf() &&
    (b.stopEvent(), a.attributes.class == "logout" ? Ext.MessageBox.confirm("Logout", "Are sure want logout?", function (a) {
        if (a === "no") return;
        LOGOUT = true;
        window.location.href = "site/logout";
    }, this) : jun.mainPanel.loadClass(a.attributes.class));
});
var clock = new Ext.Toolbar.TextItem("Jam");
jun.Send = Ext.extend(Ext.Window, {
    width: 1,
    height: 1,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "padding: 10px",
                id: "form-Send",
                labelWidth: 1,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: []
            }
        ];
        jun.Send.superclass.initComponent.call(this);
    }
});
//var send = new jun.Send({});
//send.show(), send.hide(),
jun.mainPanel = new jun.TabsUi();
jun.ViewportUi = Ext.extend(Ext.Viewport, {
    layout: "border",
    initComponent: function () {
//        clearText();
        this.items = [
            {
                xtype: "box",
                region: "north",
                id: "app-header",
                html: SYSTEM_LOGO,
                height: 100
            },
            jun.sidebar,
            jun.mainPanel
        ];
        jun.ViewportUi.superclass.initComponent.call(this);
//        displayText("Welcome To Natasha",0);
    }
});
jun.myMask = new Ext.LoadMask(Ext.getBody(), {msg: "Processing... please wait"});
Ext.onReady(function () {
    Ext.EventManager.addListener(document, "keypress", function (e) {
        if (jun.ajaxCounter > 0) {
            return false;
        }
    });
    Ext.Ajax.timeout = 1800000;
    var a = function () {
        Ext.get("loading").remove(), Ext.fly("loading-mask").fadeOut({
            remove: !0
        });
    };
    Ext.QuickTips.init();
    //    loadText = "Sedang proses... silahkan tunggu";
    Ext.Ajax.on("beforerequest", function (conn, opts) {
        if (opts.url == "site/Sync") return;
        jun.myMask.show();
        jun.ajaxCounter++;
    });
    Ext.Ajax.on("requestcomplete", function (conn, response, opts) {
        if (opts.url == "site/Sync") return;
        if (jun.ajaxCounter > 1) {
            jun.ajaxCounter--;
        } else {
            jun.ajaxCounter = 0;
            jun.myMask.hide();
        }
    });
    Ext.Ajax.on("requestexception", function (conn, response, opts) {
        if (opts.url == "site/Sync") return;
        if (jun.ajaxCounter > 1) {
            jun.ajaxCounter--;
        } else {
            jun.ajaxCounter = 0;
            jun.myMask.hide();
        }
        switch (response.status) {
            case 403:
                window.location.href = 'site/logout';
                break;
            case 500:
                Ext.Msg.alert('Internal Server Error', response.responseText);
                break;
            default :
                Ext.Msg.alert(response.status + " " + response.statusText, response.responseText);
                break;
        }
    });
    var b = new jun.ViewportUi({});
    displayText("Welcome To Natasha", 0);
    //if (ENABLESYNC) {
    //    Ext.Direct.addProvider(
    //        {
    //            type: 'polling',
    //            url: 'site/Sync',
    //            interval: INTERVALSYNC
    //        }
    //    );
    //    Ext.Direct.on('message', function (e) {
    //        console.log(String.format('<p>{0} : {1}</p>', e.time, Ext.encode(e.data)));
    //    });
    //}
});