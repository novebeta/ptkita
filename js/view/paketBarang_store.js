jun.PaketBarangstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.PaketBarangstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'PaketBarangStoreId',
            url: 'PaketBarang',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'paket_barang_id'},
                {name: 'paket_details_id'},
                {name: 'barang_id'},
                {name: 'disc'}
            ]
        }, cfg));
    }
});
jun.rztPaketBarang = new jun.PaketBarangstore();
//jun.rztPaketBarang.load();
