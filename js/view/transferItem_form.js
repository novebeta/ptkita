jun.TransferItemWin = Ext.extend(Ext.Window, {
    title: 'Purchase Item',
    modez: 1,
    width: 605,
    height: 445,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4;padding: 10px',
                id: 'form-TransferItem',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "label",
                        text: "Doc. Ref:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'doc_ref',
                        hideLabel: false,
                        //hidden:true,
                        name: 'doc_ref',
                        id: 'doc_refid',
                        ref: '../doc_ref',
                        maxLength: 50,
                        x: 85,
                        y: 2,
                        height: 20,
                        width: 175,
                        readOnly: true
                    },
                    {
                        xtype: "label",
                        text: "Date:",
                        x: 5,
                        y: 35
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        fieldLabel: 'tgl',
                        name: 'tgl',
                        id: 'tglid',
                        format: 'd M Y',
                        width: 175,
                        readOnly: true,
                        allowBlank: false,
                        value : DATE_NOW,
                        x: 85,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "Due Date:",
                        x: 5,
                        y: 65
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl_jatuh_tempo',
                        fieldLabel: 'tgl',
                        name: 'tgl_jatuh_tempo',
                        id: 'tgl_jatuh_tempoid',
                        format: 'd M Y',
                        width: 175,
                        x: 85,
                        y: 62
                    },
                    {
                        xtype: "label",
                        text: "Invoice No:",
                        x: 295,
                        y: 35
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'doc_ref_other',
                        hideLabel: false,
                        //hidden:true,
                        name: 'doc_ref_other',
                        id: 'doc_ref_otherid',
                        ref: '../doc_ref_other',
                        maxLength: 50,
                        width: 170,
                        x: 400,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "Supplier:",
                        x: 295,
                        y: 5
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        ref: '../supplier',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztSupplierCmp,
                        emptyText: "Please select supplier",
                        hiddenName: 'supplier_id',
                        valueField: 'supplier_id',
                        displayField: 'supplier_name',
                        allowBlank: true,
                        width: 170,
                        x: 400,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "Note:",
                        x: 295,
                        y: 65
                    },
                    {
                        xtype: 'textarea',
                        fieldLabel: 'note',
                        hideLabel: false,
                        enableKeyEvents: true,
                        style : {textTransform: "uppercase"},
                        listeners: {
                            change: function (field, newValue, oldValue) {
                                field.setValue(newValue.toUpperCase());
                            }
                        },
                        name: 'note',
                        id: 'noteid',
                        ref: '../note',
                        width: 170,
                        x: 400,
                        y: 62,
                        height: 22
                    },
                    new jun.TransferItemDetailsGrid({
                        x: 5,
                        y: 95,
                        height: 260,
                        frameHeader: !1,
                        header: !1
                    }),
//                    {
//                        xtype: "label",
//                        text: "Disc:",
//                        x: 5,
//                        y: 365
//                    },
//                    {
//                        xtype: 'numericfield',
//                        hideLabel: false,
//                        enableKeyEvents: true,
//                        name: 'disc',
//                        id: 'discid',
//                        ref: '../disc',
//                        maxLength: 3,
//                        value: 0,
//                        minValue: 0,
//                        maxValue: 100,
//                        width: 175,
//                        x: 85,
//                        y: 362
//                    },
//                    {
//                        xtype: "label",
//                        text: "Disc  Amount:",
//                        x: 5,
//                        y: 395
//                    },
//                    {
//                        xtype: 'numericfield',
//                        hideLabel: false,
//                        //hidden:true,
//                        name: 'discrp',
//                        id: 'discrptransferouid',
//                        ref: '../discrp',
//                        readOnly: true,
//                        maxLength: 30,
//                        value: 0,
//                        width: 175,
//                        x: 85,
//                        y: 392
//                    },
//                    {
//                        xtype: "label",
//                        text: "Sub Total:",
//                        x: 295,
//                        y: 365
//                    },
//                    {
//                        xtype: 'numericfield',
//                        hideLabel: false,
//                        //hidden:true,
//                        name: 'bruto',
//                        id: 'subtotaltransferoutid',
//                        ref: '../subtotal',
//                        maxLength: 30,
//                        value: 0,
//                        readOnly: true,
//                        width: 170,
//                        x: 400,
//                        y: 362
//                    },
//                    {
//                        xtype: "label",
//                        text: "VAT:",
//                        x: 5,
//                        y: 395
//                    },
//                    {
//                        xtype: 'numericfield',
//                        hideLabel: false,
//                        //hidden:true,
//                        name: 'vat',
//                        id: 'vattransferitemid',
//                        ref: '../vat',
//                        maxLength: 30,
//                        value: 0,
//                        readOnly: true,
//                        width: 175,
//                        x: 85,
//                        y: 392
//                    },
//                    {
//                        xtype: "label",
//                        text: "Total:",
//                        x: 295,
//                        y: 395
//                    },
//                    {
//                        xtype: 'numericfield',
//                        hideLabel: false,
//                        //hidden:true,
//                        name: 'total',
//                        id: 'totaltransferoutid',
//                        ref: '../total',
//                        maxLength: 30,
//                        value: 0,
//                        readOnly: true,
//                        width: 170,
//                        x: 400,
//                        y: 392
//                    },
                    {
                        xtype: 'hidden',
                        name: 'type_',
                        value: 0
                    },
                    {
                        xtype: 'hidden',
                        id: 'totalpotid',
                        ref: '../totalpot',
                        name: 'totalpot'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Save',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Save & Close',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Cancel',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.TransferItemWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
//        this.disc.on('keyup', this.onDiscChange, this);
        this.on("close", this.onWinClose, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
            this.btnSaveClose.setVisible(true);
        }
        //this.setDateTime();
    },
    //setDateTime: function () {
    //    Ext.Ajax.request({
    //        url: 'GetDateTime',
    //        method: 'POST',
    //        scope: this,
    //        success: function (f, a) {
    //            var response = Ext.decode(f.responseText);
    //            this.tgl.setValue(Date.parseDate(response.datetime, 'Y-m-d H:i:s'));
    //        },
    //        failure: function (f, a) {
    //            switch (a.failureType) {
    //                case Ext.form.Action.CLIENT_INVALID:
    //                    Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
    //                    break;
    //                case Ext.form.Action.CONNECT_FAILURE:
    //                    Ext.Msg.alert('Failure', 'Ajax communication failed');
    //                    break;
    //                case Ext.form.Action.SERVER_INVALID:
    //                    Ext.Msg.alert('Failure', a.result.msg);
    //            }
    //        }
    //    });
    //},
    onDiscChange: function (txt, e) {
        var disc1 = parseFloat(txt.getValue());
        if (disc1 == 0) return;
        jun.rztTransferItemDetails.each(function (record) {
            var barang = jun.getBarang(record.data.barang_id);
            var price = parseFloat(record.data.price);
            var qty = parseFloat(record.data.qty);
            var disc = parseFloat(record.data.disc);
            var discrp = parseFloat(record.data.discrp);
            var bruto = round(price * qty,2);
            var vat = jun.getTax(record.data.barang_id);
            discrp = disc == 0 ? discrp : round((disc / 100) * bruto,2);
            var subtotal = bruto - discrp;
            var discrp1 = disc1 == 0 ? 0 : round((disc1 / 100) * subtotal,2);
            var totalpot = discrp + discrp1;
            var total = bruto - totalpot;
            var vatrp = round(total * vat,2);
            record.set('discrp', discrp);
            record.set('vat', vat);
            record.set('vatrp', vatrp);
            record.set('total_pot', totalpot);
            record.set('total', total);
            record.set('bruto', bruto);
            record.set('disc1', disc1);
            record.set('discrp1', discrp1);
            record.commit();
        });
    },
    onWinClose: function () {
        jun.rztTransferItemDetails.removeAll();
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        if (jun.rztTransferItemDetails.data.length == 0) {
            Ext.Msg.alert('Error', "Item details must set");
            this.btnDisabled(false);
            return;
        }
        var urlz = 'TransferItem/createin/';
        Ext.getCmp('form-TransferItem').getForm().submit({
            url: urlz,
            scope: this,
            params: {
                detil: Ext.encode(Ext.pluck(
                    jun.rztTransferItemDetails.data.items, "data"))
            },
            success: function (f, a) {
                jun.rztTransferItem.reload();
                jun.rztBarang.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-TransferItem').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }

        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }

});
jun.ReturnTransferItemWin = Ext.extend(Ext.Window, {
    title: 'Return Supplier Item',
    modez: 1,
    width: 605,
    height: 445,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4;padding: 10px',
                id: 'form-ReturnTransferItem',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "label",
                        text: "Doc. Ref:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'doc_ref',
                        hideLabel: false,
                        //hidden:true,
                        name: 'doc_ref',
                        id: 'doc_refid',
                        ref: '../doc_ref',
                        maxLength: 50,
                        x: 85,
                        y: 2,
                        height: 20,
                        width: 175,
                        readOnly: true
                    },
                    {
                        xtype: "label",
                        text: "Date:",
                        x: 5,
                        y: 35
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        fieldLabel: 'tgl',
                        name: 'tgl',
                        id: 'tglid',
                        format: 'd M Y',
                        readOnly: true,
                        allowBlank: false,
                        value : DATE_NOW,
                        width: 175,
                        x: 85,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "Supplier:",
                        x: 5,
                        y: 65
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        ref: '../supplier',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztSupplierCmp,
                        emptyText: "Please select supplier",
                        hiddenName: 'supplier_id',
                        valueField: 'supplier_id',
                        displayField: 'supplier_name',
                        allowBlank: true,
                        width: 175,
                        x: 85,
                        y: 62
                    },
                    {
                        xtype: "label",
                        text: "Note:",
                        x: 295,
                        y: 5
                    },
                    {
                        xtype: 'textarea',
                        fieldLabel: 'note',
                        hideLabel: false,
                        enableKeyEvents: true,
                        style : {textTransform: "uppercase"},
                        listeners: {
                            change: function (field, newValue, oldValue) {
                                field.setValue(newValue.toUpperCase());
                            }
                        },
                        name: 'note',
                        id: 'noteid',
                        ref: '../note',
                        width: 170,
                        x: 400,
                        y: 2,
                        height: 52
                    },
                    {
                        xtype: "label",
                        text: "Doc. Ref Purc:",
                        x: 295,
                        y: 65
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'doc_ref_other',
                        hideLabel: false,
                        //hidden:true,
                        name: 'doc_ref_other',
                        id: 'doc_ref_otherid',
                        ref: '../doc_ref_other',
                        maxLength: 50,
                        width: 170,
                        x: 400,
                        y: 62
                    },
                    new jun.TransferItemDetailsGrid({
                        x: 5,
                        y: 95,
                        height: 260,
                        frameHeader: !1,
                        header: !1
                    }),
//                    {
//                        xtype: "label",
//                        text: "Disc:",
//                        x: 5,
//                        y: 365
//                    },
//                    {
//                        xtype: 'numericfield',
//                        hideLabel: false,
//                        enableKeyEvents: true,
//                        name: 'disc',
//                        id: 'discid',
//                        ref: '../disc',
//                        maxLength: 3,
//                        value: 0,
//                        minValue: 0,
//                        maxValue: 100,
//                        width: 175,
//                        x: 85,
//                        y: 362
//                    },
//                    {
//                        xtype: "label",
//                        text: "Disc  Amount:",
//                        x: 5,
//                        y: 395
//                    },
//                    {
//                        xtype: 'numericfield',
//                        hideLabel: false,
//                        readOnly: true,
//                        name: 'discrp',
//                        id: 'discrptransferouid',
//                        ref: '../discrp',
//                        maxLength: 30,
//                        value: 0,
//                        width: 175,
//                        x: 85,
//                        y: 392
//                    },
//                    {
//                        xtype: "label",
//                        text: "Sub Total:",
//                        x: 295,
//                        y: 365
//                    },
//                    {
//                        xtype: 'numericfield',
//                        hideLabel: false,
//                        //hidden:true,
//                        name: 'bruto',
//                        id: 'subtotaltransferoutid',
//                        ref: '../subtotal',
//                        maxLength: 30,
//                        value: 0,
//                        readOnly: true,
//                        width: 170,
//                        x: 400,
//                        y: 362
//                    },
//                    {
//                        xtype: "label",
//                        text: "VAT:",
//                        x: 5,
//                        y: 395
//                    },
//                    {
//                        xtype: 'numericfield',
//                        hideLabel: false,
//                        //hidden:true,
//                        name: 'vat',
//                        id: 'vattransferitemid',
//                        ref: '../vat',
//                        maxLength: 30,
//                        value: 0,
//                        readOnly: true,
//                        width: 175,
//                        x: 85,
//                        y: 392
//                    },
//                    {
//                        xtype: "label",
//                        text: "Total:",
//                        x: 295,
//                        y: 395
//                    },
//                    {
//                        xtype: 'numericfield',
//                        hideLabel: false,
//                        //hidden:true,
//                        name: 'total',
//                        id: 'totaltransferoutid',
//                        ref: '../total',
//                        maxLength: 30,
//                        value: 0,
//                        readOnly: true,
//                        width: 170,
//                        x: 400,
//                        y: 392
//                    },
                    {
                        xtype: 'hidden',
                        name: 'type_',
                        value: 1
                    },
                    {
                        xtype: 'hidden',
                        id: 'totalpotid',
                        ref: '../totalpot',
                        name: 'totalpot'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Save',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Save & Close',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Cancel',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.ReturnTransferItemWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
//        this.disc.on('keyup', this.onDiscChange, this);
        this.on("close", this.onWinClose, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
            this.btnSaveClose.setVisible(true);
        }
        //this.setDateTime();
    },
    //setDateTime: function () {
    //    Ext.Ajax.request({
    //        url: 'GetDateTime',
    //        method: 'POST',
    //        scope: this,
    //        success: function (f, a) {
    //            var response = Ext.decode(f.responseText);
    //            this.tgl.setValue(Date.parseDate(response.datetime, 'Y-m-d H:i:s'));
    //        },
    //        failure: function (f, a) {
    //            switch (a.failureType) {
    //                case Ext.form.Action.CLIENT_INVALID:
    //                    Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
    //                    break;
    //                case Ext.form.Action.CONNECT_FAILURE:
    //                    Ext.Msg.alert('Failure', 'Ajax communication failed');
    //                    break;
    //                case Ext.form.Action.SERVER_INVALID:
    //                    Ext.Msg.alert('Failure', a.result.msg);
    //            }
    //        }
    //    });
    //},
    onDiscChange: function (txt, e) {
        var disc1 = parseFloat(txt.getValue());
        if (disc1 == 0) return;
        jun.rztTransferItemDetails.each(function (record) {
            var barang = jun.getBarang(record.data.barang_id);
            var price = parseFloat(record.data.price);
            var qty = parseFloat(record.data.qty);
            var disc = parseFloat(record.data.disc);
            var discrp = parseFloat(record.data.discrp);
            var bruto = round(price * qty,2);
            var vat = jun.getTax(record.data.barang_id);
            discrp = disc == 0 ? discrp : round((disc / 100) * bruto,2);
            var subtotal = bruto - discrp;
            var discrp1 = disc1 == 0 ? 0 : round((disc1 / 100) * subtotal,2);
            var totalpot = discrp + discrp1;
            var total = bruto - totalpot;
            var vatrp = round(total * vat,2);
            record.set('discrp', discrp);
            record.set('vat', vat);
            record.set('vatrp', vatrp);
            record.set('total_pot', totalpot);
            record.set('total', total);
            record.set('bruto', bruto);
            record.set('disc1', disc1);
            record.set('discrp1', discrp1);
            record.commit();
        });
    },
    onWinClose: function () {
        jun.rztTransferItemDetails.removeAll();
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        if (jun.rztTransferItemDetails.data.length == 0) {
            Ext.Msg.alert('Error', "Item details must set");
            this.btnDisabled(false);
            return;
        }
        var urlz = 'TransferItem/CreateOut/';
        Ext.getCmp('form-ReturnTransferItem').getForm().submit({
            url: urlz,
            scope: this,
            params: {
                detil: Ext.encode(Ext.pluck(
                    jun.rztTransferItemDetails.data.items, "data"))
            },
            success: function (f, a) {
                jun.rztReturnTransferItem.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-ReturnTransferItem').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }

        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }

});