jun.ClinicalTransstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.ClinicalTransstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'ClinicalTransStoreId',
            url: 'ClinicalTrans',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'clinical_trans_id'},
                {name: 'doc_ref'},
                {name: 'tgl'},
                {name: 'note'},
                {name: 'arus'},
                {name: 'store'},
                {name: 'tdate'},
                {name: 'id_user'},
                {name: 'up'},
                {name: 'tipe_clinical_id'}
            ]
        }, cfg));
    }
});
jun.rztClinicalTrans = new jun.ClinicalTransstore({baseParams: {arus: 1}});
jun.rztClinicalTransOut = new jun.ClinicalTransstore({baseParams: {arus: -1}});
//jun.rztClinicalTrans.load();
