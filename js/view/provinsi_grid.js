jun.ProvinsiGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "State",
    id: 'docs-jun.ProvinsiGrid',
    stripeRows: true,
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'State',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_provinsi',
            width: 100
        },
        {
            header: 'Country',
            sortable: true,
            resizable: true,
            dataIndex: 'negara_id',
            renderer: jun.renderNegara,
            width: 100
        },
    ],
    initComponent: function () {
        jun.rztNegaraLib.on({
            scope: this,
            load: {
                fn: function (a, b, c) {
                    if (jun.rztNegaraCmp.getTotalCount() === 0) {
                        jun.rztNegaraCmp.add(b);
                    }
                    this.store.baseParams = {mode: "grid"};
                    this.store.reload();
                    this.store.baseParams = {};
                }
            }
        });
        if (jun.rztNegaraLib.getTotalCount() === 0) {
            jun.rztNegaraLib.load();
        }
        this.store = jun.rztProvinsi;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }
            ]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Add State',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Edit State',
                    ref: '../btnEdit'
                }
            ]
        };
        jun.ProvinsiGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.ProvinsiWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a province");
            return;
        }
        var idz = selectedz.json.provinsi_id;
        var form = new jun.ProvinsiWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a province");
            return;
        }
        Ext.Ajax.request({
            url: 'Provinsi/delete/id/' + record.json.provinsi_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztProvinsi.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
})
