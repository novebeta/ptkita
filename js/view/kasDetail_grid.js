jun.KasDetailGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "KasDetail",
    id: 'docs-jun.KasDetailGrid',
    iconCls: "silk-grid",
    stripeRows: true,
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        {
            header: 'COA',
            resizable: true,
            dataIndex: 'account_code',
            menuDisabled: true,
            width: 100
        },
        {
            header: 'Note',
            resizable: true,
            dataIndex: 'item_name',
            menuDisabled: true,
            width: 100
        },
        {
            header: 'Total',
            resizable: true,
            dataIndex: 'total',
            menuDisabled: true,
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        }
    ],
    initComponent: function () {
        this.store = jun.rztKasDetail;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'buttongroup',
                    columns: 4,
                    defaults: {
                        scale: 'small'
                    },
                    items: [
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'COA :'
                        },
                        {
                            xtype: 'combo',
                            typeAhead: true,
                            triggerAction: 'all',
                            lazyRender: true,
                            style: 'margin-bottom:2px',
                            mode: 'local',
                            forceSelection: true,
                            fieldLabel: 'COA',
                            //store: this.mode,
                            store: jun.rztChartMasterCmp,
                            ref: '../../account_code',
                            matchFieldWidth: !1,
                            itemSelector: "div.search-item",
                            tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                                '<h3><span">{account_code} - {account_name}</span></h3><br />{description}',
                                "</div></tpl>"),
                            listWidth: 500,
                            hiddenName: 'account_code',
                            valueField: 'account_code',
                            displayField: 'account_code',
                            anchor: '100%'
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Total :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'totalid',
                            ref: '../../total',
                            width: 75,
                            value: 0,
                            minValue: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Note :'
                        },
                        {
                            xtype: 'uctextfield',
                            ref: '../../item',
                            width: 300,
                            colspan: 3,
                            maxLength: 100
                        }
                    ]
                },
                {
                    xtype: 'buttongroup',
                    columns: 3,
                    defaults: {
                        scale: 'large'
                    },
                    items: [
                        {
                            xtype: 'button',
                            text: 'Add',
                            height: 44,
                            ref: '../../btnAdd'
                        },
                        {
                            xtype: 'button',
                            text: 'Edit',
                            height: 44,
                            ref: '../../btnEdit'
                        },
                        {
                            xtype: 'button',
                            text: 'Del',
                            height: 44,
                            ref: '../../btnDelete'
                        }
                    ]
                }
            ]
        };
        jun.KasDetailGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.onClickbtnEdit, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    onStoreChange: function () {
        jun.rztKasDetail.refreshData();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var item_name = this.item.getValue();
        var account_code = this.account_code.getValue();
        var total = parseFloat(this.total.getValue());
        if (account_code == "" || account_code == undefined) {
            Ext.MessageBox.alert("Error", "COA must selected.");
            return;
        }
        if (total === 0) {
            Ext.MessageBox.alert("Error", "Total must greater than 0.");
            return;
        }
        if (this.btnEdit.text == 'Save') {
            var record = this.sm.getSelected();
            record.set('account_code', account_code);
            record.set('item_name', item_name);
            record.set('total', total);
            record.commit();
        } else {
            var c = jun.rztKasDetail.recordType,
                d = new c({
                    account_code: account_code,
                    item_name: item_name,
                    total: total
                });
            jun.rztKasDetail.add(d);

        }
        this.account_code.reset();
        this.item.reset();
        this.total.reset();
    },
    btnDisable: function (s) {
        this.btnAdd.setDisabled(s);
        this.btnDelete.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    onClickbtnEdit: function (btn) {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih item.");
            return;
        }
        if (btn.text == 'Edit') {
            this.account_code.setValue(record.data.account_code);
            this.item.setValue(record.data.item_name);
            this.total.setValue(record.data.total);
            btn.setText("Save");
            this.btnDisable(true);
        } else {
            this.loadForm();
            btn.setText("Edit");
            this.btnDisable(false);
        }
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih item.");
            return;
        }
        this.store.remove(record);
    }
});
