jun.SalestransDetailsstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.SalestransDetailsstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'SalestransDetailsStoreId',
            url: 'SalestransDetails',
            autoLoad: !1,
            autoSave: !1,
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'salestrans_details'},
                {name: 'barang_id'},
                {name: 'kode_barang'},
                {name: 'nama_barang'},
                {name: 'salestrans_id'},
                {name: 'qty', type: 'float'},
                {name: 'beauty_id'},
                {name: 'beauty2_id'},
                {name: 'disc', type: 'float'},
                {name: 'discrp', type: 'float'},
                {name: 'ketpot'},
                {name: 'vat', type: 'float'},
                {name: 'vatrp', type: 'float'},
                {name: 'beauty_tip', type: 'float'},
                {name: 'bruto', type: 'float'},
                {name: 'total', type: 'float'},
                {name: 'total_pot', type: 'float'},
                {name: 'price', type: 'float'},
                {name: 'jasa_dokter', type: 'float'},
                {name: 'dokter_id'},
                {name: 'disc_name'},
                {name: 'disc1', type: 'float'},
                {name: 'discrp1', type: 'float'},
                {name: 'paket_trans_id'},
                {name: 'paket_details_id'}
            ]
        }, cfg));
        this.on('add', this.refreshData, this);
        this.on('update', this.refreshData, this);
        this.on('remove', this.refreshData, this);
    },
    refreshData: function (t, rec, idx) {
        var subtotal = round(this.sum("total"), 2);
        var vatrp = this.sum("vatrp");
        var totalpot = this.sum("total_pot");
        var total_discrp1 = this.sum("discrp1");
//        var discrp = parseFloat(Ext.getCmp("discrpid").getValue());
        var bayar = parseFloat(Ext.getCmp("bayarid").getValue());
        Ext.getCmp("subtotalid").setValue(subtotal);
        Ext.getCmp("vatid").setValue(vatrp);
        Ext.getCmp("totalpotid").setValue(totalpot);
        Ext.getCmp("total_discrp1id").setValue(total_discrp1);
        var total_ori = round(subtotal - total_discrp1 + vatrp, 2);// - discrp;
        var total = nwis_round_up(total_ori);
        var rounding = total - total_ori;
        var kembali = bayar - total;
        Ext.getCmp("totalid").setValue(total);
        Ext.getCmp("roundingid").setValue(rounding);
        Ext.getCmp("kembaliid").setValue(kembali);
    }
});
jun.rztSalestransDetails = new jun.SalestransDetailsstore();
//jun.rztSalestransDetails.load();
