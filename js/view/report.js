jun.ReportNewCustomers = Ext.extend(Ext.Window, {
    title: "New Customers",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 175,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportNewCustomers",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportNewCustomers.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportNewCustomers").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportNewCustomers").getForm().url = "Report/NewCustomers";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportNewCustomers').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/NewCustomers";
        this.format.setValue("excel");
        if (notReady()) {
            Ext.getCmp("form-ReportNewCustomers").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportNewCustomers").getForm().url = url;
            var form = Ext.getCmp('form-ReportNewCustomers').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                '.xls', Ext.getCmp("form-ReportNewCustomers").getForm().getValues());
        }
    }
});
jun.ReportBirthDayCustomers = Ext.extend(Ext.Window, {
    title: "BirthDay Customers",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 175,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportBirthDayCustomers",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportBirthDayCustomers.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportBirthDayCustomers").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportBirthDayCustomers").getForm().url = "Report/BirthDayCustomers";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportBirthDayCustomers').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/BirthDayCustomers";
        this.format.setValue("excel");
        if (notReady()) {
            Ext.getCmp("form-ReportBirthDayCustomers").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportBirthDayCustomers").getForm().url = url;
            var form = Ext.getCmp('form-ReportBirthDayCustomers').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                '.xls', Ext.getCmp("form-ReportBirthDayCustomers").getForm().getValues());
        }
    }
});
jun.ReportCategoryCustomers = Ext.extend(Ext.Window, {
    title: "Category Patient",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 180,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportCategoryCustomers",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Category',
                        ref: '../kategori',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: new Ext.data.ArrayStore({
                            fields: ["id", "name"],
                            data: [
                                [1, "Most Active"],
                                [2, "very Active"],
                                [3, "Active Patient"],
                                [4, "Passive Patient"]
                            ]
                        }),
                        hiddenName: 'kategori',
                        name: 'kategori',
                        valueField: 'id',
                        displayField: 'name',
                        anchor: '100%'
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportCategoryCustomers.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportCategoryCustomers").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportCategoryCustomers").getForm().url = "Report/CategoryCustomers";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportCategoryCustomers').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/CategoryCustomers";
        this.format.setValue("excel");
        if (notReady()) {
            Ext.getCmp("form-ReportCategoryCustomers").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportCategoryCustomers").getForm().url = url;
            var form = Ext.getCmp('form-ReportCategoryCustomers').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                '.xls', Ext.getCmp("form-ReportCategoryCustomers").getForm().getValues());
        }
    }
});
jun.ReportBiaya = Ext.extend(Ext.Window, {
    title: "Cash Out",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 145,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportBiaya",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportBiaya.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportBiaya").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportBiaya").getForm().url = "Report/CashOut";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportBiaya').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/CashOut";
        this.format.setValue("excel");
        if (notReady()) {
            Ext.getCmp("form-ReportBiaya").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportBiaya").getForm().url = url;
            var form = Ext.getCmp('form-ReportBiaya').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                '.xls', Ext.getCmp("form-ReportBiaya").getForm().getValues());
        }
    }
});
jun.ReportInventoryMovements = Ext.extend(Ext.Window, {
    title: "Inventory Movements",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 175,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportInventoryMovements",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportInventoryMovements.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportInventoryMovements").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportInventoryMovements").getForm().url = "Report/InventoryMovements";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportInventoryMovements').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/InventoryMovements";
        this.format.setValue("excel");
        if (notReady()) {
            Ext.getCmp("form-ReportInventoryMovements").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportInventoryMovements").getForm().url = url;
            var form = Ext.getCmp('form-ReportInventoryMovements').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                '.xls', Ext.getCmp("form-ReportInventoryMovements").getForm().getValues());
        }
    }
});
jun.ReportInventoryMovementsClinical = Ext.extend(Ext.Window, {
    title: "Inventory Movements Clinical",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 205,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztKategoriClinicalCmp.getTotalCount() === 0) {
            jun.rztKategoriClinicalCmp.load();
        }
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportInventoryMovementsClinical",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        emptyText: 'ALL Clinical Category',
                        fieldLabel: 'Clinical Category',
                        store: jun.rztKategoriClinicalCmp,
                        hiddenName: 'kategori_clinical_id',
                        valueField: 'kategori_clinical_id',
                        displayField: 'nama_kategori',
                        anchor: '100%'
                    },
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportInventoryMovementsClinical.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportInventoryMovementsClinical").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportInventoryMovementsClinical").getForm().url = "Report/InventoryMovementsClinical";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportInventoryMovementsClinical').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/InventoryMovementsClinical";
        this.format.setValue("excel");
        if (notReady()) {
            Ext.getCmp("form-ReportInventoryMovementsClinical").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportInventoryMovementsClinical").getForm().url = url;
            var form = Ext.getCmp('form-ReportInventoryMovementsClinical').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                '.xls', Ext.getCmp("form-ReportInventoryMovementsClinical").getForm().getValues());
        }
    }
});
jun.ReportInventoryCard = Ext.extend(Ext.Window, {
    title: "Inventory Card",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 200,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztBarangNonJasa.getTotalCount() === 0) {
            jun.rztBarangNonJasa.load();
        }
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportInventoryCard",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Item',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        store: jun.rztBarangNonJasa,
                        hiddenName: 'barang_id',
                        valueField: 'barang_id',
                        ref: '../barang',
                        displayField: 'kode_barang',
                        anchor: '100%'
                    },
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        forceSelection: true,
                        allowBlank: false,
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportInventoryCard.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportInventoryCard").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportInventoryCard").getForm().url = "Report/InventoryCard";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportInventoryCard').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/InventoryCard";
        this.format.setValue("excel");
        if (notReady()) {
            Ext.getCmp("form-ReportInventoryCard").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportInventoryCard").getForm().url = url;
            var form = Ext.getCmp('form-ReportInventoryCard').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                '.xls', Ext.getCmp("form-ReportInventoryCard").getForm().getValues());
        }
    }
});
jun.ReportBeautySummary = Ext.extend(Ext.Window, {
    title: "Beautician Services Summary",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 175,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportBeautySummary",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportBeautySummary.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportBeautySummary").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportBeautySummary").getForm().url = "Report/BeautySummary";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportBeautySummary').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.Ajax.request({
            url: 'BeautyServices/CheckFinal',
            method: 'POST',
            scope: this,
            params: {
                tglfrom: this.tglfrom.getValue(),
                tglto: this.tglto.getValue(),
                store: this.store.getValue()
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                if (response.success == false) {
                    Ext.MessageBox.show({
                        title: 'Fatal Error',
                        msg: response.msg,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.ERROR
                    });
                    return;
                }
                var url = "Report/BeautySummary";
                this.format.setValue("excel");
                if (notReady()) {
                    Ext.getCmp("form-ReportBeautySummary").getForm().standardSubmit = !0;
                    Ext.getCmp("form-ReportBeautySummary").getForm().url = url;
                    var form = Ext.getCmp('form-ReportBeautySummary').getForm();
                    var el = form.getEl().dom;
                    var target = document.createAttribute("target");
                    target.nodeValue = "myFrame";
                    el.setAttributeNode(target);
                    el.action = form.url;
                    el.submit();
                } else {
                    DownloadOpen(url, 'POST',
                        '.xls', Ext.getCmp("form-ReportBeautySummary").getForm().getValues());
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
jun.ReportBeautyDetails = Ext.extend(Ext.Window, {
    title: "Beautician Services Details",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 175,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportBeautyDetails",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportBeautyDetails.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportBeautyDetails").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportBeautyDetails").getForm().url = "Report/BeautyDetails";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportBeautyDetails').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.Ajax.request({
            url: 'BeautyServices/CheckFinal',
            method: 'POST',
            scope: this,
            params: {
                tglfrom: this.tglfrom.getValue(),
                tglto: this.tglto.getValue(),
                store: this.store.getValue()
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                if (response.success == false) {
                    Ext.MessageBox.show({
                        title: 'Fatal Error',
                        msg: response.msg,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.ERROR
                    });
                    return;
                }
                var url = "Report/BeautyDetails";
                this.format.setValue("excel");
                if (notReady()) {
                    Ext.getCmp("form-ReportBeautyDetails").getForm().standardSubmit = !0;
                    Ext.getCmp("form-ReportBeautyDetails").getForm().url = url;
                    var form = Ext.getCmp('form-ReportBeautyDetails').getForm();
                    var el = form.getEl().dom;
                    var target = document.createAttribute("target");
                    target.nodeValue = "myFrame";
                    el.setAttributeNode(target);
                    el.action = form.url;
                    el.submit();
                } else {
                    DownloadOpen(url, 'POST',
                        '.xls', Ext.getCmp("form-ReportBeautyDetails").getForm().getValues());
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
jun.ReportSalesSummary = Ext.extend(Ext.Window, {
    title: "Sales Summary by Group",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 200,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztGrup.getTotalCount() === 0) {
            jun.rztGrup.load();
        }
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportSalesSummary",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Group',
                        store: jun.rztGrup,
                        ref: '../grup',
                        allowBlank: true,
                        emptyText: 'ALL Group',
                        hiddenName: 'grup_id',
                        valueField: 'grup_id',
                        displayField: 'nama_grup',
                        anchor: '100%'
                    },
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportSalesSummary.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportSalesSummary").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportSalesSummary").getForm().url = "Report/SalesSummary";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportSalesSummary').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/SalesSummary";
        this.format.setValue("excel");
        if (notReady()) {
            Ext.getCmp("form-ReportSalesSummary").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportSalesSummary").getForm().url = url;
            var form = Ext.getCmp('form-ReportSalesSummary').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            //target.nodeValue = "_blank";
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen('Report/SalesSummary', 'POST',
                '.xls', Ext.getCmp("form-ReportSalesSummary").getForm().getValues());
        }
    }
});
jun.ReportSalesDetails = Ext.extend(Ext.Window, {
    title: "Sales Details by Group",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 225,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztGrup.getTotalCount() === 0) {
            jun.rztGrup.load();
        }
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportSalesDetails",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Group',
                        store: jun.rztGrup,
                        ref: '../grup',
                        allowBlank: true,
                        emptyText: 'ALL Group',
                        hiddenName: 'grup_id',
                        valueField: 'grup_id',
                        displayField: 'nama_grup',
                        anchor: '100%'
                    },
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    },
                    {
                        xtype: 'checkbox',
                        fieldLabel: 'Real Sales',
                        boxLabel: "",
                        value: 0,
                        inputValue: 1,
                        uncheckedValue: 0,
                        name: "real"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportSalesDetails.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportSalesDetails").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportSalesDetails").getForm().url = "Report/SalesDetails";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportSalesDetails').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/SalesDetails";
        this.format.setValue("excel");
        if (notReady()) {
            Ext.getCmp("form-ReportSalesDetails").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportSalesDetails").getForm().url = url;
            var form = Ext.getCmp('form-ReportSalesDetails').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                '.xls', Ext.getCmp("form-ReportSalesDetails").getForm().getValues());
        }
    }
});
jun.ReportReturSalesSummary = Ext.extend(Ext.Window, {
    title: "Return Sales Summary",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 200,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztGrup.getTotalCount() === 0) {
            jun.rztGrup.load();
        }
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportReturSalesSummary",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Group',
                        store: jun.rztGrup,
                        ref: '../grup',
                        allowBlank: true,
                        emptyText: 'ALL Group',
                        hiddenName: 'grup_id',
                        valueField: 'grup_id',
                        displayField: 'nama_grup',
                        anchor: '100%'
                    },
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportReturSalesSummary.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportReturSalesSummary").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportReturSalesSummary").getForm().url = "Report/ReturSalesSummary";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportReturSalesSummary').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/ReturSalesSummary";
        this.format.setValue("excel");
        if (notReady()) {
            Ext.getCmp("form-ReportReturSalesSummary").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportReturSalesSummary").getForm().url = url;
            var form = Ext.getCmp('form-ReportReturSalesSummary').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                '.xls', Ext.getCmp("form-ReportReturSalesSummary").getForm().getValues());
        }
    }
});
jun.ReportReturSalesDetails = Ext.extend(Ext.Window, {
    title: "Sales Return Details by Group",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 200,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztGrup.getTotalCount() === 0) {
            jun.rztGrup.load();
        }
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportReturSalesDetails",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Group',
                        store: jun.rztGrup,
                        ref: '../grup',
                        allowBlank: true,
                        emptyText: 'ALL Group',
                        hiddenName: 'grup_id',
                        valueField: 'grup_id',
                        displayField: 'nama_grup',
                        anchor: '100%'
                    },
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportReturSalesDetails.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportReturSalesDetails").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportReturSalesDetails").getForm().url = "Report/ReturSalesDetails";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportReturSalesDetails').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/ReturSalesDetails";
        this.format.setValue("excel");
        if (notReady()) {
            Ext.getCmp("form-ReportReturSalesDetails").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportReturSalesDetails").getForm().url = url;
            var form = Ext.getCmp('form-ReportReturSalesDetails').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                '.xls', Ext.getCmp("form-ReportReturSalesDetails").getForm().getValues());
        }
    }
});
jun.ReportLaha = Ext.extend(Ext.Window, {
    title: "Daily Report",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 145,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportLaha",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        forceSelection: true,
                        allowBlank: false,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                }
//                {
//                    xtype: "button",
//                    iconCls: "silk13-html",
//                    text: "Show HTML",
//                    ref: "../btnPdf"
//                }
            ]
        };
        jun.ReportLaha.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
//        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportLaha").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportLaha").getForm().url = "Report/Laha";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportLaha').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportLaha").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportLaha").getForm().url = "Report/Laha";
        this.format.setValue("openOffice");
        var form = Ext.getCmp('form-ReportLaha').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportTender = Ext.extend(Ext.Window, {
    title: "Tenders",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 145,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportTender",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        forceSelection: true,
                        allowBlank: false,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
//                {
//                    xtype: "button",
//                    iconCls: "silk13-page_white_excel",
//                    text: "Save to Excel",
//                    ref: "../btnSave"
//                },
//                {
//                    xtype: "button",
//                    iconCls: "silk13-html",
//                    text: "Show HTML",
//                    ref: "../btnPdf"
//                },
                {
                    xtype: "button",
                    iconCls: "silk13-printer",
                    text: "Print",
                    ref: "../btnPrint"
                }
            ]
        };
        jun.ReportTender.superclass.initComponent.call(this);
//        this.btnSave.on("click", this.onbtnSaveclick, this);
//        this.btnPdf.on("click", this.onbtnPdfclick, this);
        this.btnPrint.on("click", this.onbtnPrintclick, this);
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportTender").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportTender").getForm().url = "Report/tenders";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportTender').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/tenders";
        this.format.setValue("excel");
        if (notReady()) {
            Ext.getCmp("form-ReportTender").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportTender").getForm().url = url;
            var form = Ext.getCmp('form-ReportTender').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                '.xls', Ext.getCmp("form-ReportTender").getForm().getValues());
        }
    },
    onbtnPrintclick: function () {
        Ext.Ajax.request({
            url: 'Tender/print',
            method: 'POST',
            scope: this,
            params: {
                tglfrom: this.tglfrom.getValue(),
                store: this.store.getValue()
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                //findDefaultPrinter();
                if (notReady()) {
                    return;
                }
//                qz.append(chr(27) + "\x70" + "\x30" + chr(25) + chr(25) + "\r");
//                qz.print();
//                qz.appendImage(getPath() + "images/img_receipt.png", "ESCP");
//                while (!qz.isDoneAppending()) {}
//                qz.append("\x1B\x40"); // 1
                //qz.append("\x1B\x21\x08"); // 2
//                qz.append("\x1B\x21\x01"); // 3
//                qz.append(response.msg);
//                qz.append("\x1D\x56\x41"); // 4
//                qz.append("\x1B\x40"); // 5
//                qz.print();
//                qz.appendHTML('<html><pre>' + response.msg + '</pre></html>');
//                qz.printHTML();
                printHTML('default', '<html><pre>' + response.msg + '</pre></html>');
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
jun.ReportSalesSummaryReceipt = Ext.extend(Ext.Window, {
    title: "Sales Summary",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 175,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportSalesSummaryReceipt",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportSalesSummaryReceipt.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportSalesSummaryReceipt").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportSalesSummaryReceipt").getForm().url = "Report/SalesSummaryReceipt";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportSalesSummaryReceipt').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/SalesSummary";
        this.format.setValue("excel");
        if (notReady()) {
            Ext.getCmp("form-ReportSalesSummaryReceipt").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportSalesSummaryReceipt").getForm().url = url;
            var form = Ext.getCmp('form-ReportSalesSummaryReceipt').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                '.xls', Ext.getCmp("form-ReportSalesSummaryReceipt").getForm().getValues());
        }
    }
});
jun.ReportSalesSummaryReceiptDetails = Ext.extend(Ext.Window, {
    title: "Sales Summary",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 175,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportSalesSummaryReceiptDetails",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportSalesSummaryReceiptDetails.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportSalesSummaryReceiptDetails").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportSalesSummaryReceiptDetails").getForm().url = "Report/SalesSummaryReceiptDetails";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportSalesSummaryReceiptDetails').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/SalesSummaryReceiptDetails";
        this.format.setValue("excel");
        if (notReady()) {
            Ext.getCmp("form-ReportSalesSummaryReceiptDetails").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportSalesSummaryReceiptDetails").getForm().url = url;
            var form = Ext.getCmp('form-ReportSalesSummaryReceiptDetails').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                '.xls', Ext.getCmp("form-ReportSalesSummaryReceiptDetails").getForm().getValues());
        }
    }
});
jun.ReportSalesNReturnDetails = Ext.extend(Ext.Window, {
    title: "Sales and Sales Return by Group",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 200,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        if (jun.rztGrup.getTotalCount() === 0) {
            jun.rztGrup.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportSalesNReturnDetails",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Group',
                        store: jun.rztGrup,
                        ref: '../grup',
                        allowBlank: true,
                        emptyText: 'ALL Group',
                        hiddenName: 'grup_id',
                        valueField: 'grup_id',
                        displayField: 'nama_grup',
                        anchor: '100%'
                    },
                    {
                        fieldLabel: 'From Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Print",
                    ref: "../btnPrint"
                }
            ]
        };
        jun.ReportSalesNReturnDetails.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportSalesNReturnDetails").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportSalesNReturnDetails").getForm().url = "Report/SalesnReturDetails";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportSalesNReturnDetails').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/SalesSummary";
        this.format.setValue("excel");
        if (notReady()) {
            Ext.getCmp("form-ReportSalesNReturnDetails").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportSalesNReturnDetails").getForm().url = url;
            var form = Ext.getCmp('form-ReportSalesNReturnDetails').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                '.xls', Ext.getCmp("form-ReportSalesNReturnDetails").getForm().getValues());
        }
    }
});
jun.ReportDokterSummary = Ext.extend(Ext.Window, {
    title: "Doctor Summary",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 175,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportDokterSummary",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportDokterSummary.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportDokterSummary").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportDokterSummary").getForm().url = "Report/DokterSummary";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportDokterSummary').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/DokterSummary";
        this.format.setValue("excel");
        if (notReady()) {
            Ext.getCmp("form-ReportDokterSummary").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportDokterSummary").getForm().url = url;
            var form = Ext.getCmp('form-ReportDokterSummary').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                '.xls', Ext.getCmp("form-ReportDokterSummary").getForm().getValues());
        }
    }
});
jun.ReportDokterDetails = Ext.extend(Ext.Window, {
    title: "Doctor Details",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 175,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportDokterDetails",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportDokterDetails.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportDokterDetails").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportDokterDetails").getForm().url = "Report/DokterDetails";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportDokterDetails').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/DokterDetails";
        this.format.setValue("excel");
        if (notReady()) {
            Ext.getCmp("form-ReportDokterDetails").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportDokterDetails").getForm().url = url;
            var form = Ext.getCmp('form-ReportDokterDetails').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                '.xls', Ext.getCmp("form-ReportDokterDetails").getForm().getValues());
        }
    }
});
jun.ReportGeneralLedger = Ext.extend(Ext.Window, {
    title: "General Ledger",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 200,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztChartMasterLib.getTotalCount() === 0) {
            jun.rztChartMasterLib.load();
        }
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportGeneralLedger",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        fieldLabel: 'COA',
                        store: jun.rztChartMasterLib,
                        hiddenName: 'account_code',
                        valueField: 'account_code',
                        forceSelection: true,
                        matchFieldWidth: !1,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span">{account_code} - {account_name}</span></h3><br />{description}',
                            "</div></tpl>"),
                        listWidth: 500,
                        displayField: 'account_code',
                        ref: '../cmbkode',
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportGeneralLedger.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportGeneralLedger").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportGeneralLedger").getForm().url = "Report/GeneralLedger";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportGeneralLedger').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/GeneralLedger";
        this.format.setValue("excel");
        if (notReady()) {
            Ext.getCmp("form-ReportGeneralLedger").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportGeneralLedger").getForm().url = url;
            var form = Ext.getCmp('form-ReportGeneralLedger').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                '.xls', Ext.getCmp("form-ReportGeneralLedger").getForm().getValues());
        }
    }
});
jun.ReportGeneralJournal = Ext.extend(Ext.Window, {
    title: "General Journal",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 175,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportGeneralJournal",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportGeneralJournal.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportGeneralJournal").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportGeneralJournal").getForm().url = "Report/GeneralJournal";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportGeneralJournal').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/GeneralJournal";
        this.format.setValue("excel");
        if (notReady()) {
            Ext.getCmp("form-ReportGeneralJournal").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportGeneralJournal").getForm().url = url;
            var form = Ext.getCmp('form-ReportGeneralJournal').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                '.xls', Ext.getCmp("form-ReportGeneralJournal").getForm().getValues());
        }
    }
});
jun.ReportLabaRugi = Ext.extend(Ext.Window, {
    title: "Profit and Loss Report",
    iconCls: "silk13-report",
    modez: 1,
    width: 325,
    height: 175,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportLabaRugi",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'Dari Tanggal',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "from_date",
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        fieldLabel: 'Sampai Tanggal',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "to_date",
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        anchor: '100%'
                    }
                ]
            }
        ], this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
//                {
//                    xtype: "button",
//                    iconCls: "silk13-page_white_acrobat",
//                    text: "Save to PDF",
//                    ref: "../btnPdf"
//                }
            ]
        };
        jun.ReportLabaRugi.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
//        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportLabaRugi").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportLabaRugi").getForm().url = "Report/LabaRugi";
        this.format.setValue("pdf");
        var form = Ext.getCmp('form-ReportLabaRugi').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/LabaRugi";
        this.format.setValue("excel");
        if (notReady()) {
            Ext.getCmp("form-ReportLabaRugi").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportLabaRugi").getForm().url = url;
            var form = Ext.getCmp('form-ReportLabaRugi').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                '.xls', Ext.getCmp("form-ReportLabaRugi").getForm().getValues());
        }
    }
});
jun.ReportBalanceSheet = Ext.extend(Ext.Window, {
    title: "Balance Sheet Movement",
    iconCls: "silk13-report",
    modez: 1,
    width: 325,
    height: 155,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportBalanceSheet",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'Dari Tanggal',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "from_date",
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        fieldLabel: 'Sampai Tanggal',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "to_date",
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ], this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
//                {
//                    xtype: "button",
//                    iconCls: "silk13-page_white_acrobat",
//                    text: "Save to PDF",
//                    ref: "../btnPdf"
//                }
            ]
        };
        jun.ReportBalanceSheet.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
//        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportBalanceSheet").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportBalanceSheet").getForm().url = "Report/BalanceSheet";
        this.format.setValue("pdf");
        var form = Ext.getCmp('form-ReportBalanceSheet').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/BalanceSheet";
        this.format.setValue("excel");
        if (notReady()) {
            Ext.getCmp("form-ReportBalanceSheet").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportBalanceSheet").getForm().url = url;
            var form = Ext.getCmp('form-ReportBalanceSheet').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                '.xls', Ext.getCmp("form-ReportBalanceSheet").getForm().getValues());
        }
    }
});
jun.ReportPayments = Ext.extend(Ext.Window, {
    title: "Payments Report",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 200,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportPayments",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Payment Method',
                        store: jun.rztBankCmp,
                        ref: '../bank',
                        hiddenName: 'bank_id',
                        valueField: 'bank_id',
                        displayField: 'nama_bank',
                        anchor: '100%'
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportPayments.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportPayments").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportPayments").getForm().url = "Report/Payments";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportPayments').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/Payments";
        this.format.setValue("excel");
        if (notReady()) {
            Ext.getCmp("form-ReportPayments").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportPayments").getForm().url = url;
            var form = Ext.getCmp('form-ReportPayments').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                '.xls', Ext.getCmp("form-ReportPayments").getForm().getValues());
        }
    }
});
jun.GenerateLabaRugi = Ext.extend(Ext.Window, {
    title: "Generate Profit Lost",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 170,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-GenerateLabaRugi",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    new jun.comboBulan({
                        name: 'month',
                        fieldLabel: 'Month',
                        anchor: '100%'
                    }),
                    {
                        xtype: "spinnerfield",
                        fieldLabel: "Year",
                        name: "year",
                        ref: "../year",
                        maxLength: 4,
                        minValue: 2012,
                        maxValue: 9999,
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        forceSelection: true,
                        allowBlank: false,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                }
            ]
        };
        jun.GenerateLabaRugi.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        var dt = new Date();
        this.year.setValue(dt.format('Y'));
    },
    onbtnSaveclick: function () {
        Ext.getCmp('form-GenerateLabaRugi').getForm().submit({
            url: 'Report/GenerateLabaRugi',
            scope: this,
            success: function (f, a) {
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                this.close();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
jun.ReportNeraca = Ext.extend(Ext.Window, {
    title: "Balance Sheet Report",
    iconCls: "silk13-report",
    modez: 1,
    width: 325,
    height: 120,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportNeraca",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "to_date",
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ], this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
//                {
//                    xtype: "button",
//                    iconCls: "silk13-page_white_acrobat",
//                    text: "Save to PDF",
//                    ref: "../btnPdf"
//                }
            ]
        };
        jun.ReportNeraca.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
//        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportNeraca").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportNeraca").getForm().url = "Report/Neraca";
        this.format.setValue("pdf");
        var form = Ext.getCmp('form-ReportNeraca').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportNeraca").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportNeraca").getForm().url = "Report/Neraca";
        this.format.setValue("openOffice");
        var form = Ext.getCmp('form-ReportNeraca').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportSalesPrice = Ext.extend(Ext.Window, {
    title: "Sales Price",
    iconCls: "silk13-report",
    modez: 1,
    width: 325,
    height: 120,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportSalesPrice",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        forceSelection: true,
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ], this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
//                {
//                    xtype: "button",
//                    iconCls: "silk13-page_white_acrobat",
//                    text: "Save to PDF",
//                    ref: "../btnPdf"
//                }
            ]
        };
        jun.ReportSalesPrice.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
//        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportSalesPrice").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportSalesPrice").getForm().url = "Report/DaftarHargaJual";
        this.format.setValue("pdf");
        var form = Ext.getCmp('form-ReportSalesPrice').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/DaftarHargaJual";
        this.format.setValue("excel");
        if (notReady()) {
            Ext.getCmp("form-ReportSalesPrice").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportSalesPrice").getForm().url = url;
            var form = Ext.getCmp('form-ReportSalesPrice').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit()
        } else {
            DownloadOpen(url, 'POST',
                '.xls', Ext.getCmp("form-ReportSalesPrice").getForm().getValues());
        }
    }
});
jun.ReportPurchasePrice = Ext.extend(Ext.Window, {
    title: "Purchase Price",
    iconCls: "silk13-report",
    modez: 1,
    width: 325,
    height: 125,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportPurchasePrice",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        forceSelection: true,
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ], this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
//                {
//                    xtype: "button",
//                    iconCls: "silk13-page_white_acrobat",
//                    text: "Save to PDF",
//                    ref: "../btnPdf"
//                }
            ]
        };
        jun.ReportPurchasePrice.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
//        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportPurchasePrice").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportPurchasePrice").getForm().url = "Report/DaftarHargaBeli";
        this.format.setValue("pdf");
        var form = Ext.getCmp('form-ReportPurchasePrice').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportPurchasePrice").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportPurchasePrice").getForm().url = "Report/DaftarHargaBeli";
        this.format.setValue("openOffice");
        var form = Ext.getCmp('form-ReportPurchasePrice').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ClosingStore = Ext.extend(Ext.Window, {
    title: "Yearly Closing",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 155,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ClosingStore",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: "spinnerfield",
                        fieldLabel: "Year",
                        name: "year",
                        ref: "../year",
                        maxLength: 4,
                        minValue: 2012,
                        maxValue: 9999,
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        forceSelection: true,
                        allowBlank: false,
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    //iconCls: "silk13-page_white_excel",
                    text: "CLOSING",
                    ref: "../btnSave"
                }
            ]
        };
        jun.ClosingStore.superclass.initComponent.call(this);
        this.btnSave.on("click", this.deleteRec, this);
        var dt = new Date();
        this.year.setValue(dt.format('Y'));
    },
    deleteRec: function () {
        Ext.MessageBox.confirm("Confirmation", "Are you sure want to closing yearly? <br>" +
        "This will clear all transaction and can't undo. Please backup first!", this.onbtnSaveclick, this);
    },
    onbtnSaveclick: function (a) {
        if (a == "no") return;
        Ext.getCmp('form-ClosingStore').getForm().submit({
            url: 'site/Closing',
            scope: this,
            success: function (f, a) {
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                this.close();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
jun.ReportRekeningKoran = Ext.extend(Ext.Window, {
    title: "Bank/Cash Account Statement",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 175,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        if (jun.rztBankTransCmpPusat.getTotalCount() === 0) {
            jun.rztBankTransCmpPusat.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportRekeningKoran",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Bank',
                        store: jun.rztBankTransCmpPusat,
                        ref: '../bank',
                        hiddenName: 'bank_id',
                        valueField: 'bank_id',
                        displayField: 'nama_bank',
                        anchor: '100%'
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportRekeningKoran.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportRekeningKoran").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportRekeningKoran").getForm().url = "Report/RekeningKoran";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportRekeningKoran').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/RekeningKoran";
        this.format.setValue("excel");
        if (notReady()) {
            Ext.getCmp("form-ReportRekeningKoran").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportRekeningKoran").getForm().url = url;
            var form = Ext.getCmp('form-ReportRekeningKoran').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                '.xls', Ext.getCmp("form-ReportRekeningKoran").getForm().getValues());
        }
    }
});
jun.ReportKartuHutang = Ext.extend(Ext.Window, {
    title: "Account Payable",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 200,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztSupplierCmp.getTotalCount() === 0) {
            jun.rztSupplierCmp.load();
        }
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportKartuHutang",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        fieldLabel: 'Supplier',
                        typeAhead: true,
                        triggerAction: 'all',
                        id: 'supplierid',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        ref: '../supplier',
                        store: jun.rztSupplierCmp,
                        hiddenName: 'supplier_id',
                        valueField: 'supplier_id',
                        displayField: 'supplier_name',
                        matchFieldWidth: !1,
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportKartuHutang.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportKartuHutang").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportKartuHutang").getForm().url = "Report/KartuHutang";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportKartuHutang').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/KartuHutang";
        this.format.setValue("excel");
        if (notReady()) {
            Ext.getCmp("form-ReportKartuHutang").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportKartuHutang").getForm().url = url;
            var form = Ext.getCmp('form-ReportKartuHutang').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                '.xls', Ext.getCmp("form-ReportKartuHutang").getForm().getValues());
        }
    }
});
jun.CustAttstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.CustAttstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'CustAttstoreId',
            url: 'Report/CustAtt',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'tgl'},
                {name: 'visits'}
            ]
        }, cfg));
    }
});
jun.rztCustAttstore = new jun.CustAttstore();
jun.ReportChartCustAtt = Ext.extend(Ext.Panel, {
    title: 'Customers Attandence',
    ////renderTo: 'container',
    //width:500,
    //height:300,
    layout: 'fit',
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Branch :'
                },
                {
                    xtype: 'combo',
                    typeAhead: true,
                    fieldLabel: 'Branch',
                    ref: '../store',
                    triggerAction: 'all',
                    lazyRender: true,
                    mode: 'local',
                    store: jun.rztStoreCmp,
                    hiddenName: 'store',
                    name: 'store',
                    valueField: 'store_kode',
                    displayField: 'store_kode',
                    forceSelection: true,
                    allowBlank: false,
                    value: STORE,
                    readOnly: !HEADOFFICE,
                    width: 100
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'From :'
                },
                {
                    xtype: 'xdatefield',
                    id: 'tglfromchartattid',
                    ref: '../fromtgl'
                },
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'To :'
                },
                {
                    xtype: 'xdatefield',
                    id: 'tgltochartattid',
                    ref: '../totgl'
                },
                {
                    xtype: 'button',
                    text: 'Refresh',
                    ref: '../btnRefresh'
                }
            ]
        };
        this.items = {
            xtype: 'linechart',
            store: jun.rztCustAttstore,
            xField: 'tgl',
            yField: 'visits',
            background: '#FDDFFE',
            yAxis: new Ext.chart.NumericAxis({
                title: 'Visits',
                displayName: 'Visits',
                labelRenderer: Ext.util.Format.numberRenderer('0,0'),
                label: {
                    display: 'inside', // or 'rotate'
                    field: 'visits',
                    'text-anchor': 'start'
                }
            }),
            xAxis: new Ext.chart.CategoryAxis({
                title: 'Date'
            }),
            extraStyle: {
                font: {
                    color: '#8B1D51'
                },
                background: {
                    color: '#FDDFFE',
                    alpha: .9
                },
                xAxis: {
                    color: '#8B1D51',
                    labelRotation: -90
                },
                yAxis: {
                    color: '#8B1D51',
                    titleRotation: -90
                }
            },
            listeners: {
                itemclick: function (o) {
                    var rec = store.getAt(o.index);
                    Ext.example.msg('Item Selected', 'You chose {0}.', rec.get('name'));
                }
            }
        }
        jun.ReportChartCustAtt.superclass.initComponent.call(this);
        this.btnRefresh.on('Click', this.loadRecord, this);
    },
    loadRecord: function () {
        var fromtgl = this.fromtgl.getValue();
        var totgl = this.totgl.getValue();
        var store = this.store.getValue();
        if (fromtgl == '' || fromtgl == undefined) {
            Ext.example.msg('From Date', 'From Date must selected');
            return;
        }
        if (totgl == '' || totgl == undefined) {
            Ext.example.msg('To Date', 'To Date must selected');
            return;
        }
        jun.rztCustAttstore.baseParams = {
            from: fromtgl,
            to: totgl,
            store: store
        };
        jun.rztCustAttstore.load();
        jun.rztCustAttstore.baseParams = {};
    }
});
jun.ChartNewCustomers = Ext.extend(Ext.Window, {
    title: "Chart New Customers",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 175,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ChartNewCustomers",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "from",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "to",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-chart_line",
                    text: "Show Chart",
                    ref: "../btnSave"
                }
            ]
        };
        jun.ChartNewCustomers.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ChartNewCustomers").getForm().standardSubmit = !0;
        Ext.getCmp("form-ChartNewCustomers").getForm().url = "Chart/NewCust";
        var form = Ext.getCmp('form-ChartNewCustomers').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ChartCustAtt = Ext.extend(Ext.Window, {
    title: "Chart Customers Attendance",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 175,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ChartCustAtt",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "from",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "to",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-chart_line",
                    text: "Show Chart",
                    ref: "../btnSave"
                }
            ]
        };
        jun.ChartCustAtt.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ChartCustAtt").getForm().standardSubmit = !0;
        Ext.getCmp("form-ChartCustAtt").getForm().url = "Chart/custatt";
        var form = Ext.getCmp('form-ChartCustAtt').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ChartSalesGrup = Ext.extend(Ext.Window, {
    title: "Chart Sales Group",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 175,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ChartSalesGrup",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "from",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "to",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-chart_pie",
                    text: "Show Chart",
                    ref: "../btnSave"
                }
            ]
        };
        jun.ChartSalesGrup.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ChartSalesGrup").getForm().standardSubmit = !0;
        Ext.getCmp("form-ChartSalesGrup").getForm().url = "Chart/SalesGrup";
        var form = Ext.getCmp('form-ChartSalesGrup').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ChartTopCust = Ext.extend(Ext.Window, {
    title: "Chart Top Customers",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 200,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ChartTopCust",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "from",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "to",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'Limit',
                        xtype: 'numericfield',
                        name: "limit",
                        ref: '../limit',
                        value: 10,
                        anchor: "100%"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-chart_bar",
                    text: "Show Chart",
                    ref: "../btnSave"
                }
            ]
        };
        jun.ChartTopCust.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ChartTopCust").getForm().standardSubmit = !0;
        Ext.getCmp("form-ChartTopCust").getForm().url = "Chart/TopCust";
        var form = Ext.getCmp('form-ChartTopCust').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ChartTopSalesGrup = Ext.extend(Ext.Window, {
    title: "Chart Top Sales Group",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 230,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztGrup.getTotalCount() === 0) {
            jun.rztGrup.load();
        }
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ChartTopSalesGrup",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Group',
                        store: jun.rztGrup,
                        ref: '../grup',
                        allowBlank: true,
                        emptyText: 'ALL Group',
                        hiddenName: 'grup_id',
                        valueField: 'grup_id',
                        displayField: 'nama_grup',
                        anchor: '100%'
                    },
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "from",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "to",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'Limit',
                        xtype: 'numericfield',
                        name: "limit",
                        ref: '../limit',
                        value: 10,
                        anchor: "100%"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-chart_bar",
                    text: "Show Chart",
                    ref: "../btnSave"
                }
            ]
        };
        jun.ChartTopSalesGrup.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ChartTopSalesGrup").getForm().standardSubmit = !0;
        Ext.getCmp("form-ChartTopSalesGrup").getForm().url = "Chart/TopSalesGrup";
        var form = Ext.getCmp('form-ChartTopSalesGrup').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});