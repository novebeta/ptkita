jun.BarangClinicalWin = Ext.extend(Ext.Window, {
    title: 'Operational Item',
    modez: 1,
    width: 400,
    height: 205,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    resizable: !1,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4;padding: 10px',
                id: 'form-BarangClinical',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'Item Code',
                        hideLabel: false,
                        //hidden:true,
                        name: 'kode_barang',
                        id: 'kode_barangid',
                        ref: '../kode_barang',
                        maxLength: 15,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'Item Name',
                        hideLabel: false,
                        //hidden:true,
                        name: 'nama_barang',
                        id: 'nama_barangid',
                        ref: '../nama_barang',
                        maxLength: 100,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'Unit',
                        hideLabel: false,
                        //hidden:true,
                        name: 'sat',
                        id: 'satid',
                        ref: '../sat',
                        maxLength: 10,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Clinical Category',
                        store: jun.rztKategoriClinicalCmp,
                        hiddenName: 'kategori_clinical_id',
                        valueField: 'kategori_clinical_id',
                        displayField: 'nama_kategori',
                        anchor: '100%'
                    }
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Save',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Save & Close',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Close',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.BarangClinicalWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz;
        if (this.modez == 1 || this.modez == 2) {
            urlz = 'BarangClinical/update/id/' + this.id;
        } else {
            urlz = 'BarangClinical/create/';
        }
        Ext.getCmp('form-BarangClinical').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            success: function (f, a) {
                jun.rztBarangClinical.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-BarangClinical').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});