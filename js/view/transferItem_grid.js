jun.TransferItemGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Purchase Item",
    id: 'docs-jun.TransferItemGrid',
    iconCls: "silk-grid",
    stripeRows: true,
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        {
            header: 'Date',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 100
        },
        {
            header: 'Doc. Ref',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100
        },
        {
            header: 'Note',
            sortable: true,
            resizable: true,
            dataIndex: 'note',
            width: 100
        },
        {
            header: 'Branch',
            sortable: true,
            resizable: true,
            dataIndex: 'store',
            width: 100
        }
    ],
    initComponent: function () {
        if (jun.rztBarangLib.getTotalCount() === 0) {
            jun.rztBarangLib.load();
        }
        if (jun.rztBarangNonJasaAll.getTotalCount() === 0) {
            jun.rztBarangNonJasaAll.load();
        }
        if (jun.rztSupplierCmp.getTotalCount() === 0) {
            jun.rztSupplierCmp.load();
        }
        if (jun.rztGrup.getTotalCount() === 0) {
            jun.rztGrup.load();
        }
        jun.rztTransferItem.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    b.params = {
                        tgl: Ext.getCmp('tgltransferitemgrid').getValue()
                    }
                }
            }
        });
        this.store = jun.rztTransferItem;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Add Purchase Item',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'View Purchase Item',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Date :'
                },
                {
                    xtype: 'xdatefield',
                    ref: '../tgl',
                    id:'tgltransferitemgrid'
                }
            ]
        };
        jun.TransferItemGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.tgl.on('select', this.refreshTgl, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.store.removeAll();
    },
    refreshTgl: function () {
        this.store.reload();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.TransferItemWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a transaction");
            return;
        }
        var idz = selectedz.json.transfer_item_id;
        var form = new jun.TransferItemWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        //form.tgl.setValue(Date.parseDate(this.record.data.tgl, 'Y-m-d H:i:s'));
        jun.rztTransferItemDetails.proxy.setUrl('TransferItemDetails/IndexIn');
        jun.rztTransferItemDetails.baseParams = {
            transfer_item_id: idz
        };
        jun.rztTransferItemDetails.load();
        jun.rztTransferItemDetails.baseParams = {};
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'TransferItem/delete/id/' + record.json.transfer_item_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztTransferItem.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
jun.ReturnTransferItemGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Return Supplier Item",
    id: 'docs-jun.ReturnTransferItemGrid',
    iconCls: "silk-grid",
    stripeRows: true,
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        {
            header: 'Date',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 100
        },
        {
            header: 'Doc. Ref',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100
        },
        {
            header: 'Note',
            sortable: true,
            resizable: true,
            dataIndex: 'note',
            width: 100
        },
        {
            header: 'Branch',
            sortable: true,
            resizable: true,
            dataIndex: 'store',
            width: 100
        }
    ],
    initComponent: function () {
        if (jun.rztBarangLib.getTotalCount() === 0) {
            jun.rztBarangLib.load();
        }
        if (jun.rztBarangNonJasa.getTotalCount() === 0) {
            jun.rztBarangNonJasa.load();
        }
        if (jun.rztSupplierCmp.getTotalCount() === 0) {
            jun.rztSupplierCmp.load();
        }
        if (jun.rztGrup.getTotalCount() === 0) {
            jun.rztGrup.load();
        }
        jun.rztReturnTransferItem.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    b.params = {
                        tgl: Ext.getCmp('tglreturntransferitemgrid').getValue()
                    }
                }
            }
        });
        this.store = jun.rztReturnTransferItem;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Add Return Item',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'View Return Item',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: '\xA0Date :'
                },
                {
                    xtype: 'xdatefield',
                    ref: '../tgl',
                    id:'tglreturntransferitemgrid'
                }
            ]
        };
        jun.ReturnTransferItemGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.tgl.on('select', this.refreshTgl, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.store.removeAll();
    },
    refreshTgl: function () {
        this.store.reload();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.ReturnTransferItemWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a transaction");
            return;
        }
        var idz = selectedz.json.transfer_item_id;
        var form = new jun.ReturnTransferItemWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        //form.tgl.setValue(Date.parseDate(this.record.data.tgl, 'Y-m-d H:i:s'));
        jun.rztTransferItemDetails.proxy.setUrl('TransferItemDetails/IndexOut');
        jun.rztTransferItemDetails.baseParams = {
            transfer_item_id: idz
        };
        jun.rztTransferItemDetails.load();
        jun.rztTransferItemDetails.baseParams = {};
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'ReturnTransferItem/delete/id/' + record.json.transfer_item_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztReturnTransferItem.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
})

