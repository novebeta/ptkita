jun.Diskonstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Diskonstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'DiskonStoreId',
            url: 'Diskon',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'diskon_id'},
                {name: 'status_cust_id'},
                {name: 'barang_id'},
                {name: 'value'},
                {name: 'store'}
            ]
        }, cfg));
    }
});
jun.rztDiskon = new jun.Diskonstore();
//jun.rztDiskon.load();
