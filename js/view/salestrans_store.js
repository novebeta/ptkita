jun.Salestransstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Salestransstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'SalestransStoreId',
            url: 'Salestrans',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'salestrans_id'},
                {name: 'tgl'},
                {name: 'doc_ref'},
                {name: 'tdate'},
                {name: 'user_id'},
                {name: 'bruto'},
                {name: 'disc'},
                {name: 'discrp'},
                {name: 'totalpot'},
                {name: 'total'},
                {name: 'ketdisc'},
                {name: 'vat'},
                {name: 'customer_id'},
                {name: 'doc_ref_sales'},
                {name: 'audit'},
                {name: 'store'},
                {name: 'printed'},
                {name: 'override'},
                {name: 'nama_customer'},
                {name: 'no_customer'},
                {name: 'bayar'},
                {name: 'kembali'},
                {name: 'dokter_id'},
                {name: 'total_discrp1'},
                {name: 'log'},
                {name: 'rounding'}
            ]
        }, cfg));
    }
});
jun.rztSalestrans = new jun.Salestransstore();
jun.rztSalestransMgMCmp = new jun.Salestransstore();
jun.rztSalestransMsDCmp = new jun.Salestransstore();
jun.rztHistory = new jun.Salestransstore({url: 'salestrans/History'});
jun.beautytransstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.beautytransstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'beautytransStoreId',
            url: 'Salestrans/BeautyService',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'salestrans_id'},
                {name: 'doc_ref'},
                {name: 'salestrans_details'},
                {name: 'barang_id'},
                {name: 'kode_barang'},
                {name: 'nama_barang'},
                {name: 'qty'},
                {name: 'beauty_id'},
                {name: 'beauty2_id'},
                {name: 'dokter_id'},
                {name: 'jasa_dokter'},
                {name: 'nama_customer'},
                {name: 'no_customer'},
                {name: 'final'},
                {name: 'store'}
            ]
        }, cfg));
    }
});
jun.rztBeutytrans = new jun.beautytransstore();
//jun.rztSalestrans.load();
