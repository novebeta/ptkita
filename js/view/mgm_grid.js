jun.MgmGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Member Get Member",
    id: 'docs-jun.MgmGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header:'Doc. Ref',
            sortable:true,
            resizable:true,
            dataIndex:'doc_ref',
            width:100
        },
        {
            header: 'New Customer',
            sortable: true,
            resizable: true,
            dataIndex: 'customer_new_name',
            width: 100
        },
        {
            header: 'Ref. Customer',
            sortable: true,
            resizable: true,
            dataIndex: 'customer_friend_name',
            width: 100
        },
        {
            header: 'Receipt No.',
            sortable: true,
            resizable: true,
            dataIndex: 'sales_doc_ref',
            width: 100
        }
    ],
    initComponent: function () {
        jun.rztMgm.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    b.params = {
                        'tgl': Ext.getCmp('tglmgmgridid').getValue()
                    }
                }
            }
        });
        this.store = jun.rztMgm;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Add Member Get Member',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Show Member Get Member',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Date :'
                },
                {
                    xtype: 'xdatefield',
                    id: 'tglmgmgridid',
                    ref: '../tgl'
                }
            ]
        };
        jun.MgmGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        //this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.tgl.on('select', this.refreshTgl, this);
    },
    refreshTgl: function () {
        this.store.reload();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
        if (selectedz == undefined) {
            return;
        }
        jun.rztCustomersMgMCmp.baseParams = {
            customer_id: this.record.data.customer_new_id
        };
        jun.rztCustomersMgMCmp.load();
        jun.rztCustomersMgMCmp.baseParams = {};
        jun.rztCustomersRefCmp.baseParams = {
            customer_id: this.record.data.customer_friend_id
        };
        jun.rztCustomersRefCmp.load();
        jun.rztCustomersRefCmp.baseParams = {};
        jun.rztSalestransMgMCmp.baseParams = {
            salestrans_id: this.record.data.salestrans_id
        };
        jun.rztSalestransMgMCmp.load();
        jun.rztSalestransMgMCmp.baseParams = {};
    },
    loadForm: function () {
        var form = new jun.MgmWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.mgm_id;
        var form = new jun.MgmWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'Mgm/delete/id/' + record.json.mgm_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztMgm.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
