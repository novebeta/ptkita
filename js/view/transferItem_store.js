jun.TransferItemstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.TransferItemstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'TransferItemStoreId',
            url: 'TransferItem',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'transfer_item_id'},
                {name: 'tgl'},
                {name: 'doc_ref'},
                {name: 'note'},
                {name: 'tdate'},
                {name: 'doc_ref_other'},
                {name: 'user_id'},
                {name: 'type_'},
                {name: 'store'},
                {name: 'supplier_id'},
                {name: 'total'},
                {name: 'disc'},
                {name: 'discrp'},
                {name: 'bruto'},
                {name: 'vat'},
                {name: 'total_discrp1'},
                {name: 'total_pot'}
            ]
        }, cfg));
    }
});
jun.rztTransferItem = new jun.TransferItemstore({url: 'TransferItem/IndexIn'});
jun.rztReturnTransferItem = new jun.TransferItemstore({url: 'TransferItem/IndexOut'});
//jun.rztTransferItem.load();
