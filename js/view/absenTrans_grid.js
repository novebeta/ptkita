jun.AbsenTransGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Absent Transaction",
    id: 'docs-jun.AbsenTransGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Date',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 100
        },
        {
            header: 'Employee',
            sortable: true,
            resizable: true,
            dataIndex: 'employee_name',
            width: 100
        },
        {
            header: 'Absent Type',
            sortable: true,
            resizable: true,
            dataIndex: 'tipe_absen_name',
            width: 100
        },
        {
            header: 'Remarks',
            sortable: true,
            resizable: true,
            dataIndex: 'remarks',
            width: 100
        }
    ],
    initComponent: function () {
        if (jun.rztEmployeeCmp.getTotalCount() === 0) {
            jun.rztEmployeeCmp.load();
        }
        if (jun.rztTipeAbsenCmp.getTotalCount() === 0) {
            jun.rztTipeAbsenCmp.load();
        }
        jun.rztTransaksiAbsen.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    var month = Ext.getCmp('monthabsentransid').getValue();
                    var year = Ext.getCmp('yearabsentransid').getValue();
                    if(month == "" || year == ""){
                        return false;
                    }
                    b.params = {
                        'month': Ext.getCmp('monthabsentransid').getValue(),
                        'year': Ext.getCmp('yearabsentransid').getValue()
                    }
                }
            }
        });
        this.store = jun.rztTransaksiAbsen;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Add Absent',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Edit Absent',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Delete Absent',
                    ref: '../btnDelete'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Month :'
                },
                new jun.comboBulan({
                    width: 150,
                    id: 'monthabsentransid',
                    ref: '../cmbMonth'
                }),
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Year :'
                },
                {
                    xtype: "spinnerfield",
                    maxLength: 4,
                    minValue: 2012,
                    maxValue: 9999,
                    width: 100,
                    id: 'yearabsentransid',
                    ref: '../cmbYear'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Initialize Current Year',
                    ref: '../btnInit'
                }
            ]
        };
        jun.AbsenTransGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.btnInit.on('Click', this.initRec, this);
        this.cmbYear.on('spin', this.refreshTgl, this);
        this.cmbMonth.on('select', this.refreshTgl, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    refreshTgl: function () {
        this.store.reload();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.AbsenTransWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.absen_trans_id;
        var form = new jun.AbsenTransWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'AbsenTrans/delete/id/' + record.json.absen_trans_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztAbsenTrans.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    initRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Are you sure want initialize current year?', this.initRecYes, this);
    },
    initRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        Ext.Ajax.request({
            url: 'AbsenTrans/init/',
            method: 'POST',
            success: function (f, a) {
                jun.rztAbsenTrans.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
