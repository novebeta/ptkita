jun.MgmWin = Ext.extend(Ext.Window, {
    title: 'Member Get Member',
    modez: 1,
    width: 400,
    height: 230,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4;padding: 10px',
                id: 'form-Mgm',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Doc. Ref',
                        hideLabel: false,
                        //hidden:true,
                        name: 'doc_ref',
                        id: 'doc_refid',
                        ref: '../doc_ref',
                        maxLength: 50,
                        readOnly: true,
                        anchor: '100%'
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        fieldLabel: 'Date',
                        name: 'tgl',
                        id: 'tglid',
                        format: 'd M Y',
                        readOnly: true,
                        allowBlank: false,
                        value : DATE_NOW,
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        ref: '../customerNew',
                        fieldLabel: 'New Customer',
                        triggerAction: 'query',
                        lazyRender: true,
                        mode: 'remote',
                        forceSelection: true,
                        autoSelect: false,
                        store: jun.rztCustomersMgMCmp,
                        id: 'mgm_customer_new_id',
                        hiddenName: 'customer_new_id',
                        valueField: 'customer_id',
                        displayField: 'nama_customer',
                        hideTrigger: true,
                        minChars: 3,
                        matchFieldWidth: !1,
                        pageSize: 20,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span>{telp} <br /> {tgl_lahir:date("M j, Y")}</span>{no_customer} | {nama_customer}</h3>',
                            '{alamat}',
                            "</div></tpl>"),
                        allowBlank: false,
                        listWidth: 750,
                        lastQuery: "",
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        ref: '../customerRef',
                        fieldLabel: 'Ref. Customer',
                        triggerAction: 'query',
                        lazyRender: true,
                        mode: 'remote',
                        forceSelection: true,
                        autoSelect: false,
                        store: jun.rztCustomersRefCmp,
                        id: 'mgm_customer_friend_id',
                        hiddenName: 'customer_friend_id',
                        valueField: 'customer_id',
                        displayField: 'nama_customer',
                        hideTrigger: true,
                        minChars: 3,
                        matchFieldWidth: !1,
                        pageSize: 20,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span>{telp} <br /> {tgl_lahir:date("M j, Y")}</span>{no_customer} | {nama_customer}</h3>',
                            '{alamat}',
                            "</div></tpl>"),
                        allowBlank: false,
                        listWidth: 750,
                        lastQuery: "",
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        fieldLabel: 'Sales Trans',
                        triggerAction: 'query',
                        lazyRender: true,
                        mode: 'remote',
                        forceSelection: true,
                        autoSelect: false,
                        store: jun.rztSalestransMgMCmp,
                        hiddenName: 'salestrans_id',
                        valueField: 'salestrans_id',
                        displayField: 'doc_ref',
                        hideTrigger: true,
                        minChars: 1,
                        matchFieldWidth: !1,
                        pageSize: 20,
                        itemSelector: "div.search-item-table",
                        tpl: new Ext.XTemplate('<div class="container">','<tpl for="."><div class="search-item-table">',
                            '<div class="cell4" style="width: 150px;">{doc_ref}</div>',
                            '<div class="cell4" style="width: 100px;">{tgl:date("M j, Y")}</div>',
                            '<div class="cell4" style="width: 100px;">{no_customer}</div>',
                            '<div class="cell4" style="width: 350px;">{nama_customer}</div>',
                            '<div class="cell4" style="width: 50px;text-align: right;">{total:number("0,0.00")}</div>',
                            "</div></tpl>",'</div>'),
                        allowBlank: false,
                        listWidth: 750,
                        lastQuery: "",
                        name: 'salestrans_id',
                        ref: '../salestrans_id',
                        anchor: '100%'
                    }
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Save',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Save & Close',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Close',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.MgmWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.customerNew.on('select', this.onCustNewSelect, this);
        jun.rztSalestransMgMCmp.on('beforeload', this.onBeforeloadSalesTrans, this);
        jun.rztCustomersRefCmp.on('load', this.onloadCustomerRef, this);
        this.on("close", this.onWinClose, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
            this.btnSaveClose.setVisible(true);
        }
    },
    onWinClose: function(){
        jun.rztSalestransMgMCmp.un('beforeload', this.onBeforeloadSalesTrans, this);
        jun.rztCustomersRefCmp.un('load', this.onloadCustomerRef, this);
    },
    onloadCustomerRef: function (store, records, options) {
        if(store.getTotalCount() > 0){
            var friend = records[0];
            var result = this.customerRef.setValue(friend.data.customer_id);
        }
    },
    onBeforeloadSalesTrans: function (a, b) {
        var customer_id = Ext.getCmp('mgm_customer_new_id').getValue();
        if(customer_id == ""){
            return false;
        }
        b.params = {
            'customer_id': customer_id
        }
    },
    onCustNewSelect: function(combo, record, index){
        this.customerRef.reset();
        jun.rztCustomersRefCmp.baseParams = {
            customer_id: record.data.friend_id
        };
        jun.rztCustomersRefCmp.load();
        jun.rztCustomersRefCmp.baseParams = {};
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz;
        if (this.modez == 1 || this.modez == 2) {
            urlz = 'Mgm/update/id/' + this.id;
        } else {
            urlz = 'Mgm/create/';
        }
        Ext.getCmp('form-Mgm').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            success: function (f, a) {
                jun.rztMgm.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-Mgm').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});