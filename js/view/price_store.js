jun.Pricestore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Pricestore.superclass.constructor.call(this, Ext.apply({
            storeId: 'PriceStoreId',
            url: 'Price',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'price_id'},
                {name: 'value'},
                {name: 'barang_id'},
                {name: 'gol_id'},
                {name: 'store'}
            ]
        }, cfg));
    }
});
jun.rztPrice = new jun.Pricestore();
//jun.rztPrice.load();
