jun.ClinicalTransWin = Ext.extend(Ext.Window, {
    title: 'Operational Item In',
    modez: 1,
    width: 610,
    height: 415,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4;padding: 10px',
                id: 'form-ClinicalTrans',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "label",
                        text: "Doc. Ref:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        //hidden:true,
                        name: 'doc_ref',
                        id: 'doc_refid',
                        ref: '../doc_ref',
                        maxLength: 50,
                        x: 85,
                        y: 2,
                        height: 20,
                        width: 175,
                        readOnly: true
                    },
                    {
                        xtype: "label",
                        text: "Date:",
                        x: 5,
                        y: 35
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        fieldLabel: 'tgl',
                        name: 'tgl',
                        id: 'tglid',
                        format: 'd M Y',
                        width: 175,
                        value : DATE_NOW,
                        readOnly: true,
                        allowBlank: false,
                        x: 85,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "Type:",
                        x: 295,
                        y: 5
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        store: jun.rztClinicalIn,
                        hiddenName: 'tipe_clinical_id',
                        valueField: 'tipe_clinical_id',
                        displayField: 'nama',
                        width: 175,
                        x: 400,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "Note:",
                        x: 295,
                        y: 35
                    },
                    {
                        xtype: 'uctextfield',
                        hideLabel: false,
                        //hidden:true,
                        name: 'note',
                        id: 'noteid',
                        ref: '../note',
                        x: 400,
                        y: 32,
                        height: 20,
                        width: 175
                    },
                    new jun.ClinicalTransDetailGrid({
                        x: 5,
                        y: 65,
                        height: 260,
                        frameHeader: !1,
                        header: !1
                    })
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Save',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Save & Close',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Close',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.ClinicalTransWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.on("close", this.onWinClose, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
            this.btnSaveClose.setVisible(true);
            //this.setDateTime();
        }
    },
    onWinClose: function () {
        jun.rztClinicalTransDetail.removeAll();
    },
    //setDateTime: function () {
    //    Ext.Ajax.request({
    //        url: 'GetDateTime',
    //        method: 'POST',
    //        scope: this,
    //        success: function (f, a) {
    //            var response = Ext.decode(f.responseText);
    //            this.tgl.setValue(Date.parseDate(response.datetime, 'Y-m-d H:i:s'));
    //        },
    //        failure: function (f, a) {
    //            switch (a.failureType) {
    //                case Ext.form.Action.CLIENT_INVALID:
    //                    Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
    //                    break;
    //                case Ext.form.Action.CONNECT_FAILURE:
    //                    Ext.Msg.alert('Failure', 'Ajax communication failed');
    //                    break;
    //                case Ext.form.Action.SERVER_INVALID:
    //                    Ext.Msg.alert('Failure', a.result.msg);
    //            }
    //        }
    //    });
    //},
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        if (jun.rztClinicalTransDetail.data.length == 0) {
            Ext.Msg.alert('Error', "Item details must set");
            this.btnDisabled(false);
            return;
        }
        var urlz = 'ClinicalTrans/createIn/';
        Ext.getCmp('form-ClinicalTrans').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            params: {
                detil: Ext.encode(Ext.pluck(
                    jun.rztClinicalTransDetail.data.items, "data")),
                id: this.id,
                mode: this.modez
            },
            success: function (f, a) {
                jun.rztClinicalTrans.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-ClinicalTrans').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});
jun.ClinicalTransOutWin = Ext.extend(Ext.Window, {
    title: 'Operational Item Out',
    modez: 1,
    width: 610,
    height: 415,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4;padding: 10px',
                id: 'form-ClinicalTransOut',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "label",
                        text: "Doc. Ref:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        //hidden:true,
                        name: 'doc_ref',
                        id: 'doc_refid',
                        ref: '../doc_ref',
                        maxLength: 50,
                        x: 85,
                        y: 2,
                        height: 20,
                        width: 175,
                        readOnly: true
                    },
                    {
                        xtype: "label",
                        text: "Date:",
                        x: 5,
                        y: 35
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        fieldLabel: 'tgl',
                        name: 'tgl',
                        id: 'tglid',
                        format: 'd M Y',
                        width: 175,
                        readOnly: true,
                        allowBlank: false,
                        value : DATE_NOW,
                        x: 85,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "Type:",
                        x: 295,
                        y: 5
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        store: jun.rztClinicalOut,
                        hiddenName: 'tipe_clinical_id',
                        valueField: 'tipe_clinical_id',
                        displayField: 'nama',
                        width: 175,
                        x: 400,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "Note:",
                        x: 295,
                        y: 35
                    },
                    {
                        xtype: 'uctextfield',
                        hideLabel: false,
                        //hidden:true,
                        name: 'note',
                        id: 'noteid',
                        ref: '../note',
                        x: 400,
                        y: 32,
                        height: 20,
                        width: 175
                    },
                    new jun.ClinicalTransDetailOutGrid({
                        x: 5,
                        y: 65,
                        height: 260,
                        frameHeader: !1,
                        header: !1
                    })
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Save',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Save & Close',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Close',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.ClinicalTransOutWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.on("close", this.onWinClose, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
            this.btnSaveClose.setVisible(true);
            //this.setDateTime();
        }
    },
    onWinClose: function () {
        jun.rztClinicalTransDetail.removeAll();
    },
    //setDateTime: function () {
    //    Ext.Ajax.request({
    //        url: 'GetDateTime',
    //        method: 'POST',
    //        scope: this,
    //        success: function (f, a) {
    //            var response = Ext.decode(f.responseText);
    //            this.tgl.setValue(Date.parseDate(response.datetime, 'Y-m-d H:i:s'));
    //        },
    //        failure: function (f, a) {
    //            switch (a.failureType) {
    //                case Ext.form.Action.CLIENT_INVALID:
    //                    Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
    //                    break;
    //                case Ext.form.Action.CONNECT_FAILURE:
    //                    Ext.Msg.alert('Failure', 'Ajax communication failed');
    //                    break;
    //                case Ext.form.Action.SERVER_INVALID:
    //                    Ext.Msg.alert('Failure', a.result.msg);
    //            }
    //        }
    //    });
    //},
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        if (jun.rztClinicalTransDetail.data.length == 0) {
            Ext.Msg.alert('Error', "Item details must set");
            this.btnDisabled(false);
            return;
        }
        var  urlz = 'ClinicalTrans/createOut/';
        Ext.getCmp('form-ClinicalTransOut').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            params: {
                detil: Ext.encode(Ext.pluck(
                    jun.rztClinicalTransDetail.data.items, "data")),
                id: this.id,
                mode: this.modez
            },
            success: function (f, a) {
                jun.rztClinicalTransOut.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-ClinicalTransOut').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});