jun.Souvenirstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Souvenirstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'SouvenirStoreId',
            url: 'Souvenir',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'souvenir_id'},
                {name: 'souvenir_name'},
                {name: 'up'}
            ]
        }, cfg));
    }
});
jun.rztSouvenir = new jun.Souvenirstore();
jun.rztSouvenirLib = new jun.Souvenirstore();
jun.rztSouvenirCmp = new jun.Souvenirstore();
//jun.rztSouvenir.load();
