jun.Produksistore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Produksistore.superclass.constructor.call(this, Ext.apply({
            storeId: 'ProduksiStoreId',
            url: 'Produksi',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'produksi_id'},
                {name: 'tgl'},
                {name: 'doc_ref'},
                {name: 'barang_id'},
                {name: 'qty'},
                {name: 'price'},
                {name: 'tdate'},
                {name: 'up'},
                {name: 'store'},
                {name: 'user_id'},
                {name: 'pl'},
                {name: 'arus'},
                {name: 'doc_ref_produksi'}
            ]
        }, cfg));
    }
});
jun.rztProduksi = new jun.Produksistore({baseParams: {arus: 1}});
jun.rztProduksiReturn = new jun.Produksistore({baseParams: {arus: -1}});
//jun.rztProduksi.load();
