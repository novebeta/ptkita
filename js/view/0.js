jun.jenis_kas = new Ext.data.ArrayStore({
    fields: ["code", "desc"],
    data: [
        [0, "Branch's Expend"],
        [1, "Other Branch's Expend"],
        [2, "Home Expend"],
    ]
});
jun.cmbJenisKas = Ext.extend(Ext.form.ComboBox, {
    displayField: "desc",
    valueField: "code",
    typeAhead: !0,
    mode: "local",
    forceSelection: !0,
    triggerAction: "all",
    selectOnFocus: !0,
    name: "type_",
    hiddenName: "type_",
    initComponent: function () {
        this.store = jun.jenis_kas;
        jun.cmbJenisKas.superclass.initComponent.call(this);
    }
});
jun.Golstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Golstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'GolStoreId',
            url: 'Gol',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {
                    name: 'gol_id'
                },
                {
                    name: 'nama_gol'
                }
            ]
        }, cfg));
    }
});
jun.rztGol = new jun.Golstore();
jun.rztGolLib = new jun.Golstore();
jun.rztGolCmp = new jun.Golstore();
jun.getGrup = function (a) {
    var b = jun.rztGrupLib, c = b.findExact("grup_id", a);
    return b.getAt(c);
};
jun.renderGrup = function (a, b, c) {
    var jb = jun.getGrup(a);
    if (jb == null) {
        return '';
    }
    return jb.data.nama_grup;
};
jun.getmyEvent = function (a) {
    var b = jun.rztEventLib, c = b.findExact("event_id", a);
    return b.getAt(c);
};
jun.rendermyEvent = function (a, b, c) {
    var jb = jun.getmyEvent(a);
    if (jb == null) {
        return '';
    }
    return jb.data.event_name;
};
jun.getSouvenir = function (a) {
    var b = jun.rztSouvenirLib, c = b.findExact("souvenir_id", a);
    return b.getAt(c);
};
jun.renderSouvenir = function (a, b, c) {
    var jb = jun.getSouvenir(a);
    if (jb == null) {
        return '';
    }
    return jb.data.souvenir_name;
};
jun.getBarang = function (a) {
    var b = jun.rztBarangLib, c = b.findExact("barang_id", a);
    return b.getAt(c);
};
jun.renderBarang = function (a, b, c) {
    var jb = jun.getBarang(a);
    if (jb == null) {
        return '';
    }
    return jb.data.nama_barang;
};
jun.getPaket = function (a) {
    var b = jun.rztPaketLib, c = b.findExact("paket_id", a);
    return b.getAt(c);
};
jun.renderPaket = function (a, b, c) {
    var jb = jun.getPaket(a);
    if (jb == null) {
        return '';
    }
    return jb.data.paket_name;
};
jun.getBarangClinical = function (a) {
    var b = jun.rztBarangClinicalLib, c = b.findExact("barang_clinical", a);
    return b.getAt(c);
};
jun.renderKodeBarangClinical = function (a, b, c) {
    var jb = jun.getBarangClinical(a);
    if (jb == null) {
        return '';
    }
    return jb.data.kode_barang;
};
jun.renderBarangClinical = function (a, b, c) {
    var jb = jun.getBarangClinical(a);
    if (jb == null) {
        return '';
    }
    return jb.data.nama_barang;
};
jun.renderUnitBarangClinical = function (a, b, c) {
    var jb = jun.getBarangClinical(a);
    if (jb == null) {
        return '';
    }
    return jb.data.sat;
};
jun.getSupplier = function (a) {
    var b = jun.rztSupplierLib, c = b.findExact("supplier_id", a);
    return b.getAt(c);
};
jun.renderSupplier = function (a, b, c) {
    var jb = jun.getSupplier(a);
    if (jb == null) {
        return '';
    }
    return jb.data.supplier_name;
};
jun.renderBarangSat = function (a, b, c) {
    var jb = jun.getBarang(a);
    if (jb == null) {
        return '';
    }
    return jb.data.sat;
};
jun.renderKodeBarang = function (a, b, c) {
    var jb = jun.getBarang(a);
    if (jb == null) {
        return '';
    }
    return jb.data.kode_barang;
};
jun.getTax = function (a) {
    var brg = jun.getBarang(a);
    jun.rztGrupAttrLib.filter('store', STORE);
    var index = jun.rztGrupAttrLib.findBy(function (r, id) {
        return (r.data.grup_id == brg.data.grup_id && r.data.store == STORE);
    });
    var vat = 0;
    if (index > -1) {
        var grp = jun.rztGrupAttrLib.getAt(index);
        if (grp.data.tax == "1" && grp.data.vat > 0) {
            vat = round(grp.data.vat / 100, 2);
        }
    }
    jun.rztGrupAttrLib.clearFilter();
    return vat;
};
jun.isCatJasa = function (a) {
    var brg = jun.getBarang(a);
    var grp = jun.getGrup(brg.data.grup_id);
    return grp.data.kategori_id == 1;
};
jun.getGol = function (a) {
    var b = jun.rztGolLib, c = b.findExact("gol_id", a);
    return b.getAt(c)
};
jun.renderGol = function (a, b, c) {
    var jb = jun.getGol(a);
    return jb.data.nama_gol;
};
jun.getKategori = function (a) {
    var b = jun.rztKategori, c = b.findExact("kategori_id", a);
    return b.getAt(c)
};
jun.renderKategori = function (a, b, c) {
    var jb = jun.getKategori(a);
    return jb.data.nama_kategori;
};
jun.getBeauty = function (a) {
    var b = jun.rztBeautyLib, c = b.findExact("beauty_id", a);
    return b.getAt(c)
};
jun.renderBeauty = function (a, b, c) {
    if (a == null || a == "" || a == undefined)
        return "";
    var jb = jun.getBeauty(a);
    if (jb == null || jb == "" || jb == undefined)
        return "";
    return jb.data.nama_beauty;
};
jun.getNegara = function (a) {
    var b = jun.rztNegaraLib, c = b.findExact("negara_id", a);
    return b.getAt(c)
};
jun.renderNegara = function (a, b, c) {
    var jb = jun.getNegara(a);
    return jb.data.nama_negara;
};
jun.getCoa = function (a) {
    var b = jun.rztChartMasterLib, c = b.findExact("account_code", a);
    return b.getAt(c)
};
jun.renderCoa = function (a, b, c) {
    var jb = jun.getCoa(a);
    return jb.data.account_name;
};
jun.getProvinsi = function (a) {
    var b = jun.rztProvinsiLib, c = b.findExact("provinsi_id", a);
    return b.getAt(c)
};
jun.renderProvinsi = function (a, b, c) {
    var jb = jun.getProvinsi(a);
    return jb.data.nama_provinsi;
};
jun.getKota = function (a) {
    var b = jun.rztKotaLib, c = b.findExact("kota_id", a);
    return b.getAt(c)
};
jun.renderKota = function (a, b, c) {
    var jb = jun.getKota(a);
    return jb.data.nama_kota;
};
jun.getBank = function (a) {
    var b = jun.rztBankLib, c = b.findExact("bank_id", a);
    return b.getAt(c)
};
jun.renderBank = function (a, b, c) {
    var jb = jun.getBank(a);
    return jb.data.nama_bank;
};
jun.getStatusCust = function (a) {
    var b = jun.rztStatusCustLib, c = b.findExact("status_cust_id", a);
    return b.getAt(c)
};
jun.renderStatusCust = function (a, b, c) {
    var jb = jun.getStatusCust(a);
    return jb.data.nama_status;
};
jun.login = new Ext.extend(Ext.Window, {
    width: 390,
    height: 150,
    layout: "form",
    modal: !0,
    resizable: !1,
    plain: !0,
    border: !1,
    title: "Login",
    padding: 5,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-Login",
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                plain: !0,
                items: [
                    {
                        xtype: "textfield",
                        hideLabel: !1,
                        id: "usernameid",
                        ref: "../username",
                        maxLength: 128,
                        anchor: "100%",
                        fieldLabel: "Username",
                        name: "loginUsername",
                        allowBlank: !1
                    },
                    {
                        xtype: "textfield",
                        hideLabel: !1,
                        id: "passwordid",
                        ref: "../password",
                        maxLength: 128,
                        anchor: "100%",
                        fieldLabel: "Password",
                        name: "loginPassword",
                        inputType: "password",
                        allowBlank: !1
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    text: "Login",
                    hidden: !1,
                    ref: "../btnLogin"
                }
            ]
        };
        jun.login.superclass.initComponent.call(this);
        this.btnLogin.on("click", this.onbtnLoginClick, this);
    },
    onbtnLoginClick: function () {
        var username = this.username.getValue();
        var password = this.password.getValue();
        if (username.trim() == "" || password.trim() == "") {
            Ext.Msg.alert("Warning!", "Login Failed");
            return;
        }
        var a = Ext.getCmp("passwordid").getValue();
        a = jun.EncryptPass(a);
        Ext.getCmp("passwordid").setValue(a);
        Ext.getCmp("form-Login").getForm().submit({
            scope: this,
            url: "site/loginOverride",
            waitTitle: "Connecting",
            waitMsg: "Sending data...",
            success: function (f, a) {
                var response = Ext.decode(a.response.responseText);
                this.close();
                //Ext.getCmp('discid').setReadOnly(false);
                Ext.getCmp('discrpid').setReadOnly(false);
                Ext.getCmp('priceid').setReadOnly(false);
                Ext.getCmp('discdetilid').setReadOnly(false);
                Ext.getCmp('discadetilid').setReadOnly(false);
                Ext.getCmp('overrideid').setValue(response.msg);
            },
            failure: function (f, a) {
                Ext.getCmp("form-Login").getForm().reset();
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
jun.BackupRestoreWin = new Ext.extend(Ext.Window, {
    width: 550,
    height: 125,
    layout: "form",
    modal: !0,
    resizable: !1,
    plain: !0,
    border: !1,
    title: "Backup / Restore",
    padding: 5,
    iswin: true,
    initComponent: function () {
        this.items = new Ext.FormPanel({
            frame: !1,
            labelWidth: 100,
            fileUpload: true,
            bodyStyle: "background-color: #E4E4E4;padding: 10px",
            id: "form-BackupRestoreWin",
            labelAlign: "left",
            layout: "form",
            ref: "formz",
            border: !1,
            plain: !0,
            defaults: {
                allowBlank: false,
                msgTarget: 'side'
            },
            items: [
                {
                    xtype: "fileuploadfield",
                    hideLabel: !1,
                    fieldLabel: "File Name",
                    emptyText: 'Select an file restore (*.pos.gz)',
                    id: "filename",
                    ref: "../filename",
                    name: "filename",
                    anchor: "95%"
                }
            ]
        });
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    text: "Delete All Transaction",
                    hidden: !1,
                    ref: "../btnDelete"
                },
                {
                    xtype: "button",
                    text: "Download Backup",
                    hidden: !1,
                    ref: "../btnBackup"
                },
                {
                    xtype: "button",
                    text: "Upload Restore",
                    hidden: !1,
                    ref: "../btnRestore"
                }
            ]
        };
        jun.BackupRestoreWin.superclass.initComponent.call(this);
        this.btnBackup.on("click", this.onbtnBackupClick, this);
        this.btnRestore.on("click", this.btnRestoreClick, this);
        this.btnDelete.on("click", this.deleteRec, this);
    },
    onbtnBackupClick: function () {
        Ext.getCmp("form-BackupRestoreWin").getForm().reset();
        Ext.getCmp("form-BackupRestoreWin").getForm().standardSubmit = !0;
        Ext.getCmp("form-BackupRestoreWin").getForm().url = "Site/BackupAll";
        var form = Ext.getCmp('form-BackupRestoreWin').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    btnRestoreClick: function () {
        Ext.getCmp("form-BackupRestoreWin").getForm().standardSubmit = false;
        Ext.getCmp("form-BackupRestoreWin").getForm().url = "site/RestoreAll";
        if (Ext.getCmp("form-BackupRestoreWin").getForm().isValid()) {
            Ext.getCmp("form-BackupRestoreWin").getForm().submit({
                url: 'site/RestoreAll',
                waitMsg: 'Uploading your restore...',
                success: function (f, a) {
                    var response = Ext.decode(a.response.responseText);
                    Ext.Msg.alert('Successfully', response.msg);
                },
                failure: function (f, a) {
                    switch (a.failureType) {
                        case Ext.form.Action.CLIENT_INVALID:
                            Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                            break;
                        case Ext.form.Action.CONNECT_FAILURE:
                            Ext.Msg.alert('Failure', 'Ajax communication failed');
                            break;
                        case Ext.form.Action.SERVER_INVALID:
                            Ext.Msg.alert('Failure', a.result.msg);
                    }
                }
            });
        }
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Confirm', 'Are you sure delete all transaction?', this.btnDeleteClick, this);
    },
    btnDeleteClick: function (btn) {
        if (btn == 'no') {
            return;
        }
        Ext.Ajax.request({
            method: 'POST',
            scope: this,
            url: 'site/DeleteTransAll',
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                Ext.Msg.alert('Successfully', response.msg);
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
var itemFile = null;
jun.ImportXlsx = new Ext.extend(Ext.Window, {
    width: 500,
    height: 90,
    layout: "form",
    modal: !0,
    resizable: !1,
    plain: !0,
    border: !1,
    title: "Import Data",
    iswin: true,
    padding: 5,
    initComponent: function () {
        this.items = new Ext.FormPanel({
            frame: !1,
            labelWidth: 75,
            fileUpload: true,
            bodyStyle: "background-color: #E4E4E4;padding: 10px",
            id: "form-BackupRestoreWin",
            labelAlign: "left",
            layout: "form",
            ref: "formz",
            html: "File Type : <select id='importType'><option  value='item'>Item</option><option value='cust'>Customers</option></select><input id='inputFile' type='file' name='uploaded'/>",
            border: !1,
            plain: !0,
            listeners: {
                afterrender: function () {
                    itemFile = document.getElementById("inputFile");
                    itemFile.addEventListener('change', EventChange, false);
                }
            }
        });
        jun.ImportXlsx.superclass.initComponent.call(this);
    }
});
function to_json(workbook) {
    var result = {};
    workbook.SheetNames.forEach(function (sheetName) {
        var roa = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
        if (roa.length > 0) {
            result[sheetName] = roa;
        }
    });
    return result;
}
function EventChange(e) {
    var files = itemFile.files;
    var f = files[0];
    if (f.name.split('.').pop() != "xlsx") {
        Ext.Msg.alert('Failure', 'Need file *.xlsx');
        return;
    }
    var rABS = typeof FileReader !== "undefined" && typeof FileReader.prototype !== "undefined" &&
        typeof FileReader.prototype.readAsBinaryString !== "undefined";
    var reader = new FileReader();
    reader.onload = function (e) {
        if (typeof console !== 'undefined') console.log("onload", new Date());
        var data = e.target.result;
        var wb = XLSX.read(data, {type: 'binary'});
        var s = wb.Strings;
        var e = document.getElementById("importType");
        var tipe = e.value;
        var iscust = tipe == "cust";
        if (iscust) {
            if (s[1].h.indexOf("NOBASE") == -1) {
                Ext.Msg.alert('Failure', 'Not valid column NOBASE');
                return;
            }
            if (s[2].h.indexOf("NAMACUS") == -1) {
                Ext.Msg.alert('Failure', 'Not valid column NAMACUS');
                return;
            }
            if (s[4].h.indexOf("ALAMAT") == -1) {
                Ext.Msg.alert('Failure', 'Not valid column ALAMAT');
                return;
            }
            if (s[6].h.indexOf("TELP") == -1) {
                Ext.Msg.alert('Failure', 'Not valid column TELP');
                return;
            }
            if (s[8].h.indexOf("TGLLH") == -1) {
                Ext.Msg.alert('Failure', 'Not valid column TGLLH');
                return;
            }
            if (s[9].h.indexOf("SEX") == -1) {
                Ext.Msg.alert('Failure', 'Not valid column SEX');
                return;
            }
            if (s[11].h.indexOf("KERJA") == -1) {
                Ext.Msg.alert('Failure', 'Not valid column KERJA');
                return;
            }
            if (s[12].h.indexOf("AWAL") == -1) {
                Ext.Msg.alert('Failure', 'Not valid column AWAL');
                return;
            }
            if (s[13].h.indexOf("AKHIR") == -1) {
                Ext.Msg.alert('Failure', 'Not valid column AKHIR');
                return;
            }
        } else {
            if (s[0].h.indexOf("KODEBRG") == -1) {
                Ext.Msg.alert('Failure', 'Not valid column KODEBRG');
                return;
            }
            if (s[1].h.indexOf("JNSBRG") == -1) {
                Ext.Msg.alert('Failure', 'Not valid column JNSBRG');
                return;
            }
            if (s[2].h.indexOf("NAMABRG") == -1) {
                Ext.Msg.alert('Failure', 'Not valid column NAMABRG');
                return;
            }
            if (s[3].h.indexOf("SATUAN1") == -1) {
                Ext.Msg.alert('Failure', 'Not valid column SATUAN1');
                return;
            }
            if (s[8].h.indexOf("HJUAL") == -1) {
                Ext.Msg.alert('Failure', 'Not valid column HJUAL');
                return;
            }
        }
        var output = to_json(wb);
        var url = iscust ? "customers/import" : "barang/import";
        Ext.Ajax.request({
            method: 'POST',
            scope: this,
            url: url,
            params: {
                detil: Ext.encode(output)
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                var win = new Ext.Window({
                    width: 1000,
                    height: 600,
                    modal: !0,
                    border: !1,
                    layout: 'fit',
                    autoScroll: true,
                    title: "Duplicate Data",
                    iswin: true,
                    padding: 5,
                    html: response.msg
                });
                win.show();
//                Ext.Msg.alert('Successfully', response.msg);
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
    reader.readAsBinaryString(f);
}
jun.renderPackageIco = function (a, b, c) {
    return a == "" || a == null || a == undefined ? '<img alt="" src="css/silk_v013/icons/pill.png" class="x-action-col-icon x-action-col-0  " ext:qtip="PCS">' :
        '<img alt="" src="css/silk_v013/icons/package_green.png" class="x-action-col-icon x-action-col-0  " ext:qtip="Package">';
};