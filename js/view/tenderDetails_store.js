jun.TenderDetailsstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.TenderDetailsstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'TenderDetailsStoreId',
            url: 'TenderDetails',
            root: 'results',
            autoLoad: !1,
            autoSave: !1,
            totalProperty: 'total',
            fields: [
                {name: 'tender_details_id'},
                {name: 'tender_id'},
                {name: 'bank_id'},
                {name: 'amount', type: 'float'}
            ]
        }, cfg));
        this.on('add', this.refreshData, this);
        this.on('update', this.refreshData, this);
        this.on('remove', this.refreshData, this);
    },
    refreshData: function (a) {
        var amount = this.sum("amount");
        Ext.getCmp("totaltenderid").setValue(amount);
    }
});
jun.rztTenderDetails = new jun.TenderDetailsstore();
//jun.rztTenderDetails.load();
