jun.TransferItemDetailsGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "TransferItemDetails",
    id: 'docs-jun.TransferItemDetailsGrid',
    iconCls: "silk-grid",
    stripeRows: true,
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        {
            header: 'Item Code',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_id',
            width: 200,
            renderer: jun.renderKodeBarang
        },
        {
            header: 'Item Name',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_id',
            width: 200,
            renderer: jun.renderBarang
        },
        {
            header: 'Qty',
            sortable: true,
            resizable: true,
            dataIndex: 'qty',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        }
    ],
    initComponent: function () {
        this.store = jun.rztTransferItemDetails;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'buttongroup',
                    columns: 6,
                    defaults: {
                        scale: 'small'
                    },
                    items: [
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Item :'
                        },
                        {
                            xtype: 'combo',
                            typeAhead: true,
                            triggerAction: 'all',
                            lazyRender: true,
                            mode: 'local',
//                            style: 'margin-bottom:2px',
                            forceSelection: true,
                            store: jun.rztBarangNonJasaAll,
                            hiddenName: 'barang_id',
                            valueField: 'barang_id',
                            ref: '../../barang',
                            displayField: 'kode_barang'
//                            colspan: 3
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Qty :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'qtyid',
                            ref: '../../qty',
                            width: 50,
                            value: 1,
                            minValue: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: '\xA0Unit :'
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            ref: '../../sat',
                            text: '\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0'
                        }
                    ]
                },
                {
                    xtype: 'buttongroup',
                    columns: 3,
                    defaults: {
                        scale: 'small'
                    },
                    items: [
                        {
                            xtype: 'button',
                            text: 'Add',
//                            height: 44,
                            ref: '../../btnAdd'
                        },
                        {
                            xtype: 'button',
                            text: 'Edit',
//                            height: 44,
                            ref: '../../btnEdit'
                        },
                        {
                            xtype: 'button',
                            text: 'Del',
//                            height: 44,
                            ref: '../../btnDelete'
                        }
                    ]
                }
            ]
        };
        jun.TransferItemDetailsGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.barang.on('Change', this.onItemChange, this);
//        this.disc.on('keyup', this.onDiscChange, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    onDiscChange: function () {
        var price = parseFloat(this.price.getValue());
        var qty = parseFloat(this.qty.getValue());
        var disc = parseFloat(this.disc.getValue());
        var bruto = round(price * qty,2);
        this.discrp.setValue(round(bruto * (disc / 100),2));
    },
    onItemChange: function () {
        var barang_id = this.barang.getValue();
        var barang = jun.getBarang(barang_id);
        this.sat.setText(barang.data.sat);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var barang_id = this.barang.getValue();
        if (barang_id == "") {
            Ext.MessageBox.alert("Error", "You have not selected a item");
            return
        }
//      kunci barang sama tidak boleh masuk dua kali
        if (this.btnEdit.text != 'Save') {
            var a = jun.rztTransferItemDetails.findExact("barang_id", barang_id);
            if (a > -1) {
                Ext.MessageBox.alert("Error", "Item already inputted");
                return
            }
        }
        var qty = parseFloat(this.qty.getValue());
//        var price = parseFloat(this.price.getValue());
//        var disc = parseFloat(this.disc.getValue());
//        var discrp = parseFloat(this.discrp.getValue());
//        var disc1 = parseFloat(Ext.getCmp('discid').getValue());
//        var bruto = qty * price;
//        var subtotal = bruto - discrp;
//        var discrp1 = disc1 == 0 ? 0 : (disc1/100) * subtotal;
//        var totalpot = discrp + discrp1;
//        var total_with_disc = bruto - totalpot;
//        var total = bruto - discrp;
//        var vat = jun.getTax(barang_id);
//        var vatrp = total_with_disc * vat;
        if (this.btnEdit.text == 'Save') {
            var record = this.sm.getSelected();
            record.set('barang_id', barang_id);
            record.set('qty', qty);
//            record.set('price', price);
//            record.set('total', total);
//            record.set('disc', disc);
//            record.set('discrp', discrp);
//            record.set('vat', vat);
//            record.set('vatrp', vatrp);
//            record.set('bruto', bruto);
//            record.set('total_pot', totalpot);
//            record.set('disc1', disc1);
//            record.set('discrp1', discrp1);
            record.commit();
        } else {
            var c = jun.rztTransferItemDetails.recordType,
                d = new c({
                    barang_id: barang_id,
                    qty: qty
//                    price: price,
//                    total: total,
//                    disc: disc,
//                    discrp: discrp,
//                    vat: vat,
//                    vatrp: vatrp,
//                    bruto: bruto,
//                    total_pot: totalpot,
//                    disc1: disc1,
//                    discrp1: discrp1
                });
            jun.rztTransferItemDetails.add(d);
        }
//        this.store.refreshData();
        this.barang.reset();
        this.qty.reset();
//        this.price.reset();
//        this.disc.reset();
//        this.discrp.reset();
//        this.sat.setText("");
    },
    btnDisable: function (s) {
        this.btnAdd.setDisabled(s);
        this.btnDelete.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    loadEditForm: function (btn) {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a item");
            return;
        }
        if (btn.text == 'Edit') {
            this.barang.setValue(record.data.barang_id);
            this.qty.setValue(record.data.qty);
//            this.price.setValue(record.data.price);
//            this.disc.setValue(record.data.disc);
//            this.discrp.setValue(record.data.discrp);
            btn.setText("Save");
            this.btnDisable(true);
            this.onItemChange();
        } else {
            this.loadForm();
            btn.setText("Edit");
            this.btnDisable(false);
        }
    },
    deleteRec: function () {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih item.");
            return;
        }
        Ext.MessageBox.confirm('Questions', 'Are you sure want delete this item?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih item.");
            return;
        }
        this.store.remove(record);
    }
})
