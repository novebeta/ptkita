jun.BankTransWin = Ext.extend(Ext.Window, {
    title: 'BankTrans',
    modez: 1,
    width: 400,
    height: 300,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4;padding: 10px',
                id: 'form-BankTrans',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'textfield',
                        fieldLabel: 'type_',
                        hideLabel: false,
                        //hidden:true,
                        name: 'type_',
                        id: 'type_id',
                        ref: '../type_',
                        maxLength: 11,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'trans_no',
                        hideLabel: false,
                        //hidden:true,
                        name: 'trans_no',
                        id: 'trans_noid',
                        ref: '../trans_no',
                        maxLength: 11,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'ref',
                        hideLabel: false,
                        //hidden:true,
                        name: 'ref',
                        id: 'refid',
                        ref: '../ref',
                        maxLength: 20,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        fieldLabel: 'tgl',
                        name: 'tgl',
                        id: 'tglid',
                        format: 'd M Y',
                        readOnly: true,
                        allowBlank: false,
                        value : DATE_NOW,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'amount',
                        hideLabel: false,
                        //hidden:true,
                        name: 'amount',
                        id: 'amountid',
                        ref: '../amount',
                        maxLength: 30,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'id_user',
                        hideLabel: false,
                        //hidden:true,
                        name: 'id_user',
                        id: 'id_userid',
                        ref: '../id_user',
                        maxLength: 11,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tdate',
                        fieldLabel: 'tdate',
                        name: 'tdate',
                        id: 'tdateid',
                        format: 'd M Y',
                        readOnly: true,
                        allowBlank: false,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.BankTransWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
        //this.setDateTime();
    },
    //setDateTime: function () {
    //    Ext.Ajax.request({
    //        url: 'GetDateTime',
    //        method: 'POST',
    //        scope: this,
    //        success: function (f, a) {
    //            var response = Ext.decode(f.responseText);
    //            this.tgl.setValue(Date.parseDate(response.datetime, 'Y-m-d H:i:s'));
    //        },
    //        failure: function (f, a) {
    //            switch (a.failureType) {
    //                case Ext.form.Action.CLIENT_INVALID:
    //                    Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
    //                    break;
    //                case Ext.form.Action.CONNECT_FAILURE:
    //                    Ext.Msg.alert('Failure', 'Ajax communication failed');
    //                    break;
    //                case Ext.form.Action.SERVER_INVALID:
    //                    Ext.Msg.alert('Failure', a.result.msg);
    //            }
    //        }
    //    });
    //},
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz;
        if (this.modez == 1 || this.modez == 2) {
            urlz = 'BankTrans/update/id/' + this.id;
        } else {
            urlz = 'BankTrans/create/';
        }
        Ext.getCmp('form-BankTrans').getForm().submit({
            url: urlz,
            scope: this,
            success: function (f, a) {
                jun.rztBankTrans.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-BankTrans').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }

        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }

});
jun.TranferBankWin = Ext.extend(Ext.Window, {title: "Cash/Bank Transfer",
    modez: 1,
    id: "win-transfer-bank",
    width: 600,
    height: 250,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if(HEADOFFICE){
            if (jun.rztBankTransCmpPusat.getTotalCount() === 0) {
                jun.rztBankTransCmpPusat.load();
            }
        }else{
            if (jun.rztBankTransCmp.getTotalCount() === 0) {
                jun.rztBankTransCmp.load();
            }
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-Transfer-bank",
                labelWidth: 100,
                labelAlign: "left",
                layout: "column",
                ref: "formz",
                border: !1,
                items: [
                    {
                        columnWidth: .5,
                        baseCls: "x-plain",
                        bodyStyle: "background-color: #E4E4E4;padding:1px",
                        layout: "form",
                        ref: "col1", items: [
                        {
                            xtype: "combo",
                            typeAhead: !0,
                            allowBlank: false,
                            triggerAction: "all",
                            lazyRender: !0,
                            mode: "local",
                            fieldLabel: "From Bank",
                            store: HEADOFFICE ? jun.rztBankTransCmpPusat : jun.rztBankTransCmp,
                            hiddenName: "bank_act_asal",
                            valueField: "bank_id",
                            ref: "../../cmbBankAsal",
                            forceSelection: !0,
                            displayField: "nama_bank",
                            anchor: "100%",
                            lastQuery: ""
                        },
                        {
                            xtype: "numericfield",
                            fieldLabel: "Bank Balance",
                            hideLabel: !1,
                            name: "bank_bal",
                            id: "bank_bal_id",
                            readOnly: !0,
                            ref: "../../lblBankBal",
                            anchor: "100%"
                        },
                        {
                            xtype: "combo",
                            typeAhead: !0,
                            allowBlank: false,
                            triggerAction: "all",
                            lazyRender: !0,
                            mode: "local",
                            fieldLabel: "To Bank",
                            store: HEADOFFICE ? jun.rztBankTransCmpPusat : jun.rztBankTransCmp,
                            hiddenName: "bank_act_tujuan",
                            valueField: "bank_id",
                            forceSelection: !0,
                            displayField: "nama_bank",
                            anchor: "100%",
                            ref: "../../cmbBankTujuan",
                            lastQuery: ""
                        },
                        {
                            xtype: "xdatefield",
                            fieldLabel: "Date",
                            hideLabel: !1,
                            name: "trans_date",
                            id: "refid",
                            ref: "../../tgl",
                            maxLength: 40,
                            readOnly: true,
                            value : DATE_NOW,
                            allowBlank: false,
                            anchor: "100%"
                        },
                        {
                            xtype: "numericfield",
                            fieldLabel: "Amount",
                            allowBlank: false,
                            value: 0,
                            hideLabel: !1,
                            name: "amount",
                            id: "amountid",
                            ref: "../../amount",
                            maxLength: 20,
                            anchor: "100%"
                        },
                        {
                            xtype: "numericfield",
                            fieldLabel: "Bank Charge",
                            allowBlank: false,
                            value: 0,
                            hideLabel: !1,
                            name: "charge",
                            id: "chargeid",
                            ref: "../../charge",
                            maxLength: 20,
                            anchor: "100%"
                        }
                    ]},
                    {
                        columnWidth: .5,
                        baseCls: "x-plain",
                        bodyStyle: "background-color: #E4E4E4;padding:0px 0 5px 5px",
                        layout: "form",
                        labelWidth: 50,
                        items: [
                            {
                                xtype: 'combo',
                                typeAhead: true,
                                fieldLabel: 'Branch',
                                ref: '../store',
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                store: jun.rztStoreCmp,
                                hiddenName: 'store',
                                name: 'store',
                                valueField: 'store_kode',
                                displayField: 'store_kode',
                                emptyText: "All Branch",
                                value: STORE,
                                readOnly: !HEADOFFICE,
                                width: 218
                            },
                            {
                                xtype: "textarea",
                                fieldLabel: 'Memo',
                                name: "memo",
                                enableKeyEvents: true,
                                style : {textTransform: "uppercase"},
                                listeners: {
                                    change: function (field, newValue, oldValue) {
                                        field.setValue(newValue.toUpperCase());
                                    }
                                },
                                id: "memoid",
                                height: 122,
                                maxLength: 100,
                                width: "100%"
                            }
                        ]}
                ]}
        ];
        this.fbar = {xtype: "toolbar",
            items: [
            {
                xtype: "button",
                text: "Save & Close",
                ref: "../btnSaveClose"
            },
            {
                xtype: "button",
                text: "Close",
                ref: "../btnCancel"
            }
        ]};
        jun.TranferBankWin.superclass.initComponent.call(this);
        this.cmbBankAsal.on("select", this.oncmbBankAsalChange, this);
        this.btnSaveClose.on("click", this.onbtnSaveCloseClick, this);
//        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnCancel.on("click", this.onbtnCancelclick, this);
        this.cmbBankAsal.on("focus", this.onLoadBank, this);
        this.cmbBankTujuan.on("focus", this.onLoadBank, this);
        this.on("close", this.onWinClose, this);
        this.on('activate', this.onActivate, this);
        this.oncmbBankAsalChange();
        //this.setDateTime();
    },
    //setDateTime: function () {
    //    Ext.Ajax.request({
    //        url: 'GetDateTime',
    //        method: 'POST',
    //        scope: this,
    //        success: function (f, a) {
    //            var response = Ext.decode(f.responseText);
    //            this.tgl.setValue(Date.parseDate(response.datetime, 'Y-m-d H:i:s'));
    //        },
    //        failure: function (f, a) {
    //            switch (a.failureType) {
    //                case Ext.form.Action.CLIENT_INVALID:
    //                    Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
    //                    break;
    //                case Ext.form.Action.CONNECT_FAILURE:
    //                    Ext.Msg.alert('Failure', 'Ajax communication failed');
    //                    break;
    //                case Ext.form.Action.SERVER_INVALID:
    //                    Ext.Msg.alert('Failure', a.result.msg);
    //            }
    //        }
    //    });
    //},
    btnDisabled: function (a) {
//        this.btnSave.setDisabled(a);
        this.btnSaveClose.setDisabled(a);
    },
    onWinClose: function () {
//        jun.rztBankCmpPusat.clearFilter()
    },
    onLoadBank: function () {
//        jun.rztBankCmpPusat.FilterData()
    },
    onActivate: function () {
        if (this.modez == 1 || this.modez == 2) {
            this.btnSaveClose.setVisible(false);
        } else {
            this.btnSaveClose.setVisible(true);
        }
    },
    oncmbBankAsalChange: function () {
        Ext.Ajax.request({
            waitMsg: "Please Wait",
            url: "BankTrans/GetBalance",
            scope: this,
            params: {
                id: this.cmbBankAsal.getValue()
            },
            success: function (a) {
                var a = Ext.decode(a.responseText);
                Ext.getCmp("bank_bal_id").setValue(a.id)
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert(a.response.statusText, a.response.responseText);
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }})
    },
    saveForm: function () {
        this.btnDisabled(!0);
        if (this.cmbBankAsal.getValue() == this.cmbBankTujuan.getValue()) {
            Ext.MessageBox.alert("Error", "The source and destination bank accouts cannot be the same.");
            this.btnDisabled(!1);
            return
        }
        if (parseInt(this.amount.getValue()) == 0) {
            Ext.MessageBox.alert("Error", "The entered amount is invalid or less than zero.");
            this.btnDisabled(!1);
            return
        }
        var a;
        a = "Kas/CreateTransfer",
            Ext.getCmp("form-Transfer-bank").getForm().submit({
                url: a,
                scope: this,
                success: function (a, b) {
                    var c = Ext.decode(b.response.responseText);
                    if (c.success == 0) {
                        Ext.MessageBox.show({title: "Transfer", msg: c.msg, buttons: Ext.MessageBox.OK, icon: Ext.MessageBox.ERROR});
                        this.btnDisabled(!1);
                        return
                    }
                    Ext.MessageBox.show({title: "Transfer", msg: c.msg + "<br /> Ref. Dokumen : " +
                        c.id, buttons: Ext.MessageBox.OK, icon: Ext.MessageBox.INFO});
                    Ext.getCmp("form-Transfer-bank").getForm().reset();
                    jun.rztBankTrans.reload();
                    this.close();
                },
                failure: function (f, a) {
                    switch (a.failureType) {
                        case Ext.form.Action.CLIENT_INVALID:
                            Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                            break;
                        case Ext.form.Action.CONNECT_FAILURE:
                            Ext.Msg.alert(a.response.statusText, a.response.responseText);
                            break;
                        case Ext.form.Action.SERVER_INVALID:
                            Ext.Msg.alert('Failure', a.result.msg);
                    }
                    this.btnDisabled(!1);
                }})
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = !0;
        this.saveForm(!0);
    },
    onbtnSaveclick: function () {
        this.closeForm = !1;
        this.saveForm(!1);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});

