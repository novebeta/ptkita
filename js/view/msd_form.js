jun.MsdWin = Ext.extend(Ext.Window, {
    title: 'My Special Day',
    modez: 1,
    width: 400,
    height: 195,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4;padding: 10px',
                id: 'form-Msd',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Doc. Ref',
                        hideLabel: false,
                        //hidden:true,
                        name: 'doc_ref',
                        id: 'doc_refid',
                        ref: '../doc_ref',
                        maxLength: 50,
                        readOnly: true,
                        anchor: '100%'
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        fieldLabel: 'Date',
                        name: 'tgl',
                        id: 'tglid',
                        format: 'd M Y',
                        readOnly: true,
                        allowBlank: false,
                        value: DATE_NOW,
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        ref: '../customer',
                        fieldLabel: 'Customer',
                        triggerAction: 'query',
                        lazyRender: true,
                        mode: 'remote',
                        forceSelection: true,
                        autoSelect: false,
                        store: jun.rztCustomersMsDCmp,
                        id: 'msd_customer_id',
                        hiddenName: 'customer_id',
                        valueField: 'customer_id',
                        displayField: 'nama_customer',
                        hideTrigger: true,
                        minChars: 3,
                        matchFieldWidth: !1,
                        pageSize: 20,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span>{telp} <br /> {tgl_lahir:date("M j, Y")}</span>{no_customer} | {nama_customer}</h3>',
                            '{alamat}',
                            "</div></tpl>"),
                        allowBlank: false,
                        listWidth: 750,
                        lastQuery: "",
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        fieldLabel: 'Sales Trans',
                        triggerAction: 'query',
                        lazyRender: true,
                        mode: 'remote',
                        forceSelection: true,
                        autoSelect: false,
                        store: jun.rztSalestransMsDCmp,
                        hiddenName: 'salestrans_id',
                        valueField: 'salestrans_id',
                        displayField: 'doc_ref',
                        hideTrigger: true,
                        minChars: 1,
                        matchFieldWidth: !1,
                        pageSize: 20,
                        itemSelector: "div.search-item-table",
                        tpl: new Ext.XTemplate('<div class="container">', '<tpl for="."><div class="search-item-table">',
                            '<div class="cell4" style="width: 150px;">{doc_ref}</div>',
                            '<div class="cell4" style="width: 100px;">{tgl:date("M j, Y")}</div>',
                            '<div class="cell4" style="width: 100px;">{no_customer}</div>',
                            '<div class="cell4" style="width: 350px;">{nama_customer}</div>',
                            '<div class="cell4" style="width: 50px;text-align: right;">{total:number("0,0.00")}</div>',
                            "</div></tpl>", '</div>'),
                        allowBlank: false,
                        listWidth: 750,
                        lastQuery: "",
                        name: 'salestrans_id',
                        ref: '../salestrans_id',
                        anchor: '100%'
                    }
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Save',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Save & Close',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Close',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.MsdWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        jun.rztSalestransMsDCmp.on('beforeload', this.onBeforeload, this);
        this.on("close", this.onWinClose, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
            this.btnSaveClose.setVisible(true);
        }
    },
    onWinClose: function(){
        jun.rztSalestransMsDCmp.un('beforeload', this.onBeforeload, this);
    },
    onBeforeload: function (a, b) {
        var customer_id = Ext.getCmp('msd_customer_id').getValue();
        if (customer_id == "") {
            return false;
        }
        b.params = {
            'customer_id': customer_id
        }
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz;
        if (this.modez == 1 || this.modez == 2) {
            urlz = 'Msd/update/id/' + this.id;
        } else {
            urlz = 'Msd/create/';
        }
        Ext.getCmp('form-Msd').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            success: function (f, a) {
                jun.rztMsd.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-Msd').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});