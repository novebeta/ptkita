jun.ProduksiDetilstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.ProduksiDetilstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'ProduksiDetilStoreId',
            url: 'ProduksiDetil',           
            root: 'results',
            totalProperty: 'total',
            fields: [                
                {name:'produksi_detil_id'},
{name:'qty'},
{name:'price'},
{name:'produksi_id'},
{name:'barang_id'},
                
            ]
        }, cfg));
    }
});
jun.rztProduksiDetil = new jun.ProduksiDetilstore();
//jun.rztProduksiDetil.load();
