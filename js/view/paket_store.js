jun.Paketstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Paketstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'PaketStoreId',
            url: 'Paket',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'paket_id'},
                {name: 'paket_name'},
                {name: 'regular_price'},
                {name: 'proposed_price'},
                {name: 'total_disc'},
                {name: 'disc'},
                {name: 'store'}
            ]
        }, cfg));
    }
});
jun.rztPaket = new jun.Paketstore();
jun.rztPaketLib = new jun.Paketstore();
jun.rztPaketCmp = new jun.Paketstore();
//jun.rztPaket.load();
