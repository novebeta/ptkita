jun.AbsenTransstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.AbsenTransstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'AbsenTransStoreId',
            url: 'AbsenTrans',           
            root: 'results',
            totalProperty: 'total',
            fields: [                
                {name:'absen_trans_id'},
{name:'tgl'},
{name:'tdate'},
{name:'remarks'},
{name:'employee_id'},
{name:'jml'},
{name:'tipe_absen_id'},
                
            ]
        }, cfg));
    }
});
jun.rztAbsenTrans = new jun.AbsenTransstore();
//jun.rztAbsenTrans.load();
