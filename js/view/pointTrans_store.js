jun.PointTransstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.PointTransstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'PointTransStoreId',
            url: 'PointTrans',           
            root: 'results',
            totalProperty: 'total',
            fields: [                
                {name:'point_trans_id'},
{name:'type_no'},
{name:'trans_no'},
{name:'ref'},
{name:'tgl'},
{name:'qty'},
{name:'customer_id'},
{name:'tdate'},
{name:'id_user'},
{name:'up'},
                
            ]
        }, cfg));
    }
});
jun.rztPointTrans = new jun.PointTransstore();
//jun.rztPointTrans.load();
