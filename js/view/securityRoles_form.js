jun.SecurityRolesWin = Ext.extend(Ext.Window, {
    title: "Security Role",
    modez: 1,
    width: 800,
    height: 600,
    layout: "form",
    modal: !0,
    padding: 3,
    resizable: !1,
    closeForm: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-SecurityRoles",
                labelWidth: 100,
                labelAlign: "left",
                layout: "accordion",
                ref: "formz",
                border: !1,
                anchor: "100% 100%",
                items: [
                    {
                        xtype: "panel",
                        title: "Description",
                        layout: "form",
                        bodyStyle: "background-color: #E4E4E4;padding: 10px",
                        items: [
                            {
                                xtype: "textfield",
                                fieldLabel: "Role Name",
                                hideLabel: !1,
                                name: "role",
                                id: "roleid",
                                ref: "../role",
                                maxLength: 30,
                                anchor: "100%"
                            },
                            {
                                xtype: "textarea",
                                fieldLabel: "Description",
                                enableKeyEvents: true,
                                style : {textTransform: "uppercase"},
                                listeners: {
                                    change: function (field, newValue, oldValue) {
                                        field.setValue(newValue.toUpperCase());
                                    }
                                },
                                hideLabel: !1,
                                name: "ket",
                                id: "ketid",
                                ref: "../ket",
                                maxLength: 255,
                                anchor: "100%"
                            }
                        ]
                    },
                    {
                        xtype: "panel",
                        title: "Master Section",
                        layout:'column',
                        bodyStyle: "background-color: #E4E4E4;padding: 10px;",
                        defaultType: "checkbox",
                        items: [
                            {
                                columnWidth: .33,
                                boxLabel: "Country",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "100"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Province",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "101"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "City",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "102"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Sub District",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "103"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Doctor",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "104"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Bank",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "105"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Group",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "106"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Items",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "107"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Customers",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "108"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Beautician",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "109"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Beauty Tips Class",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "110"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Value Tips Class",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "111"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Status Customers",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "113"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Discount",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "114"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Store",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "115"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Supplier",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "116"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Chart Of Account",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "117"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Default Purchase Price",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "118"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Region",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "119"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Card Type",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "121"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Group Attribute",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "122"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Selling Price",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "123"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Employee",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "124"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Clinical Item",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "125"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Clinical Category",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "126"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Info",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "127"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Souvenir",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "128"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Promotion",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "129"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Packages",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "130"
                            }
                        ]
                    },
                    {
                        xtype: "panel",
                        title: "Transaction Section",
                        layout:'column',
                        bodyStyle: "background-color: #E4E4E4;padding: 10px",
                        defaultType: "checkbox",
                        items: [
                            {
                                columnWidth: .33,
                                boxLabel: "Sales",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "200"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Return Sales",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "201"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Edit Service",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "202"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Other Income',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "203"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Petty Cash Expenses',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "204"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Purchase Item",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "205"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Return Supplier Item",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "206"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Tender Declaration",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "207"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Audit",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "208"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Override Price & Discount",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "209"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "History",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "210"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "General Journal",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "211"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Cash/Bank Home Deposit',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "212"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Cash/Bank Home Expenses',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "213"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Cash/Bank Transfer",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "214"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Generate Profit Lost",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "215"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Yearly Closing",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "216"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Absent Transaction',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "217"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Production",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "218"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Production Return',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "219"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Clinical In',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "220"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Clinical Out',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "221"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Souvenir In',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "222"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Souvenir Out',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "223"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Point In',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "224"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Point Out',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "225"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Member Get Member',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "226"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'My Special Day',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "227"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Debt Repayment',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "230"
                            }
                        ]
                    },
                    {
                        xtype: "panel",
                        title: "Report Section",
                        layout:'column',
                        bodyStyle: "background-color: #E4E4E4;padding: 10px",
                        defaultType: "checkbox",
                        items: [
                            {
                                columnWidth: .33,
                                boxLabel: 'Sales Summary',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "313"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Sales Details',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "316"
                            },
                            {
                                columnWidth: .33,
                                boxLabel:'Payments',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "321"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Sales Summary by Group",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "300"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Sales Details by Group",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "301"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Sales and Sales Return by Group',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "331"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Inventory Movement",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "302"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Inventory Movements Clinical',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "333"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Inventory Card",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "303"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Show Cost Price on Inventory Card",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "317"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Beautician Services Summary",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "304"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Beautician Services Details",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "305"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Doctors Services Summary",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "311"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Doctors Services Details",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "312"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "New Customers",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "306"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Category Customers",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "332"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Tenders",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "307"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Daily Report",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "308"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Return Sales Summary",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "309"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Return Sales Details",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "310"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "General Ledger",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "314"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "General Journal",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "315"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Profit Lost Report",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "318"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Balance Sheet",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "319"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Neraca",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "320"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Payment",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "321"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Sales Price",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "322"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Purchase Price',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "330"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Rekening Koran",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "323"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Kartu Hutang',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "324"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Chart Customers Attendance',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "325"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Chart New Customers',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "326"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Chart Sales Group',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "327"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Chart Top Customers',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "328"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Chart Top Sales Group',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "329"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'BirthDay Customers',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "334"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Fixed Asset',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "335"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Account Receivable',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "336"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Loan',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "337"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Sales Tax',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "338"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Tax',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "339"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Capital',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "340"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Retained Earning',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "341"
                            }
                        ]
                    },
                    {
                        xtype: "panel",
                        title: "Administration Section",
                        layout:'column',
                        bodyStyle: "background-color: #E4E4E4;padding: 10px",
                        defaultType: "checkbox",
                        items: [
                            {
                                columnWidth: .33,
                                boxLabel: "User Management",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "400"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Security Roles",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "401"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Backup / Restore",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "402"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Import",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "403"
                            }
                        ]
                    },
                    {
                        xtype: "panel",
                        title: "General Section",
                        layout:'column',
                        bodyStyle: "background-color: #E4E4E4;padding: 10px",
                        defaultType: "checkbox",
                        items: [
                            {
                                columnWidth: .33,
                                boxLabel: "Change Password",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "000"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Logout",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "001"
                            }
                        ]
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    text: "Save & Close",
                    ref: "../btnSaveClose"
                },
                {
                    xtype: "button",
                    text: "Close",
                    ref: "../btnCancel"
                }
            ]
        };
        jun.SecurityRolesWin.superclass.initComponent.call(this);
        this.on("activate", this.onActivate, this);
        this.btnSaveClose.on("click", this.onbtnSaveCloseClick, this);
//        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnCancel.on("click", this.onbtnCancelclick, this);
    },
    btnDisabled: function (a) {
        this.btnSaveClose.setDisabled(a);
    },
    onActivate: function () {
        this.btnSaveClose.hidden = !1;
    },
    saveForm: function () {
        this.btnDisabled(!0);
        var a;
        this.modez == 1 || this.modez == 2 ? a = "SecurityRoles/update/id/" + this.id : a = "SecurityRoles/create/",
            Ext.getCmp("form-SecurityRoles").getForm().submit({
                url: a,
                scope: this,
                success: function (a, b) {
//                    var a = BASE_URL;
                    window.location = BASE_URL;
//                    jun.rztSecurityRoles.reload();
//                    jun.sidebar.getRootNode().reload();
//                    var c = Ext.decode(b.response.responseText);
//                    this.close();
//                    this.closeForm ? this.close() : (c.data != undefined && Ext.MessageBox.alert("Pelayanan", c.data.msg),
//                        this.modez == 0 && Ext.getCmp("form-SecurityRoles").getForm().reset());
                },
                failure: function (a, b) {
                    Ext.MessageBox.alert("Error", "Can't Communicate With The Server");
                    this.btnDisabled(!1);
                }
            });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = !0, this.saveForm(!0);
    },
    onbtnSaveclick: function () {
        this.closeForm = !1, this.saveForm(!1);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});