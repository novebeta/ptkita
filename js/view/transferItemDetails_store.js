jun.TransferItemDetailsstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.TransferItemDetailsstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'TransferItemDetailsStoreId',
            url: 'TransferItemDetails',
            root: 'results',
            autoLoad: !1,
            autoSave: !1,
            totalProperty: 'total',
            fields: [
                {name: 'transfer_item_details_id'},
                {name: 'qty', type: 'float'},
                {name: 'barang_id'},
                {name: 'transfer_item_id'},
                {name: 'price', type: 'float'},
                {name: 'total', type: 'float'},
                {name: 'disc', type: 'float'},
                {name: 'discrp', type: 'float'},
                {name: 'bruto', type: 'float'},
                {name: 'vat', type: 'float'},
                {name: 'vatrp', type: 'float'},
                {name: 'disc1', type: 'float'},
                {name: 'discrp1', type: 'float'},
                {name: 'total_pot', type: 'float'}
            ]
        }, cfg));
//        this.on('add', this.refreshData, this);
//        this.on('update', this.refreshData, this);
//        this.on('remove', this.refreshData, this);
    },
    refreshData: function (a) {
//        var bruto = this.sum("total");
//        var vatrp = this.sum("vatrp");
//        Ext.getCmp("subtotaltransferoutid").setValue(bruto);
//        Ext.getCmp("vattransferitemid").setValue(vatrp);
//        var total = bruto + vatrp;
//        Ext.getCmp("totaltransferoutid").setValue(total);
    }
});
jun.rztTransferItemDetails = new jun.TransferItemDetailsstore();
//jun.rztTransferItemDetails.load();
