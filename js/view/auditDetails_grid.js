jun.AuditDetailsGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "AuditDetails",
    id: 'docs-jun.AuditDetailsGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        {
            header: 'Items',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_id',
            width: 200,
            renderer: jun.renderBarang
        },
        {
            header: 'Qty',
            sortable: true,
            resizable: true,
            dataIndex: 'qty',
            width: 25,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Disc',
            sortable: true,
            resizable: true,
            dataIndex: 'disc',
            width: 45,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
        {
            header: 'Disc Amount',
            sortable: true,
            resizable: true,
            dataIndex: 'discrp',
            width: 75,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
        {
            header: 'Total',
            sortable: true,
            resizable: true,
            dataIndex: 'total',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
        {
            header: 'Beautician',
            sortable: true,
            resizable: true,
            dataIndex: 'beauty_id',
            width: 150,
            renderer: jun.renderBeauty
        }
    ],
    initComponent: function () {
        this.store = jun.rztAuditDetails;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'buttongroup',
                    columns: 10,
                    defaults: {
                        scale: 'small'
                    },
                    items: [
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Item :'
                        },
                        {
                            xtype: 'combo',
                            style: 'margin-bottom:2px',
                            typeAhead: true,
                            triggerAction: 'all',
                            lazyRender: true,
                            mode: 'local',
                            //forceSelection: true,
                            store: jun.rztBarangCmp,
                            hiddenName: 'barang_id',
                            valueField: 'barang_id',
                            ref: '../../barang',
                            displayField: 'nama_barang',
                            anchor: '100%'
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Qty :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'qtyid',
                            ref: '../../qty',
                            style: 'margin-bottom:2px',
                            width: 50,
                            value: 1,
                            minValue: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Price :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'priceid',
                            ref: '../../price',
                            style: 'margin-bottom:2px',
                            width: 75,
                            value: 0,
                            minValue: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Disc :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'discdetilid',
                            style: 'margin-bottom:2px',
                            ref: '../../disc',
                            width: 50,
                            value: 0,
                            minValue: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Disc Amount :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'discadetilid',
                            style: 'margin-bottom:2px',
                            ref: '../../disca',
                            width: 75,
                            value: 0,
                            minValue: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Note :'
                        },
                        {
                            xtype: 'textfield',
                            id: 'ketpotid',
                            ref: '../../ketpot',
                            width: 475,
                            colspan: 7,
                            maxLength: 255
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Beautician :'
                        },
                        {
                            xtype: 'combo',
                            typeAhead: true,
                            ref: '../../beauty',
                            triggerAction: 'all',
                            lazyRender: true,
                            mode: 'local',
                            store: jun.rztBeautyCmp,
                            hiddenName: 'beauty_id',
                            valueField: 'beauty_id',
                            displayField: 'nama_beauty',
                            allowBlank: true
                        }
                    ]},
                {
                    xtype: 'buttongroup',
                    columns: 3,
                    defaults: {
                        scale: 'large'
                    },
                    items: [
                        {
                            xtype: 'button',
                            text: 'Add',
                            height: 44,
                            ref: '../../btnAdd'
                        },
                        {
                            xtype: 'button',
                            text: 'Edit',
                            height: 44,
                            ref: '../../btnEdit'
                        },
                        {
                            xtype: 'button',
                            text: 'Delete',
                            height: 44,
                            ref: '../../btnDelete'
                        }
                    ]
                }
            ]
        };
        jun.AuditDetailsGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.onClickbtnEdit, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.disc.on('change', this.onDiscChange, this);
        this.disca.on('change', this.onDiscChange, this);
        this.barang.on('change', this.onChangeBarang, this);
//        this.store.on('add', this.onStoreChange, this);
//        this.store.on('edit', this.onStoreChange, this);
//        this.store.on('remove', this.onStoreChange, this);
    },
    onDiscChange: function (a) {
        var price = parseFloat(this.price.getValue());
        var qty = parseFloat(this.qty.getValue());
        var disc = parseFloat(this.disc.getValue());
        var disca = parseFloat(this.disca.getValue());
        var bruto = round(price * qty,2);
        if (a.id == "discdetilid") {
            this.disca.setValue(round(bruto * (disc / 100),2));
        }
        else {
            this.disc.setValue(round((disca / bruto) * 100,2));
        }
    },
    onStoreChange: function () {
        jun.rztAuditDetails.refreshData();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var barang_id = this.barang.getValue();
        if (barang_id == "") {
            Ext.MessageBox.alert("Error", "Barang harus dipilih.");
            return
        }
//      kunci barang sama tidak boleh masuk dua kali
        if (this.btnEdit.text != 'Save') {
            var a = jun.rztAuditDetails.findExact("barang_id", barang_id);
            if (a > -1) {
                Ext.MessageBox.alert("Error", "Item barang sudah dimansukkan.");
                return
            }
        }
        var barang = jun.getBarang(barang_id);
        var price = parseFloat(this.price.getValue());
        var qty = parseFloat(this.qty.getValue());
        var disc = parseFloat(this.disc.getValue());
        var disca = parseFloat(this.disca.getValue());
        var ketpot = this.ketpot.getValue();
        var beauty = this.beauty.getValue();
        var bruto = round(price * qty,2);
        var vat = jun.getTax(barang_id);
        var vatrp = round(bruto * vat,2);
        var totalpot = disca;
        var total = bruto - totalpot;
        if (this.btnEdit.text == 'Save') {
            var record = this.sm.getSelected();
            record.set('barang_id', barang_id);
            record.set('qty', qty);
            record.set('price', price);
            record.set('disc', disc);
            record.set('discrp', disca);
            record.set('ketpot', ketpot);
            record.set('beauty_id', beauty);
            record.set('vat', vat);
            record.set('vatrp', vatrp);
            record.set('totalpot', totalpot);
            record.set('total', total);
            record.set('bruto', bruto);
            record.commit();
        } else {
            var c = jun.rztAuditDetails.recordType,
                d = new c({
                    barang_id: barang_id,
                    qty: qty,
                    price: price,
                    disc: disc,
                    discrp: disca,
                    ketpot: ketpot,
                    beauty_id: beauty,
                    vat: vat,
                    vatrp: vatrp,
                    totalpot: totalpot,
                    bruto: bruto,
                    total: total
                });
            jun.rztAuditDetails.add(d);
        }
//        this.store.refreshData();
        this.barang.reset();
        this.qty.reset();
        this.price.reset();
        this.ketpot.reset();
        this.disc.reset();
        this.disca.reset();
        this.beauty.reset();
    },
    onChangeBarang: function () {
        var barang_id = this.barang.getValue();
        if (barang_id == "") {
            Ext.MessageBox.alert("Error", "Barang harus dipilih.");
            return
        }
        var barang = jun.getBarang(barang_id);
        //this.price.setValue(barang.data.price);
        if (jun.isCatJasa(barang_id)) {
            this.beauty.setDisabled(false);
        } else {
            this.beauty.reset();
            this.beauty.setDisabled(true);
        }
        Ext.Ajax.request({
            url: 'SalestransDetails/GetDiskon',
            method: 'POST',
            scope: this,
            params: {
                customer_id: customer_id,
                barang_id: barang_id
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                this.price.setValue(response.msg.price);
                this.disc.setValue(response.msg.value);
                this.disc_name.setValue(response.msg.nama_status);
                this.onDiscChange(this.disc);
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    btnDisable: function (s) {
        this.btnAdd.setDisabled(s);
        this.btnDelete.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    onClickbtnEdit: function (btn) {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih item.");
            return;
        }
        if (btn.text == 'Edit') {
            this.barang.setValue(record.data.barang_id);
            this.onChangeBarang();
            this.qty.setValue(record.data.qty);
            this.disc.setValue(record.data.disc);
            this.disca.setValue(record.data.discrp);
            this.ketpot.setValue(record.data.ketpot);
            this.beauty.setValue(record.data.beauty_id);
            this.price.setValue(record.data.price);
            btn.setText("Save");
            this.btnDisable(true);
        } else {
            this.loadForm();
            btn.setText("Edit");
            this.btnDisable(false);
        }
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a item");
            return;
        }
        var idz = selectedz.json.salestrans_details;
        var form = new jun.AuditDetailsWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a item");
            return;
        }
        this.store.remove(record);
    }
})
