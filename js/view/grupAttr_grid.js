jun.GrupAttrGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Attribute Grup",
    id: 'docs-jun.GrupAttrGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Group Name',
            sortable: true,
            resizable: true,
            dataIndex: 'grup_id',
            width: 100,
            renderer: jun.renderGrup
        },
        {
            header: 'VAT (%)',
            sortable: true,
            resizable: true,
            dataIndex: 'vat',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
        {
            header: 'Branch',
            sortable: true,
            resizable: true,
            dataIndex: 'store',
            width: 100
        }
    ],
    initComponent: function () {
        this.store = jun.rztGrupAttr;
        jun.rztGrupLib.on({
            scope: this,
            load: {
                fn: function (a, b, c) {
                    this.store.baseParams = {mode: "grid"};
                    this.store.reload();
                    this.store.baseParams = {};
                }
            }
        });
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        if (jun.rztGrupLib.getTotalCount() === 0) {
            jun.rztGrupLib.load();
        }
        if (jun.rztGrupCmp.getTotalCount() === 0) {
            jun.rztGrupCmp.load();
        }
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Add Group Attibute',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Edit Group Attibute',
                    ref: '../btnEdit'
                }
            ]
        };
        jun.GrupAttrGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        //this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.GrupAttrWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.grup_attr_id;
        var form = new jun.GrupAttrWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    }
})
