jun.Usersstore = Ext.extend(Ext.data.JsonStore, {constructor: function (a) {
    a = a || {}, jun.Usersstore.superclass.constructor.call(this, Ext.apply({
        storeId: "UsersStoreId",
        url: "Users/?output=json",
        root: "results",
        totalProperty: "total", fields: [
            {name: "id"},
            {name: "user_id"},
            {name: "name"},
            {name: "password"},
            {name: "last_visit_date"},
            {name: "active"},
            {name: "security_roles_id"},
            {name: 'store'}
        ]}, a))
}}), jun.rztUsers = new jun.Usersstore;