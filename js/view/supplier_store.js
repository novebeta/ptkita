jun.Supplierstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Supplierstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'SupplierStoreId',
            url: 'Supplier',
            autoLoad: true,
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'supplier_id'},
                {name: 'supplier_name'},
                {name: 'account_code'}
            ]
        }, cfg));
    }
});
jun.rztSupplier = new jun.Supplierstore();
jun.rztSupplierLib = new jun.Supplierstore();
jun.rztSupplierCmp = new jun.Supplierstore();
//jun.rztSupplier.load();
