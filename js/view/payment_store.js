jun.Paymentstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Paymentstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'PaymentStoreId',
            url: 'Payment',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'payment_id'},
                {name: 'card_number'},
                {name: 'bank_id'},
                {name: 'salestrans_id'},
                {name: 'amount', type: 'float'},
                {name: 'kembali', type: 'float'},
                {name: 'card_id'}
            ]
        }, cfg));
        this.on('add', this.refreshData, this);
        this.on('update', this.refreshData, this);
        this.on('remove', this.refreshData, this);
    },
    refreshData: function (t, rec, idx) {
        var total = this.sum("amount");
        var bayar = Ext.getCmp("bayarid").setValue(total);
        var form = Ext.getCmp("form-Salestrans");
        var title = form.ownerCt.title;
        if (title == 'Return Sales') {
            jun.rztReturSalestransDetails.refreshData();
        } else {
            jun.rztSalestransDetails.refreshData();
        }

    }
});
jun.rztPayment = new jun.Paymentstore();
//jun.rztPayment.load();
