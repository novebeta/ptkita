jun.Employeestore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Employeestore.superclass.constructor.call(this, Ext.apply({
            storeId: 'EmployeeStoreId',
            url: 'Employee',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'employee_id'},
                {name: 'employee_name'},
                {name: 'no_kp'}
            ]
        }, cfg));
    }
});
jun.rztEmployee = new jun.Employeestore();
jun.rztEmployeeCmp = new jun.Employeestore();
jun.rztEmployeeLib = new jun.Employeestore();
//jun.rztEmployee.load();
