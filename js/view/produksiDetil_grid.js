jun.ProduksiDetilGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "ProduksiDetil",
    id: 'docs-jun.ProduksiDetilGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Items Code',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_id',
            width: 200,
            renderer: jun.renderKodeBarang
        },
        {
            header: 'Items Name',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_id',
            width: 200,
            renderer: jun.renderBarang
        },
        {
            header: 'Qty',
            sortable: true,
            resizable: true,
            dataIndex: 'qty',
            width: 25,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        }
    ],
    initComponent: function () {
        this.store = jun.rztProduksiDetil;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'label',
                    text: '\xA0Product :\xA0'
                },
                {
                    xtype: 'combo',
                    typeAhead: true,
                    triggerAction: 'all',
                    lazyRender: true,
                    mode: 'local',
                    forceSelection: true,
                    store: jun.rztBarangRaw,
                    ref: '../barang',
                    valueField: 'barang_id',
                    displayField: 'kode_barang'
                },
                {
                    xtype: 'label',
                    text: '\xA0Qty :\xA0'
                },
                {
                    xtype: 'numericfield',
                    ref: '../qty',
                    style: 'margin-bottom:2px',
                    width: 50,
                    value: 1,
                    minValue: 0
                },
                {
                    xtype: 'button',
                    text: 'Add',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'button',
                    text: 'Edit',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'button',
                    text: 'Del',
                    ref: '../btnDel'
                }
            ]
        };
        jun.ProduksiDetilGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.onClickbtnEdit, this);
        this.btnDel.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var barang_id = this.barang.getValue();
        if (barang_id == "" || barang_id == undefined) {
            Ext.MessageBox.alert("Error", "You have not selected a item");
            return;
        }
//      kunci barang sama tidak boleh masuk dua kali
        if (this.btnEdit.text != 'Save') {
            var a = jun.rztProduksiDetil.findExact("barang_id", barang_id);
            if (a > -1) {
                Ext.MessageBox.alert("Error", "Item already inputted");
                return;
            }
        }
        var barang = jun.getBarang(barang_id);
        var qty = parseFloat(this.qty.getValue());
        if (this.btnEdit.text == 'Save') {
            var record = this.sm.getSelected();
            record.set('barang_id', barang_id);
            record.set('qty', qty);
            record.commit();
        } else {
            var c = jun.rztProduksiDetil.recordType,
                d = new c({
                    barang_id: barang_id,
                    qty: qty
                });
            jun.rztProduksiDetil.add(d);
        }
        this.barang.reset();
        this.qty.reset();
    },
    btnDisable: function (s) {
        this.btnAdd.setDisabled(s);
        this.btnDel.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    onClickbtnEdit: function (btn) {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a item");
            return;
        }
        if (btn.text == 'Edit') {
            this.barang.setValue(record.data.barang_id);
            this.qty.setValue(record.data.qty);
            btn.setText("Save");
            this.btnDisable(true);
        } else {
            this.loadForm();
            btn.setText("Edit");
            this.btnDisable(false);
        }
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a item");
            return;
        }
        var idz = selectedz.json.produksi_detil_id;
        var form = new jun.ProduksiDetilWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    deleteRec: function () {
        var record = this.sm.getSelected();
        if (record == "") {
            Ext.MessageBox.alert("Warning", "You have not selected a item.");
            return;
        }
        Ext.MessageBox.confirm('Questions', 'Are you sure want delete this item?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        this.store.remove(record);
    }
});
