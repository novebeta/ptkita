jun.PaketTransWin = Ext.extend(Ext.Window, {
    title: 'Paket Transaction',
    modez: 1,
    arus: 1,
    id: 'window-PaketTrans',
    width: 665,
    height: 280,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4;padding: 10px',
                id: 'form-PaketTrans',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "label",
                        text: "Package Name:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        id: 'paket_idid',
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'paket_id',
                        ref: '../paket',
                        store: jun.rztPaketCmp,
                        hiddenName: 'paket_id',
                        valueField: 'paket_id',
                        displayField: 'paket_name',
                        x: 105,
                        y: 2,
                        width: 175
                    },
                    {
                        xtype: "label",
                        text: "Qty:",
                        x: 315,
                        y: 5
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'qty',
                        hideLabel: false,
                        //hidden:true,
                        name: 'qty',
                        id: 'qtypaketid',
                        ref: '../qty',
                        maxLength: 11,
                        width: 75,
                        x: 350,
                        y: 2,
                        value: 1,
                        minValue: 1
                    },
                    new jun.PaketDetailsTransGrid({
                        x: 5,
                        y: 35,
                        height: 160,
                        frameHeader: !1,
                        header: !1
                    })
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Add',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Add & Close',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Close',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.PaketTransWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.paket.on('select', this.onpaketSelect, this);
        this.on("close", this.onWinClose, this);
        jun.rztPaketTransItem.on("load", this.onPaketTransItemLoad, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },
    onPaketTransItemLoad: function (store, records, options) {
        var paket_trans_id = '';
        jun.rztPaketTransItem.data.each(function (item, index, totalItems) {
            //alert(item.data['oneOfMyColumnNames']);
            var barang_id = item.data['barang_id'];
            var price = parseFloat(item.data['price']);
            var qty = parseFloat(item.data['qty']);
            var disc = parseFloat(item.data['disc']);
            var disca = parseFloat(item.data['discrp']);
            var disc1 = parseFloat(item.data['disc1']);
            var ketpot = item.data['ketpot'];
            var disc_name = item.data['disc_name'];
            var bruto = parseFloat(item.data['bruto']);
            var vat = parseFloat(item.data['vat']);
            //var subtotal = parseFloat(item.data['vat']);
            var discrp1 = parseFloat(item.data['discrp1']);
            var totalpot = parseFloat(item.data['total_pot']);
            //var total_with_disc = bruto - totalpot;
            var total = parseFloat(item.data['total']);
            var vatrp = parseFloat(item.data['vatrp']);
            paket_trans_id = item.data['paket_trans_id'];
            var paket_details_id = item.data['paket_details_id'];
            var detail_trans;
            var windows_trans = Ext.getCmp('window-PaketTrans');
            if (windows_trans.arus == 1) {
                detail_trans = jun.rztSalestransDetails;
            }else{
                detail_trans = jun.rztReturSalestransDetails;
            }
            var c = detail_trans.recordType,
                d = new c({
                    barang_id: barang_id,
                    qty: qty,
                    price: price,
                    disc: disc,
                    discrp: disca,
                    ketpot: ketpot,
                    vat: vat,
                    vatrp: vatrp,
                    total_pot: totalpot,
                    bruto: bruto,
                    disc_name: disc_name,
                    total: total,
                    disc1: disc1,
                    discrp1: discrp1,
                    paket_trans_id: paket_trans_id,
                    paket_details_id: paket_details_id
                });
            detail_trans.add(d);
        });
        var paket_id = this.paket.getValue();
        var jml = parseFloat(this.qty.getValue());
        var c = jun.rztPaketTrans.recordType,
            d = new c({
                paket_id: paket_id,
                qty: jml,
                paket_trans_id: paket_trans_id
            });
        jun.rztPaketTrans.add(d);
        if (this.closeForm) {
            this.close();
        } else {
            this.btnDisabled(false);
            Ext.getCmp('form-PaketTrans').getForm().reset();
            jun.rztTransPaketDetails.removeAll();
            jun.rztBarangPaketTransCmp.removeAll();
        }
        //jun.rztSalestransDetails.refreshData();
    },
    onWinClose: function () {
        jun.rztTransPaketDetails.removeAll();
        jun.rztBarangPaketTransCmp.removeAll();
        jun.rztPaketTransItem.un("load", this.onPaketTransItemLoad, this);
    },
    onpaketSelect: function (combo, record, index) {
        //var qty = parseFloat(Ext.getCmp('qtyid').getValue());
        jun.rztTransPaketDetails.baseParams = {
            paket_id: record.data.paket_id
        };
        jun.rztTransPaketDetails.load();
        jun.rztTransPaketDetails.baseParams = {};
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var qty = parseFloat(this.qty.getValue());
        var paket_id = Ext.getCmp('paket_idid').getValue();
        if (paket_id == undefined || paket_id == '') {
            Ext.MessageBox.alert("Warning", "You have not selected a paket");
            return;
        }
        jun.rztPaketTransItem.baseParams = {
            detil: Ext.encode(Ext.pluck(
                jun.rztTransPaketDetails.data.items, "data")),
            qty: qty,
            paket_id: paket_id
        };
        jun.rztPaketTransItem.load();
        jun.rztPaketTransItem.baseParams = {};
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});