jun.PaketTransstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.PaketTransstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'PaketTransStoreId',
            url: 'PaketTrans',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'paket_trans_id'},
                {name: 'paket_id'},
                {name: 'salestrans_id'},
                {name: 'qty'}
            ]
        }, cfg));
    }
});
jun.rztPaketTrans = new jun.PaketTransstore();
jun.PaketTransItemstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.PaketTransItemstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'PaketTransItemStoreId',
            url: 'PaketTrans/Item',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'paket_trans_id'},
                {name: 'paket_details_id'},
                {name: 'barang_id'},
                {name: 'qty', type: 'float'},
                {name: 'disc', type: 'float'},
                {name: 'discrp', type: 'float'},
                {name: 'ketpot'},
                {name: 'vat', type: 'float'},
                {name: 'vatrp', type: 'float'},
                {name: 'bruto', type: 'float'},
                {name: 'total', type: 'float'},
                {name: 'total_pot', type: 'float'},
                {name: 'price', type: 'float'},
                {name: 'disc_name'},
                {name: 'disc1', type: 'float'},
                {name: 'discrp1', type: 'float'}
            ]
        }, cfg));
    }
});
jun.rztPaketTransItem = new jun.PaketTransItemstore();
