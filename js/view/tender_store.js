jun.Tenderstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Tenderstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'TenderStoreId',
            url: 'Tender',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'tender_id'},
                {name: 'tgl'},
                {name: 'doc_ref'},
                {name: 'user_id'},
                {name: 'tdate'},
                {name: 'total'},
                {name: 'store'}
            ]
        }, cfg));
    }
});
jun.rztTender = new jun.Tenderstore();
//jun.rztTender.load();
