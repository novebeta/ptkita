jun.ReturSalestransgrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Return Sales",
    id: 'docs-jun.ReturSalestransgrid',
    iconCls: "silk-grid",
    stripeRows: true,
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        {
            header: 'No. Receipt',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100
        },
        {
            header: 'No. Customers',
            sortable: true,
            resizable: true,
            dataIndex: 'no_customer'
        },
        {
            header: 'Customers Name',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_customer'
        },
        {
            header: 'Date',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 100
        },
        {
            header: 'Total',
            sortable: true,
            resizable: true,
            dataIndex: 'total',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
        {
            header: 'Branch',
            sortable: true,
            resizable: true,
            dataIndex: 'store',
            width: 100
        }
    ],
    initComponent: function () {
        if (jun.rztBarangLib.getTotalCount() === 0) {
            jun.rztBarangLib.load();
        }
        if (jun.rztBarangCmp.getTotalCount() === 0) {
            jun.rztBarangCmp.load();
        }
        if (jun.rztBankTransCmp.getTotalCount() === 0) {
            jun.rztBankTransCmp.load();
        }
        if (jun.rztBeautyLib.getTotalCount() === 0) {
            jun.rztBeautyLib.load();
        }
        if (jun.rztBeautyCmp.getTotalCount() === 0) {
            jun.rztBeautyCmp.load();
        }
        if (jun.rztGrup.getTotalCount() === 0) {
            jun.rztGrup.load();
        }

        if (jun.rztCardLib.getTotalCount() === 0) {
            jun.rztCardLib.load();
        }
        if (jun.rztCardCmp.getTotalCount() === 0) {
            jun.rztCardCmp.load();
        }
        if (jun.rztPaketCmp.getTotalCount() === 0) {
            jun.rztPaketCmp.load();
        }
        if (jun.rztPaketLib.getTotalCount() === 0) {
            jun.rztPaketLib.load();
        }
        jun.rztReturSalestrans.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    b.params = {
                        'tgl': Ext.getCmp('tglretursalesgridid').getValue()
                    }
                }
            }
        });
        this.store = jun.rztReturSalestrans;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Add Return',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'View Return',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Print Retun Sales',
                    ref: '../btnPrint'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Date :'
                },
                {
                    xtype: 'xdatefield',
                    id: 'tglretursalesgridid',
                    ref: '../tgl'
                }
            ]
        };
        jun.ReturSalestransgrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnPrint.on('Click', this.printSales, this);
        this.tgl.on('select', this.refreshTgl, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.store.removeAll();
    },
    printSales: function () {
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a transaction");
            return;
        }
        Ext.Ajax.request({
            url: 'Salestrans/print',
            method: 'POST',
            scope: this,
            params: {
                id: record.json.salestrans_id
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                //findPrinterReceipt();
                if (notReady()) { return; }
//                qz.append(chr(27) + "\x70" + "\x30" + chr(25) + chr(25) + "\r");
//                qz.print();
//                qz.appendImage(getPath() + "images/img_receipt.png", "ESCP");
//                while (!qz.isDoneAppending()) {}
//                qz.append("\x1B\x40"); // 1
                //qz.append("\x1B\x21\x08"); // 2
//                qz.append("\x1B\x21\x01"); // 3
//                qz.append("\x1B\x30"); // 3
//                qz.append(response.msg);
//                qz.append("\x1D\x56\x41"); // 4
//                qz.append("\x1B\x40"); // 5
//                qz.print();
//                qz.appendHTML('<html><pre>'+response.msg+'</pre></html>');
//                qz.printHTML();

                opencashdrawer();
                printHTML(PRINTER_RECEIPT,'<html><pre>' + response.msg + '</pre></html>');
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    refreshTgl: function () {
        this.store.reload();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
        jun.rztCustomersReturSalesCmp.baseParams = {
            customer_id: this.record.data.customer_id
        };
        jun.rztCustomersReturSalesCmp.load();
        jun.rztCustomersReturSalesCmp.baseParams = {};
    },
    //editForm: function () {
    //    var selectedz = this.sm.getSelected();
    //    if (selectedz == undefined) {
    //        Ext.MessageBox.alert("Warning", "You have not selected a transaction");
    //        return;
    //    }
    //    var idz = selectedz.json.salestrans_id;
    //    var form = new jun.ReturSalestransWin({modez: 1, id: idz});
    //    form.show(this);
    //    form.formz.getForm().loadRecord(this.record);
    //    //form.tgl.setValue(Date.parseDate(this.record.data.tgl, 'Y-m-d H:i:s'));
    //    //jun.rztCustomersCmp.un('load', this.editForm, this);
    //},
    loadForm: function () {
        //jun.rztCustomersCmp.un('load', this.editForm, this);
        var form = new jun.ReturSalestransWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        //jun.rztCustomersCmp.removeAll();
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a transaction");
            return;
        }
        var idz = selectedz.json.salestrans_id;
        var form = new jun.ReturSalestransWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        jun.rztReturSalestransDetails.baseParams = {
            salestrans_id: idz
        };
        jun.rztReturSalestransDetails.load();
        jun.rztReturSalestransDetails.baseParams = {};
        jun.rztPayment.baseParams = {
            salestrans_id: idz
        };
        jun.rztPayment.load();
        jun.rztPayment.baseParams = {};
        jun.rztPaketTrans.baseParams = {
            salestrans_id: idz
        };
        jun.rztPaketTrans.load();
        jun.rztPaketTrans.baseParams = {};
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'Salestrans/delete/id/' + record.json.salestrans_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztReturSalestrans.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});

