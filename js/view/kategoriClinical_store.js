jun.KategoriClinicalstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.KategoriClinicalstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'KategoriClinicalStoreId',
            url: 'KategoriClinical',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'kategori_clinical_id'},
                {name: 'nama_kategori'}
            ]
        }, cfg));
    }
});
jun.rztKategoriClinical = new jun.KategoriClinicalstore();
jun.rztKategoriClinicalLib = new jun.KategoriClinicalstore();
jun.rztKategoriClinicalCmp = new jun.KategoriClinicalstore();
//jun.rztKategoriClinical.load();
