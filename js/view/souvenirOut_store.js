jun.SouvenirOutstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.SouvenirOutstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'SouvenirOutStoreId',
            url: 'SouvenirOut',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'souvenir_out_id'},
                {name: 'doc_ref'},
                {name: 'tgl'},
                {name: 'qty_souvenir'},
                {name: 'qty_point'},
                {name: 'souvenir_id'},
                {name: 'event_id'},
                {name: 'customer_id'},
                {name: 'note'},
                {name: 'store'}
            ]
        }, cfg));
    }
});
jun.rztSouvenirOut = new jun.SouvenirOutstore();
//jun.rztSouvenirOut.load();
