jun.AuditWin = Ext.extend(Ext.Window, {
    title: 'Sales',
    modez: 1,
    width: 945,
    height: 565,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4;padding: 10px',
                id: 'form-Audit',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "label",
                        text: "No. Receipt:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'No. Receipt',
                        hideLabel: false,
                        //hidden:true,
                        name: 'doc_ref',
                        id: 'doc_refid',
                        ref: '../doc_ref',
                        maxLength: 50,
                        x: 85,
                        y: 2,
                        height: 20,
                        width: 175,
                        readOnly: true
                    },
                    {
                        xtype: "label",
                        text: "Card Number:",
                        x: 610,
                        y: 5
                    },
                    {
                        xtype: 'textfield',
                        //hidden:true,
                        name: 'card_number',
                        enableKeyEvents: true,
                        id: 'card_numberid',
                        ref: '../card_number',
                        maxLength: 500,
                        width: 200,
//                        readOnly: true,
                        x: 715,
                        y: 5
                    },
                    {
                        xtype: "label",
                        text: "Date:",
                        x: 295,
                        y: 5
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        fieldLabel: 'Date',
                        name: 'tgl',
                        id: 'tglid',
                        format: 'd M Y',
                        width: 175,
                        readOnly: true,
                        value : DATE_NOW,
                        x: 400,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "Customer:",
                        x: 5,
                        y: 35
                    },
                    {
                        xtype: 'combo',
//                        typeAhead: true,
                        triggerAction: 'query',
                        lazyRender: true,
                        mode: 'remote',
                        forceSelection: true,
                        autoSelect: false,
                        store: jun.rztCustomersCmp,
                        hiddenName: 'customer_id',
                        valueField: 'customer_id',
                        displayField: 'nama_customer',
                        hideTrigger: true,
                        minChars: 3,
                        matchFieldWidth: !1,
                        pageSize: 20,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span>{telp} <br /> {tgl_lahir:date("M j, Y")}</span>{no_customer} | {nama_customer}</h3>',
                            '{alamat}',
                            "</div></tpl>"),
                        allowBlank: false,
                        listWidth: 750,
                        x: 85,
                        y: 32,
                        height: 20,
                        width: 175
                    },
                    {
                        xtype: "label",
                        text: "Payment Method:",
                        x: 295,
                        y: 35
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        ref: '../bank',
                        mode: 'local',
                        forceSelection: true,
                        store: jun.rztBankCmp,
                        hiddenName: 'bank_id',
                        valueField: 'bank_id',
                        displayField: 'nama_bank',
                        width: 175,
                        x: 400,
                        y: 32
                    },
                    new jun.AuditDetailsGrid({
                        x: 5,
                        y: 65,
                        height: 285,
                        frameHeader: !1,
                        header: !1
                    }),
                    {
                        xtype: "label",
                        text: "Sub Total:",
                        x: 295,
                        y: 365
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        //hidden:true,
                        name: 'bruto',
                        id: 'subtotalid',
                        ref: '../subtotal',
                        maxLength: 30,
                        value: 0,
                        readOnly: true,
                        width: 175,
                        x: 400,
                        y: 362
                    },
                    {
                        xtype: "label",
                        text: "VAT:",
                        x: 295,
                        y: 395
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        //hidden:true,
                        name: 'vat',
                        id: 'vatid',
                        ref: '../vat',
                        maxLength: 30,
                        value: 0,
                        readOnly: true,
                        width: 175,
                        x: 400,
                        y: 392
                    },
                    {
                        xtype: "label",
                        text: "Disc (%):",
                        x: 295,
                        y: 425
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        //hidden:true,
                        name: 'disc',
                        id: 'discid',
                        ref: '../disc',
                        value: 0,
                        width: 175,
                        x: 400,
                        y: 422
                    },
                    {
                        xtype: "label",
                        text: "Disc Amount:",
                        x: 295,
                        y: 455
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        //hidden:true,
                        name: 'discrp',
                        id: 'discrpid',
                        ref: '../discrp',
                        value: 0,
                        maxLength: 30,
                        width: 175,
                        x: 400,
                        y: 452
                    },
                    {
                        xtype: "label",
                        text: "Comment:",
                        x: 610,
                        y: 365
                    },
                    {
                        xtype: 'textarea',
                        enableKeyEvents: true,
                        style : {textTransform: "uppercase"},
                        listeners: {
                            change: function (field, newValue, oldValue) {
                                field.setValue(newValue.toUpperCase());
                            }
                        },
                        name: 'ketdisc',
                        id: 'ketdiscid',
                        ref: '../ketdisc',
                        maxLength: 255,
                        width: 200,
                        height: 80,
                        x: 715,
                        y: 365
                    },
                    {
                        xtype: "label",
                        text: "Total:",
                        x: 610,
                        y: 455
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        //hidden:true,
                        name: 'total',
                        id: 'totalid',
                        ref: '../total',
                        value: 0,
                        readOnly: true,
                        maxLength: 30,
                        width: 200,
                        x: 715,
                        y: 455
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Save',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Save & Close',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Cancel',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.AuditWin.superclass.initComponent.call(this);
        this.disc.on('change', this.onDiscChange, this);
        this.discrp.on('change', this.onDiscChange, this);
        this.bank.on('change', this.onBankChange, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.on("close", this.onWinClose, this);
        this.card_number.on("specialkey", this.onCardnumber, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
            this.btnSaveClose.setVisible(true);
            //this.setDateTime();
        }
    },
    onBankChange: function () {
        var bank = this.bank.getValue();
        if (bank == "" || bank == undefined) {
            return;
        }
        if (bank == SYSTEM_BANK_CASH) {
            this.card_number.reset();
            this.card_number.setDisabled(true);
        } else {
            this.card_number.setDisabled(false);
        }
    },
    onCardnumber: function (f, e) {
        if (e.getKey() == e.ENTER) {
            var val = this.card_number.getValue();
            if (val == "" || val == undefined) {
                return;
            }
            var p = new SwipeParserObj(val);
            this.card_number.setValue(p.account);
        }
    },
    //setDateTime: function () {
    //    Ext.Ajax.request({
    //        url: 'site/GetDateTime',
    //        method: 'POST',
    //        scope: this,
    //        success: function (f, a) {
    //            var response = Ext.decode(f.responseText);
    //            this.tgl.setValue(Date.parseDate(response.datetime, 'Y-m-d H:i:s'));
    //        },
    //        failure: function (f, a) {
    //            switch (a.failureType) {
    //                case Ext.form.Action.CLIENT_INVALID:
    //                    Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
    //                    break;
    //                case Ext.form.Action.CONNECT_FAILURE:
    //                    Ext.Msg.alert('Failure', 'Ajax communication failed');
    //                    break;
    //                case Ext.form.Action.SERVER_INVALID:
    //                    Ext.Msg.alert('Failure', a.result.msg);
    //            }
    //        }
    //    });
    //},
    onWinClose: function () {
        jun.rztAuditDetails.removeAll();
    },
    onDiscChange: function (a) {
        var subtotal = parseFloat(this.subtotal.getValue());
        var vat = parseFloat(this.vat.getValue());
        var disc = parseFloat(this.disc.getValue());
        var discrp = parseFloat(this.discrp.getValue());
        var bruto = subtotal - vat;
        if (a.id == "discid") {
            this.discrp.setValue(round(bruto * (disc / 100),2));
        }
        else {
            this.disc.setValue(round((discrp / bruto) * 100),2);
        }
        jun.rztAuditDetails.refreshData();
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        if (jun.rztAuditDetails.data.length === 0) {
            Ext.MessageBox.alert("Error", "Must have at least 1 item details!");
            this.btnDisabled(!1);
            return;
        }
        var urlz = 'Audit/create/';
        Ext.getCmp('form-Audit').getForm().submit({
            url: urlz,
            scope: this,
            params: {
                detil: Ext.encode(Ext.pluck(
                    jun.rztAuditDetails.data.items, "data")),
                id: this.id,
                mode: this.modez
            },
            success: function (f, a) {
                jun.rztAudit.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-Audit').getForm().reset();
                    //this.setDateTime();
                    this.onWinClose();
                    jun.rztAuditDetails.refreshData();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }

        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }

});

jun.Sales2AuditWin = Ext.extend(Ext.Window, {
    title: "Sales To Audit",
    modez: 1,
    width: 825,
    height: 440,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    iswin: !0,
    resizable: !1,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-Sales2AuditWin",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        fieldLabel: 'Date',
                        name: 'tgl',
                        id: 'tglauditid',
                        format: 'd M Y',
                        width: 175
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        fieldLabel: 'Total',
                        name: 'total',
                        id: 'totalauditid',
                        ref: '../total',
                        value: 0,
                        readOnly: true,
                        maxLength: 30,
                        width: 175
                    },
                    new jun.Sales2AuditGrid({
                        height: 285,
                        frameHeader: !1,
                        header: !1
                    })
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    text: "Save to Audit",
                    ref: "../btnSave"
                }
            ]
        };
        jun.Sales2AuditWin.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.tgl.on('select',this.ontglChangeclick, this)
        this.on("close", this.onWinClose, this);
        jun.rztSales2Audit.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    b.params = {
                        tgl: Ext.getCmp('tglauditid').getValue()
                    }
                }
            }
        });
    },
    onWinClose: function () {
        jun.rztSales2Audit.removeAll();
    },
    ontglChangeclick: function(){
        jun.rztSales2Audit.load();
    },
    onbtnSaveclick: function () {
        var data = jun.rztSales2Audit.query('cetak',true);
        var sales_id = [];
        data.each(function(entry) {
            sales_id.push(entry.data.salestrans_id);
        });
        Ext.Ajax.request({
            url: 'audit/Save2Audit',
            method: 'POST',
            scope: this,
            params: {
                salestrans_id: Ext.encode(sales_id)
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                this.close();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
//        this.detil.setValue(Ext.encode(Ext.pluck(
//            jun.rztPiutangPerSales.data.items, "data")));
//        Ext.getCmp("form-Sales2AuditWin").getForm().standardSubmit = !0;
//        Ext.getCmp("form-Sales2AuditWin").getForm().url = "Report/Save2Audit";
//        this.format.setValue("excel");
//        var form = Ext.getCmp('form-Sales2AuditWin').getForm();
//        var el = form.getEl().dom;
//        var target = document.createAttribute("target");
//        target.nodeValue = "_blank";
//        el.setAttributeNode(target);
//        el.action = form.url;
//        el.submit();
    }
});

jun.Sales2AuditGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Sales2Audit",
    id: 'docs-jun.Sales2AuditGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        {
            header: 'No. Receipt',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100
        },
        {
            header: 'No. Customers',
            sortable: true,
            resizable: true,
            dataIndex: 'no_customer',
            width: 100
        },
        {
            header: 'Customers Name',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_customer',
            width: 100
        },
        {
            header: 'Pay Method',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_bank',
            width: 100
        },
        {
            header: 'Total',
            sortable: true,
            resizable: true,
            dataIndex: 'total',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
        {
            xtype: 'checkcolumn',
            header: 'To Audit',
            dataIndex: 'cetak',
            width: 55
        }
    ],
    initComponent: function () {
        this.store = jun.rztSales2Audit;
        jun.Sales2AuditGrid.superclass.initComponent.call(this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.on('mousedown', this.onClick, this);
    },
    onClick: function () {
        this.store.commitChanges();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    }
});
