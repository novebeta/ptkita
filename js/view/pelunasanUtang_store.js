jun.PelunasanUtangstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.PelunasanUtangstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'PelunasanUtangStoreId',
            url: 'PelunasanUtang',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'pelunasan_utang_id'},
                {name: 'total', type: 'float'},
                {name: 'no_bg_cek'},
                {name: 'no_bukti'},
                {name: 'doc_ref'},
                {name: 'tgl'},
                {name: 'bank_id'},
                {name: 'supplier_id'},
            ]
        }, cfg));
    }
});
jun.rztPelunasanUtang = new jun.PelunasanUtangstore();
//jun.rztPelunasanUtang.load();
