jun.Infostore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Infostore.superclass.constructor.call(this, Ext.apply({
            storeId: 'InfoStoreId',
            url: 'Info',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'info_id'},
                {name: 'info_name'},
                {name: 'up'}
            ]
        }, cfg));
    }
});
jun.rztInfo = new jun.Infostore();
jun.rztInfoLib = new jun.Infostore();
jun.rztInfoCmp = new jun.Infostore();
//jun.rztInfo.load();
