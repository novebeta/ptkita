jun.Retursalestransstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Retursalestransstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'RetursalestransstoreId',
            url: 'ReturSalestrans',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'salestrans_id'},
                {name: 'tgl'},
                {name: 'doc_ref'},
                {name: 'tdate'},
                {name: 'user_id'},
                {name: 'bruto'},
                {name: 'disc'},
                {name: 'discrp'},
                {name: 'totalpot'},
                {name: 'total'},
                {name: 'ketdisc'},
                {name: 'vat'},
                {name: 'customer_id'},
                {name: 'nama_customer'},
                {name: 'no_customer'},
                {name: 'doc_ref_sales'},
                {name: 'audit'},
                {name: 'store'},
                {name: 'printed'},
                {name: 'override'},
                {name: 'nama_customer'},
                {name: 'no_customer'},
                {name: 'bayar'},
                {name: 'kembali'},
                {name: 'total_discrp1'},
                {name: 'rounding'}
            ]
        }, cfg));
    }
});
jun.rztReturSalestrans = new jun.Retursalestransstore();