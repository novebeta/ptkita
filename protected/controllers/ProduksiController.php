<?php
Yii::import('application.components.PrintProduksi');
Yii::import('application.components.GL');
class ProduksiController extends GxController
{
    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            $this->redirect(url('/'));
        }
        if (isset($_POST) && !empty($_POST)) {
            $msg = "Stored data failed.";
            $detils = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $model = new Produksi;
                $ref = new Reference();
                $gl = new GL;
                $docref = $ref->get_next_reference(PRODUKSI);
                $_POST['store'] = STOREID;
                foreach ($_POST as $k => $v) {
                    if (is_angka($v)) {
                        $v = get_number($v);
                    }
                    $_POST['Produksi'][$k] = $v;
                }
                $_POST['Produksi']['arus'] = 1;
                $_POST['Produksi']['doc_ref'] = $docref;
                $model->attributes = $_POST['Produksi'];
                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Produksi')) . CHtml::errorSummary($model));
                }
                $total_price = 0;
                foreach ($detils as $detil) {
                    $produksi_details = new ProduksiDetil;
                    /* @var $barang Barang */
                    $barang = Barang::model()->findByPk($detil['barang_id']);
                    if ($barang == null) {
                        throw new Exception("Product not found.");
                    }
                    $_POST['ProduksiDetil']['qty'] = get_number($detil['qty']);
                    $_POST['ProduksiDetil']['price'] = $barang->get_cost($model->store);
                    $_POST['ProduksiDetil']['barang_id'] = $detil['barang_id'];
                    $_POST['ProduksiDetil']['total'] = round($_POST['ProduksiDetil']['qty'] * $_POST['ProduksiDetil']['price'],
                        2);
                    $_POST['ProduksiDetil']['produksi_id'] = $model->produksi_id;
                    $produksi_details->attributes = $_POST['ProduksiDetil'];
                    if (!$produksi_details->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => 'Produksi Detail')) . CHtml::errorSummary($produksi_details));
                    }
                    $total_price += $_POST['ProduksiDetil']['total'];
                    $saldo_stock = StockMoves::get_saldo_item($barang->barang_id, $model->store);
                    if ($saldo_stock < $produksi_details->qty) {
                        throw new Exception(t('saldo.item.fail',
                            'app', array(
                                '{item}' => $produksi_details->barang->kode_barang,
                                '{h}' => $saldo_stock,
                                '{r}' => $produksi_details->qty
                            )));
                    }
                    U::add_stock_moves(PRODUKSI, $model->produksi_id, $model->tgl,
                        $produksi_details->barang_id, -$produksi_details->qty, $model->doc_ref,
                        $produksi_details->price, $model->store);
                    $gl->add_gl(PRODUKSI, $model->produksi_id, $model->tgl, $docref,
                        $produksi_details->barang->tipeBarang->coa,
                        "Production " . $model->barang->nama_barang, "Production " . $model->barang->nama_barang,
                        -$produksi_details->total, 0, STOREID);
                }
                $model->total = $total_price;
                $model->price = round($model->total / $model->qty, 2);
                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Produksi')) . CHtml::errorSummary($model));
                }
                $gl->add_gl(PRODUKSI, $model->produksi_id, $model->tgl, $docref, $model->barang->tipeBarang->coa,
                    "Production " . $model->barang->nama_barang, "Production " . $model->barang->nama_barang,
                    $model->total, 0, STOREID);
                $gl->validate();
                $model->barang->count_biaya_beli($model->qty, $model->qty * $model->price, $model->store);
                U::add_stock_moves(PRODUKSI, $model->produksi_id, $model->tgl,
                    $model->barang_id, $model->qty, $model->doc_ref,
                    $model->price, $model->store);
                $ref->save(PRODUKSI, $model->produksi_id, $docref);
                $transaction->commit();
                $msg = t('save.success', 'app');
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }
    public function actionReturn()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            $this->redirect(url('/'));
        }
        if (isset($_POST) && !empty($_POST)) {
            $tgl = date('Y-m-d');
            /* @var $prod Produksi */
            $prod = Produksi::model()->findByPk($_POST['id']);
            if ($prod == null) {
                throw new Exception("Production transaction  not found.");
            }
            $prodExists = Produksi::model()->count('doc_ref_produksi = :doc_ref_produksi',
                array(':doc_ref_produksi' => $prod->doc_ref));
            if($prodExists > 0){
                throw new Exception("Production Return already created.");
            }
            $detils = $prod->produksiDetils;
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $model = new Produksi();
                $ref = new Reference();
                $gl = new GL;
                $docref = $ref->get_next_reference(RETURN_PRODUKSI);
                $model->store = $prod->store;
                $model->arus = -1;
                $model->doc_ref = $docref;
                $model->doc_ref_produksi = $prod->doc_ref;
                $model->tgl = $tgl;
                $model->barang_id = $prod->barang_id;
                $model->qty = -$prod->qty;
                $model->price = $prod->price;
                $model->pl = $prod->pl;
                $model->total = -$prod->total;
                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Produksi')) . CHtml::errorSummary($model));
                }
                foreach ($detils as $detil) {
                    $produksi_details = new ProduksiDetil;
                    $produksi_details->qty = $detil->qty;
                    $produksi_details->price = $detil->price;
                    $produksi_details->barang_id = $detil->barang_id;
                    $produksi_details->total = $detil->total;
                    $produksi_details->produksi_id = $model->produksi_id;
                    if (!$produksi_details->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => 'Produksi Detail')) . CHtml::errorSummary($produksi_details));
                    }
                    U::add_stock_moves(RETURN_PRODUKSI, $model->produksi_id, $model->tgl,
                        $produksi_details->barang_id, $produksi_details->qty, $model->doc_ref,
                        $produksi_details->price, $model->store);
                    $gl->add_gl(RETURN_PRODUKSI, $model->produksi_id, $model->tgl, $docref,
                        $produksi_details->barang->tipeBarang->coa,
                        "Production Return " . $model->barang->nama_barang,
                        "Production Return " . $model->barang->nama_barang,
                        $produksi_details->total, 0, STOREID);
                }
                $gl->add_gl(RETURN_PRODUKSI, $model->produksi_id, $model->tgl, $docref, $model->barang->tipeBarang->coa,
                    "Production Return " . $model->barang->nama_barang,
                    "Production Return " . $model->barang->nama_barang,
                    $model->total, 0, STOREID);
                $gl->validate();
                U::add_stock_moves(RETURN_PRODUKSI, $model->produksi_id, $model->tgl,
                    $model->barang_id, $model->qty, $model->doc_ref,
                    -$model->price, $model->store);
                $ref->save(RETURN_PRODUKSI, $model->produksi_id, $docref);
                $transaction->commit();
                $msg = t('save.success', 'app');
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'Produksi');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) {
                    $v = get_number($v);
                }
                $_POST['Produksi'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['Produksi'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->produksi_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->produksi_id));
            }
        }
    }
//    public function actionDelete($id)
//    {
//        if (Yii::app()->request->isPostRequest) {
//            $msg = 'Data berhasil dihapus.';
//            $status = true;
//            try {
//                $this->loadModel($id, 'Produksi')->delete();
//            } catch (Exception $ex) {
//                $status = false;
//                $msg = $ex;
//            }
//            echo CJSON::encode(array(
//                'success' => $status,
//                'msg' => $msg
//            ));
//            Yii::app()->end();
//        } else {
//            throw new CHttpException(400,
//                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
//        }
//    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        $criteria->addCondition("tgl = :tgl");
        $param = array(':tgl' => $_POST['tgl']);
        if (isset($_POST['arus'])) {
            $criteria->addCondition("arus = :arus");
            $param[':arus'] = $_POST['arus'];
            if ($_POST['arus'] == -1) {
                $criteria->select = "produksi_id,tgl,doc_ref,barang_id,-qty qty,
                price,tdate,up,store,user_id,pl,total,arus,doc_ref_produksi";
            }
        }
        $criteria->params = $param;
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $model = Produksi::model()->findAll($criteria);
        $total = Produksi::model()->count($criteria);
        $this->renderJson($model, $total);
    }
    public function actionPrint()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            $this->redirect(url('/'));
        }
        if (isset($_POST) && !empty($_POST)) {
            $sls = Produksi::model()->findByPk($_POST['id']);
            $prt = new PrintProduksi($sls);
            echo CJSON::encode(array(
                'success' => $sls != null,
                'msg' => $prt->buildTxt()
            ));
            Yii::app()->end();
        }
    }
    public function actionPrintreturn()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            $this->redirect(url('/'));
        }
        if (isset($_POST) && !empty($_POST)) {
            $sls = Produksi::model()->findByPk($_POST['id']);
            $prt = new PrintProduksi($sls);
            echo CJSON::encode(array(
                'success' => $sls != null,
                'msg' => $prt->buildreturnTxt()
            ));
            Yii::app()->end();
        }
    }
}