<?php
class SalestransDetailsController extends GxController
{
    public function actionCreate()
    {
        $model = new SalestransDetails;
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['SalestransDetails'][$k] = $v;
            }
            $model->attributes = $_POST['SalestransDetails'];
            $msg = "Data gagal disimpan.";
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->salestrans_details;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdate()
    {
        if (Yii::app()->request->isPostRequest) {
            $status = false;
            $msg = 'Failed update data.';
            if (isset($_POST) && !empty($_POST)) {
                $id = $_POST['salestrans_details'];
                $dokter_id = $_POST['dokter_id'];
                $beauty_id = $_POST['beauty_id'];
                $beauty2_id = $_POST['beauty2_id'];
                $jasa_dokter = get_number($_POST['jasa_dokter']);
                $model = $this->loadModel($id, 'SalestransDetails');
                $model->dokter_id = $dokter_id;
                $model->jasa_dokter = $jasa_dokter;
                app()->db->autoCommit = false;
                $transaction = Yii::app()->db->beginTransaction();
                try {
                    if (!$model->save()) {
                        throw new Exception(CHtml::errorSummary($model));
                    } else {
                        $model->save_beauty_tips($beauty_id, $beauty2_id);
                        $msg = 'Success update.';
                    }
                    $transaction->commit();
                    $status = true;
                } catch (Exception $ex) {
                    $transaction->rollback();
                    $status = false;
                    $msg = $ex->getMessage();
                }
                app()->db->autoCommit = true;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(403,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
    public function actionFinal()
    {
        if (Yii::app()->request->isPostRequest) {
            $tgl = $_POST['tgl'];
            $arr = Salestrans::set_final_service($tgl);
            echo CJSON::encode(array(
                'success' => $arr > 0,
                'msg' => "Final affected $arr row"));
            Yii::app()->end();
        } else
            throw new CHttpException(403,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
    public function actionGetDiskon()
    {
        if (Yii::app()->request->isPostRequest) {
            $customer_id = $_POST['customer_id'];
            $barang_id = $_POST['barang_id'];
            $arr = SalestransDetails::get_diskon($customer_id, $barang_id);
            $jual = Jual::model()->findByAttributes(array('barang_id' => $barang_id, 'store' => STOREID));
            if (!$arr) {
                $arr['value'] = "0";
                $arr['nama_status'] = "";
            }
            if ($jual == null) {
                $arr['price'] = '0';
            } else {
                $arr['price'] = $jual->price;
            }
            echo CJSON::encode(array(
                'success' => true,
                'msg' => $arr));
            Yii::app()->end();
        } else
            throw new CHttpException(403,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
    public function actionIndex()
    {
        if (Yii::app()->request->isPostRequest) {
            $this->renderJsonArr(SalestransDetails::get_sales_trans_details($_POST['salestrans_id']));
        } else
            throw new CHttpException(403,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
}