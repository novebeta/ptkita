<?php
Yii::import('application.components.GL');
class GlTransController extends GxController
{
    public function actionCreate()
    {
        if (!app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            $msg = t('save.success', 'app');
            $detils = CJSON::decode($_POST['detil']);
            $is_new = $_POST['mode'] == 0;
            app()->db->autoCommit = false;
            $transaction = app()->db->beginTransaction();
            try {
                $gl = new GL();
                if ($is_new) {
                    $ref = new Reference;
                    $docref = $ref->get_next_reference(JURNAL_UMUM);
                    $command = Yii::app()->db->createCommand("SELECT UUID();");
                    $uuid = $command->queryScalar();
                    $jurnal_umum_id = $uuid;
                } else {
                    $docref = Reference::get_reference(JURNAL_UMUM, $_POST['id']);
                    $this->delete_gl_trans(JURNAL_UMUM, $_POST['id']);
                    $jurnal_umum_id = $_POST['id'];
                }
                foreach ($detils as $detil) {
                    $amount = $detil['debit'] > 0 ? $detil['debit'] : -$detil['kredit'];
                    $gl->add_gl(JURNAL_UMUM, $jurnal_umum_id, $_POST['tran_date'], $docref,
                        $detil['account_code'], $detil["memo_"], '', $amount, 0, $_POST['store']);
                }
                $gl->validate();
                if ($is_new) {
                    $ref->save(JURNAL_UMUM, $jurnal_umum_id, $docref);
                }
                $transaction->commit();
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'id' => $docref,
                'msg' => $msg));
            app()->end();
        }
    }

    public function actionIndex()
    {
        if (!Yii::app()->request->isAjaxRequest)
            $this->redirect(url('/'));
        if (isset($_POST) && !empty($_POST)) {
            $this->renderJsonArr(GlTrans::jurnal_umum_details($_POST['type_no']));
        }
//        if (isset($_POST['limit'])) {
//            $limit = $_POST['limit'];
//        } else {
//            $limit = 20;
//        }
//        if (isset($_POST['start'])) {
//            $start = $_POST['start'];
//        } else {
//            $start = 0;
//        }
//        $criteria = new CDbCriteria();
//        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
//            (isset($_POST['limit']) && isset($_POST['start']))
//        ) {
//            $criteria->limit = $limit;
//            $criteria->offset = $start;
//        }
//        $model = GlTrans::model()->findAll($criteria);
//        $total = GlTrans::model()->count($criteria);
//        $this->renderJson($model, $total);
    }

    public function actionListJurnalUmum()
    {
        $this->renderJsonArr(GlTrans::jurnal_umum_index($_POST['tgl']));
    }

    public function actionDelete()
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $this->delete_gl_trans(JURNAL_UMUM, $_POST['type_no']);
//                $this->delete_refs(JURNAL_UMUM, $_POST['type_no']);
                $transaction->commit();
                $status = true;
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg)
            );
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
}