<?php
class AbsenTransController extends GxController
{
    public function actionCreate()
    {
        $model = new AbsenTrans;
        if (!Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) {
                    $v = get_number($v);
                }
                $_POST['AbsenTrans'][$k] = $v;
            }
            $model->attributes = $_POST['AbsenTrans'];
            $msg = "Data gagal disimpan.";
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->absen_trans_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'AbsenTrans');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) {
                    $v = get_number($v);
                }
                $_POST['AbsenTrans'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['AbsenTrans'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->absen_trans_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->absen_trans_id));
            }
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'AbsenTrans')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        } else {
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
        }
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $model = AbsenTrans::model()->findAll($criteria);
        $total = AbsenTrans::model()->count($criteria);
        $this->renderJson($model, $total);
    }
    public function actionInit()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            return;
        }
        app()->db->autoCommit = false;
        $transaction = Yii::app()->db->beginTransaction();
        try {
            $tgl = date("Y-01-01");
            AbsenTrans::model()->deleteAll("tgl = :tgl AND jml > 0", array(":tgl" => $tgl));
            /* @var $tipes TipeAbsen[] */
            $tipes = TipeAbsen::model()->findAll();
            /* @var $employees Employee[] */
            $employees = Employee::model()->findAll();
            foreach ($tipes as $tipe) {
                foreach ($employees as $empoyee) {
                    $transAbsen = new AbsenTrans();
                    $transAbsen->employee_id = $empoyee->employee_id;
                    $transAbsen->tipe_absen_id = $tipe->tipe_absen_id;
                    $transAbsen->jml = $tipe->total_off_yearly;
                    $transAbsen->remarks = "";
                    $transAbsen->tgl = $tgl;
                    if (!$transAbsen->save()) {
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Sales')) . CHtml::errorSummary($transAbsen));
                    }
                }
            }
            $msg = t('save.success', 'app');
            $status = true;
        } catch (Exception $ex) {
            $transaction->rollback();
            $status = false;
            $msg = $ex->getMessage();
        }
        app()->db->autoCommit = true;
        echo CJSON::encode(array(
            'success' => $status,
            'msg' => $msg
        ));
        Yii::app()->end();
    }
}