<?php
class MsdController extends GxController
{
    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            $this->redirect(url('/'));
        }
        if (isset($_POST) && !empty($_POST)) {
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $tahun = DateTime::createFromFormat('Y-m-d', $_POST['tgl'])->format('Y');
                $criteria = new CDbCriteria();
                $criteria->addCondition("YEAR(tgl) = :tahun");
                $criteria->addCondition("customer_id = :customer_id");
                $criteria->params = array(':tahun' => $tahun, ':customer_id' => $_POST['customer_id']);
                $count = (float)Msd::model()->count($criteria);
                if ($count > 0) {
                    throw new Exception("My Special Day data already exists!");
                }
                $model = new Msd;
                $ref = new Reference();
                $docref = $ref->get_next_reference(MSD);
                foreach ($_POST as $k => $v) {
                    if (is_angka($v)) {
                        $v = get_number($v);
                    }
                    $_POST['Msd'][$k] = $v;
                }
                $_POST['Msd']['store'] = STOREID;
                $_POST['Msd']['doc_ref'] = $docref;
                $model->attributes = $_POST['Msd'];
                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'MSD')) . CHtml::errorSummary($model));
                }
                /* @var $sales_trans Salestrans */
                $sales_trans = Salestrans::model()->findByPk($model->salestrans_id);
                if ($sales_trans == null) {
                    throw new Exception("Sales transaction  not found.");
                }
                /* @var $cust_new Customers */
                $cust_new = Customers::model()->findByPk($model->customer_id);
                if ($cust_new == null) {
                    throw new Exception("Customer not found.");
                }
                $lahir_date = DateTime::createFromFormat('Y-m-d', $cust_new->tgl_lahir)->format('m');
                $trans_date = DateTime::createFromFormat('Y-m-d', $sales_trans->tgl)->format('m');
                if ($lahir_date != $trans_date) {
                    throw new Exception("The birthday month and the month of the transaction is different");
                }
                $ref->save(MSD, $model->msd_id, $docref);
                $msg = t('save.success', 'app');
                $transaction->commit();
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'Msd');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) {
                    $v = get_number($v);
                }
                $_POST['Msd'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['Msd'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->msd_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->msd_id));
            }
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'Msd')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        } else {
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
        }
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $param = array();
        $criteria = new CDbCriteria();
        if (isset($_POST['tgl'])) {
            $criteria->addCondition('tgl = :tgl');
            $param[':tgl'] = $_POST['tgl'];
        }
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $criteria->params = $param;
        $model = MsdView::model()->findAll($criteria);
        $total = MsdView::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}