<?php
class PromoGenericController extends GxController
{
    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            $this->redirect(url('/'));
        }
        if (isset($_POST) && !empty($_POST)) {
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $model = new PromoGeneric;
                $ref = new Reference();
                $type = $_POST['arus'] == 1 ? POINT_IN : POINT_OUT;
                $docref = $ref->get_next_reference($type);
                foreach ($_POST as $k => $v) {
                    if (is_angka($v)) {
                        $v = get_number($v);
                    }
                    $_POST['PromoGeneric'][$k] = $v;
                }
                if($_POST['arus'] == -1){
                    $_POST['PromoGeneric']['qty'] = -$_POST['PromoGeneric']['qty'];
                }
                $_POST['PromoGeneric']['doc_ref'] = $docref;
                $_POST['PromoGeneric']['store'] = STOREID;
                $model->attributes = $_POST['PromoGeneric'];
                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Promo Generic')) . CHtml::errorSummary($model));
                }
                PointTrans::save_point_trans($type,$model->promo_generic_id,$model->doc_ref,
                    $model->tgl,$model->qty,$model->customer_id,$model->event_id,$model->store);
                $point_total = PointTrans::total_point($model->customer_id,$model->event_id);
                if($point_total < 0){
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Cash')) .
                        "Insufficient point, need " .abs($point_total). " more point");
                }
                $ref->save($type, $model->promo_generic_id, $docref);
                $msg = t('save.success', 'app');
                $transaction->commit();
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'PromoGeneric');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) {
                    $v = get_number($v);
                }
                $_POST['PromoGeneric'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['PromoGeneric'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->promo_generic_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->promo_generic_id));
            }
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'PromoGeneric')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        } else {
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
        }
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        if (isset($_POST[':tgl'])) {
            $criteria->addCondition('tgl = :tgl');
            $criteria->params = array(':tgl' => $_POST['tgl']);
        }
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        if ($_POST['qty'] == '< 0') {
            $model = PointOut::model()->findAll($criteria);
            $total = PointOut::model()->count($criteria);
        } else {
            $model = PointIn::model()->findAll($criteria);
            $total = PointIn::model()->count($criteria);
        }
        $this->renderJson($model, $total);
    }
}