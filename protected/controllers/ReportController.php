<?php
Yii::import('application.components.U');
Yii::import("application.components.tbs_class_php5", true);
Yii::import("application.components.tbs_plugin_excel", true);
Yii::import("application.components.tbs_plugin_opentbs", true);
Yii::import("application.components.tbs_plugin_aggregate", true);
class ReportController extends GxController
{
    private $TBS;
    private $logo;
    private $format;
    public $is_excel;
    public function init()
    {
        parent::init();
        if (!isset($_POST) || empty($_POST)) {
            $this->redirect(url('/'));
        }
        $this->layout = "report";
        $this->logo = bu() . "/images/logo.png";
        $this->TBS = new clsTinyButStrong;
        $this->format = $_POST['format'];
        if ($this->format == 'excel') {
            $this->TBS->PlugIn(TBS_INSTALL, TBS_EXCEL);
            $this->is_excel = true;
        } elseif ($this->format == 'openOffice') {
            $this->TBS->PlugIn(TBS_INSTALL, OPENTBS_PLUGIN);
        }
        error_reporting(E_ALL & ~E_NOTICE);
    }
    private function create_laba_rugi($from, $to, $store = "")
    {
        $lr = ChartMaster::get_laba_rugi($from, $to, $store);
        $jual_sales = $jual_agen = $hpp = $beban_administrasi = $beban_umum = $beban_pemasaran
            = $pendapatan_lain = $beban_lain = $pajak = array();
        $total_sales = $total_agen = $total_hpp = $total_beban_administrasi = $total_beban_umum
            = $total_beban_pemasaran = $total_pendapatan_lain = $total_beban_lain
            = $total_pajak = 0;
        $total_sales_account_code = $total_sales_account_name = $total_hpp_account_code = $total_hpp_account_name =
        $beban_administrasi_account_code = $beban_administrasi_account_name = $beban_umum_account_code =
        $beban_umum_account_name = $beban_tax_account_code = $beban_tax_account_name =
        $pendapatan_lain_account_code = $pendapatan_lain_account_name = $beban_adv_account_code =
        $beban_adv_account_name = "";
        foreach ($lr as $chart) {
            if (preg_match('/^4.*/', $chart['account_code'])) {
                if ($chart['account_code'] == '499999') {
                    $total_sales_account_code = $chart['account_code'];
                    $total_sales_account_name = $chart['account_name'];
                } elseif ($chart['account_code'] == '400000') {
                    $chart['total'] = null;
                    $jual_sales[] = $chart;
                } else {
                    $chart['total'] = -$chart['total'];
                    $total_sales += $chart['total'];
                    $jual_sales[] = $chart;
                }
            } elseif (preg_match('/^5.*/', $chart['account_code'])) {
                if ($chart['account_code'] == '529999') {
                    $total_hpp_account_code = $chart['account_code'];
                    $total_hpp_account_name = $chart['account_name'];
                } elseif ($chart['account_code'] == '521000') {
                    $chart['total'] = null;
                    $hpp[] = $chart;
                } else {
                    $total_hpp += $chart['total'];
                    $hpp[] = $chart;
                }
            } elseif (preg_match('/^611.*/', $chart['account_code'])) {
                if ($chart['account_code'] == '611999') {
                    $beban_administrasi_account_code = $chart['account_code'];
                    $beban_administrasi_account_name = $chart['account_name'];
                } elseif ($chart['account_code'] == '611000') {
                    $chart['total'] = null;
                    $beban_administrasi[] = $chart;
                } else {
                    $total_beban_administrasi += $chart['total'];
                    $beban_administrasi[] = $chart;
                }
            } elseif (preg_match('/^612.*/', $chart['account_code'])) {
                if ($chart['account_code'] == '612999') {
                    $beban_umum_account_code = $chart['account_code'];
                    $beban_umum_account_name = $chart['account_name'];
                } elseif ($chart['account_code'] == '612000') {
                    $chart['total'] = null;
                    $beban_umum[] = $chart;
                } else {
                    $total_beban_umum += $chart['total'];
                    $beban_umum[] = $chart;
                }
            } elseif (preg_match('/^613.*/', $chart['account_code'])) {
                if ($chart['account_code'] == '613999') {
                    $beban_adv_account_code = $chart['account_code'];
                    $beban_adv_account_name = $chart['account_name'];
                } elseif ($chart['account_code'] == '613000') {
                    $chart['total'] = null;
                    $beban_pemasaran[] = $chart;
                } else {
                    $total_beban_pemasaran += $chart['total'];
                    $beban_pemasaran[] = $chart;
                }
            } elseif (preg_match('/^614.*/', $chart['account_code'])) {
                if ($chart['account_code'] == '614999') {
                    $beban_tax_account_code = $chart['account_code'];
                    $beban_tax_account_name = $chart['account_name'];
                } elseif ($chart['account_code'] == '614000') {
                    $chart['total'] = null;
                    $pajak[] = $chart;
                } else {
                    $total_pajak += $chart['total'];
                    $pajak[] = $chart;
                }
            } elseif (preg_match('/^7.*/', $chart['account_code'])) {
                if ($chart['account_code'] == '799999') {
                    $pendapatan_lain_account_code = $chart['account_code'];
                    $pendapatan_lain_account_name = $chart['account_name'];
                } elseif ($chart['account_code'] == '700000') {
                    $chart['total'] = null;
                    $pendapatan_lain[] = $chart;
                } else {
                    $chart['total'] = -$chart['total'];
                    $total_pendapatan_lain += $chart['total'];
                    $pendapatan_lain[] = $chart;
                }
            }
        }
        $laba_kotor = $total_sales - $total_hpp;
        $total_beban_operasional = $total_beban_administrasi + $total_beban_umum +
            $total_beban_pemasaran + $total_pajak;
        $laba_operasional = $laba_kotor - $total_beban_operasional;
        $laba_bersih = $total_pendapatan_lain + $laba_operasional;
        return array(
            'header' => array(
                array(
                    'from' => $from,
                    'to' => $to,
                    'total_sales' => $total_sales,
                    'total_sales_account_code' => $total_sales_account_code,
                    'total_sales_account_name' => $total_sales_account_name,
                    'total_hpp_account_code' => $total_hpp_account_code,
                    'total_hpp_account_name' => $total_hpp_account_name,
                    'beban_administrasi_account_code' => $beban_administrasi_account_code,
                    'beban_administrasi_account_name' => $beban_administrasi_account_name,
                    'total_beban_administrasi' => $total_beban_administrasi,
                    'beban_umum_account_code' => $beban_umum_account_code,
                    'beban_umum_account_name' => $beban_umum_account_name,
                    'total_beban_umum' => $total_beban_umum,
                    'beban_adv_account_code' => $beban_adv_account_code,
                    'beban_adv_account_name' => $beban_adv_account_name,
                    'total_beban_adv' => $total_beban_pemasaran,
                    'beban_tax_account_code' => $beban_tax_account_code,
                    'beban_tax_account_name' => $beban_tax_account_name,
                    'total_hpp' => $total_hpp,
                    'laba_kotor' => $laba_kotor,
                    'pendapatan_lain_account_code' => $pendapatan_lain_account_code,
                    'pendapatan_lain_account_name' => $pendapatan_lain_account_name,
                    'total_pendapatan_lain' => $total_pendapatan_lain,
                    'total_beban_pemasaran' => $total_beban_pemasaran,
                    'total_beban_operasional' => $total_beban_operasional,
                    'laba_operasional' => $laba_operasional,
                    'laba_bersih' => $laba_bersih,
                    'total_beban_tax' => $total_pajak
                )
            ),
            'sales' => $jual_sales,
            'hpp' => $hpp,
            'beban_administrasi' => $beban_administrasi,
            'beban_umum' => $beban_umum,
            'beban_tax' => $pajak,
            'beban_adv' => $beban_pemasaran,
            'pendapatan_lain' => $pendapatan_lain
        );
    }
    public function actionGenerateLabaRugi()
    {
        $month = $_POST['month'];
        $year = $_POST['year'];
        $store = $_POST['store'];
        $d = new DateTime("$year-$month-01");
        $from = $d->format('Y-m-d');
        $to = $d->format('Y-m-t');
        try {
            $result = self::create_laba_rugi($from, $to, $store);
            $amount = $result['header'][0]['laba_bersih'];
            $amount = -$amount;
            $gl = GlTrans::model()->find('type = :type AND tran_date >= :from AND tran_date <= :to AND store = :store',
                array(':type' => LABARUGI, ':from' => $from, ':to' => $to, ':store' => $store));
            if ($gl == null) {
                $id = U::get_next_trans_no_bank_trans(LABARUGI, $store);
                Yii::import('application.components.GL');
                $gl = new GL();
                $gl->add_gl(LABARUGI, $id, $to, null, COA_LABA_RUGI, 'PROFIT LOST', '', $amount, 0, $store);
                $gl->validate();
            } else {
                $gl->amount = $amount;
                if (!$gl->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'GL Trans')) . CHtml::errorSummary($gl));
                }
            }
            $msg = 'Profit Lost succesfully generated.';
            $status = true;
        } catch (Exception $ex) {
            $status = false;
            $msg = $ex->getMessage();
        }
        echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            )
        );
        Yii::app()->end();
    }
    public function actionLabaRugi()
    {
        $from = $_POST['from_date'];
        $to = $_POST['to_date'];
        $result = self::create_laba_rugi($from, $to);
        $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'LabaRugi.xml');
        $this->TBS->MergeBlock('header', $result['header']);
        $this->TBS->MergeBlock('sales', $result['sales']);
        $this->TBS->MergeBlock('hpp', $result['hpp']);
        $this->TBS->MergeBlock('beban_administrasi', $result['beban_administrasi']);
        $this->TBS->MergeBlock('beban_umum', $result['beban_umum']);
        $this->TBS->MergeBlock('beban_tax', $result['beban_tax']);
        $this->TBS->MergeBlock('beban_adv', $result['beban_adv']);
        $this->TBS->MergeBlock('pendapatan_lain', $result['pendapatan_lain']);
        $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "LabaRugi" . $from . $to . ".xls");
    }
    public function actionNeraca()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $to = $_POST['to_date'];
//            $dt = strtotime($_POST['to_date']);
            $dt_frm = date_create($to);
            $from = date_format($dt_frm, 'Y-01-01');
            $lr = ChartMaster::get_neraca($from, $to);
            $current_assets = $fixed_assets = $current_liability = $longterm_liability = $equity = 0;
            $arr_current_assets = $arr_fixed_assets = $arr_current_liability = $arr_longterm_liability = $arr_equity = array();
            foreach ($lr as $chart) {
                if (preg_match('/^1[0-1][0-1].*/', $chart['account_code'])) {
                    if ($chart['account_code'] == '111999' || $chart['header'] == 1) {
                    } else {
                        $current_assets += $chart['after'];
                        $arr_current_assets[] = $chart;
                    }
                } elseif (preg_match('/^112.*/', $chart['account_code'])) {
                    if ($chart['account_code'] == '112999' || $chart['header'] == 1) {
                    } else {
                        $current_assets += $chart['after'];
                        $arr_current_assets[] = $chart;
                    }
                } elseif (preg_match('/^113.*/', $chart['account_code'])) {
                    if ($chart['account_code'] == '113999' || $chart['header'] == 1) {
                    } else {
                        $current_assets += $chart['after'];
                        $arr_current_assets[] = $chart;
                    }
                } elseif (preg_match('/^114.*/', $chart['account_code'])) {
                    if ($chart['account_code'] == '114199' || $chart['header'] == 1) {
                    } else {
                        $current_assets += $chart['after'];
                        $arr_current_assets[] = $chart;
                    }
                } elseif (preg_match('/^115.*/', $chart['account_code'])) {
                    if ($chart['account_code'] == '115999' || $chart['header'] == 1) {
                    } else {
                        $current_assets += $chart['after'];
                        $arr_current_assets[] = $chart;
                    }
                } elseif (preg_match('/^116.*/', $chart['account_code'])) {
                    if ($chart['account_code'] == '116999' || $chart['header'] == 1) {
                    } else {
                        $current_assets += $chart['after'];
                        $arr_current_assets[] = $chart;
                    }
                } elseif (preg_match('/^12.*/', $chart['account_code'])) {
                    if ($chart['account_code'] == '129999' || $chart['header'] == 1) {
                    } else {
                        $fixed_assets += $chart['after'];
                        $arr_fixed_assets[] = $chart;
                    }
                } elseif (preg_match('/^2[0-1][0-1].*/', $chart['account_code'])) {
                    if ($chart['account_code'] == '211999' || $chart['header'] == 1) {
                    } else {
                        $chart['after'] = -$chart['after'];
                        $current_liability += $chart['after'];
                        $arr_current_liability[] = $chart;
                    }
                } elseif (preg_match('/^212.*/', $chart['account_code'])) {
                    if ($chart['account_code'] == '212999' || $chart['header'] == 1) {
                    } else {
                        $chart['after'] = -$chart['after'];
                        $current_liability += $chart['after'];
                        $arr_current_liability[] = $chart;
                    }
                } elseif (preg_match('/^2[2-9].*/', $chart['account_code'])) {
                    if ($chart['account_code'] == '229999' || $chart['account_code'] == '299999'
                        || $chart['header'] == 1
                    ) {
                    } else {
                        $chart['after'] = -$chart['after'];
                        $longterm_liability += $chart['after'];
                        $arr_longterm_liability[] = $chart;
                    }
                } elseif (preg_match('/^3.*/', $chart['account_code'])) {
                    if ($chart['account_code'] == '399999' || $chart['header'] == 1) {
                    } else {
                        $chart['after'] = -$chart['after'];
                        $equity += $chart['after'];
                        $arr_equity[] = $chart;
                    }
                }
            }
            $count_assets = count($arr_current_assets);
            $count_current_liability = count($arr_current_liability);
            $count_fixed_assets = count($arr_fixed_assets);
            $count_longterm_liability = count($arr_longterm_liability);
            if ($count_assets >= $count_current_liability) {
                for ($i = 0; $i <= $count_assets; $i++) {
                    $arr_current_assets[$i]['account_code_assets'] = $arr_current_assets[$i]['account_code'];
                    $arr_current_assets[$i]['account_assets'] = $arr_current_assets[$i]['account_name'];
                    $arr_current_assets[$i]['assets'] = $arr_current_assets[$i]['after'];
                    if ($i > $count_current_liability) {
                        $arr_current_assets[$i]['account_code'] = null;
                        $arr_current_assets[$i]['account_liabilities'] = null;
                        $arr_current_assets[$i]['liabilities'] = null;
                    } else {
                        $arr_current_assets[$i]['account_code'] = $arr_current_liability[$i]['account_code'];
                        $arr_current_assets[$i]['account_liabilities'] = $arr_current_liability[$i]['account_name'];
                        $arr_current_assets[$i]['liabilities'] = $arr_current_liability[$i]['after'];
                    }
                }
            } else {
                for ($i = 0; $i <= $count_current_liability; $i++) {
                    $arr_current_assets[$i]['account_code_temp'] = $arr_current_liability[$i]['account_code'];
                    $arr_current_assets[$i]['account_liabilities'] = $arr_current_liability[$i]['account_name'];
                    $arr_current_assets[$i]['liabilities'] = $arr_current_liability[$i]['after'];
                    if ($i > $count_assets) {
                        $arr_current_assets[$i]['account_code_assets'] = null;
                        $arr_current_assets[$i]['account_assets'] = null;
                        $arr_current_assets[$i]['assets'] = null;
                    } else {
                        $arr_current_assets[$i]['account_code_assets'] = $arr_current_assets[$i]['account_code'];
                        $arr_current_assets[$i]['account_assets'] = $arr_current_assets[$i]['account_name'];
                        $arr_current_assets[$i]['assets'] = $arr_current_assets[$i]['after'];
                    }
                    $arr_current_assets[$i]['account_code'] = $arr_current_assets[$i]['account_code_temp'];
                }
            }
            if ($count_fixed_assets >= $count_longterm_liability) {
                for ($i = 0; $i <= $count_fixed_assets; $i++) {
                    $arr_fixed_assets[$i]['account_code_assets'] = $arr_fixed_assets[$i]['account_code'];
                    $arr_fixed_assets[$i]['account_assets'] = $arr_fixed_assets[$i]['account_name'];
                    $arr_fixed_assets[$i]['assets'] = $arr_fixed_assets[$i]['after'];
                    if ($i > $count_longterm_liability) {
                        $arr_fixed_assets[$i]['account_code'] = null;
                        $arr_fixed_assets[$i]['account_liabilities'] = null;
                        $arr_fixed_assets[$i]['liabilities'] = null;
                    } else {
                        $arr_fixed_assets[$i]['account_code'] = $arr_longterm_liability[$i]['account_code'];
                        $arr_fixed_assets[$i]['account_liabilities'] = $arr_longterm_liability[$i]['account_name'];
                        $arr_fixed_assets[$i]['liabilities'] = $arr_longterm_liability[$i]['after'];
                    }
                }
            } else {
                for ($i = 0; $i <= $count_longterm_liability; $i++) {
                    $arr_fixed_assets[$i]['account_code_temp'] = $arr_longterm_liability[$i]['account_code'];
                    $arr_fixed_assets[$i]['account_liabilities'] = $arr_longterm_liability[$i]['account_name'];
                    $arr_fixed_assets[$i]['liabilities'] = $arr_longterm_liability[$i]['after'];
                    if ($i > $count_fixed_assets) {
                        $arr_fixed_assets[$i]['account_code_assets'] = null;
                        $arr_fixed_assets[$i]['account_assets'] = null;
                        $arr_fixed_assets[$i]['assets'] = null;
                    } else {
                        $arr_fixed_assets[$i]['account_code_assets'] = $arr_fixed_assets[$i]['account_code'];
                        $arr_fixed_assets[$i]['account_assets'] = $arr_fixed_assets[$i]['account_name'];
                        $arr_fixed_assets[$i]['assets'] = $arr_fixed_assets[$i]['after'];
                    }
                    $arr_fixed_assets[$i]['account_code'] = $arr_fixed_assets[$i]['account_code_temp'];
                }
            }
            $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'Neraca.xlsx',
                OPENTBS_ALREADY_UTF8);
            $this->TBS->MergeField('header', array(
                'to' => date_format($dt_frm, 'd F Y'),
                'current_assets' => $current_assets,
                'current_liabilities' => $current_liability,
                'longterm_liabilities' => $longterm_liability,
                'fixed_assets' => $fixed_assets,
                'equity' => $equity,
                'activa' => $current_assets + $fixed_assets,
                'passiva' => $current_liability + $longterm_liability + $equity
            ));
            $this->TBS->MergeBlock('assets_liabilities', $arr_current_assets);
            $this->TBS->MergeBlock('assets_liabilities_fixed', $arr_fixed_assets);
            $this->TBS->MergeBlock('equity', $arr_equity);
            $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "Neraca" . $to . ".xlsx");
        }
    }
    public function actionBalanceSheet()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['from_date'];
            $to = $_POST['to_date'];
            $store = $_POST['store'];
            $lr = ChartMaster::get_neraca($from, $to, $store);
            $assets = $bank = $tr = $beban_administrasi = $supplies = $ptax
                = $fassets = $beban_lain = $pexpenses = $liability = $taxpay = $equity = $ltliability = array();
            $total_assets = $total_bank = $total_tr = $total_beban_administrasi = $total_supplies
                = $total_ptax = $total_fassets = $total_beban_lain = $total_pexpenses = $total_liability
                = $total_taxpay = $total_equity = $total_ltliability = 0;
            $debit_assets = $debit_bank = $debit_tr = $debit_beban_administrasi = $debit_supplies
                = $debit_ptax = $debit_fassets = $debit_beban_lain = $debit_pexpenses = $debit_liability
                = $debit_taxpay = $debit_equity = $debit_ltliability = 0;
            $kredit_assets = $kredit_bank = $kredit_tr = $kredit_beban_administrasi = $kredit_supplies
                = $kredit_ptax = $kredit_fassets = $kredit_beban_lain = $kredit_pexpenses = $kredit_liability
                = $kredit_taxpay = $kredit_equity = $kredit_ltliability = 0;
            $after_assets = $after_bank = $after_tr = $after_beban_administrasi = $after_supplies
                = $after_ptax = $after_fassets = $after_beban_lain = $after_pexpenses = $after_liability
                = $after_taxpay = $after_equity = $after_ltliability = 0;
            $total_cash_account_code = $total_cash_account_name = $total_bank_account_code = $total_bank_account_name =
            $total_tr_account_code = $total_tr_account_name = $total_supplies_account_code =
            $total_supplies_account_name = $total_pexpenses_account_code = $total_pexpenses_account_name =
            $total_fassets_account_code = $total_fassets_account_name = $total_ptax_account_code = $total_ptax_account_name =
            $total_crliability_account_code = $total_crliability_account_name = $total_liability_account_code =
            $total_liability_account_name = $total_taxpay_account_code = $total_taxpay_account_name =
            $total_equity_account_code = $total_equity_account_name = $total_ltliability_account_code =
            $total_ltliability_account_name = "";
            foreach ($lr as $chart) {
                if (preg_match('/^1[0-1][0-1].*/', $chart['account_code'])) {
                    if ($chart['account_code'] == '111999') {
                        $total_cash_account_code = $chart['account_code'];
                        $total_cash_account_name = $chart['account_name'];
                    } elseif ($chart['header'] == 1) {
                        $chart['before'] = $chart['debit'] = $chart['kredit'] = $chart['after'] = null;
                        $assets[] = $chart;
                    } else {
                        $total_assets += $chart['before'];
                        $debit_assets += $chart['debit'];
                        $kredit_assets += $chart['kredit'];
                        $after_assets += $chart['after'];
                        $assets[] = $chart;
                    }
                } elseif (preg_match('/^112.*/', $chart['account_code'])) {
                    if ($chart['account_code'] == '112999') {
                        $total_bank_account_code = $chart['account_code'];
                        $total_bank_account_name = $chart['account_name'];
                    } elseif ($chart['header'] == 1) {
                        $chart['before'] = $chart['debit'] = $chart['kredit'] = $chart['after'] = null;
                        $bank[] = $chart;
                    } else {
                        $total_bank += $chart['before'];
                        $debit_bank += $chart['debit'];
                        $kredit_bank += $chart['kredit'];
                        $after_bank += $chart['after'];
                        $bank[] = $chart;
                    }
                } elseif (preg_match('/^113.*/', $chart['account_code'])) {
                    if ($chart['account_code'] == '113999') {
                        $total_tr_account_code = $chart['account_code'];
                        $total_tr_account_name = $chart['account_name'];
                    } elseif ($chart['header'] == 1) {
                        $chart['before'] = $chart['debit'] = $chart['kredit'] = $chart['after'] = null;
                        $tr[] = $chart;
                    } else {
                        $total_tr += $chart['before'];
                        $debit_tr += $chart['debit'];
                        $kredit_tr += $chart['kredit'];
                        $after_tr += $chart['after'];
                        $tr[] = $chart;
                    }
                } elseif (preg_match('/^114.*/', $chart['account_code'])) {
                    if ($chart['account_code'] == '114199') {
                        $total_supplies_account_code = $chart['account_code'];
                        $total_supplies_account_name = $chart['account_name'];
                    } elseif ($chart['header'] == 1) {
                        $chart['before'] = $chart['debit'] = $chart['kredit'] = $chart['after'] = null;
                        $supplies[] = $chart;
                    } else {
                        $total_supplies += $chart['before'];
                        $debit_supplies += $chart['debit'];
                        $kredit_supplies += $chart['kredit'];
                        $after_supplies += $chart['after'];
                        $supplies[] = $chart;
                    }
                } elseif (preg_match('/^115.*/', $chart['account_code'])) {
                    if ($chart['account_code'] == '115999') {
                        $total_ptax_account_code = $chart['account_code'];
                        $total_ptax_account_name = $chart['account_name'];
                    } elseif ($chart['header'] == 1) {
                        $chart['before'] = $chart['debit'] = $chart['kredit'] = $chart['after'] = null;
                        $ptax[] = $chart;
                    } else {
                        $total_ptax += $chart['before'];
                        $debit_ptax += $chart['debit'];
                        $kredit_ptax += $chart['kredit'];
                        $after_ptax += $chart['after'];
                        $ptax[] = $chart;
                    }
                } elseif (preg_match('/^116.*/', $chart['account_code'])) {
                    if ($chart['account_code'] == '116999') {
                        $total_pexpenses_account_code = $chart['account_code'];
                        $total_pexpenses_account_name = $chart['account_name'];
                    } elseif ($chart['header'] == 1) {
                        $chart['before'] = $chart['debit'] = $chart['kredit'] = $chart['after'] = null;
                        $pexpenses[] = $chart;
                    } else {
                        $total_pexpenses += $chart['before'];
                        $debit_pexpenses += $chart['debit'];
                        $kredit_pexpenses += $chart['kredit'];
                        $after_pexpenses += $chart['after'];
                        $pexpenses[] = $chart;
                    }
                } elseif (preg_match('/^12.*/', $chart['account_code'])) {
                    if ($chart['account_code'] == '129999') {
                        $total_fassets_account_code = $chart['account_code'];
                        $total_fassets_account_name = $chart['account_name'];
                    } elseif ($chart['header'] == 1) {
                        $chart['before'] = $chart['debit'] = $chart['kredit'] = $chart['after'] = null;
                        $fassets[] = $chart;
                    } else {
                        $total_fassets += $chart['before'];
                        $debit_fassets += $chart['debit'];
                        $kredit_fassets += $chart['kredit'];
                        $after_fassets += $chart['after'];
                        $fassets[] = $chart;
                    }
                } elseif (preg_match('/^2[0-1][0-1].*/', $chart['account_code'])) {
                    if ($chart['account_code'] == '211999') {
                        $total_crliability_account_code = $chart['account_code'];
                        $total_crliability_account_name = $chart['account_name'];
                    } elseif ($chart['header'] == 1) {
                        $chart['before'] = $chart['debit'] = $chart['kredit'] = $chart['after'] = null;
                        $liability[] = $chart;
                    } else {
                        $total_liability += $chart['before'];
                        $debit_liability += $chart['debit'];
                        $kredit_liability += $chart['kredit'];
                        $after_liability += $chart['after'];
                        $liability[] = $chart;
                    }
                } elseif (preg_match('/^212.*/', $chart['account_code'])) {
                    if ($chart['account_code'] == '212999') {
                        $total_taxpay_account_code = $chart['account_code'];
                        $total_taxpay_account_name = $chart['account_name'];
                    } elseif ($chart['header'] == 1) {
                        $chart['before'] = $chart['debit'] = $chart['kredit'] = $chart['after'] = null;
                        $taxpay[] = $chart;
                    } else {
                        $total_taxpay += $chart['before'];
                        $debit_taxpay += $chart['debit'];
                        $kredit_taxpay += $chart['kredit'];
                        $after_taxpay += $chart['after'];
                        $taxpay[] = $chart;
                    }
                } elseif (preg_match('/^2[2-9].*/', $chart['account_code'])) {
                    if ($chart['account_code'] == '229999') {
                        $total_ltliability_account_code = $chart['account_code'];
                        $total_ltliability_account_name = $chart['account_name'];
                    } elseif ($chart['account_code'] == '299999') {
                        $total_liability_account_code = $chart['account_code'];
                        $total_liability_account_name = $chart['account_name'];
                    } elseif ($chart['header'] == 1) {
                        $chart['before'] = $chart['debit'] = $chart['kredit'] = $chart['after'] = null;
                        $ltliability[] = $chart;
                    } else {
                        $total_ltliability += $chart['before'];
                        $debit_ltliability += $chart['debit'];
                        $kredit_ltliability += $chart['kredit'];
                        $after_ltliability += $chart['after'];
                        $ltliability[] = $chart;
                    }
                } elseif (preg_match('/^3.*/', $chart['account_code'])) {
                    if ($chart['account_code'] == '399999') {
                        $total_equity_account_code = $chart['account_code'];
                        $total_equity_account_name = $chart['account_name'];
                    } elseif ($chart['header'] == 1) {
                        $chart['before'] = $chart['debit'] = $chart['kredit'] = $chart['after'] = null;
                        $equity[] = $chart;
                    } else {
                        $total_equity += $chart['before'];
                        $debit_equity += $chart['debit'];
                        $kredit_equity += $chart['kredit'];
                        $after_equity += $chart['after'];
                        $equity[] = $chart;
                    }
                }
            }
            $total_activa = $after_assets + $after_bank + $after_tr + $after_supplies + $after_ptax
                + $after_pexpenses + $after_fassets;
            $after_li = $after_liability + $after_taxpay + $after_ltliability;
            $total_li = $total_liability + $total_taxpay + $total_ltliability;
            $debit_li = $debit_liability + $debit_taxpay + $debit_ltliability;
            $kredit_li = $kredit_liability + $kredit_taxpay + $kredit_ltliability;
            $total_pasiva = $after_li + $after_equity;
            $selisih = $total_activa + $total_pasiva;
            $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'BalanceSheet.xml');
            $this->TBS->MergeBlock('header', array(
                array(
                    'from' => $from,
                    'to' => $to,
                    'total_cash_before' => $total_assets,
                    'total_cash_debit' => $debit_assets,
                    'total_cash_kredit' => $kredit_assets,
                    'total_cash_after' => $after_assets,
                    'total_bank_before' => $total_bank,
                    'total_bank_debit' => $debit_bank,
                    'total_bank_kredit' => $kredit_bank,
                    'total_bank_after' => $after_bank,
                    'total_tr_before' => $total_tr,
                    'total_tr_debit' => $debit_tr,
                    'total_tr_kredit' => $kredit_tr,
                    'total_tr_after' => $after_tr,
                    'total_supplies_before' => $total_supplies,
                    'total_supplies_debit' => $debit_supplies,
                    'total_supplies_kredit' => $kredit_supplies,
                    'total_supplies_after' => $after_supplies,
                    'total_ptax_before' => $total_ptax,
                    'total_ptax_debit' => $debit_ptax,
                    'total_ptax_kredit' => $kredit_ptax,
                    'total_ptax_after' => $after_ptax,
                    'total_pexpenses_before' => $total_pexpenses,
                    'total_pexpenses_debit' => $debit_pexpenses,
                    'total_pexpenses_kredit' => $kredit_pexpenses,
                    'total_pexpenses_after' => $after_pexpenses,
                    'total_fassets_before' => $total_fassets,
                    'total_fassets_debit' => $debit_fassets,
                    'total_fassets_kredit' => $kredit_fassets,
                    'total_fassets_after' => $after_fassets,
                    'total_crliability_before' => $total_liability,
                    'total_crliability_debit' => $debit_liability,
                    'total_crliability_kredit' => $kredit_liability,
                    'total_crliability_after' => $after_liability,
                    'total_taxpay_before' => $total_taxpay,
                    'total_taxpay_debit' => $debit_taxpay,
                    'total_taxpay_kredit' => $kredit_taxpay,
                    'total_taxpay_after' => $after_taxpay,
                    'total_ltliability_before' => $total_ltliability,
                    'total_ltliability_debit' => $debit_ltliability,
                    'total_ltliability_kredit' => $kredit_ltliability,
                    'total_ltliability_after' => $after_ltliability,
                    'total_liability_before' => $total_li,
                    'total_liability_debit' => $debit_li,
                    'total_liability_kredit' => $kredit_li,
                    'total_liability_after' => $after_li,
                    'total_equity_before' => $total_equity,
                    'total_equity_debit' => $debit_equity,
                    'total_equity_kredit' => $kredit_equity,
                    'total_equity_after' => $after_equity,
                    'total_activa' => $total_activa,
                    'total_passiva' => $total_pasiva,
                    'selisih' => $selisih,
                    'total_cash_account_code' => $total_cash_account_code,
                    'total_cash_account_name' => $total_cash_account_name,
                    'total_bank_account_code' => $total_bank_account_code,
                    'total_bank_account_name' => $total_bank_account_name,
                    'total_tr_account_code' => $total_tr_account_code,
                    'total_tr_account_name' => $total_tr_account_name,
                    'total_supplies_account_code' => $total_supplies_account_code,
                    'total_supplies_account_name' => $total_supplies_account_name,
                    'total_ptax_account_code' => $total_ptax_account_code,
                    'total_ptax_account_name' => $total_ptax_account_name,
                    'total_pexpenses_account_code' => $total_pexpenses_account_code,
                    'total_pexpenses_account_name' => $total_pexpenses_account_name,
                    'total_fassets_account_code' => $total_fassets_account_code,
                    'total_fassets_account_name' => $total_fassets_account_name,
                    'total_crliability_account_code' => $total_crliability_account_code,
                    'total_crliability_account_name' => $total_crliability_account_name,
                    'total_taxpay_account_code' => $total_taxpay_account_code,
                    'total_taxpay_account_name' => $total_taxpay_account_name,
                    'total_ltliability_account_code' => $total_ltliability_account_code,
                    'total_ltliability_account_name' => $total_ltliability_account_name,
                    'total_liability_account_code' => $total_liability_account_code,
                    'total_liability_account_name' => $total_liability_account_name,
                    'total_equity_account_code' => $total_equity_account_code,
                    'total_equity_account_name' => $total_equity_account_name
                )
            ));
            $this->TBS->MergeBlock('cash', $assets);
            $this->TBS->MergeBlock('bank', $bank);
            $this->TBS->MergeBlock('tr', $tr);
            $this->TBS->MergeBlock('supplies', $supplies);
            $this->TBS->MergeBlock('ptax', $ptax);
            $this->TBS->MergeBlock('pexpenses', $pexpenses);
            $this->TBS->MergeBlock('fassets', $fassets);
            $this->TBS->MergeBlock('liability', $liability);
            $this->TBS->MergeBlock('taxpay', $taxpay);
            $this->TBS->MergeBlock('ltliability', $ltliability);
            $this->TBS->MergeBlock('equity', $equity);
            $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "BalanceSheet" . $from . $to . ".xls");
        }
    }
    public function actionViewCustomerHistory()
    {
//        if (Yii::app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            $customer_id = $_POST['customer_id'];
            $tgl = $_POST['tglfrom'];
            $cust = U::report_view_customer_history($customer_id, $tgl);
            $customer = Customers::model()->findByPk($customer_id);
            if ($this->format == 'excel') {
                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'newcustomers.xml');
            } else {
//                $this->renderJsonArr($cust);
//                Yii::app()->end();
                $dataProvider = new CArrayDataProvider($cust, array(
                    'id' => 'user',
                    'pagination' => false
                ));
                $this->render('ViewCustomerHistory',
                    array('dp' => $dataProvider, 'start' => sql2date($tgl, 'dd MMM yyyy'), 'cust' => $customer));
                Yii::app()->end();
                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'view_customer_history.htm');
            }
            $this->TBS->MergeBlock('header',
                array(array('logo' => $this->logo, 'customer' => $customer->nama_customer)));
            $this->TBS->MergeBlock('history', $cust);
            if ($this->format == 'excel') {
                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "ViewCustomerHistory_$customer->nama_customer.xls");
            } else {
                $this->TBS->Show();
            }
        }
    }
    public function actionTenders()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $store = $_POST['store'];
            $tenders = U::report_tenders($from, $store);
            $sum_system = array_sum(array_column($tenders, 'system'));
            $sum_tenders = array_sum(array_column($tenders, 'tenders'));
            $selisih = 0;
            $status = "";
            if ($sum_system >= $sum_tenders) {
                $selisih = $sum_system - $sum_tenders;
                $status = "Short";
            } elseif ($sum_system < $sum_tenders) {
                $selisih = $sum_tenders - $sum_system;
                $status = "Over";
            }
            if ($store == "") {
                $store = "All Branch";
            }
            $dataProvider = new CArrayDataProvider($tenders, array(
                'id' => 'Tenders',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=Tenders_$from.xls");
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'tenders.xml');
                echo $this->render('Tenders', array(
                    'dp' => $dataProvider,
                    'start' => sql2date($from, 'dd MMM yyyy'),
                    'tender' => $status,
                    'selisih' => $selisih,
                    'store' => $store
                ), true);
            } else {
                $this->render('Tenders', array(
                    'dp' => $dataProvider,
                    'start' => sql2date($from, 'dd MMM yyyy'),
                    'tender' => $status,
                    'selisih' => $selisih,
                    'store' => $store
                ));
//                Yii::app()->end();
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'tenders.htm');
            }
//            $this->TBS->MergeBlock('header', array(array('logo' => $this->logo, 'branch' => $store, 'start' => $from)));
//            $this->TBS->MergeBlock('tender', $tenders);
//            $this->TBS->MergeBlock('s', array(array('tender' => $status, 'selisih' => $selisih)));
//            if ($this->format == 'excel') {
//                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "Tenders_$from.xls");
//            } else {
//                $this->TBS->Show();
//            }
        }
    }
    public function actionNewCustomers()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $store = $_POST['store'];
            $cust = U::report_new_customers($from, $to, $store);
            if ($store == "") {
                $store = "All Branch";
            }
            $dataProvider = new CArrayDataProvider($cust, array(
                'id' => 'InventoryMovements',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=newcustomers$from-$to.xls");
                echo $this->render('NewCustomers', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'store' => $store
                ), true);
            } else {
                $this->render('NewCustomers', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'store' => $store
                ));
            }
        }
    }
    public function actionCategoryCustomers()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $tgl = $_POST['tglfrom'];
            $store = $_POST['store'];
            $kategori = $_POST['kategori'];
            $kat_name = "";
            $cust = U::report_kategori_customer($kategori, $tgl, $store);
            if ($store == "") {
                $store = "All Branch";
            }
            switch ($kategori) {
                case 1 :
                    $kat_name = "MOST ACTIVE";
                    break;
                case 2 :
                    $kat_name = "VERY ACTIVE";
                    break;
                case 3 :
                    $kat_name = "ACTIVE PATIENT";
                    break;
                case 4 :
                    $kat_name = "PASSIVE PATIENT";
                    break;
            }
            $dataProvider = new CArrayDataProvider($cust, array(
                'id' => 'InventoryMovements',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=KategoriCustomer.xls");
                echo $this->render('KategoriCustomer', array(
                    'dp' => $dataProvider,
                    'kat' => $kat_name,
                    'tgl' => $tgl,
                    'store' => $store
                ), true);
            } else {
                $this->render('KategoriCustomer', array(
                    'dp' => $dataProvider,
                    'kat' => $kat_name,
                    'tgl' => $tgl,
                    'store' => $store
                ));
            }
        }
    }
    public function actionBirthDayCustomers()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $store = $_POST['store'];
            $cust = U::report_birthday_customers($from, $to, $store);
            if ($store == "") {
                $store = "All Branch";
            }
            $dataProvider = new CArrayDataProvider($cust, array(
                'id' => 'InventoryMovements',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=BirthDayCustomers$from-$to.xls");
                echo $this->render('BirthDayCustomers', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'store' => $store
                ), true);
            } else {
                $this->render('BirthDayCustomers', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'store' => $store
                ));
            }
        }
    }
    public function actionCashOut()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            if ($this->format == 'excel') {
                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'biaya.xml');
            } else {
                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'biaya.htm');
            }
            $biaya = U::report_biaya($from, $to);
            $this->TBS->MergeBlock('header', array(array('logo' => $this->logo, 'start' => $from, 'to' => $to)));
            $this->TBS->MergeBlock('cash', $biaya);
            if ($this->format == 'excel') {
                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "cashout$from-$to.xls");
            } else {
                $this->TBS->Show();
            }
        }
    }
    public function actionInventoryMovements()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $store = $_POST['store'];
            $mutasi = U::report_mutasi_stok($from, $to, $store);
            $dataProvider = new CArrayDataProvider($mutasi, array(
                'id' => 'InventoryMovements',
                'pagination' => false
            ));
            if ($store == "") {
                $store = "All Branch";
            }
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=InventoryMovements$from-$to.xls");
                echo $this->render('InventoryMovements', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'store' => $store
                ), true);
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'mutasi_stok.xml');
            } else {
                $this->render('InventoryMovements', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'store' => $store
                ));
//                Yii::app()->end();
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'mutasi_stok.htm');
            }
//            $this->TBS->MergeBlock('header', array(array('logo' => $this->logo, 'start' => $from, 'to' => $to)));
//            $this->TBS->MergeBlock('mutasi', $mutasi);
//            if ($this->format == 'excel') {
//                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "InventoryMovements$from-$to.xls");
//            } else {
//                $this->TBS->Show();
//            }
        }
    }
    public function actionInventoryMovementsClinical()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $store = $_POST['store'];
            $kategori_clinical_id = $_POST['kategori_clinical_id'];
            $grup_name = 'All Category Clinical';
            $mutasi = U::report_mutasi_stok_clinical($from, $to, $kategori_clinical_id, $store);
            $dataProvider = new CArrayDataProvider($mutasi, array(
                'id' => 'InventoryMovementsClinical',
                'pagination' => false
            ));
            if ($store == "") {
                $store = "All Branch";
            }
            if ($kategori_clinical_id != null) {
                $grup = KategoriClinical::model()->findByPk($kategori_clinical_id);
                $grup_name = $grup->nama_kategori;
            }
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=InventoryMovementsClinical$from-$to.xls");
                echo $this->render('InventoryMovementClinical', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'store' => $store,
                    'grup_name' => $grup_name
                ), true);
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'mutasi_stok.xml');
            } else {
                $this->render('InventoryMovementClinical', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'store' => $store,
                    'grup_name' => $grup_name
                ));
//                Yii::app()->end();
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'mutasi_stok.htm');
            }
//            $this->TBS->MergeBlock('header', array(array('logo' => $this->logo, 'start' => $from, 'to' => $to)));
//            $this->TBS->MergeBlock('mutasi', $mutasi);
//            if ($this->format == 'excel') {
//                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "InventoryMovements$from-$to.xls");
//            } else {
//                $this->TBS->Show();
//            }
        }
    }
    public function actionInventoryCard()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $barang_id = $_POST['barang_id'];
            $store = $_POST['store'];
            $barang = Barang::model()->findByPk($barang_id);
            $saldo_awal = StockMoves::get_saldo_item_before($barang->kode_barang, $from, $store);
            $row = U::report_kartu_stok($barang->kode_barang, $from, $to, $store);
            $stock_card = array();
            foreach ($row as $newrow) {
                $newrow['before'] = $saldo_awal;
                $newrow['after'] += $newrow['before'] + $newrow['qty'];
                $saldo_awal = $newrow['after'];
                $stock_card[] = $newrow;
            }
            if ($store == "") {
                $store = "All Branch";
            }
            $dataProvider = new CArrayDataProvider($stock_card, array(
                'id' => 'InventoryCard',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=InventoryCard$from-$to.xls");
                echo $this->render('InventoryCard', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'item' => $barang,
                    'store' => $store
                ), true);
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'kartu_stok.xml');
            } else {
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'kartu_stok.htm');
                $this->render('InventoryCard', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'item' => $barang,
                    'store' => $store
                ));
//                Yii::app()->end();
            }
//            $this->TBS->MergeBlock('header', array(array('logo' => $this->logo, 'start' => $from, 'to' => $to, 'item' => $barang->nama_barang)));
//            $this->TBS->MergeBlock('mutasi', $stock_card);
//            if ($this->format == 'excel') {
//                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "InventoryCard$from-$to.xls");
//            } else {
//                $this->TBS->Show();
//            }
        }
    }
    public function actionBeautySummary()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $store = $_POST['store'];
            $summary = U::report_beauty_summary($from, $to, $store);
            $total_tip = array_sum(array_column($summary, 'tip'));
            if ($store == "") {
                $store = "All Branch";
            }
            $dataProvider = new CArrayDataProvider($summary, array(
                'id' => 'BeautySummary',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=BeautySummary$from-$to.xls");
                echo $this->render('BeautySummary', array(
                    'dp' => $dataProvider,
                    'start' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'tip' => $total_tip,
                    'store' => $store
                ), true);
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'beauty_rekap.xml');
            } else {
                $this->render('BeautySummary', array(
                    'dp' => $dataProvider,
                    'start' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'tip' => $total_tip,
                    'store' => $store
                ));
//                Yii::app()->end();
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'beauty_rekap.htm');
            }
//            $this->TBS->MergeBlock('header', array(array('logo' => $this->logo, 'start' => $from, 'to' => $to)));
//            $this->TBS->MergeBlock('beauty', $summary);
//            if ($this->format == 'excel') {
//                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "BeautySummary$from-$to.xls");
//            } else {
//                $this->TBS->Show();
//            }
        }
    }
    public function actionDokterSummary()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $store = $_POST['store'];
            $summary = U::report_dokter_summary($from, $to, $store);
            if ($store == "") {
                $store = "All Branch";
            }
            $dataProvider = new CArrayDataProvider($summary, array(
                'id' => 'DokterSummary',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=DokterSummary$from-$to.xls");
                echo $this->render('DokterSummary', array(
                    'dp' => $dataProvider,
                    'start' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'store' => $store
                ), true);
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'dokter_rekap.xml');
            } else {
                $this->render('DokterSummary', array(
                    'dp' => $dataProvider,
                    'start' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'store' => $store
                ));
//                Yii::app()->end();
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'dokter_rekap.htm');
            }
//            $this->TBS->MergeBlock('header', array(array('logo' => $this->logo, 'start' => $from, 'to' => $to)));
//            $this->TBS->MergeBlock('dokter', $summary);
//            if ($this->format == 'excel') {
//                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "DokterSummary$from-$to.xls");
//            } else {
//                $this->TBS->Show();
//            }
        }
    }
    public function actionDokterDetails()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $store = $_POST['store'];
            $summary = U::report_dokter_details($from, $to, $store);
            if ($store == "") {
                $store = "All Branch";
            }
            $dataProvider = new CArrayDataProvider($summary, array(
                'id' => 'DokterDetails',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=DokterDetails$from-$to.xls");
                echo $this->render('DokterDetails', array(
                    'dp' => $dataProvider,
                    'start' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'store' => $store
                ), true);
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'dokter_details.xml');
            } else {
                $this->render('DokterDetails', array(
                    'dp' => $dataProvider,
                    'start' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'store' => $store
                ));
//                Yii::app()->end();
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'dokter_details.htm');
            }
//            $this->TBS->MergeBlock('header', array(array('logo' => $this->logo, 'start' => $from, 'to' => $to)));
//            $this->TBS->MergeBlock('dokter', $summary);
//            if ($this->format == 'excel') {
//                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "DokterDetails$from-$to.xls");
//            } else {
//                $this->TBS->Show();
//            }
        }
    }
    public function actionBeautyDetails()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $store = $_POST['store'];
            $summary = U::report_beauty_details($from, $to, $store);
            $total_beauty_tip = array_sum(array_column($summary, 'beauty_tip'));
            if ($store == "") {
                $store = "All Branch";
            }
            $dataProvider = new CArrayDataProvider($summary, array(
                'id' => 'BeautyDetails',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=BeautyDetails$from-$to.xls");
                echo $this->render('BeautyDetails', array(
                    'dp' => $dataProvider,
                    'start' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'total_beauty_tip' => $total_beauty_tip,
                    'store' => $store
                ), true);
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'beauty_details.xml');
            } else {
                $this->render('BeautyDetails', array(
                    'dp' => $dataProvider,
                    'start' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'total_beauty_tip' => $total_beauty_tip,
                    'store' => $store
                ));
//                Yii::app()->end();
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'beauty_details.htm');
            }
//            $this->TBS->MergeBlock('header', array(array('logo' => $this->logo, 'start' => $from, 'to' => $to)));
//            $this->TBS->MergeBlock('beauty', $summary);
//            if ($this->format == 'excel') {
//                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "BeautyDetails$from-$to.xls");
//            } else {
//                $this->TBS->Show();
//            }
        }
    }
    public function actionSalesSummary()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $grup_id = $_POST['grup_id'];
            $store = $_POST['store'];
            $grup_name = '';
            if ($grup_id != null) {
                $grup = Grup::model()->findByPk($grup_id);
                $grup_name = $grup->nama_grup;
            }
            $summary = Users::is_audit() ? U::report_audit_summary($grup_id, $from,
                $to) : U::report_sales_summary($grup_name, $from, $to, $store);
            $total = array_sum(array_column($summary, 'total'));
            $qty = array_sum(array_column($summary, 'qty'));
            if ($grup_name == "") {
                $grup_name = "All Group";
            }
            if ($store == "") {
                $store = "All Branch";
            }
            $dataProvider = new CArrayDataProvider($summary, array(
                'id' => 'SalesSummary',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'sales_rekap.xml');
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=SalesSummary$from-$to.xls");
                echo $this->render('SalesSummary', array(
                    'dp' => $dataProvider,
                    'start' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'grup' => $grup_name,
                    'qty' => $qty,
                    'total' => $total,
                    'store' => $store
                ), true);
            } else {
                $this->render('SalesSummary', array(
                    'dp' => $dataProvider,
                    'start' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'grup' => $grup_name,
                    'qty' => $qty,
                    'total' => $total,
                    'store' => $store
                ));
//                Yii::app()->end();
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'sales_rekap.htm');
            }
//            $this->TBS->MergeBlock('header', array(array('logo' => $this->logo, 'start' => $from, 'to' => $to, 'grup' => $grup_name)));
//            $this->TBS->MergeBlock('sales', $summary);
//            if ($this->format == 'excel') {
//                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "SalesSummary$from-$to.xls");
//            } else {
//                $this->TBS->Show();
//            }
        }
    }
    public function actionSalesSummaryReceipt()
    {
//        if (Yii::app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $store = $_POST['store'];
            $summary = U::report_sales_summary_receipt($from, $to, $store);
            $query = U::report_sales_summary_receipt_amount_total($from, $to, $store);
            $total = $query->total;
            $amount = array_sum(array_column($summary, 'amount'));
            $kembali = array_sum(array_column($summary, 'kembali'));
            if ($store == "") {
                $store = "All Branch";
            }
            $dataProvider = new CArrayDataProvider($summary, array(
                'id' => 'user',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=SalesSummaryReceipt$from.xls");
                echo $this->render('SalesSummaryReceipt',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'total' => $total,
                        'amount' => $amount,
                        'kembali' => $kembali,
                        'store' => $store
                    ), true);
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'sales.xml');
            } else {
                $this->render('SalesSummaryReceipt',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'total' => $total,
                        'amount' => $amount,
                        'kembali' => $kembali,
                        'store' => $store
                    ));
//                $this->renderJsonArr($summary);
//                Yii::app()->end();
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'sales.htm');
            }
//            $this->TBS->MergeBlock('header', array(array('logo' => $this->logo, 'start' => $from,)));
//            $this->TBS->MergeBlock('sales', $summary);
//            $this->TBS->MergeBlock('s', array(array('total' => $total, 'amount' => $amount)));
//            if ($this->format == 'excel') {
//                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "SalesSummaryReceipt$from.xls");
//            } else {
//                $this->TBS->Show();
//            }
        }
    }
    public function actionSalesSummaryReceiptDetails()
    {
//        if (Yii::app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $store = $_POST['store'];
            $summary = U::report_sales_summary_receipt_details($from, $to, $store);
            $total = array_sum(array_column($summary, 'total'));
            $bruto = array_sum(array_column($summary, 'bruto'));
            $vatrp = array_sum(array_column($summary, 'vatrp'));
            $qty = array_sum(array_column($summary, 'qty'));
            $discrp = array_sum(array_column($summary, 'discrp'));
            $price = array_sum(array_column($summary, 'price'));
            if ($store == "") {
                $store = "All Branch";
            }
            $dataProvider = new CArrayDataProvider($summary, array(
                'id' => 'user',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=SalesSummaryReceiptDetails$from-$to.xls");
                echo $this->render('SalesSummaryReceiptDetails',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'qty' => $qty,
                        'total' => $total,
                        'discrp' => $discrp,
                        'vatrp' => $vatrp,
                        'price' => $price,
                        'bruto' => $bruto,
                        'store' => $store
                    ), true);
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'sales.xml');
            } else {
                $this->render('SalesSummaryReceiptDetails',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'qty' => $qty,
                        'total' => $total,
                        'discrp' => $discrp,
                        'vatrp' => $vatrp,
                        'price' => $price,
                        'bruto' => $bruto,
                        'store' => $store
                    ));
//                $this->renderJsonArr($summary);
//                Yii::app()->end();
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'sales.htm');
            }
//            $this->TBS->MergeBlock('header', array(array('logo' => $this->logo, 'start' => $from,)));
//            $this->TBS->MergeBlock('sales', $summary);
//            $this->TBS->MergeBlock('s', array(array('total' => $total)));
//            if ($this->format == 'excel') {
//                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "SalesSummaryReceiptDetails$from.xls");
//            } else {
//                $this->TBS->Show();
//            }
        }
    }
    public function actionReturSalesSummary()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $grup_id = $_POST['grup_id'];
            $store = $_POST['store'];
            $grup_name = '';
            if ($grup_id != null) {
                $grup = Grup::model()->findByPk($grup_id);
                $grup_name = $grup->nama_grup;
            }
            $summary = U::report_retursales_summary($grup_name, $from, $to, $store);
            if ($store == "") {
                $store = "All Branch";
            }
            if ($grup_name == "") {
                $grup_name = "All Group";
            }
            $total = array_sum(array_column($summary, 'total'));
            $qty = array_sum(array_column($summary, 'qty'));
            $dataProvider = new CArrayDataProvider($summary, array(
                'id' => 'ReturSalesSummary',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=SalesSummary$from-$to.xls");
                echo $this->render('ReturSalesSummary', array(
                    'dp' => $dataProvider,
                    'start' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'grup' => $grup_name,
                    'qty' => $qty,
                    'total' => $total,
                    'store' => $store
                ), true);
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'retursales_rekap.xml');
            } else {
                $this->render('ReturSalesSummary', array(
                    'dp' => $dataProvider,
                    'start' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'grup' => $grup_name,
                    'qty' => $qty,
                    'total' => $total,
                    'store' => $store
                ));
//                Yii::app()->end();
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'retursales_rekap.htm');
            }
//            $this->TBS->MergeBlock('header', array(array('logo' => $this->logo, 'start' => $from, 'to' => $to, 'grup' => $grup_name)));
//            $this->TBS->MergeBlock('sales', $summary);
//            if ($this->format == 'excel') {
//                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "SalesSummary$from-$to.xls");
//            } else {
//                $this->TBS->Show();
//            }
        }
    }
    public function actionSalesDetails()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $grup_id = $_POST['grup_id'];
            $real = 0;
            if (isset($_POST['real'])) {
                $real = $_POST['real'];
            }
//            $grup_name = 'All Group';
            $store = $_POST['store'];
            if ($grup_id != null) {
                $grup = Grup::model()->findByPk($grup_id);
                $grup_name = $grup->nama_grup;
            }
            $summary = Users::is_audit() ? U::report_audit_details($grup_id, $from,
                $to) : U::report_sales_details($grup_name, $from, $to, $real, $store);
            if ($store == "") {
                $store = "All Branch";
            }
            if ($grup_name == "") {
                $grup_name = "All Group";
            }
            $total = array_sum(array_column($summary, 'total'));
            $vatrp = array_sum(array_column($summary, 'vatrp'));
            $bruto = array_sum(array_column($summary, 'bruto'));
            $qty = array_sum(array_column($summary, 'qty'));
            $discrp = array_sum(array_column($summary, 'discrp'));
            $price = array_sum(array_column($summary, 'price'));
            $dataProvider = new CArrayDataProvider($summary, array(
                'id' => 'SalesDetails',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=SalesDetails$from-$to.xls");
                echo $this->render('SalesDetails', array(
                    'dp' => $dataProvider,
                    'start' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'grup' => $grup_name,
                    'qty' => $qty,
                    'total' => $total,
                    'discrp' => $discrp,
                    'price' => $price,
                    'vatrp' => $vatrp,
                    'store' => $store,
                    'bruto' => $bruto
                ), true);
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'sales_details.xml');
            } else {
                $this->render('SalesDetails', array(
                    'dp' => $dataProvider,
                    'start' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'grup' => $grup_name,
                    'qty' => $qty,
                    'total' => $total,
                    'discrp' => $discrp,
                    'price' => $price,
                    'vatrp' => $vatrp,
                    'store' => $store,
                    'bruto' => $bruto
                ));
//                Yii::app()->end();
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'sales_details.htm');
            }
//            $this->TBS->MergeBlock('header', array(array('logo' => $this->logo, 'start' => $from, 'to' => sql2date($to, 'dd MMM yyyy'), 'grup' => $grup_name)));
//            $this->TBS->MergeBlock('sales', $summary);
//            if ($this->format == 'excel') {
//                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "SalesDetails$from-$to.xls");
//            } else {
//                $this->TBS->Show();
//            }
        }
    }
    public function actionSalesnReturDetails()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $store = $_POST['store'];
            $grup_id = $_POST['grup_id'];
            $summary = U::report_sales_n_return_details($grup_id, $from, $to, $store);
            $total = array_sum(array_column($summary, 'total'));
            $bruto = array_sum(array_column($summary, 'bruto'));
            $vatrp = array_sum(array_column($summary, 'vatrp'));
            $qty = array_sum(array_column($summary, 'qty'));
            $discrp = array_sum(array_column($summary, 'discrp'));
            $price = array_sum(array_column($summary, 'price'));
            $dataProvider = new CArrayDataProvider($summary, array(
                'id' => 'user',
                'pagination' => false
            ));
            if ($grup_id != null) {
                $grup = Grup::model()->findByPk($grup_id);
                $grup_name = $grup->nama_grup;
            }
            if ($grup_name == "") {
                $grup_name = "All Group";
            }
            if ($store == "") {
                $store = "All Branch";
            }
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=SalesNReturnSalesDetails$from-$to.xls");
                echo $this->render('SalesnReturDetails',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'grup' => $grup_name,
                        'qty' => $qty,
                        'total' => $total,
                        'discrp' => $discrp,
                        'vatrp' => $vatrp,
                        'price' => $price,
                        'bruto' => $bruto,
                        'store' => $store
                    ), true);
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'sales.xml');
            } else {
                $this->render('SalesnReturDetails',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'grup' => $grup_name,
                        'qty' => $qty,
                        'total' => $total,
                        'discrp' => $discrp,
                        'vatrp' => $vatrp,
                        'price' => $price,
                        'bruto' => $bruto,
                        'store' => $store
                    ));
//                $this->renderJsonArr($summary);
//                Yii::app()->end();
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'sales.htm');
            }
        }
    }
    public function actionReturSalesDetails()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $grup_id = $_POST['grup_id'];
            $grup_name = '';
            $store = $_POST['store'];
            if ($grup_id != null) {
                $grup = Grup::model()->findByPk($grup_id);
                $grup_name = $grup->nama_grup;
            }
            $summary = U::report_retur_sales_details($grup_name, $from, $to, $store);
            if ($store == "") {
                $store = "All Branch";
            }
            if ($grup_name == "") {
                $grup_name = "All Group";
            }
            $total = array_sum(array_column($summary, 'total'));
            $bruto = array_sum(array_column($summary, 'bruto'));
            $vatrp = array_sum(array_column($summary, 'vatrp'));
            $qty = array_sum(array_column($summary, 'qty'));
            $discrp = array_sum(array_column($summary, 'discrp'));
            $price = array_sum(array_column($summary, 'price'));
            $dataProvider = new CArrayDataProvider($summary, array(
                'id' => 'ReturSalesDetails',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=ReturSalesDetails$from-$to.xls");
                echo $this->render('ReturSalesDetails', array(
                    'dp' => $dataProvider,
                    'start' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'grup' => $grup_name,
                    'qty' => $qty,
                    'total' => $total,
                    'discrp' => $discrp,
                    'price' => $price,
                    'vatrp' => $vatrp,
                    'store' => $store,
                    'bruto' => $bruto
                ), true);
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'sales_details.xml');
            } else {
                $this->render('ReturSalesDetails', array(
                    'dp' => $dataProvider,
                    'start' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'grup' => $grup_name,
                    'qty' => $qty,
                    'total' => $total,
                    'discrp' => $discrp,
                    'price' => $price,
                    'vatrp' => $vatrp,
                    'store' => $store,
                    'bruto' => $bruto
                ));
//                Yii::app()->end();
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'sales_details.htm');
            }
//            $this->TBS->MergeBlock('header', array(array('logo' => $this->logo, 'start' => $from, 'to' => sql2date($to, 'dd MMM yyyy'), 'grup' => $grup_name)));
//            $this->TBS->MergeBlock('sales', $summary);
//            if ($this->format == 'excel') {
//                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "SalesDetails$from-$to.xls");
//            } else {
//                $this->TBS->Show();
//            }
        }
    }
    public function actionLaha()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $tgl = $_POST['tglfrom'];
            $branch = $_POST['store'];
            $cash = U::report_cash_laha($tgl);
            $modal = U::report_modal($tgl);
            $vat_sales = U::report_vat_sales($tgl);
            $vat_retursales = U::report_vat_retursales($tgl);
            $disc_sales = U::report_discount_sales($tgl);
            $disc_retursales = U::report_discount_returnsales($tgl);
            $noncash = U::report_noncash_laha($tgl);
            $noncashsales = U::report_noncash_sales($tgl);
            $noncash_details = U::report_noncash_details_laha($tgl);
            $sales_laha = U::report_sales_laha($tgl);
            $retursales_laha = U::report_retursales_laha($tgl);
            $cashin = U::report_casin($tgl, $branch);
            $cashout = U::report_casout($tgl, 0, $branch);
            $cashoutother = U::report_casout($tgl, 1, $branch);
            $cash_before = U::get_balance_before_for_bank_account($tgl, Bank::get_bank_cash_id($branch));
            $sum_sales_laha = array_sum(array_column($sales_laha, 'total'));
            $sum_cashin = array_sum(array_column($cashin, 'total'));
            $sum_retursales = array_sum(array_column($retursales_laha, 'total'));
            $sum_cashout = array_sum(array_column($cashout, 'total'));
            $sum_cashoutother = array_sum(array_column($cashoutother, 'total'));
            $sumall = $sum_sales_laha + $sum_cashin - $sum_retursales - $sum_cashout -
                $sum_cashoutother - $disc_sales + $disc_retursales + $vat_sales - $vat_retursales;
            $noncashsales = ($sum_sales_laha + $vat_sales - $disc_sales) + $sum_cashin;
            $total_cashout = $sum_cashout + $sum_cashoutother;
            $sum_noncash_details = array_sum(array_column($noncash_details, 'total'));
            $cash_transfer = U::report_laha_bank_transfer($tgl, $branch);
            $pnl = $cash_before + $noncashsales - $sum_noncash_details - $total_cashout;
            $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'laha.xlsx');
//            if ($this->format == 'excel') {
//
//            } else {
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'laha.htm');
//            }
            $this->TBS->MergeBlock('header', array(
                array(
                    'logo' => $this->logo,
                    'branch' => $branch,
                    'day' => date('l', strtotime($tgl)),
                    'date' => sql2date($tgl),
                    'daily' => $sumall,
                    'sum_sales' => $sum_sales_laha,
                    'cashdrawer' => $cash_before,
                    'modal' => $modal,
                    'cashdrawerafter' => $pnl - $cash_transfer,
                    'sales_vat_disc' => $sum_sales_laha + $vat_sales - $disc_sales,
                    'noncashsales' => $noncashsales,
                    'total_cashout' => $total_cashout,
                    'pnl' => $pnl,
                    'sum_retursales' => $sum_retursales,
                    'sum_cashin' => $sum_cashin,
                    'sum_cashout' => $sum_cashout,
                    'sum_cashoutother' => $sum_cashoutother,
                    'cashnoncash' => $cash + $noncash,
                    'noncashdetails' => $sum_noncash_details,
                    'cashtransfer' => $cash_transfer
                )
            ));
            $this->TBS->MergeBlock('sales', $sales_laha);
            $this->TBS->MergeBlock('retursales', $retursales_laha);
            $this->TBS->MergeBlock('cashin', $cashin);
            $this->TBS->MergeBlock('cashout', $cashout);
            $this->TBS->MergeBlock('cashoutother', $cashoutother);
            $this->TBS->MergeBlock('money', array(array('cash' => $cash, 'noncash' => $noncash)));
            $this->TBS->MergeBlock('noncash', $noncash_details);
            $this->TBS->MergeBlock('d', array(array('sales' => $disc_sales, 'retursales' => $disc_retursales)));
            $this->TBS->MergeBlock('v', array(array('sales' => $vat_sales, 'retursales' => $vat_retursales)));
            $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "Laha$branch$tgl.xlsx");
        }
    }
    public function actionGeneralLedger()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $coa = $_POST['account_code'];
            $store = $_POST['store'];
            $chart_account = ChartMaster::model()->findByPk($coa);
            $result = U::get_general_ledger($coa, $from, $to, $store);
            $begin = U::get_gl_before($coa, $from, $store);
            $begin_arr = array(
                'tgl' => '',
                'memo_' => 'INITIAL BALANCE',
                'Debit' => $begin > 0 ? $begin : '',
                'Credit' => $begin < 0 ? -$begin : '',
                'Balance' => $begin,
                'amount' => $begin
            );
            $gl[] = $begin_arr;
            foreach ($result as $newrow) {
                $move = $newrow['amount'];
                $newrow['Balance'] = $newrow['amount'] + $begin;
                $begin = $newrow['Balance'];
                $gl[] = $newrow;
            }
            if ($store == "") {
                $store = "All Branch";
            }
            $dataProvider = new CArrayDataProvider($gl, array(
                'id' => 'GeneralLedger',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=GeneralLedger$from-$to.xls");
                echo $this->render('GeneralLedger',
                    array(
                        'dp' => $dataProvider,
                        'from' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'account_code' => $chart_account->account_code,
                        'account_name' => $chart_account->account_name,
                        'store' => $store
                    ), true);
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'sales.xml');
            } else {
                $this->render('GeneralLedger', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'account_code' => $chart_account->account_code,
                    'account_name' => $chart_account->account_name,
                    'store' => $store
                ));
            }
        }
    }
    public function actionKartuHutang()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $supplier_id = $_POST['supplier_id'];
            $store = $_POST['store'];
            /* @var $supplier Supplier */
            $supplier = Supplier::model()->findByPk($supplier_id);
            $result = $supplier->get_pelunasan($from, $to, $store);
            $begin = $supplier->total_hutang_before($from, $store);
            $begin_arr = array(
                'tgl' => '',
                'doc_ref' => '',
                'note' => 'INITIAL BALANCE',
                'no_faktur' => '',
                'hutang' => $begin >= 0 ? $begin : 0,
                'payment' => $begin < 0 ? -$begin : 0,
                'saldo' => $begin,
                'total' => $begin
            );
            $kartu_hutang[] = $begin_arr;
            foreach ($result as $newrow) {
                $newrow['saldo'] = $newrow['total'] + $begin;
                $begin = $newrow['saldo'];
                $kartu_hutang[] = $newrow;
            }
            if ($store == "") {
                $store = "All Branch";
            }
            $dataProvider = new CArrayDataProvider($kartu_hutang, array(
                'id' => 'KartuHutang',
                'pagination' => false
            ));
            $this->render('KartuHutang', array(
                'dp' => $dataProvider,
                'from' => sql2date($from, 'dd MMM yyyy'),
                'to' => sql2date($to, 'dd MMM yyyy'),
                'supplier' => $supplier->supplier_name,
                'store' => $store
            ));
        }
    }
    public function actionGeneralJournal()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $store = $_POST['store'];
            $result = U::get_general_journal($from, $to, $store);
            if ($store == "") {
                $store = "All Branch";
            }
            $dataProvider = new CArrayDataProvider($result, array(
                'id' => 'GeneralJournal',
                'pagination' => false
            ));
            $this->render('GeneralJournal', array(
                'dp' => $dataProvider,
                'from' => sql2date($from, 'dd MMM yyyy'),
                'to' => sql2date($to, 'dd MMM yyyy'),
                'store' => $store
            ));
        }
    }
    public function actionPayments()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $store = $_POST['store'];
            $bank_id = $_POST['bank_id'];
            $summary = U::get_report_payment($from, $to, $bank_id, $store);
            $amount = array_sum(array_column($summary, 'amount'));
            $kembali = array_sum(array_column($summary, 'kembali'));
            $dataProvider = new CArrayDataProvider($summary, array(
                'id' => 'user',
                'pagination' => false
            ));
            if ($store == "") {
                $store = "All Branch";
            }
            $bank = Bank::model()->findByPk($bank_id);
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=SalesNReturnSalesDetails$from-$to.xls");
                echo $this->render('Payment',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'amount' => $amount,
                        'kembali' => $kembali,
                        'store' => $store,
                        'bank' => $bank
                    ), true);
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'sales.xml');
            } else {
                $this->render('Payment',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'amount' => $amount,
                        'kembali' => $kembali,
                        'store' => $store,
                        'bank' => $bank
                    ));
//                $this->renderJsonArr($summary);
//                Yii::app()->end();
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'sales.htm');
            }
        }
    }
    public function actionDaftarHargaJual()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $store = $_POST['store'];
            $summary = U::get_daftar_harga_jual($store);
            $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'SalesPrice.xml');
            $this->TBS->MergeBlock('brg', $summary);
            $this->TBS->MergeField('header', array('branch' => $store));
            $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "SalesPrice-$store.xls");
        }
    }
    public function actionDaftarHargaBeli()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $store = $_POST['store'];
            $comm = Yii::app()->db->createCommand("
            SELECT nb.kode_barang,nbl.price,nb.nama_barang
            FROM nscc_beli AS nbl
            INNER JOIN nscc_barang AS nb ON nbl.barang_id = nb.barang_id
            WHERE nbl.store = :store");
            $summary = $comm->queryAll(true, array(':store' => $store));
            $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'PurchasePrice.xlsx');
            $this->TBS->MergeBlock('brg', $summary);
            $this->TBS->MergeField('header', array('branch' => $store));
            $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "PurchasePrice-$store.xlsx");
        }
    }
    public function actionRekeningKoran()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $bank_id = $_POST['bank_id'];
            $summary = U::get_bank_trans($from, $to, $bank_id);
            $bank = Bank::model()->findByPk($bank_id);
            $begin = U::get_balance_before_for_bank_account($from, $bank_id);
            $begin_arr = array(
                'tgl' => '',
                'ref' => 'INITIAL BALANCE',
                'Debit' => $begin > 0 ? $begin : '',
                'Credit' => $begin < 0 ? -$begin : '',
                'Balance' => $begin,
                'amount' => $begin
            );
            $gl[] = $begin_arr;
            foreach ($summary as $newrow) {
                $move = $newrow['amount'];
                $newrow['Balance'] = round($newrow['amount'] + $begin, 2);
                $begin = $newrow['Balance'];
                $gl[] = $newrow;
            }
            $dataProvider = new CArrayDataProvider($gl, array(
                'id' => 'user',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=RekeningKoran$from-$to.xls");
                echo $this->render('RekeningKoran',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'bank' => $bank
                    ), true);
            } else {
                $this->render('RekeningKoran',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'bank' => $bank
                    ));
//                $this->renderJsonArr($summary);
            }
        }
    }
    public function actionCustAtt()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            $this->redirect(url('/'));
        }
        if (isset($_POST) && !empty($_POST)) {
            $where = "";
            $param = array(':from' => $_POST['from'], ':to' => $_POST['to']);
            if ($_POST['store'] != null) {
                $where = "AND ns.store = :store";
                $param[':store'] = $_POST['store'];
            }
            $comm = app()->db->createCommand("
            SELECT DATE_FORMAT(ns.tgl,'%d %b %y') tgl,
            COUNT(DISTINCT(ns.customer_id)) visits
            FROM nscc_salestrans AS ns
            WHERE ns.tgl >= :from AND ns.tgl <= :to
            $where
            GROUP BY ns.tgl");
            $this->renderJsonArr($comm->queryAll(true, $param));
        }
    }
}