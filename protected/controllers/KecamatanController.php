<?php
class KecamatanController extends GxController
{
    public function actionCreate()
    {
        $model = new Kecamatan;
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['Kecamatan'][$k] = $v;
            }
            $model->attributes = $_POST['Kecamatan'];
            $msg = t('save.fail','app');
            if ($model->save()) {
                $status = true;
                $msg = t('save.success.id','app',array('{id}'=>$model->kecamatan_id));
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'Kecamatan');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['Kecamatan'][$k] = $v;
            }
            $msg = t('save.fail','app');
            $model->attributes = $_POST['Kecamatan'];
            if ($model->save()) {
                $status = true;
                $msg = t('save.success.id','app',array('{id}'=>$model->kecamatan_id));
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->kecamatan_id));
            }
        }
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $model = Kecamatan::model()->findAll($criteria);
        $total = Kecamatan::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}