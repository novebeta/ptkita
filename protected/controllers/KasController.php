<?php
Yii::import('application.components.Reference');
Yii::import('application.components.U');
Yii::import('application.components.GL');
Yii::import('application.components.PrintCashOut');
class KasController extends GxController
{
    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            $this->redirect(url('/'));
        }
        if (isset($_POST) && !empty($_POST)) {
            if ($_POST['bank_id'] == SysPrefs::get_val('kas_cabang') &&
                Tender::is_exist($_POST['tgl'])
            ) {
                echo CJSON::encode(array(
                    'success' => false,
                    'msg' => 'Tender Declaration already created'
                ));
                Yii::app()->end();
            }
            $is_new = $_POST['mode'] == 0;
            $is_in = $_POST['arus'] == 1;
            $msg = "Data gagal disimpan.";
            $detils = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $model = $is_new ? new Kas : $this->loadModel($_POST['id'], 'Kas');
                if ($is_new && ($model == null)) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Kas')) . "Fatal error, record not found.");
                }
                if ($is_new) {
                    $ref = new Reference();
                    $docref = $ref->get_next_reference($is_in ? CASHIN : CASHOUT);
                } else {
                    $docref = $model->doc_ref;
                    KasDetail::model()->deleteAll('kas_id = :kas_id', array(':kas_id' => $model->kas_id));
                    $type = $model->arus == 1 ? CASHIN : CASHOUT;
                    $type_no = $model->kas_id;
                    $this->delete_bank_trans($type, $type_no);
                    $this->delete_gl_trans($type, $type_no);
                }
                foreach ($_POST as $k => $v) {
                    if ($k == 'detil') {
                        continue;
                    }
                    if (is_angka($v)) {
                        $v = get_number($v);
                    }
                    $_POST['Kas'][$k] = $v;
                }
//                $bankModel = Bank::model()->findByPk($_POST['Kas']['bank_id']);
//                $_POST['Kas']['store'] = $store;
                $_POST['Kas']['doc_ref'] = $docref;
//                $_POST['Kas']['tgl'] = new CDbExpression('NOW()');
                $_POST['Kas']['total'] = $is_in ? $_POST['Kas']['total'] : -$_POST['Kas']['total'];
                $model->attributes = $_POST['Kas'];
                $balance = BankTrans::get_balance($_POST['bank_id']);
                if ($_POST['arus'] == -1 && ($balance - abs($_POST['Kas']['total'])) < 0) {
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Cash')) . "Insufficient funds");
                }
                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Cash')) . CHtml::errorSummary($model));
                }
                $gl = new GL();
                $gl->add_gl($is_in ? CASHIN : CASHOUT, $model->kas_id, $model->tgl, $docref, $model->bank->account_code,
                    $is_in ? "Cash In" : "Cash Out", $is_in ? "Cash In" : "Cash Out", $model->total, 0,
                    $model->store);
                foreach ($detils as $detil) {
                    $kas_detail = new KasDetail;
                    $_POST['KasDetail']['account_code'] = $detil['account_code'];
                    $_POST['KasDetail']['item_name'] = $detil['item_name'];
//                    $_POST['KasDetail']['qty'] = get_number($detil['qty']);
//                    $_POST['KasDetail']['price'] = $is_in ? get_number($detil['price']) : -get_number($detil['price']);
                    $_POST['KasDetail']['total'] = $is_in ? get_number($detil['total']) : -get_number($detil['total']);
                    $_POST['KasDetail']['kas_id'] = $model->kas_id;
                    $kas_detail->attributes = $_POST['KasDetail'];
                    if (!$kas_detail->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => 'Detail Cash')) . CHtml::errorSummary($kas_detail));
                    }
                    $gl->add_gl($is_in ? CASHIN : CASHOUT, $model->kas_id, $model->tgl, $docref,
                        $kas_detail->account_code,
                        $is_in ? "Cash In" : "Cash Out", "", -$kas_detail->total, 1, $model->store);
                }
                $gl->validate();
                if ($is_new) {
                    $ref->save($is_in ? CASHIN : CASHOUT, $model->kas_id, $docref);
                }
                $msg = t('save.success', 'app');
                $transaction->commit();
                $status = true;
                $prt = new PrintCashOut($model);
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'id' => $docref,
                'msg' => $msg,
                'print' => $prt->buildTxt()
            ));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'Kas');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) {
                    $v = get_number($v);
                }
                $_POST['Kas'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['Kas'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->kas_id;
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->kas_id));
            }
        }
    }
    public function actionIndex()
    {
        $criteria = new CDbCriteria();
        if (isset ($_POST['mode'])) {
//            $param = array(':tgl' => $_POST['tgl']);
            $criteria->select = "kas_id,doc_ref,no_kwitansi,keperluan,IF(total>=0,total,-total) total,bank_id,tgl,user_id,tdate,type_,store,arus";
            $criteria->addCondition(($_POST['mode'] == 'masuk') || ($_POST['mode'] == 'masuk_pusat') ? "arus = 1" : "arus = -1");
            $criteria->addCondition("visible = 1 AND tgl = '".$_POST['tgl']."'");
//            $criteria->addCondition('visible = 1');
            if ($_POST['mode'] == 'masuk') {
                $criteria->addInCondition('bank_id', explode(',', SysPrefs::get_val('filter_payment')));
            } elseif ($_POST['mode'] == 'keluar') {
                $criteria->addInCondition('bank_id', explode(',', SysPrefs::get_val('petty_cash')));
            }
//            $criteria->params = array(':tgl' => $_POST['tgl']);
        }
        $model = Kas::model()->findAll($criteria);
        $total = Kas::model()->count($criteria);
        $this->renderJson($model, $total);
    }
    public function actionPrint()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            $this->redirect(url('/'));
        }
        if (isset($_POST) && !empty($_POST)) {
            $sls = Kas::model()->findByPk($_POST['id']);
            $prt = new PrintCashOut($sls);
            echo CJSON::encode(array(
                'success' => $sls != null,
                'msg' => $prt->buildTxt()
            ));
            Yii::app()->end();
        }
    }
    public function actionCreateTransfer()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $status = false;
            $msg = 'Transfer has been entered.';
            $id = -1;
            $bank_asal = $_POST['bank_act_asal'];
            $bank_tujuan = $_POST['bank_act_tujuan'];
            $trans_date = $_POST['trans_date'];
            $store = $_POST['store'];
            $memo = $_POST['memo'];
            $amount = get_number($_POST['amount']);
            $charge = get_number($_POST['charge']);
            Yii::import('application.components.U');
            Yii::import('application.components.Reference');
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $balance = BankTrans::get_balance($bank_asal);
                if ($amount < 0) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Bank Transfer')) . "Insufficient funds");
                }
                if (($balance - abs($amount)) < 0) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Bank Transfer')) . "Insufficient funds");
                }
                $ref = new Reference();
                $docref = $ref->get_next_reference(BANKTRANSFER);
                $bank_model_asal = Bank::model()->findByPk($bank_asal);
                $bank_model_tujuan = Bank::model()->findByPk($bank_tujuan);
                $bank_account_asal = $bank_model_asal->account_code;
                $bank_account_tujuan = $bank_model_tujuan->account_code;
                $command = Yii::app()->db->createCommand("SELECT UUID();");
                $uuid = $command->queryScalar();
                $trans_no = $uuid;
                $user = Yii::app()->user->getId();
                //debet kode bank tujuan - kredit kode bank asal
                $gl = new GL();
                $gl->add_gl(BANKTRANSFER, $trans_no, $trans_date, $docref, $bank_account_tujuan,
                    '', $memo, $amount, 0, $store);
                $gl->add_gl(BANKTRANSFER, $trans_no, $trans_date, $docref, $bank_account_asal,
                    '', $memo, -$amount, 0, $store);
                if ($charge > 0) {
                    $gl->add_gl(BANKTRANSFERCHARGE, $trans_no, $trans_date, $docref, COA_BIAYA_ADM_BANK,
                        'Transfer kas/bank charge', $memo, $charge, 1, $store);
                    $gl->add_gl(BANKTRANSFERCHARGE, $trans_no, $trans_date, $docref, $bank_account_asal,
                        'Transfer kas/bank charge', $memo, -$charge, 0, $store);
                }
                $gl->validate();
                $ref->save(BANKTRANSFER, $trans_no, $docref);
                $id = $docref;
                $transaction->commit();
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'id' => $id,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }
    public function actionDelete()
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $model = $this->loadModel($_POST['id'], 'Kas');
//                KasDetail::model()->deleteAll('kas_id = :kas_id', array(':kas_id' => $model->kas_id));
                $type = $model->arus == 1 ? CASHIN : CASHOUT;
                $type_no = $model->kas_id;
                $this->delete_bank_trans($type, $type_no);
                $this->delete_gl_trans($type, $type_no);
                $model->visible = 0;
                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Cash')) . CHtml::errorSummary($model));
                }
                $transaction->commit();
                $status = true;
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                )
            );
            Yii::app()->end();
        } else {
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
        }
    }
}