<?php
Yii::import('application.components.Reference');
Yii::import('application.components.U');
class AuditController extends GxController
{
    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest)
            $this->redirect(url('/'));
        if (isset($_POST) && !empty($_POST)) {
            $msg = "Stored data failed.";
            $detils = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $model = new Audit;
                $ref = new Reference();
                $docref = $ref->get_next_reference(AUDIT);
                foreach ($_POST as $k => $v) {
                    if ($k == 'detil') continue;
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['Audit'][$k] = $v;
                }
                $_POST['Audit']['doc_ref'] = $docref;
//                $_POST['Audit']['tgl'] = new CDbExpression('NOW()');
                $model->attributes = $_POST['Audit'];
                if (!$model->save())
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Sales')) . CHtml::errorSummary($model));
                foreach ($detils as $detil) {
                    $audit_detail = new AuditDetails;
                    $_POST['AuditDetails']['barang_id'] = $detil['barang_id'];
                    $_POST['AuditDetails']['qty'] = get_number($detil['qty']);
                    $_POST['AuditDetails']['price'] = get_number($detil['price']);
                    $_POST['AuditDetails']['disc'] = get_number($detil['disc']);
                    $_POST['AuditDetails']['discrp'] = get_number($detil['discrp']);
                    $_POST['AuditDetails']['ketpot'] = $detil['ketpot'];
                    $_POST['AuditDetails']['beauty_id'] = $detil['beauty_id'];
                    $_POST['AuditDetails']['vat'] = get_number($detil['vat']);
                    $_POST['AuditDetails']['vatrp'] = get_number($detil['vatrp']);
                    $_POST['AuditDetails']['total_pot'] = get_number($detil['totalpot']);
                    $_POST['AuditDetails']['total'] = get_number($detil['total']);
                    $_POST['AuditDetails']['bruto'] = get_number($detil['bruto']);
                    $_POST['AuditDetails']['audit_id'] = $model->audit_id;
                    $audit_detail->attributes = $_POST['AuditDetails'];
                    if (!$audit_detail->save())
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Sales Detail')) . CHtml::errorSummary($audit_detail));
                }
                $ref->save(AUDIT, $model->audit_id, $docref);
                $transaction->commit();
                $msg = t('save.success', 'app');
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'id' => $docref,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionSave2Audit()
    {
        $data = CJSON::decode($_POST['salestrans_id']);
//        app()->db->autoCommit = false;
//        $transaction = Yii::app()->db->beginTransaction();
//        try {
        foreach ($data as $salestrans_id) {
            Audit::save_audit($salestrans_id);
        }
//            $transaction->commit();
//        } catch (Exception $ex) {
//            $transaction->rollback();
//            $status = false;
//            $msg = $ex->getMessage();
//        }
        echo CJSON::encode(array(
            'success' => true,
            'msg' => t('save.success', 'app')));
        Yii::app()->end();
//        $model = new Audit;
//        if (!Yii::app()->request->isAjaxRequest)
//            return;
//        if (isset($_POST) && !empty($_POST)) {
//            foreach ($_POST as $k => $v) {
//                if (is_angka($v)) $v = get_number($v);
//                $_POST['Audit'][$k] = $v;
//            }
//            $model->attributes = $_POST['Audit'];
//            $msg = "Data gagal disimpan.";
//            if ($model->save()) {
//                $status = true;
//                $msg = "Data berhasil di simpan dengan id " . $model->audit_id;
//            } else {
//                $msg .= " " . implode(", ", $model->getErrors());
//                $status = false;
//            }
//            echo CJSON::encode(array(
//                'success' => $status,
//                'msg' => $msg));
//            Yii::app()->end();
//        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'Audit');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['Audit'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['Audit'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->audit_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->audit_id));
            }
        }
    }
    public function actionGetSalesTrans()
    {
        if (!Yii::app()->request->isAjaxRequest)
            $this->redirect(url('/'));
        if (isset($_POST) && !empty($_POST)) {
            $tgl = $_POST['tgl'];
            $this->renderJsonArr(U::get_sales_trans_for_audit($tgl));
        }
    }
    public function actionIndex()
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition('DATE(tgl) = :tgl');
        $criteria->params = array(':tgl' => $_POST['tgl']);
        $model = Audit::model()->findAll($criteria);
        $total = Audit::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}