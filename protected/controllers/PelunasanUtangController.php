<?php
Yii::import('application.components.GL');
class PelunasanUtangController extends GxController
{
    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest)
            $this->redirect(url('/'));
        if (isset($_POST) && !empty($_POST)) {
            $msg = "Data gagal disimpan.";
            $status = false;
            $detils = CJSON::decode($_POST['detil']);
            $is_new = $_POST['mode'] == 0;
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $model = $is_new ? new PelunasanUtang : $this->loadModel($_POST['id'], 'PelunasanUtang');
                if ($is_new && ($model == null)) {
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'PelunasanUtang')) .
                        "Fatal error, record not found.");
                }
                if ($is_new) {
                    $ref = new Reference();
                    $docref = $ref->get_next_reference(PELUNASANUTANG);
                } else {
                    $docref = $model->doc_ref;
                    PelunasanUtangDetil::model()->deleteAll('pelunasan_utang_id = :pelunasan_utang_id',
                        array(':pelunasan_utang_id' => $model->pelunasan_utang_id));
                    $type = PELUNASANUTANG;
                    $type_no = $model->pelunasan_utang_id;
                    $this->delete_bank_trans($type, $type_no);
                    $this->delete_gl_trans($type, $type_no);
                }
                foreach ($_POST as $k => $v) {
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['PelunasanUtang'][$k] = $v;
                }
                $_POST['PelunasanUtang']['doc_ref'] = $docref;
                $model->attributes = $_POST['PelunasanUtang'];
                $balance = BankTrans::get_balance($_POST['bank_id']);
                if (($balance - $_POST['PelunasanUtang']['total']) < 0) {
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Cash')) . "Insufficient funds");
                }
                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Debt Payment')) . CHtml::errorSummary($model));
                }
                foreach ($detils as $detil) {
                    $pelunasan_detil = new PelunasanUtangDetil;
                    $_POST['PelunasanUtangDetil']['kas_dibayar'] = get_number($detil['kas_dibayar']);
                    $_POST['PelunasanUtangDetil']['no_faktur'] = $detil['no_faktur'];
                    $_POST['PelunasanUtangDetil']['transfer_item_id'] = $detil['transfer_item_id'];
                    $_POST['PelunasanUtangDetil']['sisa'] = get_number($detil['sisa']);
                    $_POST['PelunasanUtangDetil']['pelunasan_utang_id'] = $model->pelunasan_utang_id;
                    $pelunasan_detil->attributes = $_POST['PelunasanUtangDetil'];
                    if (!$pelunasan_detil->save())
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Detail Debt Payment')) . CHtml::errorSummary($pelunasan_detil));
                    if($pelunasan_detil->sisa == 0){
                        $transferItem = $pelunasan_detil->transferItem;
                        $transferItem->lunas = 1;
                        if (!$transferItem->save())
                            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Purchase')) . CHtml::errorSummary($transferItem));
                    }
                }
                $gl = new GL();
                $gl->add_gl(PELUNASANUTANG, $model->pelunasan_utang_id, $model->tgl, $docref, $model->supplier->account_code,
                    "Pelunasan Hutang", "Pelunasan Hutang", $model->total, 1, $model->store);
                $gl->add_gl(PELUNASANUTANG, $model->pelunasan_utang_id, $model->tgl, $docref, $model->bank->account_code,
                    "Pelunasan Hutang", "Pelunasan Hutang", -$model->total, 0, $model->store);
                $gl->validate();
                if ($is_new) {
                    $ref->save(PELUNASANUTANG, $model->pelunasan_utang_id, $docref);
                }
                $transaction->commit();
                $msg = t('save.success', 'app');
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'PelunasanUtang');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['PelunasanUtang'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['PelunasanUtang'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->pelunasan_utang_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->pelunasan_utang_id));
            }
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'PelunasanUtang')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $model = PelunasanUtang::model()->findAll($criteria);
        $total = PelunasanUtang::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}