<?php
class MgmController extends GxController
{
    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            $this->redirect(url('/'));
        }
        if (isset($_POST) && !empty($_POST)) {
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $model = new Mgm;
                $ref = new Reference();
                $docref = $ref->get_next_reference(MGM);
                foreach ($_POST as $k => $v) {
                    if (is_angka($v)) {
                        $v = get_number($v);
                    }
                    $_POST['Mgm'][$k] = $v;
                }
                $_POST['Mgm']['store'] = STOREID;
                $_POST['Mgm']['doc_ref'] = $docref;
                $model->attributes = $_POST['Mgm'];
                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'MGM')) . CHtml::errorSummary($model));
                }
                /* @var $sales_trans Salestrans */
                $sales_trans = Salestrans::model()->findByPk($model->salestrans_id);
                if($sales_trans == null){
                    throw new Exception("Sales transaction  not found.");
                }
                /* @var $cust_new Customers */
                $cust_new = Customers::model()->findByPk($model->customer_new_id);
                if($cust_new == null){
                    throw new Exception("Customer not found.");
                }
                $awal_date = DateTime::createFromFormat('Y-m-d H:i:s', $cust_new->awal)->format('Y-m-d');
                if($sales_trans->tgl != $awal_date){
                    throw new Exception("The registration date and the date of the transaction is different");
                }
                $min_val = (float)SysPrefs::get_val('min_trans_mgm');
                if($sales_trans->total < $min_val){
                    throw new Exception("Total transactions must be greater than or equal to $min_val");
                }
                PointTrans::save_point_trans(MGM,$model->mgm_id,$model->doc_ref,
                    $model->tgl,1,$model->customer_friend_id,MGM_EVENT,$model->store);
                $ref->save(MGM, $model->mgm_id, $docref);
                $msg = t('save.success', 'app');
                $transaction->commit();
                $status = true;
            }
            catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'Mgm');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) {
                    $v = get_number($v);
                }
                $_POST['Mgm'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['Mgm'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->mgm_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->mgm_id));
            }
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'Mgm')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        } else {
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
        }
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $param = array();
        $criteria = new CDbCriteria();
        if (isset($_POST['tgl'])) {
            $criteria->addCondition('tgl = :tgl');
            $param[':tgl'] = $_POST['tgl'];
        }
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $criteria->params = $param;
        $model = MgmView::model()->findAll($criteria);
        $total = MgmView::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}