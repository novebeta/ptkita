<?php
Yii::import('application.components.Reference');
Yii::import('application.components.U');
class TenderController extends GxController
{
    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest)
            $this->redirect(url('/'));
        if (isset($_POST) && !empty($_POST)) {
            if (Tender::is_exist($_POST['tgl'])) {
                echo CJSON::encode(array(
                    'success' => false,
                    'msg' => 'Tender Declaration already created'));
                Yii::app()->end();
            }
            if (strtotime($_POST['tgl']) > strtotime(date('Y-m-d'))) {
                echo CJSON::encode(array(
                    'success' => false,
                    'msg' => 'Sorry, future Tender Declaration not allowed!'));
                Yii::app()->end();
            }
            $msg = "Stored data failed.";
            $detils = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $model = new Tender;
                $ref = new Reference();
                $docref = $ref->get_next_reference(TENDER);
                foreach ($_POST as $k => $v) {
                    if ($k == 'detil') continue;
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['Tender'][$k] = $v;
                }
                $_POST['Tender']['doc_ref'] = $docref;
                $model->attributes = $_POST['Tender'];
                if (!$model->save())
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Tender')) . CHtml::errorSummary($model));
                foreach ($detils as $detil) {
                    $tender_details = new TenderDetails;
                    $_POST['TenderDetails']['bank_id'] = get_number($detil['bank_id']);
                    $_POST['TenderDetails']['amount'] = get_number($detil['amount']);
                    $_POST['TenderDetails']['tender_id'] = $model->tender_id;
                    $tender_details->attributes = $_POST['TenderDetails'];
                    if (!$tender_details->save())
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Tender Detail')) . CHtml::errorSummary($tender_details));
                }
                $ref->save(TENDER, $model->tender_id, $docref);
                $model->save_printz();
                $transaction->commit();
                $prt = new PrintTenders($model->tgl);
                $msg = $prt->buildTxt();
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'id' => $docref,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'Tender');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['Tender'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['Tender'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->tender_id;
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->tender_id));
            }
        }
    }
    public function actionCountData()
    {
        if (!Yii::app()->request->isAjaxRequest)
            $this->redirect(url('/'));
        if (isset($_POST) && !empty($_POST)) {
            $tgl = $_POST['tgl'];
//            $tender = Tender::model()->find();
            echo CJSON::encode(array(
                'success' => true,
                'msg' => Tender::model()->count('tgl = :tgl',array(':tgl'=>$tgl))));
            Yii::app()->end();
        }
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        $criteria->order = "tgl DESC";
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $model = Tender::model()->findAll($criteria);
        $total = Tender::model()->count($criteria);
        $this->renderJson($model, $total);
    }
    public function actionPrint(){
        if (!Yii::app()->request->isAjaxRequest)
            $this->redirect(url('/'));
        if (isset($_POST) && !empty($_POST)) {
            $prt = new PrintTenders($_POST['tglfrom'],$_POST['store']);
            $msg = $prt->buildTxt();
            echo CJSON::encode(array(
                'success' => true,
                'msg' => $msg));

        }

    }
}