<?php
Yii::import('application.components.Reference');
Yii::import('application.components.U');
Yii::import('application.components.PrintReceipt');
Yii::import('application.components.GL');
class SalestransController extends GxController
{
    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            $this->redirect(url('/'));
        }
        if (isset($_POST) && !empty($_POST)) {
            if (Tender::is_exist($_POST['tgl'])) {
                echo CJSON::encode(array(
                    'success' => false,
                    'msg' => 'Tender Declaration already created'
                ));
                Yii::app()->end();
            }
            $gl = new GL();
            $detils = CJSON::decode($_POST['detil']);
            $payments = CJSON::decode($_POST['payment']);
            $pakets = CJSON::decode($_POST['paket']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $model = new Salestrans;
                $ref = new Reference();
                $docref = $ref->get_next_reference(PENJUALAN);
                if ($_POST['log'] == 0) {
                    $_POST['store'] = STOREID;
                }
                foreach ($_POST as $k => $v) {
                    //if ($k == 'detil' || $k = 'payment') continue;
                    if (is_angka($v)) {
                        $v = get_number($v);
                    }
                    $_POST['Salestrans'][$k] = $v;
                }
//                $_POST['Salestrans']['salestrans_id'] = "1";
                $_POST['Salestrans']['type_'] = 1;
                $_POST['Salestrans']['doc_ref'] = $docref;
//                $_POST['Salestrans']['tgl'] = new CDbExpression('NOW()');
                $model->attributes = $_POST['Salestrans'];
                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Sales')) . CHtml::errorSummary($model));
                }
                if ($model->rounding != 0) {
                    $gl->add_gl(PENJUALAN, $model->salestrans_id, $model->tgl, $docref,
                        COA_ROUNDING, "Sales $docref", "Sales $docref",
                        -$model->rounding, 1, $model->store);
                }
//                $transaction->commit();
//                Yii::app()->end();
                $kembali_sudah_dikurangkan = 0;
                foreach ($payments as $pay) {
                    $amount = get_number($pay['amount']);
                    $kembali = 0;
                    if ($pay['bank_id'] == Bank::get_bank_cash_id() && $kembali_sudah_dikurangkan == 0) {
                        if ($model->kembali > 0) {
                            $kembali = $model->kembali;
                            $kembali_sudah_dikurangkan = 1;
                        }
                    }
                    Payment::add_payment($pay['bank_id'], $model->salestrans_id, $pay['card_number'], $amount,
                        $pay['card_id'], $kembali);
                }
                if($kembali_sudah_dikurangkan == 0 && $model->kembali > 0){
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Sales')) . CHtml::errorSummary("Besides cash payments there should be no change!!!"));
                }
                foreach ($pakets as $paket) {
                    $paket_trans = new PaketTrans;
                    $paket_trans->qty = get_number($paket['qty']);
                    $paket_trans->paket_trans_id = $paket['paket_trans_id'];
                    $paket_trans->paket_id = $paket['paket_id'];
                    $paket_trans->salestrans_id = $model->salestrans_id;
                    if (!$paket_trans->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => 'Paket Trans')) . CHtml::errorSummary($paket_trans));
                    }
                }
                //GL BANK (sesuai cara bayar)
                //        VAT
                //        Diskon Faktur
                foreach ($model->payments as $pay) {
                    if ($pay->amount != 0) {
                        if ($pay->bank->is_bank_cash()) {
                            $gl->add_gl(PENJUALAN, $model->salestrans_id, $model->tgl, $docref,
                                $pay->bank->account_code,
                                "Sales $docref", "Sales $docref", $pay->amount - $pay->kembali, 0, $model->store);
                        } else {
                            if ($pay->card == null) {
                                throw new Exception(t('save.model.fail', 'app',
                                        array('{model}' => 'Sales')) . 'Card type must select!');
                            }
                            $fee = ($pay->card->persen_fee / 100) * $pay->amount;
                            $fee = round($fee, 2);
                            $edc = $pay->amount - $fee;
                            $gl->add_gl(PENJUALAN, $model->salestrans_id, $model->tgl, $docref,
                                $pay->bank->account_code,
                                "Sales $docref", "Sales $docref", $edc, 0, $model->store);
                            $gl->add_gl(PENJUALAN, $model->salestrans_id, $model->tgl, $docref, COA_FEE_CARD,
                                "Sales $docref", "Sales $docref", $fee, 0, $model->store);
                        }
                    }
                }
                if ($model->discrp != 0) {
//              DISKON VOUCHER
                    $gl->add_gl(PENJUALAN, $model->salestrans_id, $model->tgl, $docref, COA_VOUCHER,
                        "Sales $docref", "Sales $docref", $model->discrp, 1);
                }
                if ($model->vat != 0) {
                    $gl->add_gl(PENJUALAN, $model->salestrans_id, $model->tgl, $docref, COA_VAT,
                        "Sales $docref", "Sales $docref", -$model->vat, 1);
                }
                foreach ($detils as $detil) {
                    $salestrans_detail = new SalestransDetails;
                    $_POST['SalestransDetails']['barang_id'] = $detil['barang_id'];
                    $_POST['SalestransDetails']['qty'] = get_number($detil['qty']);
                    $_POST['SalestransDetails']['price'] = get_number($detil['price']);
                    $_POST['SalestransDetails']['disc'] = get_number($detil['disc']);
                    $_POST['SalestransDetails']['discrp'] = get_number($detil['discrp']);
                    $_POST['SalestransDetails']['ketpot'] = $detil['ketpot'];
                    $_POST['SalestransDetails']['vat'] = get_number($detil['vat']);
                    $_POST['SalestransDetails']['vatrp'] = get_number($detil['vatrp']);
                    $_POST['SalestransDetails']['total_pot'] = get_number($detil['total_pot']);
                    $_POST['SalestransDetails']['total'] = get_number($detil['total']);
                    $_POST['SalestransDetails']['bruto'] = get_number($detil['bruto']);
                    $_POST['SalestransDetails']['disc_name'] = $detil['disc_name'];
                    $_POST['SalestransDetails']['disc1'] = get_number($detil['disc1']);
                    $_POST['SalestransDetails']['discrp1'] = get_number($detil['discrp1']);
                    $_POST['SalestransDetails']['paket_trans_id'] = $detil['paket_trans_id'];
                    $_POST['SalestransDetails']['paket_details_id'] = $detil['paket_details_id'];
                    $_POST['SalestransDetails']['salestrans_id'] = $model->salestrans_id;
                    $salestrans_detail->attributes = $_POST['SalestransDetails'];
                    if (!$salestrans_detail->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => 'Sales Detail')) . CHtml::errorSummary($salestrans_detail));
                    }
                    $salestrans_detail->save_beauty_tips($detil['beauty_id'], $detil['beauty2_id']);
                    $tipe_jual = "Sales " . $salestrans_detail->barang->grup->nama_grup;
                    //GL Diskon item
                    if ($salestrans_detail->total_pot != 0) {
                        $gl->add_gl(PENJUALAN, $model->salestrans_id, $model->tgl, $docref,
                            $salestrans_detail->barang->get_coa_sales_disc($model->store),
                            $tipe_jual, $tipe_jual, $salestrans_detail->total_pot, 1);
                    }
                    //GL Penjualan sesuai grup barang
                    if ($salestrans_detail->bruto != 0) {
                        $gl->add_gl(PENJUALAN, $model->salestrans_id, $model->tgl, $docref,
                            $salestrans_detail->barang->get_coa_sales($model->store),
                            $tipe_jual, $tipe_jual, -$salestrans_detail->bruto, 1, $model->store);
                    }
//                    if ($model->log == 0) {
                    if ($salestrans_detail->barang->grup->kategori->is_have_stock()) {
                        //------------------------------ khusus barang non jasa ------------------------------------
                        $saldo_stock = StockMoves::get_saldo_item($salestrans_detail->barang_id, $model->store);
                        if ($saldo_stock < $salestrans_detail->qty) {
                            throw new Exception(t('saldo.item.fail',
                                'app', array(
                                    '{item}' => $salestrans_detail->barang->kode_barang,
                                    '{h}' => $saldo_stock,
                                    '{r}' => $salestrans_detail->qty
                                )));
                        }
                        U::add_stock_moves(PENJUALAN, $model->salestrans_id, $model->tgl,
                            $salestrans_detail->barang_id, -$salestrans_detail->qty, $model->doc_ref,
                            $salestrans_detail->barang->get_cost($model->store), $model->store);
                        // Hitung HPP
                        $cost = $salestrans_detail->barang->get_cost($model->store);
                        $hpp = $salestrans_detail->qty * $cost;
                        $salestrans_detail->cost = $cost;
                        $salestrans_detail->hpp = $hpp;
                        if (!$salestrans_detail->save()) {
                            throw new Exception(t('save.model.fail', 'app',
                                    array('{model}' => 'Sales Hpp Detail')) . CHtml::errorSummary($salestrans_detail));
                        }
                        //GL HPP
                        //  Persediaan
                        $coa_sales_hpp = $salestrans_detail->barang->get_coa_sales_hpp($model->store);
                        if ($hpp != 0 && $coa_sales_hpp != null) {
                            $gl->add_gl(PENJUALAN, $model->salestrans_id, $model->tgl, $docref, $coa_sales_hpp,
                                $tipe_jual, $tipe_jual, $hpp, 0);
                            $gl->add_gl(PENJUALAN, $model->salestrans_id, $model->tgl, $docref,
                                $salestrans_detail->barang->tipeBarang->coa,
                                $tipe_jual, $tipe_jual, -$hpp, 0);
                        }
                    }
//                    }
                }
                $gl->validate();
                $ref->save(PENJUALAN, $model->salestrans_id, $docref);
                $prt = new PrintReceipt($model);
                $msg = $prt->buildTxt();
                $transaction->commit();
//                if ($model->log == 0) {
//                    $prt = new PrintReceipt($model);
//                    $msg = $prt->buildTxt();
//                } else {
//                    $msg = t('save.success', 'app');
//                }
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'id' => $docref,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }
    public function actionReturAll()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            $this->redirect(url('/'));
        }
        if (isset($_POST) && !empty($_POST)) {
            $tgl = date('Y-m-d');
            if (Tender::is_exist($tgl)) {
                echo CJSON::encode(array(
                    'success' => false,
                    'msg' => 'Tender Declaration already created'
                ));
                Yii::app()->end();
            }
            /* @var $Sales Salestrans */
            $Sales = Salestrans::model()->findByPk($_POST['id']);
            if ($Sales == null) {
                throw new Exception("Sales transaction  not found.");
            }
            $prodExists = Salestrans::model()->count('doc_ref_sales = :doc_ref_sales',
                array(':doc_ref_sales' => $Sales->doc_ref));
            if ($prodExists > 0) {
                throw new Exception("Sales Return already created.");
            }
            $gl = new GL();
            $detils = $Sales->salestransDetails;
            $payments = $Sales->payments;
            $pakets = $Sales->paketTrans;
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $model = new Salestrans;
                $ref = new Reference();
                $docref = $ref->get_next_reference(RETURJUAL);
                $model->tgl = $tgl;
                $model->doc_ref = $docref;
                $model->type_ = -1;
                $model->bruto = -$Sales->bruto;
                $model->disc = $Sales->disc;
                $model->discrp = -$Sales->discrp;
                $model->totalpot = -$Sales->totalpot;
                $model->total = -$Sales->total;
                $model->ketdisc = $Sales->ketdisc;
                $model->vat = -$Sales->vat;
                $model->customer_id = $Sales->customer_id;
                $model->doc_ref_sales = $Sales->doc_ref;
                $model->audit = $Sales->audit;
                $model->store = $Sales->store;
                $model->printed = $Sales->printed;
                $model->override = $Sales->override;
                $model->bayar = -$Sales->bayar;
                $model->kembali = -$Sales->kembali;
                $model->rounding = -$Sales->rounding;
                $model->total_discrp1 = -$Sales->total_discrp1;
                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Return Sales')) . CHtml::errorSummary($model));
                }
                if ($model->rounding != 0) {
                    $gl->add_gl(RETURJUAL, $model->salestrans_id, $model->tgl, $docref,
                        COA_ROUNDING, "Sales $docref", "Sales $docref",
                        -$model->rounding, 1, $model->store);
                }
                foreach ($payments as $pay) {
                    $amount = -$pay->amount;
                    $kembali = -$pay->kembali;
                    Payment::add_payment($pay->bank_id, $model->salestrans_id, $pay->card_number,
                        $amount, $pay->card_id, $kembali);
                }
                $paket_trans_id = $this->generate_uuid();
                foreach ($pakets as $paket) {
                    $paket_trans = new PaketTrans;
                    $paket_trans->paket_trans_id = $paket_trans_id;
                    $paket_trans->qty = -$paket->qty;
                    $paket_trans->paket_id = $paket->paket_id;
                    $paket_trans->salestrans_id = $model->salestrans_id;
                    if (!$paket_trans->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => 'Paket Trans')) . CHtml::errorSummary($paket_trans));
                    }
                }
//                Payment::add_payment($_POST['Salestrans']['bank_id'], $model->salestrans_id, '', $model->total);
                //GL BANK (sesuai cara bayar)
                //        VAT
                //        Diskon Faktur
                foreach ($model->payments as $pay) {
                    if ($pay->amount != 0) {
                        if ($pay->bank->is_bank_cash()) {
                            $gl->add_gl(RETURJUAL, $model->salestrans_id, $model->tgl, $docref,
                                $pay->bank->account_code,
                                "Retur Sales $docref", "Retur Sales $docref", $pay->amount - $pay->kembali, 0, $model->store);
                        } else {
                            if ($pay->card == null) {
                                throw new Exception(t('save.model.fail', 'app',
                                        array('{model}' => 'Return Sales')) . 'Card type must select!');
                            }
                            $fee = ($pay->card->persen_fee / 100) * $pay->amount;
                            $fee = round($fee, 2);
                            $edc = $pay->amount - $fee;
                            $gl->add_gl(RETURJUAL, $model->salestrans_id, $model->tgl, $docref,
                                $pay->bank->account_code,
                                "Retur Sales $docref", "Retur Sales $docref", $edc, 0, $model->store);
                            $gl->add_gl(RETURJUAL, $model->salestrans_id, $model->tgl, $docref, COA_FEE_CARD,
                                "Retur Sales $docref", "Retur Sales $docref", $fee, 0, $model->store);
                        }
                    }
                }
                if ($model->discrp != 0) {
//              DISKON VOUCHER
                    $gl->add_gl(RETURJUAL, $model->salestrans_id, $model->tgl, $docref, COA_VOUCHER,
                        "Retur Sales $docref", "Retur Sales $docref", $model->discrp, 1);
                }
                if ($model->vat != 0) {
                    $gl->add_gl(RETURJUAL, $model->salestrans_id, $model->tgl, $docref, COA_VAT,
                        "Retur Sales $docref", "Retur Sales $docref", -$model->vat, 1);
                }
                foreach ($detils as $detil) {
                    $salestrans_detail = new SalestransDetails;
                    $salestrans_detail->barang_id = $detil->barang_id;
                    $salestrans_detail->qty = -$detil->qty;
                    $salestrans_detail->price = $detil->price;
                    $salestrans_detail->disc = -$detil->disc;
                    $salestrans_detail->discrp = -$detil->discrp;
                    $salestrans_detail->ketpot = $detil->ketpot;
                    $salestrans_detail->vat = -$detil->vat;
                    $salestrans_detail->vatrp = -$detil->vatrp;
                    $salestrans_detail->total_pot = -$detil->total_pot;
                    $salestrans_detail->total = -$detil->total;
                    $salestrans_detail->bruto = -$detil->bruto;
                    $salestrans_detail->disc_name = $detil->disc_name;
                    $salestrans_detail->disc1 = $detil->disc1;
                    $salestrans_detail->discrp1 = -$detil->discrp1;
                    $salestrans_detail->paket_trans_id = ($detil->paket_details_id == null) ? null : $paket_trans_id;
                    $salestrans_detail->paket_details_id = $detil->paket_details_id;
                    $salestrans_detail->salestrans_id = $model->salestrans_id;
                    if (!$salestrans_detail->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => 'Return Sales Detail')) . CHtml::errorSummary($salestrans_detail));
                    }
//                    $salestrans_detail->save_beauty_tips();
                    $tipe_jual = "Retur Sales " . $salestrans_detail->barang->grup->nama_grup;
                    //GL Diskon item
                    if ($salestrans_detail->total_pot != 0) {
                        $gl->add_gl(RETURJUAL, $model->salestrans_id, $model->tgl, $docref,
                            $salestrans_detail->barang->get_coa_sales_disc($model->store),
                            $tipe_jual, $tipe_jual, $salestrans_detail->total_pot, 1, $model->store);
                    }
                    //GL Penjualan sesuai grup barang
                    if ($salestrans_detail->bruto != 0) {
                        $gl->add_gl(RETURJUAL, $model->salestrans_id, $model->tgl, $docref, COA_SALES_RETURN,
                            $tipe_jual, $tipe_jual, -$salestrans_detail->bruto, 1, $model->store);
                    }
                    if ($salestrans_detail->barang->grup->kategori->is_have_stock()) {
                        U::add_stock_moves(RETURJUAL, $model->salestrans_id, $model->tgl,
                            $salestrans_detail->barang_id, -$salestrans_detail->qty, $model->doc_ref,
                            $salestrans_detail->barang->get_cost($model->store), $model->store);
                        // Hitung HPP
                        $cost = $salestrans_detail->barang->get_cost($model->store);
                        $hpp = $salestrans_detail->qty * $cost;
                        $salestrans_detail->cost = $cost;
                        $salestrans_detail->hpp = $hpp;
                        if (!$salestrans_detail->save()) {
                            throw new Exception(t('save.model.fail', 'app',
                                    array('{model}' => 'Sales Hpp Detail')) . CHtml::errorSummary($salestrans_detail));
                        }
                        //GL HPP
                        //  Persediaan
                        if ($hpp != 0) {
                            $gl->add_gl(RETURJUAL, $model->salestrans_id, $model->tgl, $docref,
                                $salestrans_detail->barang->get_coa_sales_hpp($model->store),
                                $tipe_jual, $tipe_jual, $hpp, 0, $model->store);
                            $gl->add_gl(RETURJUAL, $model->salestrans_id, $model->tgl, $docref,
                                $salestrans_detail->barang->tipeBarang->coa,
                                $tipe_jual, $tipe_jual, -$hpp, 0, $model->store);
                        }
                    }
                }
                $gl->validate();
                $ref->save(RETURJUAL, $model->salestrans_id, $docref);
                $prt = new PrintReceipt($model);
                $msg = $prt->buildTxt();
                $transaction->commit();
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'id' => $docref,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'Salestrans');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) {
                    $v = get_number($v);
                }
                $_POST['Salestrans'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['Salestrans'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->salestrans_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->salestrans_id));
            }
        }
    }
    public function actionIndex()
    {
//        $this->renderJsonArr(Salestrans::get_trans($_POST['tgl']));
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $param = array();
        $criteria = new CDbCriteria();
        if (isset($_POST['customer_id'])) {
            $criteria->addCondition('customer_id = :customer_id');
            $param[':customer_id'] = $_POST['customer_id'];
        }
        if (isset($_POST['salestrans_id'])) {
            $criteria->addCondition('salestrans_id = :salestrans_id');
            $param[':salestrans_id'] = $_POST['salestrans_id'];
        }
        if (isset($_POST['query'])) {
            $criteria->addCondition('doc_ref like :doc_ref');
            $param[':doc_ref'] = "%" . $_POST['query'] . "%";
        }
        if (isset($_POST['tgl'])) {
            $criteria->addCondition('tgl = :tgl');
            $param[':tgl'] = $_POST['tgl'];
        }
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $criteria->params = $param;
        $model = SalestransView::model()->findAll($criteria);
        $total = SalestransView::model()->count($criteria);
        $this->renderJson($model, $total);
    }
    public function actionBeautyService()
    {
        if (Yii::app()->request->isPostRequest) {
            $tgl = $_POST['tgl'];
            $arr = Salestrans::list_beuty_service($tgl);
            $this->renderJsonArr($arr);
            Yii::app()->end();
        } else {
            throw new CHttpException(403,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
        }
    }
    public function actionHistory()
    {
        $this->renderJsonArr(Salestrans::get_trans_history($_POST['tgl']));
    }
    public function actionPrint()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            $this->redirect(url('/'));
        }
        if (isset($_POST) && !empty($_POST)) {
            $sls = Salestrans::model()->findByPk($_POST['id']);
            $prt = new PrintReceipt($sls);
            echo CJSON::encode(array(
                'success' => $sls != null,
                'msg' => $prt->buildTxt()
            ));
            Yii::app()->end();
        }
    }
}