<?php
require_once('store.php');
/**
 * This is the shortcut to DIRECTORY_SEPARATOR
 */
defined('DS') or define('DS', DIRECTORY_SEPARATOR);
defined('JSON_PRETTY_PRINT') or define ('JSON_PRETTY_PRINT', 128);
define('SALDO_AWAL', 0);
define('PENJUALAN', 1);
define('CASHOUT', 2);
define('CASHIN', 3);
define('RETURJUAL', 4);
define('SUPPIN', 5);
define('SUPPOUT', 6);
define('TENDER', 7);
define('AUDIT', 8);
define('JURNAL_UMUM', 10);
define('BANKTRANSFER', 11);
define('BANKTRANSFERCHARGE', 12);
define('LABARUGI', 13);
define('PELUNASANUTANG', 14);
define('PRODUKSI', 15);
define('CLINICAL_IN', 16);
define('CLINICAL_OUT', 17);
define('SOUVENIR_IN', 18);
define('SOUVENIR_OUT', 19);
define('POINT_IN', 20);
define('POINT_OUT', 21);
define('MGM', 22);
define('MSD', 23);
define('RETURN_PRODUKSI', 24);

define('CHARLENGTHRECEIPT', 58);
define('RUSERS', 100);
define('CUSTOMER', 101);
define('RBEAUTY', 102);
define('RGRUP', 103);
define('RDOKTER', 104);
define('RBARANG', 105);
define('RBANK', 106);
define('RKAS', 107);
define('RSALESTRANS', 108);
define('RTENDER', 109);
define('RTRANSFERITEM', 110);
define('RSALESTRANSDETAILS', 111);
define('RBELI', 112);
define('RJURNAL_UMUM', 113);
define('RPRINTZ', 114);
define('RBANKTRANSFER', 115);
define('RPELUNASANUTANG', 116);
global $systypes_array;
$systypes_array = array(
    SALDO_AWAL => "Saldo Awal",
    PENJUALAN => "Sales",
    CASHOUT => "Cash Out",
    CASHIN => "Cash In",
    RETURJUAL => "Return Sales",
    SUPPIN => "Receive Supplier Item",
    SUPPOUT => "Return Supplier Item",
    TENDER => "Tender Declaration",
    AUDIT => "Audit",
    CUSTOMER => "Customer",
    JURNAL_UMUM => "General Journal",
    BANKTRANSFER => "Cash/Bank Transfer",
);
/**
 * This is the shortcut to Yii::app()
 */
function app()
{
    return Yii::app();
}

/**
 * @param $number
 * @return int
 */
function is_angka($number)
{
    if($number == null){
        return false;
    }
    return preg_match("/^-?([\$]?)([0-9,\s]*\.?[0-9]{0,2})$/", $number);
}

/**
 * This is the shortcut to Yii::app()->clientScript
 */
function cs()
{
    // You could also call the client script instance via Yii::app()->clientScript
    // But this is faster
    return Yii::app()->getClientScript();
}

/**
 * This is the shortcut to Yii::app()->user.
 */
function user()
{
    return Yii::app()->getUser();
}

/**
 * This is the shortcut to Yii::app()->createUrl()
 */
function url($route, $params = array(), $ampersand = '&')
{
    return Yii::app()->createUrl($route, $params, $ampersand);
}

/**
 * This is the shortcut to CHtml::encode
 */
function h($text)
{
    return htmlspecialchars($text, ENT_QUOTES, Yii::app()->charset);
}

function dbTrans()
{
    return Yii::app()->db->beginTransaction();
}

/**
 * This is the shortcut to CHtml::link()
 */
function l($text, $url = '#', $htmlOptions = array())
{
    return CHtml::link($text, $url, $htmlOptions);
}

/**
 * This is the shortcut to Yii::t() with default category = 'stay'
 */
function t($message, $category = 'stay', $params = array(), $source = null, $language = null)
{
    return Yii::t($category, $message, $params, $source, $language);
}

/**
 * This is the shortcut to Yii::app()->request->baseUrl
 * If the parameter is given, it will be returned and prefixed with the app baseUrl.
 */
function bu($url = null)
{
    static $baseUrl;
    if ($baseUrl === null) {
        $baseUrl = Yii::app()->getRequest()->getBaseUrl();
    }
    return $url === null ? $baseUrl : $baseUrl . '/' . ltrim($url, '/');
}

/**
 * Returns the named application parameter.
 * This is the shortcut to Yii::app()->params[$name].
 */
function param($name)
{
    return Yii::app()->params[$name];
}

function Encrypt($string)
{
    return Yii::app()->aes256->Encrypt($string);
}

function Decrypt($string)
{
    return Yii::app()->aes256->Decrypt($string);
}

function generatePassword($length = 8)
{
    $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    $count = mb_strlen($chars);
    for ($i = 0, $result = ''; $i < $length; $i++) {
        $index = rand(0, $count - 1);
        $result .= mb_substr($chars, $index, 1);
    }
    return $result;
}

function date2longperiode($date, $format)
{
    $timestamp = CDateTimeParser::parse($date, 'yyyy-MM-dd');
    $formater = new CDateFormatter('id_ID');
    return $formater->format($format, $timestamp);
}

function period2date($month, $year)
{
    $timestamp = DateTime::createFromFormat('d/m/Y', "01/$month/$year");
    $start = $timestamp->format('Y-m-d');
    $end = $timestamp->format('Y-m-t');
    return array('start' => $start, 'end' => $end);
}

function get_jemaat_from_user($nij)
{
    return Jemaat::model()->findByPk($nij);
}

function get_number($number)
{
    return str_replace(",", "", $number);
}

function get_jemaat_from_user_id($id)
{
    $user = Users::model()->findByPk($id);
    if ($user == null) {
        return false;
    } else {
        return get_jemaat_from_user($user->nij);
    }
}

function sql2date($date, $format = 'dd/MM/yyyy')
{
    $timestamp = CDateTimeParser::parse($date, 'yyyy-MM-dd');
    return Yii::app()->dateFormatter->format($format, $timestamp);
}

function date2sql($date)
{
    $timestamp = CDateTimeParser::parse($date, 'dd/MM/yyyy');
    return Yii::app()->dateFormatter->format('yyyy-MM-dd', $timestamp);
}

function sql2long_date($date)
{
    $timestamp = CDateTimeParser::parse($date, 'yyyy-MM-dd');
    $formater = new CDateFormatter('id_ID');
    return $formater->formatDateTime($timestamp, 'long', false);
}

function get_date_tomorrow()
{
    return Yii::app()->dateFormatter->format('yyyy-MM-dd', time() + (1 * 24 * 60 * 60));
}

function get_time_now()
{
    return Yii::app()->dateFormatter->format('HH:mm:ss', time());
}

function get_date_today($format = 'yyyy-MM-dd')
{
    return Yii::app()->dateFormatter->format($format, time());
}

function Now($formatDate = 'yyyy-MM-dd')
{
    return get_date_today($formatDate) . ' ' . get_time_now();
}

function percent_format($value, $decimal = 0)
{
    return number_format($value * 100, $decimal) . '%';
}

function curr_format($value, $decimal = 0)
{
    return "Rp" . number_format($value * 100, $decimal);
}

function acc_format($value, $decimal = 0)
{
    $normalize = $value < 0 ? -$value : $value;
    $print = number_format($normalize, $decimal);
    return $value < 0 ? "($print)" : $print;
}

function mailsend($to, $from, $subject, $message, $attachdata)
{
    $mail = Yii::app()->Smtpmail;
    $mail->SetFrom($from, 'NWIS');
    $mail->Subject = $subject;
    $mail->Body = $message;
    $mail->ClearAddresses();
    $mail->ClearAttachments();
    $mail->AddAddress($to);
    $mail->AddStringAttachment($attachdata, "$subject.bz2");
    if (!$mail->Send()) {
        return $mail->ErrorInfo;
    } else {
        return 'OK';
    }
}

function mailsend_remainder($to, $from, $subject, $message)
{
    $mail = Yii::app()->Smtpmail;
    $mail->SetFrom($from, 'NWIS');
    $mail->Subject = $subject;
    $mail->AltBody = "To view the message, please use an HTML compatible email viewer!";
    $mail->MsgHTML($message);
    $mail->ClearAddresses();
    $mail->ClearAttachments();
    $mail->AddAddress($to);
    if (!$mail->Send()) {
        return $mail->ErrorInfo;
    } else {
        return 'OK';
    }
}

function yiiparam($name, $default = null)
{
    if (isset(Yii::app()->params[$name])) {
        return Yii::app()->params[$name];
    } else {
        return $default;
    }
}

function is_connected($url, $port)
{
    $connected = @fsockopen($url, $port);
    if ($connected) {
        $is_conn = true; //action when connected
        fclose($connected);
    } else {
        $is_conn = false; //action in connection failure
    }
    return $is_conn;
}

function format_number_report($num, $digit = 0)
{
    if(!is_angka($num)){
        return $num;
    }
    return (isset($_POST['format']) && $_POST['format'] == 'excel') ? $num : number_format($num, $digit);
}

function is_report_excel(){
    return (isset($_POST['format']) && $_POST['format'] == 'excel');
}