<?php
class MenuTree
{
    var $security_role;
    var $menu_users = array(
        'text' => 'User Manajement',
        'class' => 'jun.UsersGrid',
        'leaf' => true
    );
    var $security = array(
        'text' => 'Security Roles',
        'class' => 'jun.SecurityRolesGrid',
        'leaf' => true
    );
    function __construct($id)
    {
        $role = SecurityRoles::model()->findByPk($id);
        $this->security_role = explode(",", $role->sections);
    }
    function getChildMaster()
    {
        /** @var TODO 120 dan 112 kosong */
        $child = array();
        if (in_array(100, $this->security_role) && !Users::is_audit()) {
            $child[] = array(
                'text' => 'Country',
                'class' => 'jun.NegaraGrid',
                'leaf' => true
            );
        }
        if (in_array(101, $this->security_role) && !Users::is_audit()) {
            $child[] = array(
                'text' => 'State',
                'class' => 'jun.ProvinsiGrid',
                'leaf' => true
            );
        }
        if (in_array(102, $this->security_role) && !Users::is_audit()) {
            $child[] = array(
                'text' => 'City',
                'class' => 'jun.KotaGrid',
                'leaf' => true
            );
        }
        if (in_array(103, $this->security_role) && !Users::is_audit()) {
            $child[] = array(
                'text' => 'Sub District',
                'class' => 'jun.KecamatanGrid',
                'leaf' => true
            );
        }
        if (in_array(104, $this->security_role) && !Users::is_audit()) {
            $child[] = array(
                'text' => 'Doctor',
                'class' => 'jun.DokterGrid',
                'leaf' => true
            );
        }
        if (in_array(105, $this->security_role) && !Users::is_audit()) {
            $child[] = array(
                'text' => 'Bank',
                'class' => 'jun.BankGrid',
                'leaf' => true
            );
        }
        if (in_array(106, $this->security_role) && !Users::is_audit()) {
            $child[] = array(
                'text' => 'Group',
                'class' => 'jun.GrupGrid',
                'leaf' => true
            );
        }
        if (in_array(107, $this->security_role) && !Users::is_audit()) {
            $child[] = array(
                'text' => 'Product/Treatment',
                'class' => 'jun.BarangGrid',
                'leaf' => true
            );
        }
        if (in_array(108, $this->security_role) && !Users::is_audit()) {
            $child[] = array(
                'text' => 'Customers',
                'class' => 'jun.CustomersGrid',
                'leaf' => true
            );
        }
        if (in_array(109, $this->security_role) && !Users::is_audit()) {
            $child[] = array(
                'text' => 'Beautician',
                'class' => 'jun.BeautyGrid',
                'leaf' => true
            );
        }
        if (in_array(110, $this->security_role) && !Users::is_audit()) {
            $child[] = array(
                'text' => 'Beautician Class',
                'class' => 'jun.GolGrid',
                'leaf' => true
            );
        }
        if (in_array(111, $this->security_role) && !Users::is_audit()) {
            $child[] = array(
                'text' => 'Beautician Tip Rate',
                'class' => 'jun.PriceGrid',
                'leaf' => true
            );
        }
        if (in_array(113, $this->security_role) && !Users::is_audit()) {
            $child[] = array(
                'text' => 'Status Customers',
                'class' => 'jun.StatusCustGrid',
                'leaf' => true
            );
        }
        if (in_array(114, $this->security_role) && !Users::is_audit()) {
            $child[] = array(
                'text' => 'Discount',
                'class' => 'jun.DiskonGrid',
                'leaf' => true
            );
        }
        if (in_array(115, $this->security_role) && !Users::is_audit()) {
            $child[] = array(
                'text' => 'Store',
                'class' => 'jun.StoreGrid',
                'leaf' => true
            );
        }
        if (in_array(116, $this->security_role) && !Users::is_audit()) {
            $child[] = array(
                'text' => 'Supplier',
                'class' => 'jun.SupplierGrid',
                'leaf' => true
            );
        }
        if (in_array(117, $this->security_role) && !Users::is_audit()) {
            $child[] = array(
                'text' => 'Chart Of Account',
                'class' => 'jun.ChartMasterGrid',
                'leaf' => true
            );
        }
        if (in_array(118, $this->security_role) && !Users::is_audit()) {
            $child[] = array(
                'text' => 'Default Purchase Price',
                'class' => 'jun.BeliGrid',
                'leaf' => true
            );
        }
        if (in_array(119, $this->security_role) && !Users::is_audit()) {
            $child[] = array(
                'text' => 'Region',
                'class' => 'jun.WilayahGrid',
                'leaf' => true
            );
        }
        if (in_array(121, $this->security_role) && !Users::is_audit()) {
            $child[] = array(
                'text' => 'Card Type',
                'class' => 'jun.CardGrid',
                'leaf' => true
            );
        }
        if (in_array(122, $this->security_role) && !Users::is_audit()) {
            $child[] = array(
                'text' => 'Group Attribute',
                'class' => 'jun.GrupAttrGrid',
                'leaf' => true
            );
        }
        if (in_array(123, $this->security_role) && !Users::is_audit()) {
            $child[] = array(
                'text' => 'Selling Price',
                'class' => 'jun.JualGrid',
                'leaf' => true
            );
        }
        if (in_array(124, $this->security_role) && !Users::is_audit()) {
            $child[] = array(
                'text' => 'Employee',
                'class' => 'jun.EmployeeGrid',
                'leaf' => true
            );
        }
        if (in_array(125, $this->security_role) && !Users::is_audit()) {
            $child[] = array(
                'text' => 'Operational Item',
                'class' => 'jun.BarangClinicalGrid',
                'leaf' => true
            );
        }
        if (in_array(126, $this->security_role) && !Users::is_audit()) {
            $child[] = array(
                'text' => 'Operational Item Category',
                'class' => 'jun.KategoriClinicalGrid',
                'leaf' => true
            );
        }
        if (in_array(127, $this->security_role) && !Users::is_audit()) {
            $child[] = array(
                'text' => 'Info',
                'class' => 'jun.InfoGrid',
                'leaf' => true
            );
        }
        if (in_array(128, $this->security_role) && !Users::is_audit()) {
            $child[] = array(
                'text' => 'Souvenir',
                'class' => 'jun.SouvenirGrid',
                'leaf' => true
            );
        }
        if (in_array(129, $this->security_role) && !Users::is_audit()) {
            $child[] = array(
                'text' => 'Promotion',
                'class' => 'jun.EventGrid',
                'leaf' => true
            );
        }
        if (in_array(130, $this->security_role) && !Users::is_audit()) {
            $child[] = array(
                'text' => 'Packages',
                'class' => 'jun.PaketGrid',
                'leaf' => true
            );
        }
        return $child;
    }
    function getMaster($child)
    {
        if (empty($child)) {
            return array();
        }
        return array(
            'text' => 'Master',
            'expanded' => false,
            'children' => $child
        );
    }
    function getTransaction($child)
    {
        if (empty($child)) {
            return array();
        }
        return array(
            'text' => 'Transaction',
            'expanded' => false,
            'children' => $child
        );
    }
    function getChildTransaction()
    {
        $child = array();
        $childSales = array();
        $childPurchase = array();
        $childExpenses = array();
        $childAcc = array();
        $childSuppliesManagement = array();
        $childTreatment = array();
        $childMarketing = array();
        $childTenderTransferFunding = array();
        if (in_array(200, $this->security_role) && !Users::is_audit()) {
            $childSales[] = array(
                'text' => 'Sales',
                'class' => Users::is_audit() ? 'jun.PaketGrid' : 'jun.SalestransGrid',
                'leaf' => true
            );
        }
        if (in_array(201, $this->security_role) && !Users::is_audit()) {
            $childSales[] = array(
                'text' => 'Return Sales',
                'class' => 'jun.ReturSalestransgrid',
                'leaf' => true
            );
        }
        if (in_array(203, $this->security_role) && !Users::is_audit()) {
            $childSales[] = array(
                'text' => 'Other Income',
                'class' => 'jun.KasGrid',
                'leaf' => true
            );
        }
        if (!empty($childSales)) {
            $child[] = array(
                'text' => 'Sales',
                'expanded' => false,
                'children' => $childSales
            );
        }
        if (in_array(205, $this->security_role) && !Users::is_audit()) {
            $childPurchase[] = array(
                'text' => 'Purchase',
                'class' => 'jun.TransferItemGrid',
                'leaf' => true
            );
        }
        if (in_array(206, $this->security_role) && !Users::is_audit()) {
            $childPurchase[] = array(
                'text' => 'Purchase Return',
                'class' => 'jun.ReturnTransferItemGrid',
                'leaf' => true
            );
        }
        if (in_array(230, $this->security_role) && !Users::is_audit()) {
            $childPurchase[] = array(
                'text' => 'Debt Payment',
                'class' => 'jun.PelunasanUtangGrid',
                'leaf' => true
            );
        }
        if (!empty($childPurchase)) {
            $child[] = array(
                'text' => 'Purchase',
                'expanded' => false,
                'children' => $childPurchase
            );
        }
        if (in_array(204, $this->security_role) && !Users::is_audit()) {
            $childExpenses[] = array(
                'text' => 'Petty Cash Expenses',
                'class' => 'jun.KasGridOut',
                'leaf' => true
            );
        }
        if (in_array(213, $this->security_role) && !Users::is_audit()) {
            $childExpenses[] = array(
                'text' => 'Home Expenses',
                'class' => 'jun.KasGridPusatOut',
                'leaf' => true
            );
        }
        if (!empty($childExpenses)) {
            $child[] = array(
                'text' => 'Expenses',
                'expanded' => false,
                'children' => $childExpenses
            );
        }
        if (in_array(215, $this->security_role) && !Users::is_audit()) {
            $childAcc[] = array(
                'text' => 'Generate Profit Lost',
                'class' => 'jun.GenerateLabaRugi',
                'leaf' => true
            );
        }
        if (in_array(211, $this->security_role) && !Users::is_audit()) {
            $childAcc[] = array(
                'text' => 'General Journal',
                'class' => 'jun.JurnalUmum',
                'leaf' => true
            );
        }
        if (!empty($childAcc)) {
            $child[] = array(
                'text' => 'Accounting',
                'expanded' => false,
                'children' => $childAcc
            );
        }
        if (in_array(207, $this->security_role) && !Users::is_audit()) {
            $childTenderTransferFunding[] = array(
                'text' => 'Tender Declaration',
                'class' => 'jun.TenderGrid',
                'leaf' => true
            );
        }
        if (in_array(214, $this->security_role) && !Users::is_audit()) {
            $childTenderTransferFunding[] = array(
                'text' => 'Cash/Bank Transfer',
                'class' => 'jun.BankTransGrid',
                'leaf' => true
            );
        }
        if (in_array(212, $this->security_role) && !Users::is_audit()) {
            $childTenderTransferFunding[] = array(
                'text' => 'Funding',
                'class' => 'jun.KasGridPusat',
                'leaf' => true
            );
        }
        if (!empty($childTenderTransferFunding)) {
            $child[] = array(
                'text' => 'Tender, Transfer & Funding',
                'expanded' => false,
                'children' => $childTenderTransferFunding
            );
        }
        if (in_array(202, $this->security_role) && !Users::is_audit()) {
            $childTreatment[] = array(
                'text' => 'Edit Service',
                'class' => 'jun.beautytransGrid',
                'leaf' => true
            );
        }
        if (!empty($childTreatment)) {
            $child[] = array(
                'text' => 'Treatment',
                'expanded' => false,
                'children' => $childTreatment
            );
        }
        if (in_array(218, $this->security_role) && !Users::is_audit()) {
            $childProduction[] = array(
                'text' => 'Production',
                'class' => 'jun.ProduksiGrid',
                'leaf' => true
            );
        }
        if (in_array(219, $this->security_role) && !Users::is_audit()) {
            $childProduction[] = array(
                'text' => 'Production Return',
                'class' => 'jun.ProduksiReturnGrid',
                'leaf' => true
            );
        }
        if (!empty($childProduction)) {
            $child[] = array(
                'text' => 'Production',
                'expanded' => false,
                'children' => $childProduction
            );
        }
        if (in_array(220, $this->security_role) && !Users::is_audit()) {
            $childSuppliesManagement[] = array(
                'text' => 'Supplies In',
                'class' => 'jun.ClinicalTransGrid',
                'leaf' => true
            );
        }
        if (in_array(221, $this->security_role) && !Users::is_audit()) {
            $childSuppliesManagement[] = array(
                'text' => 'Supplies Out',
                'class' => 'jun.ClinicalTransOutGrid',
                'leaf' => true
            );
        }
        if (in_array(222, $this->security_role) && !Users::is_audit()) {
            $childSuppliesManagement[] = array(
                'text' => 'Souvenir In',
                'class' => 'jun.SouvenirTransGrid',
                'leaf' => true
            );
        }
        if (in_array(223, $this->security_role) && !Users::is_audit()) {
            $childSuppliesManagement[] = array(
                'text' => 'Souvenir Out',
                'class' => 'jun.SouvenirOutGrid',
                'leaf' => true
            );
        }
        if (!empty($childSuppliesManagement)) {
            $child[] = array(
                'text' => 'Supplies Management',
                'expanded' => false,
                'children' => $childSuppliesManagement
            );
        }
        if (in_array(224, $this->security_role) && !Users::is_audit()) {
            $childSuppliesManagement[] = array(
                'text' => 'Point In',
                'class' => 'jun.PromoGenericGrid',
                'leaf' => true
            );
        }
        if (in_array(225, $this->security_role) && !Users::is_audit()) {
            $childSuppliesManagement[] = array(
                'text' => 'Point Out',
                'class' => 'jun.PromoGenericOutGrid',
                'leaf' => true
            );
        }
        if (in_array(226, $this->security_role) && !Users::is_audit()) {
            $childSuppliesManagement[] = array(
                'text' => 'Member Get Member',
                'class' => 'jun.MgmGrid',
                'leaf' => true
            );
        }
        if (in_array(227, $this->security_role) && !Users::is_audit()) {
            $childSuppliesManagement[] = array(
                'text' => 'My Special Day',
                'class' => 'jun.MsdGrid',
                'leaf' => true
            );
        }
        if (!empty($childSuppliesManagement)) {
            $child[] = array(
                'text' => 'Marketing',
                'expanded' => false,
                'children' => $childSuppliesManagement
            );
        }
        if (in_array(208, $this->security_role) && !Users::is_audit()) {
            $child[] = array(
                'text' => 'Audit',
                'class' => 'jun.AuditGrid',
                'leaf' => true
            );
        }
        if (in_array(210, $this->security_role) && !Users::is_audit()) {
            $child[] = array(
                'text' => 'History',
                'class' => 'jun.HistoryGrid',
                'leaf' => true
            );
        }
        if (in_array(216, $this->security_role) && !Users::is_audit()) {
            $child[] = array(
                'text' => 'Yearly Closing',
                'class' => 'jun.ClosingStore',
                'leaf' => true
            );
        }
        if (in_array(217, $this->security_role) && !Users::is_audit()) {
            $child[] = array(
                'text' => 'Absent Transaction',
                'class' => 'jun.AbsenTransGrid',
                'leaf' => true
            );
        }
        return $child;
    }
    function getReport($child)
    {
        if (empty($child)) {
            return array();
        }
        return array(
            'text' => 'Report',
            'expanded' => false,
            'children' => $child
        );
    }
    function getChildReport()
    {
        // terakhir 334
        $child = array();
        $childBalanceSheet = array();
        $childProfitLoss = array();
        $childAssets = array();
        $childStock = array();
        $childoLiabilities = array();
        $childooEquity = array();
        $childSales = array();
        $childPurchases = array();
        $childExpenses = array();
        $childCashFlow = array();
        $childAcc = array();
        $childInvSupp = array();
        $childMarketing = array();
        $childCust = array();
        $childSalesPrice = array();
        $childPromo = array();
        $childProduct = array();
        if (in_array(319, $this->security_role) && !Users::is_audit()) {
            $childBalanceSheet[] = array(
                'text' => 'Balance Sheet Movement',
                'class' => 'jun.ReportBalanceSheet',
                'leaf' => true
            );
        }
        if (in_array(320, $this->security_role) && !Users::is_audit()) {
            $childBalanceSheet[] = array(
                'text' => 'Balance Sheet Report',
                'class' => 'jun.ReportNeraca',
                'leaf' => true
            );
        }
        if (in_array(323, $this->security_role) && !Users::is_audit()) {
            $childAssets[] = array(
                'text' => 'Bank/Cash Account Statement',
                'class' => 'jun.ReportRekeningKoran',
                'leaf' => true
            );
        }
        if (in_array(303, $this->security_role) && !Users::is_audit()) {
            $childStock[] = array(
                'text' => 'Inventory Card',
                'class' => 'jun.ReportInventoryCard',
                'leaf' => true
            );
        }
        if (in_array(302, $this->security_role) && !Users::is_audit()) {
            $childStock[] = array(
                'text' => 'Inventory Movements Summary',
                'class' => 'jun.ReportInventoryMovements',
                'leaf' => true
            );
        }
        if (in_array(335, $this->security_role) && !Users::is_audit()) {
            $childAssets[] = array(
                'text' => 'Fixed Asset',
                'class' => 'jun.ReportFixedAsset',
                'leaf' => true
            );
        }
        if (in_array(336, $this->security_role) && !Users::is_audit()) {
            $childAssets[] = array(
                'text' => 'Account Receivable',
                'class' => 'jun.ReportAccountReceivable',
                'leaf' => true
            );
        }
        if (!empty($childStock)) {
            $childAssets[] = array(
                'text' => 'Stock Inventories',
                'expanded' => false,
                'children' => $childStock
            );
        }
        if (!empty($childAssets)) {
            $childBalanceSheet[] = array(
                'text' => 'Asset',
                'expanded' => false,
                'children' => $childAssets
            );
        }
        if (in_array(337, $this->security_role) && !Users::is_audit()) {
            $childoLiabilities[] = array(
                'text' => 'Loan',
                'class' => 'jun.ReportLoan',
                'leaf' => true
            );
        }
        if (in_array(324, $this->security_role) && !Users::is_audit()) {
            $childoLiabilities[] = array(
                'text' => 'Account Payable',
                'class' => 'jun.ReportKartuHutang',
                'leaf' => true
            );
        }
        if (in_array(338, $this->security_role) && !Users::is_audit()) {
            $childoLiabilities[] = array(
                'text' => 'Sales Tax',
                'class' => 'jun.ReportSalesTax',
                'leaf' => true
            );
        }
        if (in_array(339, $this->security_role) && !Users::is_audit()) {
            $childoLiabilities[] = array(
                'text' => 'Tax',
                'class' => 'jun.ReportTax',
                'leaf' => true
            );
        }
        if (!empty($childoLiabilities)) {
            $childBalanceSheet[] = array(
                'text' => 'Liabilities',
                'expanded' => false,
                'children' => $childoLiabilities
            );
        }
        if (in_array(340, $this->security_role) && !Users::is_audit()) {
            $childooEquity[] = array(
                'text' => 'Capital',
                'class' => 'jun.ReportCapital',
                'leaf' => true
            );
        }
        if (in_array(341, $this->security_role) && !Users::is_audit()) {
            $childooEquity[] = array(
                'text' => 'Retained Earning',
                'class' => 'jun.ReportRetainedEarning',
                'leaf' => true
            );
        }
        if (!empty($childooEquity)) {
            $childBalanceSheet[] = array(
                'text' => 'Equity',
                'expanded' => false,
                'children' => $childooEquity
            );
        }
        if (!empty($childBalanceSheet)) {
            $child[] = array(
                'text' => 'Balance Sheet',
                'expanded' => false,
                'children' => $childBalanceSheet
            );
        }
        if (in_array(318, $this->security_role) && !Users::is_audit()) {
            $childProfitLoss[] = array(
                'text' => 'Profit Lost Report',
                'class' => 'jun.ReportLabaRugi',
                'leaf' => true
            );
        }
        if (in_array(322, $this->security_role) && !Users::is_audit()) {
            $childSales[] = array(
                'text' => 'Sales Price',
                'class' => 'jun.ReportSalesPrice',
                'leaf' => true
            );
        }
        if (in_array(331, $this->security_role) && !Users::is_audit()) {
            $childSales[] = array(
                'text' => 'Sales and Sales Return by Group',
                'class' => 'jun.ReportSalesNReturnDetails',
                'leaf' => true
            );
        }
        if (in_array(301, $this->security_role) && !Users::is_audit()) {
            $childSales[] = array(
                'text' => 'Sales Details by Group',
                'class' => 'jun.ReportSalesDetails',
                'leaf' => true
            );
        }
        if (in_array(300, $this->security_role) && !Users::is_audit()) {
            $childSales[] = array(
                'text' => 'Sales Summary by Group',
                'class' => 'jun.ReportSalesSummary',
                'leaf' => true
            );
        }
        if (in_array(313, $this->security_role) && !Users::is_audit()) {
            $childSales[] = array(
                'text' => 'Sales Summary',
                'class' => 'jun.ReportSalesSummaryReceipt',
                'leaf' => true
            );
        }
        if (in_array(316, $this->security_role) && !Users::is_audit()) {
            $childSales[] = array(
                'text' => 'Sales Details',
                'class' => 'jun.ReportSalesSummaryReceiptDetails',
                'leaf' => true
            );
        }
        if (in_array(310, $this->security_role) && !Users::is_audit()) {
            $childSales[] = array(
                'text' => 'Sales Return Details by Group',
                'class' => 'jun.ReportReturSalesDetails',
                'leaf' => true
            );
        }
        if (in_array(309, $this->security_role) && !Users::is_audit()) {
            $childSales[] = array(
                'text' => 'Return Sales Summary',
                'class' => 'jun.ReportReturSalesSummary',
                'leaf' => true
            );
        }
        if (in_array(321, $this->security_role) && !Users::is_audit()) {
            $childSales[] = array(
                'text' => 'Payments',
                'class' => 'jun.ReportPayments',
                'leaf' => true
            );
        }
        if (!empty($childSales)) {
            $childProfitLoss[] = array(
                'text' => 'Sales',
                'expanded' => false,
                'children' => $childSales
            );
        }
        if (in_array(330, $this->security_role) && !Users::is_audit()) {
            $childPurchases[] = array(
                'text' => 'Purchase Price',
                'class' => 'jun.ReportPurchasePrice',
                'leaf' => true
            );
        }
        if (in_array(342, $this->security_role) && !Users::is_audit()) {
            $childPurchases[] = array(
                'text' => 'Purchase and Purchase Return by Group',
                'class' => 'jun.ReportPurchasenReturnGroup',
                'leaf' => true
            );
        }
        if (in_array(343, $this->security_role) && !Users::is_audit()) {
            $childPurchases[] = array(
                'text' => 'Purchase',
                'class' => 'jun.ReportPurchase',
                'leaf' => true
            );
        }
        if (in_array(344, $this->security_role) && !Users::is_audit()) {
            $childPurchases[] = array(
                'text' => 'Purchase Return',
                'class' => 'jun.ReportPurchaseReturn',
                'leaf' => true
            );
        }
        if (in_array(345, $this->security_role) && !Users::is_audit()) {
            $childPurchases[] = array(
                'text' => 'Debt Payment',
                'class' => 'jun.ReportDebtPayment',
                'leaf' => true
            );
        }
        if (!empty($childPurchases)) {
            $childProfitLoss[] = array(
                'text' => 'Purchases',
                'expanded' => false,
                'children' => $childPurchases
            );
        }
        if (in_array(346, $this->security_role) && !Users::is_audit()) {
            $childPurchases[] = array(
                'text' => 'List of Expenses Accounts',
                'class' => 'jun.ReportListofExpensesaccounts',
                'leaf' => true
            );
        }
        if (!empty($childExpenses)) {
            $childProfitLoss[] = array(
                'text' => 'Expenses',
                'expanded' => false,
                'children' => $childExpenses
            );
        }
        if (!empty($childProfitLoss)) {
            $child[] = array(
                'text' => 'Profit and Loss',
                'expanded' => false,
                'children' => $childProfitLoss
            );
        }
        if (!empty($childCashFlow)) {
            $child[] = array(
                'text' => 'Cash Flow',
                'expanded' => false,
                'children' => $childCashFlow
            );
        }
        if (in_array(314, $this->security_role) && !Users::is_audit()) {
            $childAcc[] = array(
                'text' => 'General Ledger',
                'class' => 'jun.ReportGeneralLedger',
                'leaf' => true
            );
        }
        if (in_array(315, $this->security_role) && !Users::is_audit()) {
            $childAcc[] = array(
                'text' => 'General Journal',
                'class' => 'jun.ReportGeneralJournal',
                'leaf' => true
            );
        }
        if (!empty($childAcc)) {
            $child[] = array(
                'text' => 'Accounting',
                'expanded' => false,
                'children' => $childAcc
            );
        }
        if (in_array(303, $this->security_role) && !Users::is_audit()) {
            $childInvSupp[] = array(
                'text' => 'Inventory Card',
                'class' => 'jun.ReportInventoryCard',
                'leaf' => true
            );
        }
        if (in_array(302, $this->security_role) && !Users::is_audit()) {
            $childInvSupp[] = array(
                'text' => 'Inventory Movements Summary',
                'class' => 'jun.ReportInventoryMovements',
                'leaf' => true
            );
        }
        if (in_array(333, $this->security_role) && !Users::is_audit()) {
            $childInvSupp[] = array(
                'text' => 'Supplies Inventory Movements',
                'class' => 'jun.ReportInventoryMovementsClinical',
                'leaf' => true
            );
        }
        if (in_array(347, $this->security_role) && !Users::is_audit()) {
            $childInvSupp[] = array(
                'text' => 'Supplies Card',
                'class' => 'jun.ReportSuppliesCard',
                'leaf' => true
            );
        }
        if (!empty($childInvSupp)) {
            $child[] = array(
                'text' => 'Inventories & Supplies',
                'expanded' => false,
                'children' => $childInvSupp
            );
        }
        if (in_array(306, $this->security_role) && !Users::is_audit()) {
            $childCust[] = array(
                'text' => 'New Customers',
                'class' => 'jun.ReportNewCustomers',
                'leaf' => true
            );
        }
        if (in_array(334, $this->security_role) && !Users::is_audit()) {
            $child[] = array(
                'text' => 'Customers Birthday List',
                'class' => 'jun.ReportBirthDayCustomers',
                'leaf' => true
            );
        }
        if (in_array(332, $this->security_role) && !Users::is_audit()) {
            $childCust[] = array(
                'text' => 'Category Customers',
                'class' => 'jun.ReportCategoryCustomers',
                'leaf' => true
            );
        }
        if (in_array(325, $this->security_role) && !Users::is_audit()) {
            $childCust[] = array(
                'text' => 'Chart Customers Attendance',
                'class' => 'jun.ChartCustAtt',
                'leaf' => true
            );
        }
        if (in_array(326, $this->security_role) && !Users::is_audit()) {
            $childCust[] = array(
                'text' => 'Chart New Customers',
                'class' => 'jun.ChartNewCustomers',
                'leaf' => true
            );
        }
        if (in_array(328, $this->security_role) && !Users::is_audit()) {
            $childCust[] = array(
                'text' => 'Chart Top Customers',
                'class' => 'jun.ChartTopCust',
                'leaf' => true
            );
        }
        if (!empty($childCust)) {
            $childMarketing[] = array(
                'text' => 'Customers',
                'expanded' => false,
                'children' => $childCust
            );
        }
        if (in_array(322, $this->security_role) && !Users::is_audit()) {
            $childSalesPrice[] = array(
                'text' => 'Sales Price',
                'class' => 'jun.ReportSalesPrice',
                'leaf' => true
            );
        }
        if (in_array(330, $this->security_role) && !Users::is_audit()) {
            $childSalesPrice[] = array(
                'text' => 'Purchase Price',
                'class' => 'jun.ReportPurchasePrice',
                'leaf' => true
            );
        }
        if (in_array(331, $this->security_role) && !Users::is_audit()) {
            $childSalesPrice[] = array(
                'text' => 'Sales and Sales Return by Group',
                'class' => 'jun.ReportSalesNReturnDetails',
                'leaf' => true
            );
        }
        if (in_array(301, $this->security_role) && !Users::is_audit()) {
            $childSalesPrice[] = array(
                'text' => 'Sales Details by Group',
                'class' => 'jun.ReportSalesDetails',
                'leaf' => true
            );
        }
        if (in_array(310, $this->security_role) && !Users::is_audit()) {
            $childSalesPrice[] = array(
                'text' => 'Sales Return Details by Group',
                'class' => 'jun.ReportReturSalesDetails',
                'leaf' => true
            );
        }
        if (!empty($childSalesPrice)) {
            $childMarketing[] = array(
                'text' => 'Sales and Pricing',
                'expanded' => false,
                'children' => $childSalesPrice
            );
        }
        if (in_array(348, $this->security_role) && !Users::is_audit()) {
            $childPromo[] = array(
                'text' => 'Member Get Member',
                'class' => 'jun.ReportMemberGetMember',
                'leaf' => true
            );
        }
        if (in_array(349, $this->security_role) && !Users::is_audit()) {
            $childPromo[] = array(
                'text' => 'My Special Day',
                'class' => 'jun.ReportMySpecialDay',
                'leaf' => true
            );
        }
        if (in_array(350, $this->security_role) && !Users::is_audit()) {
            $childPromo[] = array(
                'text' => 'Events & Sponsorship',
                'class' => 'jun.ReportEventsSponsorship',
                'leaf' => true
            );
        }
        if (in_array(351, $this->security_role) && !Users::is_audit()) {
            $childPromo[] = array(
                'text' => 'Treatment Packages',
                'class' => 'jun.ReportTreatmentPackages',
                'leaf' => true
            );
        }
        if (in_array(352, $this->security_role) && !Users::is_audit()) {
            $childPromo[] = array(
                'text' => 'Product Packages',
                'class' => 'jun.ReportProductPackages',
                'leaf' => true
            );
        }
        if (!empty($childPromo)) {
            $childMarketing[] = array(
                'text' => 'Promotion',
                'expanded' => false,
                'children' => $childPromo
            );
        }
        if (in_array(327, $this->security_role) && !Users::is_audit()) {
            $childProduct[] = array(
                'text' => 'Chart Sales Group',
                'class' => 'jun.ChartSalesGrup',
                'leaf' => true
            );
        }
        if (in_array(329, $this->security_role) && !Users::is_audit()) {
            $childProduct[] = array(
                'text' => 'Chart Top Sales Group',
                'class' => 'jun.ChartTopSalesGrup',
                'leaf' => true
            );
        }
        if (!empty($childProduct)) {
            $childMarketing[] = array(
                'text' => 'Product',
                'expanded' => false,
                'children' => $childProduct
            );
        }
        if (!empty($childMarketing)) {
            $child[] = array(
                'text' => 'Marketing Analysis',
                'expanded' => false,
                'children' => $childMarketing
            );
        }
        if (in_array(304, $this->security_role) && !Users::is_audit()) {
            $child[] = array(
                'text' => 'Beautician Services Summary',
                'class' => 'jun.ReportBeautySummary',
                'leaf' => true
            );
        }
        if (in_array(305, $this->security_role) && !Users::is_audit()) {
            $child[] = array(
                'text' => 'Beautician Services Details',
                'class' => 'jun.ReportBeautyDetails',
                'leaf' => true
            );
        }
        if (in_array(311, $this->security_role) && !Users::is_audit()) {
            $child[] = array(
                'text' => 'Doctors Services Summary',
                'class' => 'jun.ReportDokterSummary',
                'leaf' => true
            );
        }
        if (in_array(312, $this->security_role) && !Users::is_audit()) {
            $child[] = array(
                'text' => 'Doctors Services Details',
                'class' => 'jun.ReportDokterDetails',
                'leaf' => true
            );
        }
        if (in_array(307, $this->security_role) && !Users::is_audit()) {
            $child[] = array(
                'text' => 'Tenders',
                'class' => 'jun.ReportTender',
                'leaf' => true
            );
        }
        if (in_array(308, $this->security_role) && !Users::is_audit()) {
            $child[] = array(
                'text' => 'Daily Report',
                'class' => 'jun.ReportLaha',
                'leaf' => true
            );
        }
        return $child;
    }
    function getAdministration($child)
    {
        if (empty($child)) {
            return array();
        }
        return array(
            'text' => 'Administration',
            'expanded' => false,
            'children' => $child
        );
    }
    function getChildAdministration()
    {
        $child = array();
        if (in_array(400, $this->security_role) && !Users::is_audit()) {
            $child[] = array(
                'text' => 'User Management',
                'class' => 'jun.UsersGrid',
                'leaf' => true
            );
        }
        if (in_array(401, $this->security_role) && !Users::is_audit()) {
            $child[] = array(
                'text' => 'Security Roles',
                'class' => 'jun.SecurityRolesGrid',
                'leaf' => true
            );
        }
        if (in_array(402, $this->security_role) && !Users::is_audit()) {
            $child[] = array(
                'text' => 'Backup / Restore',
                'class' => 'jun.BackupRestoreWin',
                'leaf' => true
            );
        }
        if (in_array(403, $this->security_role) && !Users::is_audit()) {
            $child[] = array(
                'text' => 'Import',
                'class' => 'jun.ImportXlsx',
                'leaf' => true
            );
        }
        return $child;
    }
    function getGeneral()
    {
        $username = Yii::app()->user->name;
        $child = array();
        if (in_array(000, $this->security_role) && !Users::is_audit()) {
            $child[] = array(
                'text' => 'Change Password',
                'class' => 'jun.PasswordWin',
                'leaf' => true
            );
        }
        if (in_array(001, $this->security_role) && !Users::is_audit()) {
            $child[] = array(
                'text' => "Logout ($username)",
                'class' => 'logout',
                'leaf' => true
            );
        }
        return $child;
    }
    public function get_menu()
    {
        $data = array();
        $master = self::getMaster(self::getChildMaster());
        if (!empty($master)) {
            $data[] = $master;
        }
        $trans = self::getTransaction(self::getChildTransaction());
        if (!empty($trans)) {
            $data[] = $trans;
        }
        $report = self::getReport(self::getChildReport());
        if (!empty($report)) {
            $data[] = $report;
        }
        $adm = self::getAdministration(self::getChildAdministration());
        if (!empty($adm)) {
            $data[] = $adm;
        }
        $username = Yii::app()->user->name;
        if (in_array(000, $this->security_role) && !Users::is_audit()) {
            $data[] = array(
                'text' => 'Change Password',
                'class' => 'jun.PasswordWin',
                'leaf' => true
            );
        }
        if (in_array(001, $this->security_role) && !Users::is_audit()) {
            $data[] = array(
                'text' => "Logout ($username)",
                'class' => 'logout',
                'leaf' => true
            );
        }
        return CJSON::encode($data);
    }
}
