<?php
/**
 * Created by PhpStorm.
 * User: LENOVO
 * Date: 27/12/2014
 * Time: 10:30
 */

class GL {
    private $_total_amount = 0;
    private $detil = array();
    public function add_gl($type, $trans_id, $date_, $ref, $account, $memo_, $comment_,
        $amount, $cf, $store = null)
    {
        $person_id = Yii::app()->user->getId();
        $is_bank_to = $this->is_bank_account($account);
        $this->add_gl_trans($type, $trans_id, $date_, $account, $memo_, $amount,
            $person_id, $cf, $store);
        if ($is_bank_to) {
            $bank = Bank::model()->find("account_code = :account_code",
                array(":account_code" => $account));
            if ($bank == null) {
                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Bank transaction')));
            }
            $this->add_bank_trans($type, $trans_id, $bank->bank_id, $ref, $date_,
                $amount, $person_id, $store);
        }
        if (strlen($comment_) > 0) {
            $this->add_comments($type, $trans_id, $date_, $comment_);
        }
        $this->_total_amount += $amount;
    }
    public function is_bank_account($account_code)
    {
        $comm = Yii::app()->db->createCommand("SELECT account_code from nscc_chart_master WHERE
        account_code = :account_code and (kategori = :kas or kategori = :bank)");
        $bank_act = $comm->queryAll(true, array(
            ":account_code" => $account_code,
            ':kas' => COA_GRUP_KAS,
            ':bank' => COA_GRUP_BANK
        ));
        return count($bank_act) > 0;
    }
    public function add_gl_trans($type, $trans_id, $date_, $account, $memo_, $amount, $person_id, $cf, $store = null)
    {
        $gl_trans = new GlTrans();
        $gl_trans->type = $type;
        $gl_trans->type_no = $trans_id;
        $gl_trans->tran_date = $date_;
        $gl_trans->account_code = $account;
        $gl_trans->memo_ = $memo_;
        $gl_trans->id_user = $person_id;
        $gl_trans->amount = $amount;
        $gl_trans->cf = $cf;
        $gl_trans->store = $store;
        if (!$gl_trans->save())
            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'General Ledger')) . CHtml::errorSummary($gl_trans));
    }
    public function add_bank_trans($type, $trans_no, $bank_act, $ref, $date_,
        $amount, $person_id, $store = null)
    {
        $bank_trans = new BankTrans;
        $bank_trans->type_ = $type;
        $bank_trans->trans_no = $trans_no;
        $bank_trans->bank_id = $bank_act;
        $bank_trans->ref = $ref;
        $bank_trans->tgl = $date_;
        $bank_trans->amount = $amount;
        $bank_trans->id_user = $person_id;
        $bank_trans->store = $store;
        if (!$bank_trans->save())
            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Bank transaction')) . CHtml::errorSummary($bank_trans));
    }
    public function validate()
    {
//        $this->_total_amount = array_sum(array_column($this->detil,'amount'));
        if (round($this->_total_amount,2) != 0.00)
            throw new Exception("Gagal menyimpan jurnal karena tidak balance. Total GL = " .
                number_format($this->_total_amount, 2));
    }
    public function add_comments($type, $type_no, $date_, $memo_)
    {
        if ($memo_ != null && $memo_ != "") {
            $comment = new Comments();
            $comment->type = $type;
            $comment->type_no = $type_no;
            $comment->date_ = $date_;
            $comment->memo_ = $memo_;
            if (!$comment->save())
                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Comments')) . CHtml::errorSummary($comment));
        }
    }
}