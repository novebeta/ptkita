<?php
/**
 * Created by PhpStorm.
 * User: novebeta
 * Date: 6/25/14
 * Time: 11:28 AM
 */
class PrintReceipt extends BasePrint
{
    /** @var  $s Salestrans */
    private $s;
    function PrintReceipt($s)
    {
//        $s = new Salestrans();
        $this->s = $s;
    }
    public function buildTxt()
    {
        $newLine = "\r\n";
        $raw = parent::setCenter(SysPrefs::get_val('receipt_header0'));
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_header1'));
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_header2'));
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_header3'));
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_header4'));
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_header5'));
        $raw .= $newLine;
        $raw .= parent::fillWithChar("=");
        $raw .= $newLine;
        $raw .= parent::addHeaderSales("No. Receipt", $this->s->doc_ref);
        $raw .= $newLine;
        $raw .= parent::addHeaderSales("Date", sql2date($this->s->doc_ref, "dd-MMM-yyyy"));
        $raw .= $newLine;
        $raw .= parent::addHeaderSales("Customer Code", $this->s->customer->no_customer);
        $raw .= $newLine;
        $raw .= parent::addHeaderSales("Customer Name", $this->s->customer->nama_customer);
        $raw .= $newLine;
        $user = Users::model()->findByPk(Yii::app()->getUser()->id);
        $raw .= parent::addHeaderSales("Cashier", $user->name);
        $raw .= $newLine;
        $raw .= parent::fillWithChar("-");
        $raw .= $newLine;
        $raw .= "Item Code        Item Name           QTY            TOTAL";
        $raw .= $newLine;
        $raw .= parent::fillWithChar("-");
        $raw .= $newLine;
        $disc = 0;
        foreach ($this->s->salestransDetails as $d) {
            $disc += $d->discrp1;
            $raw .= parent::addItemCodeReceipt($d->barang->kode_barang,
                number_format($d->qty), number_format(($d->bruto), 2) . ($d->vatrp > 0 ? "*" : ""));
            $raw .= $newLine;
            $raw .= parent::addItemNameReceipt($d->barang->nama_barang, 46);
            $raw .= $newLine;
            if ($d->discrp != 0) {
                $raw .= parent::addItemDiscReceipt($d->disc != 0 ? number_format($d->disc, 2) . ' %' : '',
                    number_format(-($d->discrp), 2));
                $raw .= $newLine;
            }
        }
        $raw .= $newLine;
        $raw .= parent::fillWithChar("-");
        $raw .= $newLine;
        $raw .= $newLine;
        $raw .= parent::addLeftRight("Sub Total", number_format(($this->s->bruto), 2));
        $raw .= $newLine;
        $raw .= parent::addLeftRight("Discount", number_format(($this->s->total_discrp1), 2));
        $raw .= $newLine;
        $raw .= parent::addLeftRight("VAT", number_format(($this->s->vat), 2));
        $raw .= $newLine;
        $raw .= parent::addLeftRight("Rounding", number_format(($this->s->rounding), 2));
        $raw .= $newLine;
        $raw .= parent::addLeftRight("Amount Due", number_format(($this->s->total), 2));
        $raw .= $newLine;
        $raw .= parent::addLeftRight("", self::fillWithChar("-", 17));
        $raw .= $newLine;
        $raw .= $newLine;
        foreach ($this->s->payments as $pay) {
            $raw .= parent::addLeftRight($pay->bank->nama_bank, number_format(($pay->amount), 2));
            $raw .= $newLine;
        }
        if ($this->s->kembali > 0) {
            $raw .= parent::addLeftRight("CHANGE", number_format(($this->s->kembali), 2));
            $raw .= $newLine;
        }
        $raw .= parent::fillWithChar("=");
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_footer0'));
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_footer1'));
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_footer2'));
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_footer3'));
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_footer4'));
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_footer5'));
        $raw .= $newLine;
        U::save_file(ReportPath.$this->s->doc_ref.'.txt',$raw);
        return $raw;
    }
}