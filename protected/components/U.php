<?php
class U
{
    static function get_report_detil_nota($from, $to, $jnstrans, $sales_id, $supp_id, $pasar_id, $konsumen_id)
    {
        $select [] = "CONCAT(pp.doc_ref,' ') `No Faktur`";
        if ($sales_id !== null) {
            $select [] = 'ps1.salesman_name Salesman';
//            $group[] = 'ps.salesman_name';
        }
        $select [] = "pp.tgl Tanggal";
        if ($konsumen_id !== null) {
            $select [] = 'pk.konsumen_name Nama Konsumen';
        }
        $select [] = "ptk.type_konsumen_name Tipe";
        if ($pasar_id !== null) {
            $select [] = 'pp1.pasar_name Pasar';
        }
        $select [] = "pa.area_name Area";
        $select [] = "CONCAT(pb.barcode,' ') Kode";
        $select [] = "pb.barang_name Nama Barang";
        if ($supp_id !== null) {
            $select [] = 'ps.supplier_name Suplier';
        }
        $select [] = "pdp.jml Kuantitas Penjualan";
        $select [] = "pdp.sat Satuan";
        $select [] = "pdp.pcs 'Kuantitas Jual (pieces)'";
        $select [] = "IF(pb.bonus,'B','-') Bonus";
        $select [] = "CONCAT(pdp.disc1,'%') `Disc 1`";
        $select [] = "CONCAT(pdp.disc2,'%') `Disc 2`";
        $select [] = "pdp.pot Potongan";
        $select [] = "pdp.nominal Nominal";
        $select [] = "pp.total";
        $query = Yii::app()->db->createCommand();
        $query->select($select);
        $query->from = '{{penjualan}} pp';
        $query->join('{{detil_penjualan}} pdp', '( pp.penjualan_id = pdp.penjualan_id  )');
        $query->join('{{barang}} pb', '( pdp.barang_id = pb.barang_id  )  ');
        $query->join('{{supplier}} ps', '( pb.supplier_id = ps.supplier_id  ) ');
        $query->join('{{salesman}} ps1', '( pp.salesman_id = ps1.salesman_id  )');
        $query->join('{{konsumen}} pk', '( pp.konsumen_id = pk.konsumen_id  ) ');
        $query->leftJoin('{{type_konsumen}} ptk', '( pk.type_konsumen_id = ptk.type_konsumen_id  )');
        $query->leftJoin('{{pasar}} pp1', '( pk.pasar_id = pp1.pasar_id  )  ');
        $query->join('{{area}} pa', '( pk.area_id = pa.area_id  )  ');
        $query->order("pp.doc_ref ASC");
        $query->andWhere('pp.tgl >= :from', array(':from' => $from));
        $query->andWhere('pp.tgl <= :to', array(':to' => $to));
        if ($sales_id != null) {
            $query->andWhere('pp.salesman_id = :sales_id', array(':sales_id' => $sales_id));
        }
        if ($supp_id != null) {
            $query->andWhere('pb.supplier_id = :supp_id', array(':supp_id' => $supp_id));
        }
        if ($konsumen_id != null) {
            $query->andWhere('pp.konsumen_id = :konsumen_id', array(':konsumen_id' => $konsumen_id));
        }
        if ($pasar_id != null) {
            $query->andWhere('pk.pasar_id = :pasar_id', array(':pasar_id' => $pasar_id));
        }
        if ($jnstrans != null) {
            $total = $jnstrans == 'P' ? "pp.total > 0" : "pp.total < 0";
            $query->andWhere($total);
        }
        return $query->queryAll(true);
    }
    static function get_kelola_stok()
    {
        $select [] = "DISTINCT(trans_no) trans_no";
        $select [] = "tran_date";
        $select [] = "reference";
        $select [] = "memo_ note";
        $query = Yii::app()->db->createCommand();
        $query->select($select);
        $query->from = "{{stock_moves}} psm";
        $query->leftJoin("{{comments}} c", '(psm.type = c.type) AND (psm.trans_no = c.type_no) ');
        $query->andWhere('psm.type = :type', array(':type' => KELOLASTOK));
        return $query->queryAll(true);
    }
    static function get_selisih_stok()
    {
        $select [] = "DISTINCT(trans_no) trans_no";
        $select [] = "tran_date";
        $select [] = "reference";
        $select [] = "memo_ note";
        $query = Yii::app()->db->createCommand();
        $query->select($select);
        $query->from = "{{stock_moves}} psm";
        $query->leftJoin("{{comments}} c", '(psm.type = c.type) AND (psm.trans_no = c.type_no) ');
        $query->andWhere('psm.type = :type', array(':type' => SELISIHSTOK));
        return $query->queryAll(true);
    }
    static function get_report_biaya_sales($from, $to, $salesman_id)
    {
        return Yii::app()->db->createCommand("SELECT pbs.tgl Tanggal, pbs.bbm BBM, pbs.makan Makan,
    pbs.parkir Parkir, IFNULL((SELECT SUM(pbsl.amount) FROM {{biaya_sales_lain}} pbsl
    WHERE pbsl.biaya_sales_id = pbs.biaya_sales_id),0) `Lain-lain`
    FROM {{biaya_sales}} pbs
    WHERE pbs.tgl >= :FROM AND pbs.tgl <= :TO AND pbs.salesman_id = :sales_id
    ORDER BY pbs.tgl")
            ->queryAll(true, array(
                ':from' => $from,
                ':to' => $to,
                ':sales_id' => $salesman_id
            ));
    }
    static function get_retur_jual()
    {
        return Yii::app()->db->createCommand("SELECT pp.penjualan_id, pp.salesman_id, pp.konsumen_id, pp.doc_ref,
        pp.tgl, pp.tempo, -pp.sub_total sub_total, -pp.total total, -pp.uang_muka uang_muka,
        pp.no_bg_cek, -pp.sisa_tagihan sisa_tagihan, pp.lunas,
        pp.final, pp.id_user,pp.parent, -pp.bruto bruto
        FROM {{penjualan}} pp
        WHERE pp.total <= 0")
            ->queryAll();
    }
    static function get_retur_jual_detil($id)
    {
        return Yii::app()->db->createCommand("SELECT pdp.detil_penjualan, pdp.penjualan_id, pdp.barang_id, pdp.sat,
        -pdp.jml jml, pdp.price, pdp.disc1, pdp.disc2, pdp.pot, -pdp.nominal nominal, -pdp.pcs pcs
        FROM {{detil_penjualan}} pdp
        WHERE pdp.penjualan_id = :penjualan_id")
            ->queryAll(true, array(':penjualan_id' => $id));
    }
    // ------------------------------------------------ Void ----------------------------------------------------------------
    static function get_voided($type)
    {
        $void = Yii::app()->db->createCommand()->select('id')->from('mt_voided')->where('type=:type',
            array(
                ':type' => $type
            ))->queryColumn();
        return $void;
    }
    static function get_max_type_no($type)
    {
        $type_no = app()->db->createCommand()->select("MAX(type_no)")
            ->from("{{gl_trans}}")->where('type=:type', array(':type' => $type))->queryScalar();
        return $type_no == false ? 0 : $type_no;
    }
    static function get_max_type_no_stock($type)
    {
        $type_no = app()->db->createCommand()->select("MAX(trans_no)")
            ->from("{{stock_moves}}")->where('type=:type', array(':type' => $type))->queryScalar();
        return $type_no == false ? 0 : $type_no;
    }
    // --------------------------------------------- Bank Trans -------------------------------------------------------------
    static function get_next_trans_no_bank_trans($type, $store = STOREID)
    {
//        $db = BankTrans::model()->getDbConnection();
        $total = app()->db->createCommand(
            "SELECT MAX(trans_no)
            FROM nscc_bank_trans WHERE type_= :type AND store = :store")
            ->queryScalar(array(':type' => $type, ':store' => $store));
        if ($total === false) {
            $total = 0;
        } else {
            $total++;
        }
        return $total;
    }
    static function get_next_trans_saldo_awal()
    {
        $db = GlTrans::model()->getDbConnection();
        $total = $db->createCommand(
            "SELECT MAX(type_no)
FROM mt_gl_trans WHERE type=" . SALDO_AWAL)->queryScalar();
        return $total == null ? 0 : $total + 1;
    }
    static function get_ledger_trans($from, $to)
    {
        $rows = Yii::app()->db->createCommand(
            "SELECT
            mt_gl_trans.tran_date,
            mt_gl_trans.type,
            mt_gl_trans.type_no,
            refs.reference,
            SUM(IF(mt_gl_trans.amount>0, mt_gl_trans.amount,0)) as amount,
            users.user_id
            FROM
            mt_gl_trans
            LEFT JOIN mt_refs as refs ON
            (mt_gl_trans.type=refs.type AND mt_gl_trans.type_no=refs.type_no),users
            WHERE mt_gl_trans.tran_date BETWEEN '$from' AND '$to'
            GROUP BY mt_gl_trans.tran_date,mt_gl_trans.type,
            mt_gl_trans.type_no,mt_gl_trans.users_id
            ")->queryAll();
        return $rows;
    }
    static function get_general_ledger_trans($from, $to)
    {
        $rows = Yii::app()->db->createCommand(
            "SELECT
            mt_gl_trans.type,
            mt_gl_trans.type_no,
            mt_gl_trans.tran_date,
            CONCAT(mt_chart_master.account_code,' ',mt_chart_master.account_name) as account,
            mt_gl_trans.amount
            FROM
            mt_gl_trans
            INNER JOIN mt_chart_master ON mt_gl_trans.account = mt_chart_master.account_code
            WHERE mt_gl_trans.tran_date BETWEEN '$from' AND '$to'
            ")->queryAll();
        return $rows;
    }
    static function get_bank_trans_view()
    {
        global $systypes_array;
        $bfw = U::get_balance_before_for_bank_account($_POST['trans_date_mulai'],
            $_POST['bank_act']);
        $arr['data'][] = array(
            'type' => 'Saldo Awal - ' . sql2date($_POST['trans_date_mulai']),
            'ref' => '',
            'tgl' => '',
            'debit' => $bfw >= 0 ? number_format($bfw, 2) : '',
            'kredit' => $bfw < 0 ? number_format($bfw, 2) : '',
            'neraca' => '',
            'person' => ''
        );
        $credit = $debit = 0;
        $running_total = $bfw;
        if ($bfw > 0) {
            $debit += $bfw;
        } else {
            $credit += $bfw;
        }
        $result = U::get_bank_trans_for_bank_account($_POST['bank_act'],
            $_POST['trans_date_mulai'], $_POST['trans_date_sampai']);
        foreach ($result as $myrow) {
            $running_total += $myrow->amount;
            $jemaat = get_jemaat_from_user_id($myrow->users_id);
            $arr['data'][] = array(
                'type' => $systypes_array[$myrow->type],
                'ref' => $myrow->ref,
                'tgl' => sql2date($myrow->trans_date),
                'debit' => $myrow->amount >= 0 ? number_format($myrow->amount, 2)
                    : '',
                'kredit' => $myrow->amount < 0 ? number_format(-$myrow->amount,
                    2) : '',
                'neraca' => number_format($running_total, 2),
                'person' => $jemaat->real_name
            );
            if ($myrow->amount > 0) {
                $debit += $myrow->amount;
            } else {
                $credit += $myrow->amount;
            }
        }
        $arr['data'][] = array(
            'type' => 'Saldo Akhir - ' . sql2date($_POST['trans_date_sampai']),
            'ref' => '',
            'tgl' => '',
            'debit' => $running_total >= 0 ? number_format($running_total, 2) : '',
            'kredit' => $running_total < 0 ? number_format(-$running_total, 2) : '',
            'neraca' => '', // number_format($debit + $credit, 2),
            'person' => ''
        );
        return $arr;
    }
    static function get_balance_before_for_bank_account($from, $bank_account, $store = "")
    {
        $where = '';
        $param = array(':tgl' => $from, ':bank_id' => $bank_account);
        if ($store != null) {
            $where = "AND nbt.store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("SELECT
        IFNULL(Sum(nbt.amount),0) AS total
        FROM nscc_bank_trans nbt
        WHERE nbt.visible =1 AND DATE(nbt.tgl) < :tgl AND nbt.bank_id = :bank_id $where");
        return $comm->queryScalar($param);
    }
    static function get_bank_trans_for_bank_account($bank_account, $from, $to)
    {
        $criteria = new CDbCriteria();
        if ($bank_account != null) {
            $criteria->addCondition("bank_act =" . $bank_account);
        }
        $criteria->addBetweenCondition("trans_date", $from, $to);
        $criteria->order = "trans_date, id";
        return BankTrans::model()->findAll($criteria);
    }
    static function get_prefs($name)
    {
        $criteria = new CDbCriteria();
        if ($name != null) {
            $criteria->addCondition("name ='$name'");
        } else {
            return null;
        }
        $prefs = SysPrefs::model()->find($criteria);
        return $prefs->value;
    }
    static function get_act_code_from_bank_act($bank_act)
    {
        $bank = Bank::model()->findByPk($bank_act);
        if ($bank != null) {
            return $bank->account_code;
        } else {
            return false;
        }
    }
    static function get_sql_for_journal_inquiry($from, $to)
    {
        $rows = Yii::app()->db->createCommand()->select(
            "gl_trans.tran_date,gl_trans.type,refs.reference,Sum(IF(amount>0, amount,0)) AS amount,
    comments.memo_,gl_trans.person_id,gl_trans.type_no")->from('gl_trans')->join(
            'comments',
            'gl_trans.type = comments.type AND gl_trans.type_no = comments.type_no')->Join(
            'refs',
            'gl_trans.type = refs.type AND gl_trans.type_no = refs.type_no')->where(
            "gl_trans.amount!=0 and gl_trans.tran_date >= '$from'
?  ?          AND gl_trans.tran_date <= '$to'")->group('gl_trans.type, gl_trans.type_no')->order(
            'tran_date desc')->queryAll();
        return $rows;
    }
    static function add_gl(
        $type,
        $trans_id,
        $date_,
        $ref,
        $account,
        $memo_,
        $comment_,
        $amount,
        $cf,
        $store = null
    ) {
        $person_id = Yii::app()->user->getId();
        $is_bank_to = self::is_bank_account($account);
        self::add_gl_trans($type, $trans_id, $date_, $account, $memo_, $amount,
            $person_id, $cf, $store);
        if ($is_bank_to) {
            $bank = Bank::model()->find("account_code = :account_code",
                array(":account_code" => $account));
            if ($bank == null) {
                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Bank transaction')));
            }
            self::add_bank_trans($type, $trans_id, $bank->bank_id, $ref, $date_,
                $amount, $person_id, $store);
        }
        if (strlen($comment_) > 0) {
            self::add_comments($type, $trans_id, $date_, $comment_);
        }
        // return $trans_id;
    }
    // --------------------------------------------- Gl Trans ---------------------------------------------------------------
    static function is_bank_account($account_code)
    {
//        $criteria = new CDbCriteria();
//        $criteria->addCondition("account_code = :account_code");
//        $criteria->addCondition("kategori = :kas");
//        $criteria->addCondition("kategori = :bank", 'OR');
//        $criteria->params = array(
//            ":account_code" => $account_code,
//            ':kas' => SysPrefs::get_val('coa_grup_kas'),
//            ':bank' => SysPrefs::get_val('coa_grup_bank')
//        );
        $comm = Yii::app()->db->createCommand("SELECT account_code FROM nscc_chart_master WHERE
        account_code = :account_code AND (kategori = :kas OR kategori = :bank)");
        $bank_act = $comm->queryAll(true, array(
            ":account_code" => $account_code,
            ':kas' => COA_GRUP_KAS,
            ':bank' => COA_GRUP_BANK
        ));
        return count($bank_act) > 0;
    }
    static function add_gl_trans($type, $trans_id, $date_, $account, $memo_, $amount, $person_id, $cf, $store = null)
    {
        $gl_trans = new GlTrans();
        $gl_trans->type = $type;
        $gl_trans->type_no = $trans_id;
        $gl_trans->tran_date = $date_;
        $gl_trans->account_code = $account;
        $gl_trans->memo_ = $memo_;
        $gl_trans->id_user = $person_id;
        $gl_trans->amount = $amount;
        $gl_trans->cf = $cf;
        $gl_trans->store = $store;
        if (!$gl_trans->save()) {
            throw new Exception(t('save.model.fail', 'app',
                    array('{model}' => 'General Ledger')) . CHtml::errorSummary($gl_trans));
        }
    }
    static function add_bank_trans(
        $type,
        $trans_no,
        $bank_act,
        $ref,
        $date_,
        $amount,
        $person_id,
        $store = null
    ) {
        $bank_trans = new BankTrans;
        $bank_trans->type_ = $type;
        $bank_trans->trans_no = $trans_no;
        $bank_trans->bank_id = $bank_act;
        $bank_trans->ref = $ref;
        $bank_trans->tgl = $date_;
        $bank_trans->amount = $amount;
        $bank_trans->id_user = $person_id;
        $bank_trans->store = $store;
        if (!$bank_trans->save()) {
            throw new Exception(t('save.model.fail', 'app',
                    array('{model}' => 'Bank transaction')) . CHtml::errorSummary($bank_trans));
        }
    }
    static function add_comments($type, $type_no, $date_, $memo_)
    {
        if ($memo_ != null && $memo_ != "") {
            $comment = new Comments();
            $comment->type = $type;
            $comment->type_no = $type_no;
            $comment->date_ = $date_;
            $comment->memo_ = $memo_;
            if (!$comment->save()) {
                throw new Exception(t('save.model.fail', 'app',
                        array('{model}' => 'Comments')) . CHtml::errorSummary($comment));
            }
        }
    }
    static function add_stock_moves(
        $type,
        $trans_no,
        $tran_date,
        $barang_id,
        $qty,
        $reference,
        $price,
        $store
    ) {
        $move = new StockMoves;
        $move->type_no = $type;
        $move->trans_no = $trans_no;
        $move->tran_date = $tran_date;
        $move->price = $price;
        $move->reference = $reference;
        $move->qty = $qty;
//        $move->discount_percent = $discount_percent;
        $move->store = $store;
        $move->barang_id = $barang_id;
//        $move->gudang_id = $gudang_id;
        if (!$move->save()) {
            throw new Exception(t('save.model.fail', 'app',
                    array('{model}' => 'Stock moves')) . CHtml::errorSummary($move));
        }
    }
    // --------------------------------------------- Comments ---------------------------------------------------------------
    static function get_comments($type, $type_no)
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition("type=" . $type);
        $criteria->addCondition("id=" . $type_no);
        return Comments::model()->find($criteria);
    }
    static function update_comments($type, $id, $date_, $memo_)
    {
        if ($date_ == null) {
            U::delete_comments($type, $id);
            U::add_comments($type, $id,
                Yii::app()->dateFormatter->format('yyyy-MM-dd', time()),
                $memo_);
        } else {
            $criteria = new CDbCriteria();
            $criteria->addCondition("type=" . $type);
            $criteria->addCondition("id=" . $id);
            $criteria->addCondition("date_=" . $date_);
            $comment = Comments::model()->find($criteria);
            $comment->memo_ = $memo_;
            $comment->save();
        }
    }
    static function delete_comments($type, $type_no)
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition("type=" . $type);
        $criteria->addCondition("id=" . $type_no);
        $comment = Comments::model()->find($criteria);
        $comment->delete();
    }
    // ---------------------------------------------- Report ----------------------------------------------------------------
    static function get_beban()
    {
        $rows = app()->db->createCommand("SELECT account_code FROM mt_chart_master WHERE account_code REGEXP '^5[1-9]'")->queryAll();
        return $rows;
    }
    static function get_daftar_master_konsumen($code, $nama, $phone, $hp, $hp2, $tempo, $status)
    {
        $query = app()->db->createCommand();
        $query->select("CONCAT(pk.konsumen_code,' ') `Kode Konsumen`, pk.konsumen_name `Nama Konsumen`,
        pk.phone Phone, pk.hp HP, pk.hp2 `HP 2`, pk.tempo Tempo, pk.address Alamat, pk.status `Status`");
        $query->from("{{konsumen}} pk");
        if ($code !== "") {
            $query->andWhere("konsumen_code like :konsumen_code", array(":konsumen_code" => $code . "%"));
        }
        if ($nama !== "") {
            $query->andWhere("konsumen_name like :konsumen_name", array(":konsumen_name" => "%" . $nama . "%"));
        }
        if ($phone !== "") {
            $query->andWhere("phone like :phone", array(":phone" => "%" . $phone . "%"));
        }
        if ($hp !== "") {
            $query->andWhere("hp like :hp", array(":hp" => "%" . $hp . "%"));
        }
        if ($hp2 !== "") {
            $query->andWhere("hp2 like :hp2", array(":hp2" => "%" . $hp2 . "%"));
        }
        if ($tempo !== "") {
            $query->andWhere("tempo like :tempo", array(":tempo" => "%" . $tempo . "%"));
        }
        if ($status !== "") {
            $query->andWhere("status like :status", array(":status" => "%" . $status . "%"));
        }
        return $query->queryAll(true);
    }
    static function get_arr_kode_rekening_pengeluaran($code = "")
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition("account_type='" . Prefs::TypeCostAct() . "'");
        if ($code != "account_code" && $code != "") {
            $criteria->addCondition("account_code='$code'");
        }
        $model = ChartMaster::model()->findAll($criteria);
        $daftar = array();
        foreach ($model as $coderek) {
            $daftar[$coderek['account_code']] = $coderek['account_name'];
        }
        return $daftar;
    }
    static function get_pengeluaran_detil_kode_rekening(
        $start_date,
        $end_date,
        $code
    ) {
        $rows = Yii::app()->db->createCommand()->select(
            "a.tran_date,a.memo_,IF(a.amount > 0,a.amount,'') as debit,IF(a.amount < 0,-a.amount,'') as kredit")->from(
            "mt_gl_trans a")->rightJoin("mt_chart_master b",
            "a.account=b.account_code
    AND a.tran_date between :start and :end",
            array(
                ':start' => $start_date,
                ':end' => $end_date
            ))->leftJoin('mt_voided c', "a.type_no=c.id AND c.type=a.type")->where(
            "b.account_code=:code and a.type != :type and ISNULL(c.date_)",
            array(
                'code' => $code,
                'type' => VOID
            ))->order("a.tran_date")->queryAll();
        // ->where("b.account_code=:code",array('code'=>$code))
        return $rows;
    }
    static function get_pengeluaran_per_kode_rekening($start_date, $end_date)
    {
        $rows = Yii::app()->db->createCommand()->select(
            "b.account_code,b.account_name as nama_rekening,IFNULL(sum(a.amount),0) as total_beban")->from(
            "mt_gl_trans a")->rightJoin("mt_chart_master b",
            "a.account=b.account_code
    AND a.tran_date between :start and :end",
            array(
                ':start' => $start_date,
                ':end' => $end_date
            ))->where("b.account_type=:type and !b.inactive",
            array(
                ':type' => Prefs::TypeCostAct()
            ))->group("b.account_name")->order("b.account_code")->queryAll();
        return $rows;
    }
    static function get_total_pengeluaran($start_date, $end_date, $code = "")
    {
        $kode = $code == "" ? "" : "and b.account_code = '$code'";
        $rows = Yii::app()->db->createCommand()->select("sum(a.amount) as total_beban")->from(
            "mt_gl_trans a")->join("mt_chart_master b",
            "a.account=b.account_code")->where(
            "a.tran_date between :start and :end and b.account_type=:type $kode",
            array(
                ':start' => $start_date,
                ':end' => $end_date,
                ':type' => Prefs::TypeCostAct()
            ))->queryScalar();
        return $rows == null ? 0 : $rows;
    }
    static function get_detil_pendapatan($start_date, $end_date)
    {
        $rows = Yii::app()->db->createCommand()->select(
            "b.account_name as nama_rekening,IFNULL(-sum(a.amount),0) as total_pendapatan")->from(
            "mt_gl_trans a")->rightJoin("mt_chart_master b",
            "a.account=b.account_code and
        a.tran_date between :start and :end",
            array(
                ':start' => $start_date,
                ':end' => $end_date
            ))->where("b.account_type=:type and !b.inactive",
            array(
                ':type' => Prefs::TypePendapatanAct()
            ))->group("b.account_name")->order("b.account_code")->queryAll();
        return $rows;
    }
    static function get_total_pendapatan($start_date, $end_date)
    {
        $rows = Yii::app()->db->createCommand()->select("-sum(a.amount) as total_pendapatan")->from(
            "mt_gl_trans a")->join("mt_chart_master b",
            "a.account=b.account_code")->where(
            "a.tran_date between :start and :end and b.account_type=:type",
            array(
                ':start' => $start_date,
                ':end' => $end_date,
                ':type' => Prefs::TypePendapatanAct()
            ))->order("b.account_code")->queryScalar();
        return $rows == null ? 0 : $rows;
    }
    static function get_chart_master_beban()
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition("account_type = " . Prefs::TypeCostAct());
        return ChartMaster::model()->findAll($criteria);
    }
    static function account_in_gl_trans($account)
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition("account_code = :account_code");
        $criteria->params = array(':account_code' => $account);
        $count = GlTrans::model()->count($criteria);
        return $count > 0;
    }
    static function account_used_bank($account)
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition("account_code = :account_code");
        $criteria->params = array(':account_code' => $account);
        $count = Bank::model()->count($criteria);
        return $count > 0;
    }
    static function account_used_supplier($account)
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition("account_code = :account_code");
        $criteria->params = array(':account_code' => $account);
        $count = Supplier::model()->count($criteria);
        return $count > 0;
    }
    static function report_new_customers($from, $to, $store = "")
    {
        $where = "";
        $param = array(':from' => $from, ':to' => $to);
        if ($store != null) {
            $where = "AND nc.store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("
        SELECT nc.nama_customer,nc.no_customer,nc.tempat_lahir,nc.tgl_lahir,nc.email,nc.telp,
        nc.alamat,nc.awal,nc.akhir,nc.store,nc.customer_id,nk.nama_kecamatan,
        nko.nama_kota,np.nama_provinsi,nn.nama_negara,nsc.nama_status
        FROM nscc_customers AS nc
        LEFT JOIN nscc_kecamatan AS nk ON nc.kecamatan_id = nk.kecamatan_id
        LEFT JOIN nscc_kota AS nko ON nk.kota_id = nko.kota_id
        LEFT JOIN nscc_provinsi AS np ON nko.provinsi_id = np.provinsi_id
        LEFT JOIN nscc_negara AS nn ON np.negara_id = nn.negara_id
        LEFT JOIN nscc_status_cust AS nsc ON nc.status_cust_id = nsc.status_cust_id
        WHERE DATE(nc.awal) >= :from AND DATE(nc.awal) <= :to $where
        ORDER BY nc.awal");
        return $comm->queryAll(true, $param);
    }
    static function report_kategori_customer($kategori,$tgl, $store = "")
    {
        $on = $having = $where = "";
        $param[':tgl'] = $tgl;
        if ($store != null) {
            $on = "AND ns.store = :store";
            $param[':store'] = $store;
        }
        switch ($kategori) {
            case 1 :
                $where = "WHERE a.jml_trans > 0";
                $having = "HAVING jml_bulan = 3";
                break;
            case 2 :
                $where = "WHERE a.jml_trans > 0";
                $having = "HAVING jml_bulan = 2";
                break;
            case 3 :
                $where = "WHERE a.jml_trans > 0";
                $having = "HAVING jml_bulan = 1";
                break;
            case 4 :
                $where = "WHERE a.jml_trans = 0";
                break;
        }
        //HAVING jml_bulan = 2
        $comm = Yii::app()->db->createCommand("
        SELECT a.*,COUNT(a.no_customer) jml_bulan FROM (SELECT
        nc.nama_customer,nc.no_customer,nc.tempat_lahir,nc.tgl_lahir,nc.email,nc.telp,
        nc.alamat,nc.awal,nc.akhir,nc.store,nc.customer_id,COUNT(ns.tgl) jml_trans
        FROM nscc_salestrans AS ns
        RIGHT JOIN nscc_customers AS nc ON ns.customer_id = nc.customer_id AND
	      ns.tgl >= (STR_TO_DATE(:tgl,'%Y-%m-%d') - INTERVAL 3 MONTH) AND ns.type_ = 1 $on
        GROUP BY nc.customer_id,YEAR(ns.tgl), MONTH(ns.tgl)) a
        $where
        GROUP BY a.no_customer $having");
        return $comm->queryAll(true, $param);
    }
    static function report_birthday_customers($from, $to, $store = "")
    {
        $where = "";
        $param = array(':from' => $from, ':to' => $to);
        if ($store != null) {
            $where = "AND nc.store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("
        SELECT nc.nama_customer,nc.no_customer,nc.tempat_lahir,nc.tgl_lahir,nc.email,nc.telp,
        nc.alamat,nc.awal,nc.akhir,nc.store,nc.customer_id,nk.nama_kecamatan,
        nko.nama_kota,np.nama_provinsi,nn.nama_negara,nsc.nama_status
        FROM nscc_customers AS nc
        LEFT JOIN nscc_kecamatan AS nk ON nc.kecamatan_id = nk.kecamatan_id
        LEFT JOIN nscc_kota AS nko ON nk.kota_id = nko.kota_id
        LEFT JOIN nscc_provinsi AS np ON nko.provinsi_id = np.provinsi_id
        LEFT JOIN nscc_negara AS nn ON np.negara_id = nn.negara_id
        LEFT JOIN nscc_status_cust AS nsc ON nc.status_cust_id = nsc.status_cust_id
        WHERE DAYOFYEAR(:from) <= DAYOFYEAR(nc.tgl_lahir) AND DAYOFYEAR(:to) >= DAYOFYEAR(nc.tgl_lahir) $where
        ORDER BY DAYOFYEAR(nc.tgl_lahir)");
        return $comm->queryAll(true, $param);
    }
    static function report_biaya($from, $to)
    {
        $comm = Yii::app()->db->createCommand("SELECT nk.tgl, nk.doc_ref, nk.no_kwitansi,
            nk.keperluan,-nk.total total
            FROM {{kas}} AS nk
            WHERE nk.total < 0 AND DATE(tgl) >= :FROM AND DATE(tgl) <= :TO");
        return $comm->queryAll(true, array(':from' => $from, ':to' => $to));
    }
    static function report_kartu_stok($kode_barang, $from, $to, $store = "")
    {
        $where = "";
        $param = array(
            ':kode_barang' => $kode_barang,
            ':from' => $from,
            ':to' => $to
        );
        if ($store != null) {
            $where = "AND nsm.store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("SELECT DATE(nsm.tran_date) tgl,nsm.reference doc_ref,
            0 `before`,IF(nsm.qty >0,nsm.qty,0) `in`,IF(nsm.qty <0,-nsm.qty,0) `out`,
            0 `after`,nsm.qty,nsm.price
            FROM {{stock_moves}} AS nsm
            INNER JOIN nscc_barang nb ON nsm.barang_id = nb.barang_id
            WHERE DATE(nsm.tran_date) >= :from AND DATE(nsm.tran_date) <= :to
            AND nb.kode_barang = :kode_barang $where
            ORDER BY nsm.tran_date");
        return $comm->queryAll(true, $param);
    }
    static function report_mutasi_stok($from, $to, $store = "")
    {
        $where = "";
        $param = array(':from' => $from, ':to' => $to);
        if ($store != null) {
            $where = "AND nsm.store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("
        SELECT nbl.price,nb.kode_barang, nb.nama_barang,
		        SUM(IF (DATE(nsm.tran_date) < :from, nsm.qty, 0)) `before`,
        SUM(IF (nsm.qty > 0 AND nsm.type_no = 5 AND DATE(nsm.tran_date) >= :from
		  AND DATE(nsm.tran_date) <= :to, nsm.qty, 0)) Purchase,
        SUM(IF (nsm.qty > 0 AND nsm.type_no = 4 AND DATE(nsm.tran_date) >= :from
		  AND DATE(nsm.tran_date) <= :to, nsm.qty, 0)) ReturnSales,
		SUM(IF (nsm.qty < 0 AND nsm.type_no = 6 AND DATE(nsm.tran_date) >= :from
		  AND DATE(nsm.tran_date) <= :to, ABS(nsm.qty), 0)) ReturnPurchase,
		SUM(IF (nsm.qty < 0 AND nsm.type_no = 1 AND DATE(nsm.tran_date) >= :from
		  AND DATE(nsm.tran_date) <= :to, ABS(nsm.qty), 0)) Sales,
        SUM(IF (DATE(nsm.tran_date) <= :to, nsm.qty, 0)) `after`
        FROM nscc_barang AS nb LEFT  JOIN nscc_stock_moves AS nsm ON (nsm.barang_id = nb.barang_id  $where)
        INNER JOIN nscc_grup AS ng ON nb.grup_id = ng.grup_id
        INNER JOIN nscc_kategori AS nk ON ng.kategori_id = nk.kategori_id
        INNER JOIN nscc_beli nbl ON nbl.barang_id = nb.barang_id
        WHERE nk.have_stock != 0 GROUP BY nb.kode_barang, nb.nama_barang ORDER BY nb.barang_id");
        return $comm->queryAll(true, $param);
    }
    static function report_mutasi_stok_clinical($from, $to, $kategori_clinical_id,$store = "")
    {
        $where = "";
        $group = "";
        $param = array(':from' => $from, ':to' => $to);
        if ($store != null) {
            $where = "AND nsm.store = :store";
            $param[':store'] = $store;
        }
        if ($kategori_clinical_id != null) {
            $group = "WHERE nb.kategori_clinical_id = :kategori_clinical_id";
            $param[':kategori_clinical_id'] = $kategori_clinical_id;
        }
        $comm = Yii::app()->db->createCommand("
        SELECT nb.kode_barang, nb.nama_barang,
		        SUM(IF (DATE(nsm.tran_date) < :from, nsm.qty, 0)) `BEFORE`,
        SUM(IF (nsm.qty > 0 AND nsm.tipe_clinical_id = 'b388ae3c-d1c9-11e4-96ad-00ffe52bbadb' AND DATE(nsm.tran_date) >= :from
		  AND DATE(nsm.tran_date) <= :to, nsm.qty, 0)) WH,
        SUM(IF (nsm.qty > 0 AND nsm.tipe_clinical_id = 'bc29d264-d1c9-11e4-96ad-00ffe52bbadb' AND DATE(nsm.tran_date) >= :from
		  AND DATE(nsm.tran_date) <= :to, nsm.qty, 0)) RELOKASI,
		SUM(IF (nsm.qty < 0 AND nsm.tipe_clinical_id = '17eaafe7-d1d7-11e4-96ad-00ffe52bbadb' AND DATE(nsm.tran_date) >= :from
		  AND DATE(nsm.tran_date) <= :to, ABS(nsm.qty), 0)) PENJUALAN,
		SUM(IF (nsm.qty < 0 AND nsm.tipe_clinical_id = '2b9fdd0a-d1d7-11e4-96ad-00ffe52bbadb' AND DATE(nsm.tran_date) >= :from
		  AND DATE(nsm.tran_date) <= :to, ABS(nsm.qty), 0)) RELOKASIOUT,
		SUM(IF (nsm.qty < 0 AND nsm.tipe_clinical_id = '32711c0d-d1d7-11e4-96ad-00ffe52bbadb' AND DATE(nsm.tran_date) >= :from
		  AND DATE(nsm.tran_date) <= :to, ABS(nsm.qty), 0)) RETUR,
        SUM(IF (DATE(nsm.tran_date) <= :to, nsm.qty, 0)) `AFTER`
        FROM nscc_barang_clinical AS nb INNER JOIN nscc_stock_moves_clinical AS nsm ON (nsm.barang_clinical = nb.barang_clinical $where)
        $group
        GROUP BY nb.kode_barang, nb.nama_barang ORDER BY nb.barang_clinical");
        return $comm->queryAll(true, $param);
    }
    static function report_beauty_summary($from, $to, $store = "")
    {
        $kategori_id = SysPrefs::get_val('kategori_threatment_id');
        $where = "";
        $param = array(':from' => $from, ':to' => $to, ':kategori_id' => $kategori_id);
        if ($store != null) {
            $where = "AND ns.store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("
        SELECT nb.kode_barang,Sum(nsd.qty) AS qty,Sum(nbs.total) AS tip,nbe.kode_beauty,nbe.nama_beauty
        FROM nscc_salestrans_details AS nsd
        INNER JOIN nscc_barang AS nb ON nsd.barang_id = nb.barang_id
        INNER JOIN nscc_grup AS ng ON nb.grup_id = ng.grup_id
        INNER JOIN nscc_salestrans AS ns ON nsd.salestrans_id = ns.salestrans_id
        INNER JOIN nscc_beauty_services AS nbs ON nbs.salestrans_details = nsd.salestrans_details
        INNER JOIN nscc_beauty AS nbe ON nbs.beauty_id = nbe.beauty_id
        WHERE ng.kategori_id = :kategori_id AND DATE(ns.tgl) >= :from AND DATE(ns.tgl) <= :to $where
        GROUP BY nb.nama_barang,nbe.nama_beauty
        ORDER BY nbe.nama_beauty");
        return $comm->queryAll(true, $param);
    }
    static function report_beauty_details($from, $to, $store = "")
    {
        $kategori_id = SysPrefs::get_val('kategori_threatment_id');
        $where = "";
        $param = array(':from' => $from, ':to' => $to,':kategori_id'=>$kategori_id);
        if ($store != null) {
            $where = "AND ns.store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("
        SELECT nb.kode_barang,DATE_FORMAT(ns.tgl, '%d %b %Y') AS tgl,ns.doc_ref,nc.nama_customer,nbs.total beauty_tip,
        nbe.nama_beauty,nbe.kode_beauty
        FROM nscc_salestrans_details AS nsd
        INNER JOIN nscc_salestrans AS ns ON nsd.salestrans_id = ns.salestrans_id
        INNER JOIN nscc_barang AS nb ON nsd.barang_id = nb.barang_id
        INNER JOIN nscc_grup AS ng ON nb.grup_id = ng.grup_id
        INNER JOIN nscc_customers AS nc ON ns.customer_id = nc.customer_id
        INNER JOIN nscc_beauty_services AS nbs ON nbs.salestrans_details = nsd.salestrans_details
        INNER JOIN nscc_beauty AS nbe ON nbs.beauty_id = nbe.beauty_id
        WHERE ng.kategori_id = :kategori_id AND DATE(ns.tgl) >= :from AND DATE(ns.tgl) <= :to $where
        ORDER BY nbe.nama_beauty");
        return $comm->queryAll(true, $param);
    }
    static function report_sales_summary($nama_grup, $from, $to, $store = "")
    {
        $grup = "";
        $param = array(':from' => $from, ':to' => $to);
        if ($nama_grup != null) {
            $grup = "AND ng.nama_grup = :nama_grup";
            $param[':nama_grup'] = $nama_grup;
        }
        $where = '';
        if ($store != null) {
            $where = "AND ns.store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("SELECT ns.doc_ref, SUM(nsd.total + nsd.vatrp) AS total,
  nb.kode_barang, SUM(nsd.qty) AS qty, DATE_FORMAT(ns.tgl, '%d %b %Y') AS tgl
FROM nscc_salestrans ns
  INNER JOIN nscc_salestrans_details nsd
    ON nsd.salestrans_id = ns.salestrans_id
  INNER JOIN nscc_barang nb
    ON nsd.barang_id = nb.barang_id
    INNER JOIN nscc_grup ng ON nb.grup_id = ng.grup_id
WHERE nsd.total >= 0 AND DATE(ns.tgl) >= :from AND DATE(ns.tgl) <= :to $grup $where
GROUP BY ns.doc_ref, nb.kode_barang, ns.tgl");
        return $comm->queryAll(true, $param);
    }
    static function report_sales_summary_receipt($from, $to, $store = "")
    {
        $where = "";
        $param = array(':from' => $from, ':to' => $to);
        if ($store != null) {
            $where = "AND ns.store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("SELECT DATE_FORMAT(ns.tgl, '%d %b %Y') AS tgl,
  ns.doc_ref,ns.ketdisc, nc.no_customer, nc.nama_customer, SUM(ns.total) AS total, nb.nama_bank, np.amount,np.kembali
FROM nscc_salestrans ns
  INNER JOIN nscc_customers nc
    ON ns.customer_id = nc.customer_id
  LEFT JOIN nscc_payment np
    ON np.salestrans_id = ns.salestrans_id
  LEFT JOIN nscc_bank nb
    ON np.bank_id = nb.bank_id
WHERE DATE(ns.tgl) >= :from AND DATE(ns.tgl) <= :to AND ns.type_ = 1 $where
GROUP BY ns.doc_ref, nb.nama_bank, np.amount");
        return $comm->queryAll(true, $param);
    }
    static function report_sales_summary_receipt_amount_total($from, $to, $store = "")
    {
        $where = "";
        $param = array(':from' => $from, ':to' => $to);
        if ($store != null) {
            $where = "AND ns.store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("SELECT IFNULL(SUM(ns.total),0) AS total
FROM nscc_salestrans ns
WHERE DATE(ns.tgl) >= :from AND DATE(ns.tgl) <= :to AND ns.type_ = 1 $where");
        $comm->setFetchMode(PDO::FETCH_OBJ);
        return $comm->queryRow(true, $param);
    }
    static function report_sales_summary_receipt_details($from, $to, $store = "")
    {
        $where = "";
        $param = array(':from' => $from, ':to' => $to);
        if ($store != null) {
            $where = "AND ns.store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("SELECT DATE_FORMAT(ns.tgl,'%d %b %Y') tgl,
         ns.doc_ref, ns.ketdisc,nc.no_customer, nc.nama_customer, nb.kode_barang, nsd.qty, nsd.price,nsd.bruto,
          nsd.discrp,nsd.vatrp, (nsd.total+nsd.vatrp) total,ns.total total_faktur,
          CAST(REPLACE(LEFT(ns.ketdisc,(LOCATE('/',ns.ketdisc)-1)),ns.store,'')as UNSIGNED) seq
FROM nscc_salestrans_details nsd
  INNER JOIN nscc_salestrans ns
    ON nsd.salestrans_id = ns.salestrans_id
  INNER JOIN nscc_customers nc
    ON ns.customer_id = nc.customer_id
  INNER JOIN nscc_barang nb
    ON nsd.barang_id = nb.barang_id
    WHERE DATE(ns.tgl) >= :from AND DATE(ns.tgl) <= :to
  AND ns.type_ = 1 $where
  ORDER BY seq,ns.tdate");
        return $comm->queryAll(true, $param);
    }
    static function report_sales_n_return_details($grup_id, $from, $to, $store = "")
    {
        $grup = "";
        $where = "";
        $param = array(':from' => $from, ':to' => $to);
        if ($grup_id != null) {
            $grup = "AND nb.grup_id = :grup_id";
            $param[':grup_id'] = $grup_id;
        }
        if ($store != null) {
            $where = "AND ns.store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("SELECT DATE_FORMAT(ns.tgl,'%d %b %Y') tgl,
         ns.doc_ref, nc.no_customer, nc.nama_customer, nb.kode_barang, nsd.qty, nsd.price,nsd.bruto,
          nsd.discrp,nsd.vatrp, (nsd.total+nsd.vatrp) total,ns.total total_faktur
FROM nscc_salestrans_details nsd
  INNER JOIN nscc_salestrans ns
    ON nsd.salestrans_id = ns.salestrans_id
  INNER JOIN nscc_customers nc
    ON ns.customer_id = nc.customer_id
  INNER JOIN nscc_barang nb
    ON nsd.barang_id = nb.barang_id
    WHERE DATE(ns.tgl) >= :from AND DATE(ns.tgl) <= :to $where $grup
    ");
        return $comm->queryAll(true, $param);
    }
    static function report_dokter_summary($from, $to, $store = "")
    {
        $kategori_id = SysPrefs::get_val('kategori_threatment_id');
        $where = "";
        $param = array(':from' => $from, ':to' => $to, ':kategori_id'=>$kategori_id);
        if ($store != null) {
            $where = "AND ns.store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("SELECT nd.nama_dokter, nb.kode_barang, nb.nama_barang,
        SUM(nsd.qty) AS qty, SUM(nsd.jasa_dokter) AS tip, DATE_FORMAT(ns.tgl, '%d %b %Y') AS tgl
        FROM nscc_salestrans_details nsd
        INNER JOIN nscc_barang nb ON nsd.barang_id = nb.barang_id
        INNER JOIN nscc_grup ng ON nb.grup_id = ng.grup_id
        INNER JOIN nscc_salestrans ns ON nsd.salestrans_id = ns.salestrans_id
        INNER JOIN nscc_dokter nd ON nsd.dokter_id = nd.dokter_id
        WHERE ng.kategori_id = :kategori_id AND DATE(ns.tgl) >= :from AND DATE(ns.tgl) <= :to $where
        GROUP BY nd.nama_dokter,ns.tgl,nb.kode_barang
        ORDER BY nd.nama_dokter,nb.kode_barang");
        return $comm->queryAll(true, $param);
    }
    static function report_dokter_details($from, $to, $store = "")
    {
        $kategori_id = SysPrefs::get_val('kategori_threatment_id');
        $where = "";
        $param = array(':from' => $from, ':to' => $to,':kategori_id'=>$kategori_id);
        if ($store != null) {
            $where = "AND ns.store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("SELECT DATE_FORMAT(ns.tgl, '%d %b %Y') AS tgl,nd.nama_dokter,nc.nama_customer,
        nb.kode_barang,nb.nama_barang,SUM(nsd.qty) AS qty,SUM(nsd.jasa_dokter) AS tip,ns.doc_ref
        FROM nscc_salestrans_details nsd
        INNER JOIN nscc_barang nb ON nsd.barang_id = nb.barang_id
        INNER JOIN nscc_grup ng ON nb.grup_id = ng.grup_id
        INNER JOIN nscc_salestrans ns ON nsd.salestrans_id = ns.salestrans_id
        INNER JOIN nscc_dokter nd ON nsd.dokter_id = nd.dokter_id
        INNER JOIN nscc_customers nc ON ns.customer_id = nc.customer_id
        WHERE ng.kategori_id = :kategori_id AND DATE(ns.tgl) >= :from AND DATE(ns.tgl) <= :to $where
        GROUP BY nd.nama_dokter,ns.tgl,nc.nama_customer,ns.doc_ref,nb.kode_barang
        ORDER BY ns.doc_ref,nd.nama_dokter");
        return $comm->queryAll(true, $param);
    }
    static function report_retursales_summary($nama_grup, $from, $to, $store = "")
    {
        $grup = "";
        $where = "";
        $param = array(':from' => $from, ':to' => $to);
        if ($nama_grup != null) {
            $grup = "AND ng.nama_grup = :nama_grup";
            $param[':nama_grup'] = $nama_grup;
        }
        if ($store != null) {
            $where = "AND ns.store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("SELECT ns.doc_ref, -SUM(nsd.total + nsd.vatrp) AS total,
  nb.kode_barang, -SUM(nsd.qty) AS qty, DATE_FORMAT(ns.tgl, '%d %b %Y') AS tgl
FROM nscc_salestrans ns
  INNER JOIN nscc_salestrans_details nsd
    ON nsd.salestrans_id = ns.salestrans_id
  INNER JOIN nscc_barang nb
    ON nsd.barang_id = nb.barang_id
    INNER JOIN nscc_grup ng ON nb.grup_id = ng.grup_id
WHERE nsd.total < 0 AND DATE(ns.tgl) >= :from AND DATE(ns.tgl) <= :to $grup $where
GROUP BY ns.doc_ref, nb.kode_barang, ns.tgl");
        return $comm->queryAll(true, $param);
    }
    static function report_audit_summary($grup_id, $from, $to)
    {
        $grup = "";
        $param = array(':from' => $from, ':to' => $to);
        if ($grup_id != null) {
            $grup = "AND nb.grup_id = :grup_id";
            $param[':grup_id'] = $grup_id;
        }
        $comm = Yii::app()->db->createCommand("SELECT ns.doc_ref, SUM(nsd.total) AS total,
  nb.kode_barang, SUM(nsd.qty) AS qty, DATE_FORMAT(ns.tgl, '%d %b %Y') AS tgl
FROM nscc_audit ns
  INNER JOIN nscc_audit_details nsd
    ON nsd.audit_details = ns.audit_id
  INNER JOIN nscc_barang nb
    ON nsd.barang_id = nb.barang_id
WHERE nsd.total >= 0 AND DATE(ns.tgl) >= :from AND DATE(ns.tgl) <= :to $grup
GROUP BY ns.doc_ref, nb.kode_barang, ns.tgl");
        return $comm->queryAll(true, $param);
    }
    static function report_sales_details($nama_grup, $from, $to, $real = 0, $store = "")
    {
        $grup = "";
        $where = "";
        $param = array(':from' => $from, ':to' => $to);
        if ($nama_grup != null) {
            $grup = "AND ng.nama_grup = :nama_grup";
            $param[':nama_grup'] = $nama_grup;
        }
        if ($store != null) {
            $where = "AND ns.store = :store";
            $param[':store'] = $store;
        }
        $sql = $real ? "SELECT ns.doc_ref,nscc_customers.no_customer,DATE_FORMAT(ns.tgl, '%d %b %Y') AS tgl,
        nscc_customers.nama_customer,nb.kode_barang,nsd.qty,nsd.price,nsd.bruto,nsd.discrp,nsd.vatrp,(nsd.total+nsd.vatrp) total
        FROM {{salestrans_details}} AS nsd
        INNER JOIN {{salestrans}} AS ns ON nsd.salestrans_id = ns.salestrans_id
        INNER JOIN {{barang}} AS nb ON nsd.barang_id = nb.barang_id
        INNER JOIN {{customers}} ON ns.customer_id = nscc_customers.customer_id
        INNER JOIN nscc_grup ng ON nb.grup_id = ng.grup_id
        WHERE nsd.total >= 0 AND DATE(ns.tgl) >= :from AND DATE(ns.tgl) <= :to $grup $where
        ORDER BY ns.doc_ref" : "SELECT a.doc_ref,a.no_customer,a.tgl,
        a.nama_customer,a.kode_barang,SUM(a.qty) qty,a.price,a.bruto,a.discrp,a.vatrp,a.total FROM
(SELECT ns.doc_ref,nscc_customers.no_customer,DATE_FORMAT(ns.tgl, '%d %b %Y') AS tgl,
        nscc_customers.nama_customer,nb.kode_barang,nsd.qty,nsd.price,nsd.bruto,nsd.discrp,nsd.vatrp,(nsd.total+nsd.vatrp) total
        FROM nscc_salestrans_details AS nsd
        INNER JOIN nscc_salestrans AS ns ON nsd.salestrans_id = ns.salestrans_id
        INNER JOIN nscc_barang AS nb ON nsd.barang_id = nb.barang_id
        INNER JOIN nscc_customers ON ns.customer_id = nscc_customers.customer_id
        INNER JOIN nscc_grup ng ON nb.grup_id = ng.grup_id
        WHERE ns.type_ = 1 AND DATE(ns.tgl) >= :from AND DATE(ns.tgl) <= :to $grup $where
UNION ALL
SELECT ns.doc_ref_sales doc_ref,nscc_customers.no_customer,DATE_FORMAT(ns.tgl, '%d %b %Y') AS tgl,
        nscc_customers.nama_customer,nb.kode_barang,nsd.qty,nsd.price,nsd.bruto,nsd.discrp,nsd.vatrp,(nsd.total+nsd.vatrp) total
        FROM nscc_salestrans_details AS nsd
        INNER JOIN nscc_salestrans AS ns ON nsd.salestrans_id = ns.salestrans_id
        INNER JOIN nscc_barang AS nb ON nsd.barang_id = nb.barang_id
        INNER JOIN nscc_customers ON ns.customer_id = nscc_customers.customer_id
        INNER JOIN nscc_grup ng ON nb.grup_id = ng.grup_id
        WHERE ns.type_ = -1 AND DATE(ns.tgl) >= :from AND DATE(ns.tgl) <= :to $grup $where) a
GROUP BY a.doc_ref,a.kode_barang
HAVING qty > 0 ORDER BY a.doc_ref";
        $comm = Yii::app()->db->createCommand($sql);
        return $comm->queryAll(true, $param);
    }
    static function report_retur_sales_details($nama_grup = "", $from, $to, $store = "")
    {
        $grup = "";
        $where = "";
        $param = array(':from' => $from, ':to' => $to);
        if ($nama_grup != null) {
            $grup = "AND ng.nama_grup = :nama_grup";
            $param[':nama_grup'] = $nama_grup;
        }
        if ($store != null) {
            $where = "AND ns.store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("SELECT ns.doc_ref,nscc_customers.no_customer,DATE_FORMAT(ns.tgl, '%d %b %Y') AS tgl,
        nscc_customers.nama_customer,nb.kode_barang,-nsd.qty qty,nsd.price,-nsd.bruto bruto,-nsd.discrp discrp,-nsd.total total
        FROM {{salestrans_details}} AS nsd
        INNER JOIN {{salestrans}} AS ns ON nsd.salestrans_id = ns.salestrans_id
        INNER JOIN {{barang}} AS nb ON nsd.barang_id = nb.barang_id
        INNER JOIN {{customers}} ON ns.customer_id = nscc_customers.customer_id
        INNER JOIN nscc_grup ng ON nb.grup_id = ng.grup_id
        WHERE nsd.total < 0 AND DATE(ns.tgl) >= :from AND DATE(ns.tgl) <= :to $grup $where
        ORDER BY ns.doc_ref");
        return $comm->queryAll(true, $param);
    }
    static function report_audit_details($grup_id, $from, $to)
    {
        $grup = "";
        $param = array(':from' => $from, ':to' => $to);
        if ($grup_id != null) {
            $grup = "AND nb.grup_id = :grup_id";
            $param[':grup_id'] = $grup_id;
        }
        $comm = Yii::app()->db->createCommand("SELECT ns.doc_ref,nscc_customers.no_customer,DATE_FORMAT(ns.tgl, '%d %b %Y') AS tgl,
        nscc_customers.nama_customer,nb.kode_barang,nsd.qty,nsd.price,nsd.discrp,nsd.total
        FROM nscc_audit_details AS nsd
        INNER JOIN nscc_audit AS ns ON nsd.audit_details = ns.audit_id
        INNER JOIN {{barang}} AS nb ON nsd.barang_id = nb.barang_id
        INNER JOIN {{customers}} ON ns.customer_id = nscc_customers.customer_id
        WHERE nsd.total >= 0 AND DATE(ns.tgl) >= :from AND DATE(ns.tgl) <= :to $grup
        ORDER BY ns.doc_ref");
        return $comm->queryAll(true, $param);
    }
    static function report_view_customer_history($customer_id, $tgl)
    {
        $comm = Yii::app()->db->createCommand("SELECT DATE(ns.tgl) tgl,ns.doc_ref,nb.kode_barang,nsd.qty,nb.sat
        FROM {{salestrans}} AS ns
        INNER JOIN {{salestrans_details}} AS nsd ON nsd.salestrans_id = ns.salestrans_id
        INNER JOIN {{barang}} AS nb ON nsd.barang_id = nb.barang_id
        WHERE ns.customer_id = :customer_id AND ns.type_ = 1 AND DATE(ns.tgl) > :tgl
        ORDER BY ns.tgl DESC");
        return $comm->queryAll(true, array(':customer_id' => $customer_id, ':tgl' => $tgl));
    }
    static function get_sales_trans_for_audit($date)
    {
        $comm = Yii::app()->db->createCommand("SELECT ns.salestrans_id,ns.doc_ref,
        nc.no_customer,nc.nama_customer,nba.nama_bank,Sum(nsd.total) AS total
        FROM nscc_salestrans AS ns
        INNER JOIN nscc_customers AS nc ON ns.customer_id = nc.customer_id
        INNER JOIN nscc_bank AS nba ON ns.bank_id = nba.bank_id
        INNER JOIN nscc_salestrans_details AS nsd ON nsd.salestrans_id = ns.salestrans_id
        INNER JOIN nscc_barang AS nb ON nsd.barang_id = nb.barang_id
        WHERE nb.grup_id = 1 AND nsd.total >= 0 AND ns.audit <> 1 AND DATE(ns.tgl) = :date
        GROUP BY ns.doc_ref,nc.no_customer,nc.nama_customer,nba.nama_bank");
        return $comm->queryAll(true, array(':date' => $date));
    }
    static function report_casin($tgl, $store = STOREID)
    {
        $comm = Yii::app()->db->createCommand("
        SELECT nkd.item_name keperluan,nkd.total
        FROM nscc_kas_detail AS nkd
        INNER JOIN nscc_kas AS nk ON nkd.kas_id = nk.kas_id
        WHERE nk.bank_id = :bank_id AND nk.tgl = :tgl AND nk.arus = 1
        AND nk.visible = 1 AND nk.store = :store");
        return $comm->queryAll(true, array(
            ':tgl' => $tgl,
            ':store' => $store,
            ':bank_id' => SysPrefs::get_val('kas_cabang', $store)
        ));
    }
    static function report_retursales_laha($tgl)
    {
        $comm = Yii::app()->db->createCommand("SELECT ng.nama_grup,-Sum(nsd.total) AS total
        FROM nscc_grup AS ng
        INNER JOIN nscc_barang AS nb ON nb.grup_id = ng.grup_id
        INNER JOIN nscc_salestrans_details AS nsd ON nsd.barang_id = nb.barang_id
        INNER JOIN nscc_salestrans AS ns ON nsd.salestrans_id = ns.salestrans_id
        WHERE DATE(ns.tgl) = :tgl AND ns.type_ = -1
        GROUP BY ng.nama_grup");
        return $comm->queryAll(true, array(':tgl' => $tgl));
    }
    static function report_cash_laha($tgl)
    {
        $comm = Yii::app()->db->createCommand("SELECT IFNULL(Sum(nbt.amount),0)
        FROM nscc_bank_trans AS nbt
        INNER JOIN nscc_bank AS nb ON nbt.bank_id = nb.bank_id
        WHERE nb.nama_bank = 'CASH' AND DATE(nbt.tgl) = :tgl");
        return $comm->queryScalar(array(':tgl' => $tgl));
    }
    static function report_noncash_laha($tgl)
    {
        $comm = Yii::app()->db->createCommand("SELECT IFNULL(Sum(nbt.amount),0)
        FROM nscc_bank_trans AS nbt
        INNER JOIN nscc_bank AS nb ON nbt.bank_id = nb.bank_id
        WHERE nbt.visible = 1 AND nb.bank_id NOT IN(:cash,:cash_while)
        AND DATE(nbt.tgl) = :tgl");
        return $comm->queryScalar(array(
            ':tgl' => $tgl,
            ':cash' => SysPrefs::get_val('kas_cabang'),
            ':cash_while' => SysPrefs::get_val('kas_cabang_sementara')
        ));
    }
    static function report_noncash_sales($tgl)
    {
        $comm = Yii::app()->db->createCommand("
        SELECT IFNULL(SUM(np.amount),0) AS total
        FROM nscc_payment np
  INNER JOIN nscc_salestrans ns
    ON np.salestrans_id = ns.salestrans_id
  WHERE DATE(ns.tgl) = :tgl AND
        ns.type_ = 1 AND np.bank_id <> :bank_id");
        return $comm->queryScalar(array(':tgl' => $tgl, ':bank_id' => Bank::get_bank_cash_id()));
    }
    static function report_modal($tgl)
    {
        $comm = Yii::app()->db->createCommand("
        SELECT IFNULL(SUM(nk.total),0) AS total
        FROM nscc_kas nk
        WHERE DATE(nk.tgl) = :tgl
        AND nk.bank_id = :bank_id AND nk.total > 0 AND nk.type_ = 1");
        return $comm->queryScalar(array(':tgl' => $tgl, ':bank_id' => Bank::get_bank_cash_id()));
    }
    static function report_noncash_details_laha($tgl, $store = STOREID)
    {
//        $arr_filt = explode(',', SysPrefs::get_val('filter_payment', $store));
        $bank_id = Bank::get_bank_cash_id();
        $arr_filt[] = SysPrefs::get_val('kas_cabang', $store);
        $new_arr = array();
        foreach ($arr_filt as $bank) {
            $new_arr[] = "'$bank'";
        }
//        $bank_id = implode(',', $new_arr);
        $comm = Yii::app()->db->createCommand("SELECT nb.nama_bank,SUM(np.amount) total
        FROM nscc_payment AS np
        INNER JOIN nscc_salestrans AS ns ON np.salestrans_id = ns.salestrans_id
        INNER JOIN nscc_bank AS nb ON np.bank_id = nb.bank_id
        WHERE nb.bank_id <> :bank_id AND DATE(ns.tgl) = :tgl AND ns.store = :store
        GROUP BY nb.nama_bank");
        return $comm->queryAll(true, array(
            ':tgl' => $tgl,
            ':bank_id' => $bank_id,
            ':store' => $store
        ));
    }
    static function report_sales_laha($tgl, $store = "")
    {
        $comm = Yii::app()->db->createCommand("SELECT ng.nama_grup,Sum(nsd.total) AS total
        FROM nscc_grup AS ng
        INNER JOIN nscc_barang AS nb ON nb.grup_id = ng.grup_id
        INNER JOIN nscc_salestrans_details AS nsd ON nsd.barang_id = nb.barang_id
        INNER JOIN nscc_salestrans AS ns ON nsd.salestrans_id = ns.salestrans_id
        WHERE DATE(ns.tgl) = :tgl
        GROUP BY ng.nama_grup");
        return $comm->queryAll(true, array(':tgl' => $tgl));
    }
    static function report_casout($tgl, $type, $store = STOREID)
    {
        $comm = Yii::app()->db->createCommand("
        SELECT nkd.item_name keperluan,-nkd.total total
        FROM nscc_kas_detail AS nkd
        INNER JOIN nscc_kas AS nk ON nkd.kas_id = nk.kas_id
        WHERE nk.bank_id = :bank_id AND nk.tgl = :tgl AND nk.arus = -1
        AND nk.visible = 1 AND nk.type_ = :type AND nk.store = :store");
        return $comm->queryAll(true, array(
            ':tgl' => $tgl,
            ':type' => $type,
            ':store' => $store,
            ':bank_id' => SysPrefs::get_val('kas_cabang', $store),
        ));
    }
    static function report_laha_bank_transfer($tgl, $store)
    {
        $comm = Yii::app()->db->createCommand("
    SELECT IFNULL(ABS(SUM(nbt.amount)),0)
    FROM nscc_bank_trans nbt
    WHERE nbt.visible = 1 AND nbt.type_ = 11 AND nbt.tgl = :tgl AND nbt.bank_id = :bank AND nbt.store = :store");
        return $comm->queryScalar(array(
            ':tgl' => $tgl,
            ':store' => $store,
            ':bank' => SysPrefs::get_val('kas_cabang', $store)
        ));
    }
    static function report_vat_sales($tgl)
    {
        $comm = Yii::app()->db->createCommand("SELECT IFNULL(Sum(ns.vat),0)
        FROM nscc_salestrans AS ns
        WHERE DATE(ns.tgl) = :tgl");
        return $comm->queryScalar(array(':tgl' => $tgl));
    }
    static function report_vat_retursales($tgl)
    {
        $comm = Yii::app()->db->createCommand("SELECT IFNULL(-Sum(ns.vat),0)
        FROM nscc_salestrans AS ns
        WHERE DATE(ns.tgl) = :tgl");
        return $comm->queryScalar(array(':tgl' => $tgl));
    }
    static function report_discount_sales($tgl)
    {
        $comm = Yii::app()->db->createCommand("SELECT IFNULL(Sum(ns.discrp),0)
        FROM nscc_salestrans AS ns
        WHERE DATE(ns.tgl) = :tgl");
        return $comm->queryScalar(array(':tgl' => $tgl));
    }
    static function report_discount_returnsales($tgl)
    {
        $comm = Yii::app()->db->createCommand("SELECT IFNULL(-Sum(ns.discrp),0)
        FROM nscc_salestrans AS ns
        WHERE DATE(ns.tgl) = :tgl AND ns.type_ = -1");
        return $comm->queryScalar(array(':tgl' => $tgl));
    }
    static function report_tenders($tgl, $store = "")
    {
        $where1 = "";
        $where2 = "";
        $param = array(':tgl' => $tgl);
        if ($store != null) {
            $where1 = "AND nbt.store = :store";
            $where2 = "AND nt.store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("
        SELECT nb.nama_bank,IFNULL(a.total,0) `system`,IFNULL(b.total,0) `tenders`,
        IF((IFNULL(a.total,0)-IFNULL(b.total,0)) <= 0,'SHORT','OVER') `Status`,
        ABS(IFNULL(a.total,0)-IFNULL(b.total,0)) 'diff' FROM
        (SELECT nbt.bank_id,IFNULL(Sum(nbt.amount),0) total
        FROM nscc_bank_trans AS nbt
        WHERE DATE(nbt.tgl) = :tgl $where1
        GROUP BY nbt.bank_id) a
        LEFT JOIN (
        SELECT ntd.bank_id,IFNULL(ntd.amount,0) total
        FROM nscc_tender AS nt
        INNER JOIN nscc_tender_details AS ntd ON ntd.tender_id = nt.tender_id
        Where nt.tgl = :tgl $where2
        GROUP BY ntd.bank_id) b ON (a.bank_id = b.bank_id)
        RIGHT JOIN nscc_bank nb ON a.bank_id = nb.bank_id");
        return $comm->queryAll(true, $param);
    }
    static function get_date_service_no_final($from, $to, $store = "")
    {
        $kategori_id = SysPrefs::get_val('kategori_threatment_id');
        $where = "";
        $param = array(':from' => $from, ':to' => $to,':kategori_id'=>$kategori_id);
        if ($store != null) {
            $where = "AND ns.store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("
        SELECT DATE_FORMAT(ns.tgl,'%d %b %Y') tgl
        FROM nscc_salestrans_details AS nsd
        INNER JOIN nscc_salestrans AS ns ON nsd.salestrans_id = ns.salestrans_id
        INNER JOIN nscc_barang AS nb ON nsd.barang_id = nb.barang_id
        INNER JOIN nscc_grup AS ng ON nb.grup_id = ng.grup_id
        INNER JOIN nscc_beauty_services nbs ON nbs.salestrans_details = nsd.salestrans_details
        WHERE nbs.final = 0 AND ng.kategori_id = :kategori_id  AND DATE(ns.tgl) >= :from AND DATE(ns.tgl) <= :to $where
        GROUP BY DATE(ns.tgl)");
        return $comm->queryAll(true, $param);
    }
    static function get_printed_sales()
    {
        $newLine = "\n";
        $raw = U::setCenter(SysPrefs::get_val('receipt_header0'));
        $raw .= $newLine;
        $raw .= U::setCenter(SysPrefs::get_val('receipt_header1'));
        $raw .= $newLine;
        $raw .= U::setCenter(SysPrefs::get_val('receipt_header2'));
        $raw .= $newLine;
        $raw .= U::setCenter(SysPrefs::get_val('receipt_header3'));
        $raw .= $newLine;
        $raw .= U::setCenter(SysPrefs::get_val('receipt_header4'));
        $raw .= $newLine;
        $raw .= U::setCenter(SysPrefs::get_val('receipt_header5'));
        $raw .= $newLine;
        $raw .= U::fillWithChar("=");
//        $raw .= "---------------------------------------------------------------";
        $raw .= $newLine;
        $raw .= U::fillWithChar("-");
        $raw .= $newLine;
        $raw .= "Item Code        Item Name                 QTY           TOTAL ";
        $raw .= $newLine;
        $raw .= U::fillWithChar("-");
        $raw .= $newLine;
        $raw .= U::addItemCodeReceipt("TSUNCR", '1.00', '27,500.00');
        $raw .= $newLine;
        $raw .= U::addItemNameReceipt("Teen's Day Sunscreen", 46);
        $raw .= $newLine;
        $raw .= U::addItemDiscReceipt("10.00", "-2,750.00");
        $raw .= $newLine;
        $raw .= U::fillWithChar("-");
        $raw .= $newLine;
        $raw .= U::fillWithChar("=");
        $raw .= $newLine;
        $raw .= U::setCenter(SysPrefs::get_val('receipt_footer0'));
        $raw .= $newLine;
        $raw .= U::setCenter(SysPrefs::get_val('receipt_footer1'));
        $raw .= $newLine;
        $raw .= U::setCenter(SysPrefs::get_val('receipt_footer2'));
        $raw .= $newLine;
        $raw .= U::setCenter(SysPrefs::get_val('receipt_footer3'));
        $raw .= $newLine;
        $raw .= U::setCenter(SysPrefs::get_val('receipt_footer4'));
        $raw .= $newLine;
        $raw .= U::setCenter(SysPrefs::get_val('receipt_footer5'));
        $raw .= $newLine;
        return $raw;
    }
    static function get_gl_before($account_code, $date, $store = "")
    {
        $where = "";
        $param = array(":account_code" => $account_code, ":date" => $date);
        if ($store != null) {
            $where = "AND ngt.store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("
        SELECT IFNULL(SUM(ngt.amount),0) FROM nscc_gl_trans ngt
        WHERE ngt.account_code = :account_code AND ngt.visible = 1 AND ngt.tran_date < :date $where");
        return $comm->queryScalar($param);
    }
    static function get_gl_after($account_code, $date, $store = "")
    {
        $where = "";
        $param = array(":account_code" => $account_code, ":date" => $date);
        if ($store != null) {
            $where = "AND ngt.store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("
        SELECT IFNULL(SUM(ngt.amount),0) FROM nscc_gl_trans ngt
        WHERE ngt.account_code = :account_code AND ngt.visible = 1 AND ngt.tran_date > :date $where");
        $comm->queryScalar($param);
    }
    static function get_general_ledger($account_code, $from, $to, $store = "")
    {
        $where = "";
        $param = array(
            ':account_code' => $account_code,
            ':from' => $from,
            ':to' => $to
        );
        if ($store != null) {
            $where = "AND ngt.store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("
        SELECT DATE_FORMAT(ngt.tran_date,'%d/%m/%Y') tgl,
        ngt.memo_,IF(ngt.amount > 0, ngt.amount, '') AS Debit,
        IF(ngt.amount < 0, -ngt.amount, '') AS Credit,
        0 AS Balance,
        ngt.amount
        FROM nscc_gl_trans ngt
        WHERE ngt.account_code = :account_code AND ngt.visible = 1 AND
        ngt.tran_date >= :from AND ngt.tran_date <= :to $where");
        return $comm->queryAll(true, $param);
    }
    static function get_general_journal($from, $to, $store = "")
    {
        $where = "";
        $param = array(':from' => $from, ':to' => $to);
        if ($store != null) {
            $where = "AND ngt.store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("
        SELECT DATE_FORMAT(ngt.tran_date,'%d/%m/%Y') tgl,
        ntt.tipe_name,nscc_refs.reference,
        ncm.account_code,ncm.account_name,
        ngt.memo_,IF(ngt.amount > 0, ngt.amount, '') AS Debit,
        IF(ngt.amount < 0, -ngt.amount, '') AS Credit
        FROM nscc_gl_trans ngt
        INNER JOIN nscc_chart_master ncm
            ON ngt.account_code = ncm.account_code
        LEFT JOIN nscc_trans_tipe ntt
            ON ngt.type = ntt.tipe_id
        LEFT JOIN nscc_refs
            ON ngt.type = nscc_refs.type_ AND ngt.type_no = nscc_refs.type_no
        WHERE ngt.tran_date >= :from AND ngt.visible = 1 AND ngt.tran_date <= :to $where
        ORDER BY ngt.tran_date,ngt.type_no");
        return $comm->queryAll(true, $param);
    }
    static function generate_primary_key($table)
    {
        $sys = SysTypes::model()->find("type_id = :type_id", array(':type_id' => $table));
        $newid = "";
        switch ($table) {
            case RSALESTRANS :
                $newid = STOREID . "SLST" . $sys->next_reference;
                break;
            case RSALESTRANSDETAILS :
                $newid = STOREID . "SLSTDTL" . $sys->next_reference;
                break;
            case RUSERS :
                $newid = STOREID . "USERS" . $sys->next_reference;
                break;
            case RBEAUTY :
                $newid = STOREID . "BEAUTY" . $sys->next_reference;
                break;
            case RGRUP :
                $newid = STOREID . "GRUP" . $sys->next_reference;
                break;
            case RDOKTER :
                $newid = STOREID . "DOKTER" . $sys->next_reference;
                break;
            case RBARANG :
                $newid = STOREID . "BRNG" . $sys->next_reference;
                break;
            case RBANK :
                $newid = STOREID . "BANK" . $sys->next_reference;
                break;
            case RKAS :
                $newid = STOREID . "KAS" . $sys->next_reference;
                break;
            case RTENDER :
                $newid = STOREID . "TENDER" . $sys->next_reference;
                break;
            case RTRANSFERITEM :
                $newid = STOREID . "TITEM" . $sys->next_reference;
                break;
            case RBELI :
                $newid = STOREID . "BLI" . $sys->next_reference;
                break;
            case RJURNAL_UMUM :
                $newid = STOREID . "JU" . $sys->next_reference;
                break;
            case RPRINTZ:
                $newid = STOREID . "PZ" . $sys->next_reference;
                break;
            case RBANKTRANSFER:
                $newid = STOREID . "BTR" . $sys->next_reference;
                break;
            case RPELUNASANUTANG:
                $newid = STOREID . "PELUTANG" . $sys->next_reference;
                break;
        }
        $sys->next_reference++;
        if (!$sys->save()) {
            throw new Exception(t('save.model.fail', 'app',
                    array('{model}' => 'SysTypes')) . CHtml::errorSummary($sys));
        }
        return $newid;
    }
    static function save_file($filename, $data)
    {
        if (defined('WRITE_TO_FILE') && WRITE_TO_FILE) {
            $fh = @fopen($filename, 'w');
            if ($fh === false) {
                throw new Exception('Failed create file.');
            }
            fwrite($fh, $data);
            fclose($fh);
        }
    }
    static function get_report_payment($from, $to, $bank_id, $store = '')
    {
        $where = "";
        $param = array(
            ':from' => $from,
            ':to' => $to,
            ':bank_id' => $bank_id
        );
        if ($store != null) {
            $where = "AND ns.store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("SELECT ns.doc_ref,
        nc.nama_customer,nc.no_customer,nca.card_name,np.card_number,DATE_FORMAT(ns.tgl,'%d %b %Y') tgl,
        np.amount,np.kembali
        FROM nscc_payment AS np
        LEFT JOIN nscc_card AS nca ON np.card_id = nca.card_id
        INNER JOIN nscc_salestrans AS ns ON np.salestrans_id = ns.salestrans_id
        INNER JOIN nscc_customers AS nc ON ns.customer_id = nc.customer_id
        WHERE ns.tgl >= :from AND ns.tgl <= :to AND np.bank_id = :bank_id $where
        ORDER BY ns.tgl,ns.doc_ref");
        return $comm->queryAll(true, $param);
    }
    static function get_daftar_harga_jual($store)
    {
        $comm = Yii::app()->db->createCommand("SELECT ng.nama_grup,
        nb.kode_barang,nb.nama_barang,IFNULL(nj.price,0) price
        FROM nscc_barang AS nb
        LEFT JOIN nscc_grup AS ng ON nb.grup_id = ng.grup_id
        LEFT JOIN nscc_jual AS nj ON (nj.barang_id = nb.barang_id AND nj.store = :store)");
        return $comm->queryAll(true, array(':store' => $store));
    }
    static function get_bank_trans($from, $to, $bank_id)
    {
        $comm = Yii::app()->db->createCommand("SELECT ntt.tipe_name,nbt.ref,DATE_FORMAT(nbt.tgl,'%d %b %Y') tgl,
        IF(nbt.amount >= 0,nbt.amount,0) Debit,
        IF(nbt.amount < 0,-nbt.amount,0) Credit, nbt.amount,
        0 balance,nbt.amount,COALESCE(nk.keperluan,nc.memo_) AS memo_
        FROM nscc_bank_trans AS nbt
        LEFT JOIN nscc_trans_tipe AS ntt ON nbt.type_ = ntt.tipe_id
        LEFT JOIN nscc_comments AS nc ON nbt.type_ = nc.type AND nbt.trans_no = nc.type_no
        LEFT JOIN nscc_kas AS nk ON nbt.trans_no = nk.kas_id AND (nbt.type_ = 2 OR nbt.type_ = 3)
        WHERE nbt.visible =1 AND nbt.tgl >= :from AND nbt.tgl <= :to AND nbt.bank_id = :bank_id
        GROUP BY nc.type,nc.type_no
        ORDER BY nbt.tgl,nbt.tdate,nbt.ref");
        return $comm->queryAll(true, array(':from' => $from, ':to' => $to, ':bank_id' => $bank_id));
    }
}