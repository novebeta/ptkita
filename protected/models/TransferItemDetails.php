<?php

Yii::import('application.models._base.BaseTransferItemDetails');

class TransferItemDetails extends BaseTransferItemDetails
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate()
    {
        if ($this->transfer_item_details_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->transfer_item_details_id = $uuid;
        }
        return parent::beforeValidate();
    }
}