<?php

Yii::import('application.models._base.BaseAudit');
class Audit extends BaseAudit
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->audit_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->audit_id = $uuid;
        }
        if ($this->tdate == null) {
            $this->tdate = new CDbExpression('NOW()');
        }
        if ($this->store == null) {
            $this->store = STOREID;
        }
        if ($this->user_id == null) {
            $this->user_id = Yii::app()->user->getId();
        }
        return parent::beforeValidate();
    }
    public static function save_audit($id)
    {
        app()->db->autoCommit = false;
        $transaction2 = Yii::app()->db->beginTransaction();
        try {
            $ref = new Reference();
            $doc_ref = $ref->get_next_reference(AUDIT);
            $comm = Yii::app()->db->createCommand("
        INSERT INTO `nscc_audit` (  `tgl`,  `doc_ref`,  `tdate`,  `user_id`,  `bruto`,  `disc`,  `discrp`,  `ketdisc`,  `vat`,  `totalpot`,
        `total`,  `customer_id`,  `bank_id`,  `doc_ref_sales`,  `store`, `card_number`, `printed`)
        SELECT ns.tgl,'$doc_ref' doc_ref,ns.tdate,ns.user_id,ns.bruto,ns.disc,ns.discrp,ns.ketdisc,
        ns.vat,ns.totalpot,ns.total,ns.customer_id,ns.bank_id,ns.doc_ref_sales,ns.store,ns.card_number,ns.printed
        FROM nscc_salestrans AS ns WHERE ns.salestrans_id = $id");
            if ($comm->execute() < 1) {
                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Sales')));
            }
            $audit_id = Yii::app()->db->getLastInsertID();
            $comm_dtl = Yii::app()->db->createCommand("INSERT INTO `nscc_audit_details` (  `barang_id`,  `qty`,
        `disc`,  `discrp`,  `ketpot`,  `vat`,
        `vatrp`,  `bruto`,  `total`,  `total_pot`,  `price`,  `audit_id`,  `dokter_id`)
        SELECT nsd.barang_id,nsd.qty,nsd.disc,nsd.discrp,nsd.ketpot,nsd.vat,
        nsd.vatrp,nsd.bruto,nsd.total,nsd.total_pot,nsd.price,$audit_id audit_id,nsd.dokter_id
        FROM nscc_salestrans_details AS nsd WHERE nsd.salestrans_id = $id;");
            if ($comm_dtl->execute() < 1) {
                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Sales details')));
            }
            $sales_trans = Salestrans::model()->findByPk($id);
            $sales_trans->audit = 1;
            if (!$sales_trans->save(false)) {
                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Sales')));
            }
            $ref->save(AUDIT, $audit_id, $doc_ref);
            $transaction2->commit();
        } catch (Exception $e) {
            $transaction2->rollBack();
            echo CJSON::encode(array(
                'success' => false,
                'msg' => $e->getMessage()));
        }
        app()->db->autoCommit = true;
        Yii::app()->end();
    }
}