<?php

Yii::import('application.models._base.BasePelunasanUtangDetil');
class PelunasanUtangDetil extends BasePelunasanUtangDetil
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public static function get_utang($supplier_id, $store)
    {
        $comm = Yii::app()->db->createCommand("SELECT nti.transfer_item_id,nti.tgl,
nti.doc_ref_other no_faktur,nti.total nilai,nti.doc_ref,
nti.total - COALESCE(-nti1.total,0) - COALESCE(SUM(npud.kas_dibayar),0) AS sisa
FROM nscc_transfer_item nti
LEFT JOIN nscc_pelunasan_utang_detil npud ON nti.transfer_item_id = npud.transfer_item_id
LEFT JOIN nscc_transfer_item nti1 ON nti.doc_ref = nti1.doc_ref_other AND nti1.type_ = 1
WHERE nti.supplier_id = :supplier_id AND nti.store = :store
GROUP BY nti.transfer_item_id
HAVING sisa > 0");
        return $comm->queryAll(true, array(
            ':store' => $store,
            ':supplier_id' => $supplier_id
        ));
    }

    public function beforeValidate()
    {
        if ($this->pelunasan_utang_detil_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->pelunasan_utang_detil_id = $uuid;
        }
        return parent::beforeValidate();
    }

}