<?php

Yii::import('application.models._base.BaseStatusCust');

class StatusCust extends BaseStatusCust
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate()
    {
        if ($this->status_cust_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->status_cust_id = $uuid;
        }
        return parent::beforeValidate();
    }
}