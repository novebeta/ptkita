<?php

Yii::import('application.models._base.BaseStockMoves');
class StockMoves extends BaseStockMoves
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->stock_moves_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->stock_moves_id = $uuid;
        }
        if ($this->tdate == null) {
            $this->tdate = new CDbExpression('NOW()');
        }
        if ($this->store == null) {
            $this->store = STOREID;
        }
        if ($this->id_user == null) {
            $this->id_user = Yii::app()->user->getId();
        }
        return parent::beforeValidate();
    }
    public static function get_saldo_item_before($kode_barang, $tgl, $store = "")
    {
        $where = "";
        $param = array(':kode_barang' => $kode_barang, ':tgl' => $tgl);
        if ($store != null) {
            $where = "AND nsm.store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("SELECT
        IFNULL(Sum(nsm.qty),0) FROM nscc_stock_moves AS nsm
        INNER JOIN nscc_barang nb ON nsm.barang_id = nb.barang_id
        WHERE  nb.kode_barang = :kode_barang AND nsm.tran_date < :tgl $where");
        return $comm->queryScalar($param);
    }
    public static function get_saldo_item($barang_id, $store)
    {
        $comm = Yii::app()->db->createCommand("SELECT
        IFNULL(Sum(nsm.qty),0) FROM nscc_stock_moves AS nsm
        WHERE nsm.barang_id = :barang_id AND nsm.store = :store");
        return $comm->queryScalar(array(':barang_id' => $barang_id,
            ':store' => $store));
    }
}