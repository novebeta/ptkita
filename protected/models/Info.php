<?php

Yii::import('application.models._base.BaseInfo');

class Info extends BaseInfo
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate()
    {
        if ($this->info_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->info_id = $uuid;
        }
        return parent::beforeValidate();
    }
}