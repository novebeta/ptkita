<?php

Yii::import('application.models._base.BaseBeautyServices');

class BeautyServices extends BaseBeautyServices
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public static function add_beauty_tips($salestrans_details,$beauty_id){
        $beauty = new BeautyServices;
        $beauty->beauty_id = $beauty_id;
        $beauty->salestrans_details = $salestrans_details->salestrans_details;
        if (!$beauty->save())
            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Beauty Services')) . CHtml::errorSummary($beauty));
        $price = Price::get_price($salestrans_details->barang_id, $beauty->beauty->gol_id);
        if ($price == null)
            throw new Exception("Tips value not define");
        $total_tips = $salestrans_details->qty * $price->value;
        $beauty->total = $total_tips;
        if (!$beauty->save())
            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Beauty Services')) . CHtml::errorSummary($beauty));
    }
    public function beforeValidate()
    {
        if ($this->nscc_beauty_services_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->nscc_beauty_services_id = $uuid;
        }
        return parent::beforeValidate();
    }
}