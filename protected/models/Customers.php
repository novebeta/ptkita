<?php

Yii::import('application.models._base.BaseCustomers');

class Customers extends BaseCustomers
{
    public function beforeValidate()
    {
        if ($this->customer_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->customer_id = $uuid;
        }
        return parent::beforeValidate();
    }
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public static function generete_no_customer($store){
        $res = Yii::app()->db->createCommand("
        select IFNULL(MAX(CAST(REPLACE(nc.no_customer,:store,'') AS UNSIGNED)),0)
        from nscc_customers nc where nc.store = :store");
        $counter = $res->queryScalar(array(':store'=>$store));
        $counter++;
        return $counter.$store;
    }
    public static function get_backup(){
        $res = Yii::app()->db->createCommand("
        SELECT CONCAT(nc.nama_customer, ' ', nc.no_customer) nama,
  nc.alamat, nc.customer_id noax, nkota.nama_kota kota,
  nc.telp, DATE(nc.awal) awal,
  if(nc.sex = 'MALE','PRIA','WANITA') sex,
  nc.tgl_lahir tgllh, nc.kerja
FROM nscc_customers nc
  LEFT JOIN nscc_kecamatan nk
    ON nc.kecamatan_id = nk.kecamatan_id
  LEFT JOIN nscc_kota nkota
    ON nk.kota_id = nkota.kota_id
    WHERE nc.log = 0");
        return $res->queryAll(true);
    }
}