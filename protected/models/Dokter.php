<?php

Yii::import('application.models._base.BaseDokter');
class Dokter extends BaseDokter
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->dokter_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->dokter_id = $uuid;
        }
        if ($this->store == null) {
            $this->store = STOREID;
        }
        return parent::beforeValidate();
    }
}