<?php

Yii::import('application.models._base.BaseKota');

class Kota extends BaseKota
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate()
    {
        if ($this->kota_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->kota_id = $uuid;
        }
        return parent::beforeValidate();
    }
}