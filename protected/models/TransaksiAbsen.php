<?php

Yii::import('application.models._base.BaseTransaksiAbsen');
class TransaksiAbsen extends BaseTransaksiAbsen
{
    public function primaryKey()
    {
        return 'absen_trans_id';
    }
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}