<?php

Yii::import('application.models._base.BaseSouvenirTrans');
class SouvenirTrans extends BaseSouvenirTrans
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->souvenir_trans_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->souvenir_trans_id = $uuid;
        }
        if ($this->tdate == null) {
            $this->tdate = new CDbExpression('NOW()');
        }
        if ($this->id_user == null) {
            $this->id_user = Yii::app()->user->getId();
        }
        return parent::beforeValidate();
    }
    public static function save_souvenir_trans($type_no, $trans_no, $ref, $tgl, $qty, $souvenir_id, $store)
    {
        $svr = new SouvenirTrans;
        $svr->type_no = $type_no;
        $svr->trans_no = $trans_no;
        $svr->ref = $ref;
        $svr->tgl = $tgl;
        $svr->qty = $qty;
        $svr->souvenir_id = $souvenir_id;
        $svr->store = $store;
        if (!$svr->save()) {
            throw new Exception(t('save.model.fail', 'app',
                    array('{model}' => 'Souvenir Trans')) . CHtml::errorSummary($svr));
        }
    }
    public static function total_souvenir($souvenir_id)
    {
        $command = Yii::app()->db->createCommand("select COALESCE(sum(nst.qty),0) qty from nscc_souvenir_trans nst
            where nst.souvenir_id = :souvenir_id;");
        return $command->queryScalar(array(":souvenir_id" => $souvenir_id));
    }
}