<?php

Yii::import('application.models._base.BasePrintz');

class Printz extends BasePrintz
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate()
    {
        if ($this->prntz_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->prntz_id = $uuid;
        }
        return parent::beforeValidate();
    }
}