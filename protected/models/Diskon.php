<?php

Yii::import('application.models._base.BaseDiskon');
class Diskon extends BaseDiskon
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public static function save_diskon($barang_id, $status_cust_id, $value, $store = STOREID)
    {
        $comm = Yii::app()->db->createCommand(
            "REPLACE INTO `{{diskon}}` (`value`, `barang_id`, `status_cust_id`, store)
            VALUES (:value,:barang_id,:status_cust_id,:store)"
        );
        return $comm->execute(array(':value' => $value, 'barang_id' => $barang_id,
            ':status_cust_id' => $status_cust_id, ':store' => $store));
    }
    public static function save_diskon_by_grup($status_cust_id, $grup, $value, $store = STOREID)
    {
        $comm = Yii::app()->db->createCommand(
            "REPLACE INTO {{diskon}} (`status_cust_id`,`barang_id`,`value`, store)
            SELECT $status_cust_id as status_cust_id,nb.barang_id,
            $value as `value`,'$store' as store FROM nscc_barang nb WHERE nb.grup_id = :grup_id AND store = :store"
        );
        return $comm->execute(array(':grup_id' => $grup, ':store' => $store));
    }
    public function beforeValidate()
    {
        if ($this->diskon_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->diskon_id = $uuid;
        }
        return parent::beforeValidate();
    }
}