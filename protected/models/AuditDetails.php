<?php

Yii::import('application.models._base.BaseAuditDetails');

class AuditDetails extends BaseAuditDetails
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate()
    {
        if ($this->audit_details == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->audit_details = $uuid;
        }
        return parent::beforeValidate();
    }
}