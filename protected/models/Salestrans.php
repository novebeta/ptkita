<?php

Yii::import('application.models._base.BaseSalestrans');
class Salestrans extends BaseSalestrans
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->salestrans_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->salestrans_id = $uuid;
        }
        if ($this->tdate == null) {
            $this->tdate = new CDbExpression('NOW()');
        }
        if ($this->user_id == null) {
            $this->user_id = Yii::app()->user->getId();
        }
        return parent::beforeValidate();
    }
    public static function list_beuty_service($tgl)
    {
        $kategori_id = SysPrefs::get_val('kategori_threatment_id');
        $comm = Yii::app()->db->createCommand("
        SELECT a.salestrans_id,a.doc_ref,a.salestrans_details,a.barang_id,SUM(a.qty) qty,a.jasa_dokter,
a.dokter_id,a.kode_barang,a.nama_barang,a.store,a.final,a.beauty_id,a.beauty2_id,a.nama_customer,a.no_customer
 FROM
(SELECT ns.salestrans_id,ns.doc_ref,nsd.salestrans_details,nsd.barang_id,
nsd.qty,nsd.jasa_dokter,nsd.dokter_id,nb.kode_barang,nb.nama_barang,ns.store,
  IFNULL((SELECT nsb.final FROM nscc_beauty_services nsb
WHERE nsb.salestrans_details = nsd.salestrans_details LIMIT 1),0) final,
(SELECT nsb.beauty_id FROM nscc_beauty_services nsb
WHERE nsb.salestrans_details = nsd.salestrans_details LIMIT 1) beauty_id,
(SELECT nsb.beauty_id FROM nscc_beauty_services nsb
WHERE nsb.salestrans_details = nsd.salestrans_details LIMIT 1,1) beauty2_id,
  nc.nama_customer,
  nc.no_customer
FROM nscc_salestrans AS ns
INNER JOIN nscc_salestrans_details AS nsd ON nsd.salestrans_id = ns.salestrans_id
INNER JOIN nscc_barang AS nb ON nsd.barang_id = nb.barang_id
INNER JOIN nscc_grup AS ng ON nb.grup_id = ng.grup_id
INNER JOIN nscc_customers nc ON ns.customer_id = nc.customer_id
WHERE ng.kategori_id = :kategori_id AND ns.total >= 0 AND DATE(ns.tgl) = :tgl
UNION
SELECT ns.salestrans_id,ns.doc_ref_sales doc_ref,nsd.salestrans_details,nsd.barang_id,
nsd.qty,nsd.jasa_dokter,nsd.dokter_id,nb.kode_barang,nb.nama_barang,ns.store,
  IFNULL((SELECT nsb.final FROM nscc_beauty_services nsb
WHERE nsb.salestrans_details = nsd.salestrans_details LIMIT 1),0) final,
(SELECT nsb.beauty_id FROM nscc_beauty_services nsb
WHERE nsb.salestrans_details = nsd.salestrans_details LIMIT 1) beauty_id,
(SELECT nsb.beauty_id FROM nscc_beauty_services nsb
WHERE nsb.salestrans_details = nsd.salestrans_details LIMIT 1,1) beauty2_id,
  nc.nama_customer,
  nc.no_customer
FROM nscc_salestrans AS ns
INNER JOIN nscc_salestrans_details AS nsd ON nsd.salestrans_id = ns.salestrans_id
INNER JOIN nscc_salestrans ns1 ON ns.doc_ref_sales = ns1.doc_ref
INNER JOIN nscc_barang AS nb ON nsd.barang_id = nb.barang_id
INNER JOIN nscc_grup AS ng ON nb.grup_id = ng.grup_id
INNER JOIN nscc_customers nc ON ns.customer_id = nc.customer_id
WHERE ng.kategori_id = :kategori_id AND ns.total < 0 AND DATE(ns.tgl) = :tgl) a
GROUP BY a.doc_ref,a.barang_id
HAVING qty > 0");
        return $comm->queryAll(true, array(':tgl' => $tgl, ':kategori_id' => $kategori_id));
    }
    public static function get_trans($tgl)
    {
        $comm = Yii::app()->db->createCommand("
        SELECT ns.*, nc.nama_customer, nc.no_customer
        FROM nscc_salestrans ns
        INNER JOIN nscc_customers nc
            ON ns.customer_id = nc.customer_id
            WHERE DATE(ns.tgl) = :tgl AND ns.type_ = 1");
        return $comm->queryAll(true, array(':tgl' => $tgl));
    }
    public static function get_trans_history($tgl)
    {
        $comm = Yii::app()->db->createCommand("
        SELECT ns.*, nc.nama_customer, nc.no_customer
        FROM nscc_salestrans ns
        INNER JOIN nscc_customers nc
            ON ns.customer_id = nc.customer_id
            WHERE DATE(ns.tgl) = :tgl AND ns.bruto >= 0");
        return $comm->queryAll(true, array(':tgl' => $tgl));
    }
    public static function get_retursales_trans($tgl)
    {
        $comm = Yii::app()->db->createCommand("SELECT  ns.salestrans_id,  ns.tgl,  ns.doc_ref,  ns.tdate,
  ns.user_id,  - ns.bruto AS bruto,  - ns.disc AS disc,  - ns.discrp AS discrp,  - ns.totalpot AS totalpot,
  - ns.total AS total,  ns.ketdisc,  - ns.vat AS vat,  ns.doc_ref_sales,  ns.audit,  ns.store,  ns.printed,
  ns.override,  nc.nama_customer,  nc.no_customer,  ns.customer_id,  - ns.bayar AS bayar,  - ns.kembali AS kembali,
  -ns.rounding AS rounding
FROM nscc_salestrans ns
  INNER JOIN nscc_customers nc
    ON ns.customer_id = nc.customer_id
WHERE DATE(ns.tgl) = :tgl AND ns.type_ = -1");
        return $comm->queryAll(true, array(':tgl' => $tgl));
    }
    public static function set_final_service($tgl)
    {
        $comm = Yii::app()->db->createCommand("UPDATE nscc_beauty_services AS bs
            INNER JOIN nscc_salestrans_details AS nsd ON nsd.salestrans_details = bs.salestrans_details
            INNER JOIN nscc_salestrans AS ns ON nsd.salestrans_id = ns.salestrans_id
            INNER JOIN nscc_barang AS nb ON nsd.barang_id = nb.barang_id
            INNER JOIN nscc_grup AS ng ON nb.grup_id = ng.grup_id
            SET bs.final = 1
            WHERE DATE(ns.tgl) = :tgl
        ");
        return $comm->execute(array(':tgl' => $tgl));
    }
    public static function get_backup()
    {
        $res = Yii::app()->db->createCommand("
       SELECT
	nb.kode_barang AS kodebrg,
	nsd.qty,
	CONCAT( nc.nama_customer , ' ' , nc.no_customer ) AS nama,
	nsd.ketpot AS komen,
	ns.ketdisc AS komen_resep,
	ns.doc_ref AS resep,
	nb.sat AS satuan,
	date( ns.tgl ) AS tanggal
FROM
	nscc_salestrans_details nsd
	INNER JOIN nscc_salestrans ns
	 ON nsd.salestrans_id = ns.salestrans_id
	INNER JOIN nscc_customers nc
	 ON ns.customer_id = nc.customer_id
	INNER JOIN nscc_barang nb
	 ON nsd.barang_id = nb.barang_id
WHERE
	ns.bruto >= 0
	AND ns.log = 0
UNION
SELECT
	nb.kode_barang AS kodebrg,
	nsd.qty,
	CONCAT( nc.nama_customer , ' ' , nc.no_customer ) AS nama,
	nsd.ketpot AS komen,
	ns.ketdisc AS komen_resep,
	ns.doc_ref_sales AS resep,
	nb.sat AS satuan,
	date( ns.tgl ) AS tanggal
FROM
	nscc_salestrans_details nsd
	INNER JOIN nscc_salestrans ns
	 ON nsd.salestrans_id = ns.salestrans_id
	INNER JOIN nscc_salestrans ns1
	 ON ns.doc_ref_sales = ns1.doc_ref
	INNER JOIN nscc_customers nc
	 ON ns.customer_id = nc.customer_id
	INNER JOIN nscc_barang nb
	 ON nsd.barang_id = nb.barang_id
WHERE
	ns.bruto < 0
	AND ns.log = 0");
        return $res->queryAll(true);
    }
    public static function get_total_sales($tgl, $store = STOREID)
    {
        $where = "";
        $param = array(':tgl' => $tgl);
        if ($store != null) {
            $where = "AND store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("SELECT
    IFNULL(SUM(ns.total), 0) total FROM nscc_salestrans ns
    WHERE ns.bruto >= 0 AND DATE(ns.tgl) = :tgl $where");
        return $comm->queryScalar($param);
    }
    public static function get_disc_tax_bayar_change_all($tgl, $store = STOREID)
    {
        $where = "";
        $param = array(':tgl' => $tgl);
        if ($store != null) {
            $where = "AND store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("SELECT
    IFNULL(SUM(ns.totalpot), 0) totalpot,
    IFNULL(SUM(ns.vat), 0) vat,
    IFNULL(SUM(ns.bayar), 0) bayar,
    IFNULL(SUM(ns.total), 0) total,
    IFNULL(SUM(ns.kembali), 0) kembali
    FROM nscc_salestrans ns
    WHERE DATE(ns.tgl) = :tgl $where");
        $comm->setFetchMode(PDO::FETCH_OBJ);
        return $comm->queryRow(true, $param);
    }
    public static function get_total_returnsales($tgl, $store = STOREID)
    {
        $where = "";
        $param = array(':tgl' => $tgl);
        if ($store != null) {
            $where = "AND store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("SELECT
    IFNULL(SUM(ns.total), 0) total FROM nscc_salestrans ns
    WHERE ns.bruto < 0 AND DATE(ns.tgl) = :tgl $where");
        return $comm->queryScalar($param);
    }
    public static function get_tax_all($tgl, $store = STOREID)
    {
        $where = "";
        $param = array(':tgl' => $tgl);
        if ($store != null) {
            $where = "AND store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("SELECT
    IFNULL(SUM(ns.vat), 0) vat FROM nscc_salestrans ns
    WHERE DATE(ns.tgl) = :tgl $where");
        return $comm->queryScalar($param);
    }
    public static function get_discounts_all($tgl, $store = STOREID)
    {
        $where = "";
        $param = array(':tgl' => $tgl);
        if ($store != null) {
            $where = "AND store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("SELECT
    IFNULL(SUM(ns.totalpot), 0) totalpot FROM nscc_salestrans ns
    WHERE DATE(ns.tgl) = :tgl $where");
        return $comm->queryScalar($param);
    }
}