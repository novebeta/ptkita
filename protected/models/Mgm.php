<?php

Yii::import('application.models._base.BaseMgm');

class Mgm extends BaseMgm
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate()
    {
        if ($this->mgm_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->mgm_id = $uuid;
        }
        if ($this->tdate == null) {
            $this->tdate = new CDbExpression('NOW()');
        }
        if ($this->id_user == null) {
            $this->id_user = Yii::app()->user->getId();
        }
        return parent::beforeValidate();
    }
    public function rules()
    {
        $rules = array(
            array('customer_new_id','unique','message'=>'Member Get Member data already exists!'),
        );
        return array_merge($rules,parent::rules());

    }
}