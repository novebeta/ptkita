<?php
Yii::import('application.models._base.BaseTipeBarang');

class TipeBarang extends BaseTipeBarang
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->tipe_barang_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->tipe_barang_id = $uuid;
        }
        return parent::beforeValidate();
    }
}