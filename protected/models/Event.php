<?php

Yii::import('application.models._base.BaseEvent');
class Event extends BaseEvent
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->event_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->event_id = $uuid;
        }
        return parent::beforeValidate();
    }
}