<?php

Yii::import('application.models._base.BaseProduksiDetil');

class ProduksiDetil extends BaseProduksiDetil
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate()
    {
        if ($this->produksi_detil_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->produksi_detil_id = $uuid;
        }
        return parent::beforeValidate();
    }
}