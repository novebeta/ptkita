<?php

Yii::import('application.models._base.BaseCard');

class Card extends BaseCard
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate()
    {
        if ($this->card_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->card_id = $uuid;
        }
        return parent::beforeValidate();
    }
}