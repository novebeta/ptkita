<?php

Yii::import('application.models._base.BaseSalestransDetails');
class SalestransDetails extends BaseSalestransDetails
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->salestrans_details == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->salestrans_details = $uuid;
        }
        return parent::beforeValidate();
    }
    public function save_beauty_tips($beauty_id, $beauty2_id)
    {
        $delete = Yii::app()->db->createCommand("
        DELETE FROM nscc_beauty_services WHERE salestrans_details = :salestrans_details");
        $delete->execute(array(':salestrans_details' => $this->salestrans_details));
        if ($beauty_id != null) {
            BeautyServices::add_beauty_tips($this, $beauty_id);
        }
        if ($beauty2_id != null) {
            BeautyServices::add_beauty_tips($this, $beauty2_id);
        }
    }
    public static function get_diskon($customer_id, $barang_id)
    {
        $comm = Yii::app()->db->createCommand("
        SELECT nd.`value`,nsc.nama_status
        FROM nscc_status_cust AS nsc
        INNER JOIN nscc_customers AS nc ON nc.status_cust_id = nsc.status_cust_id
        INNER JOIN nscc_diskon AS nd ON nd.status_cust_id = nsc.status_cust_id
        WHERE nc.customer_id = :customer_id AND nd.barang_id = :barang_id");
        return $comm->queryRow(true, array('customer_id' => $customer_id, ':barang_id' => $barang_id));
    }
    public static function get_sales_trans_details($salestrans_id)
    {
        $comm = Yii::app()->db->createCommand("
        SELECT nsd.salestrans_details,nsd.barang_id,nsd.salestrans_id,nsd.qty,nsd.disc,nsd.discrp,
        nsd.ketpot,nsd.vat,nsd.vatrp,nsd.bruto,nsd.total,nsd.total_pot,nsd.price,nsd.jasa_dokter,
        nsd.dokter_id,disc_name,paket_trans_id,paket_details_id,
        (SELECT nsb.beauty_id FROM nscc_beauty_services nsb
        WHERE nsb.salestrans_details = nsd.salestrans_details LIMIT 1) beauty_id,
        (SELECT nsb.beauty_id FROM nscc_beauty_services nsb
        WHERE nsb.salestrans_details = nsd.salestrans_details LIMIT 1,1) beauty2_id,
	    nb.kode_barang,nb.nama_barang
        FROM nscc_barang nb
	    INNER JOIN nscc_salestrans_details nsd
	    ON nb.barang_id = nsd.barang_id
        WHERE nsd.salestrans_id = :salestrans_id");
        return $comm->queryAll(true, array(':salestrans_id' => $salestrans_id));
    }
    public static function get_retursales_trans_details($salestrans_id)
    {
        $comm = Yii::app()->db->createCommand("
        SELECT nsd.salestrans_details,nsd.barang_id,nsd.salestrans_id,-nsd.qty qty,-nsd.disc disc,-nsd.discrp discrp,
        -nsd.vat vat,-nsd.vatrp vatrp,-nsd.bruto bruto,-nsd.total total,-nsd.total_pot total_pot,
        nsd.ketpot,nsd.price,nsd.jasa_dokter,nsd.dokter_id,disc_name,nb.kode_barang,nb.nama_barang,
        nsd.paket_trans_id,nsd.paket_details_id
        FROM nscc_barang nb
	    INNER JOIN nscc_salestrans_details nsd
	    ON nb.barang_id = nsd.barang_id
        WHERE nsd.salestrans_id = :salestrans_id");
        return $comm->queryAll(true, array(':salestrans_id' => $salestrans_id));
    }
}