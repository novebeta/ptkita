<?php

Yii::import('application.models._base.BaseSouvenir');

class Souvenir extends BaseSouvenir
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate()
    {
        if ($this->souvenir_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->souvenir_id = $uuid;
        }
        return parent::beforeValidate();
    }
}