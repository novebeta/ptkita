<?php

/**
 * This is the model base class for the table "{{card}}".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "Card".
 *
 * Columns in table "{{card}}" available as properties of the model,
 * followed by relations of table "{{card}}" available as properties of the model.
 *
 * @property string $card_id
 * @property string $card_name
 * @property string $persen_fee
 * @property integer $up
 *
 * @property Payment[] $payments
 */
abstract class BaseCard extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return '{{card}}';
	}

	public static function representingColumn() {
		return 'card_name';
	}

	public function rules() {
		return array(
			array('card_id, card_name', 'required'),
			array('up', 'numerical', 'integerOnly'=>true),
			array('card_id', 'length', 'max'=>36),
			array('card_name', 'length', 'max'=>100),
			array('persen_fee', 'length', 'max'=>30),
			array('persen_fee, up', 'default', 'setOnEmpty' => true, 'value' => null),
			array('card_id, card_name, persen_fee, up', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'payments' => array(self::HAS_MANY, 'Payment', 'card_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'card_id' => Yii::t('app', 'Card'),
			'card_name' => Yii::t('app', 'Card Name'),
			'persen_fee' => Yii::t('app', 'Persen Fee'),
			'up' => Yii::t('app', 'Up'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('card_id', $this->card_id, true);
		$criteria->compare('card_name', $this->card_name, true);
		$criteria->compare('persen_fee', $this->persen_fee, true);
		$criteria->compare('up', $this->up);

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
		));
	}
}