<?php

/**
 * This is the model base class for the table "{{pelunasan_utang}}".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "PelunasanUtang".
 *
 * Columns in table "{{pelunasan_utang}}" available as properties of the model,
 * followed by relations of table "{{pelunasan_utang}}" available as properties of the model.
 *
 * @property string $pelunasan_utang_id
 * @property string $total
 * @property string $no_bg_cek
 * @property string $no_bukti
 * @property string $doc_ref
 * @property string $tgl
 * @property string $bank_id
 * @property string $supplier_id
 * @property string $store
 * @property string $tdate
 * @property string $user_id
 * @property integer $up
 *
 * @property Supplier $supplier
 * @property Bank $bank
 * @property PelunasanUtangDetil[] $pelunasanUtangDetils
 */
abstract class BasePelunasanUtang extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return '{{pelunasan_utang}}';
	}

	public static function representingColumn() {
		return 'total';
	}

	public function rules() {
		return array(
			array('pelunasan_utang_id, tgl, bank_id, supplier_id, store, tdate', 'required'),
			array('up', 'numerical', 'integerOnly'=>true),
			array('pelunasan_utang_id, no_bg_cek, no_bukti, doc_ref, bank_id, user_id', 'length', 'max'=>50),
			array('total', 'length', 'max'=>30),
			array('supplier_id', 'length', 'max'=>36),
			array('store', 'length', 'max'=>20),
			array('total, no_bg_cek, no_bukti, doc_ref, user_id, up', 'default', 'setOnEmpty' => true, 'value' => null),
			array('pelunasan_utang_id, total, no_bg_cek, no_bukti, doc_ref, tgl, bank_id, supplier_id, store, tdate, user_id, up', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'supplier' => array(self::BELONGS_TO, 'Supplier', 'supplier_id'),
			'bank' => array(self::BELONGS_TO, 'Bank', 'bank_id'),
			'pelunasanUtangDetils' => array(self::HAS_MANY, 'PelunasanUtangDetil', 'pelunasan_utang_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'pelunasan_utang_id' => Yii::t('app', 'Pelunasan Utang'),
			'total' => Yii::t('app', 'Total'),
			'no_bg_cek' => Yii::t('app', 'No Bg Cek'),
			'no_bukti' => Yii::t('app', 'No Bukti'),
			'doc_ref' => Yii::t('app', 'Doc Ref'),
			'tgl' => Yii::t('app', 'Tgl'),
			'bank_id' => Yii::t('app', 'Bank'),
			'supplier_id' => Yii::t('app', 'Supplier'),
			'store' => Yii::t('app', 'Store'),
			'tdate' => Yii::t('app', 'Tdate'),
			'user_id' => Yii::t('app', 'User'),
			'up' => Yii::t('app', 'Up'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('pelunasan_utang_id', $this->pelunasan_utang_id, true);
		$criteria->compare('total', $this->total, true);
		$criteria->compare('no_bg_cek', $this->no_bg_cek, true);
		$criteria->compare('no_bukti', $this->no_bukti, true);
		$criteria->compare('doc_ref', $this->doc_ref, true);
		$criteria->compare('tgl', $this->tgl, true);
		$criteria->compare('bank_id', $this->bank_id);
		$criteria->compare('supplier_id', $this->supplier_id);
		$criteria->compare('store', $this->store, true);
		$criteria->compare('tdate', $this->tdate, true);
		$criteria->compare('user_id', $this->user_id, true);
		$criteria->compare('up', $this->up);

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
		));
	}
}