<?php

/**
 * This is the model base class for the table "{{transaksi_absen}}".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "TransaksiAbsen".
 *
 * Columns in table "{{transaksi_absen}}" available as properties of the model,
 * and there are no model relations.
 *
 * @property string $absen_trans_id
 * @property string $tgl
 * @property string $tdate
 * @property string $remarks
 * @property string $employee_id
 * @property integer $jml
 * @property integer $tipe_absen_id
 * @property string $employee_name
 * @property string $tipe_absen_name
 *
 */
abstract class BaseTransaksiAbsen extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return '{{transaksi_absen}}';
	}

	public static function representingColumn() {
		return 'tgl';
	}

	public function rules() {
		return array(
			array('tgl, tdate, employee_id, jml, tipe_absen_id, employee_name, tipe_absen_name', 'required'),
			array('jml, tipe_absen_id', 'numerical', 'integerOnly'=>true),
			array('absen_trans_id, employee_id', 'length', 'max'=>10),
			array('employee_name, tipe_absen_name', 'length', 'max'=>100),
			array('remarks', 'safe'),
			array('absen_trans_id, remarks', 'default', 'setOnEmpty' => true, 'value' => null),
			array('absen_trans_id, tgl, tdate, remarks, employee_id, jml, tipe_absen_id, employee_name, tipe_absen_name', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'absen_trans_id' => Yii::t('app', 'Absen Trans'),
			'tgl' => Yii::t('app', 'Tgl'),
			'tdate' => Yii::t('app', 'Tdate'),
			'remarks' => Yii::t('app', 'Remarks'),
			'employee_id' => Yii::t('app', 'Employee'),
			'jml' => Yii::t('app', 'Jml'),
			'tipe_absen_id' => Yii::t('app', 'Tipe Absen'),
			'employee_name' => Yii::t('app', 'Employee Name'),
			'tipe_absen_name' => Yii::t('app', 'Tipe Absen Name'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('absen_trans_id', $this->absen_trans_id, true);
		$criteria->compare('tgl', $this->tgl, true);
		$criteria->compare('tdate', $this->tdate, true);
		$criteria->compare('remarks', $this->remarks, true);
		$criteria->compare('employee_id', $this->employee_id, true);
		$criteria->compare('jml', $this->jml);
		$criteria->compare('tipe_absen_id', $this->tipe_absen_id);
		$criteria->compare('employee_name', $this->employee_name, true);
		$criteria->compare('tipe_absen_name', $this->tipe_absen_name, true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
		));
	}
}