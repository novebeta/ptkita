<?php

Yii::import('application.models._base.BaseGlTrans');

class GlTrans extends BaseGlTrans
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function beforeValidate()
    {
        if ($this->counter == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->counter = $uuid;
        }
        if ($this->tdate == null) {
            $this->tdate = new CDbExpression('NOW()');
        }
        if ($this->store == null) {
            $this->store = STOREID;
        }
        if ($this->visible == null) {
            $this->visible = 1;
        }
        return parent::beforeValidate();
    }

    public static function jurnal_umum_index($tgl)
    {
        $comm = Yii::app()->db->createCommand("SELECT
        ngt.type_no,ngt.tran_date,ngt.amount tot_debit,ngt.amount tot_kredit,
        nscc_refs.reference,ngt.store
        FROM nscc_gl_trans AS ngt
        INNER JOIN nscc_refs ON ngt.type = nscc_refs.type_ AND ngt.type_no = nscc_refs.type_no
        WHERE ngt.type = :type AND ngt.amount > 0 AND ngt.tran_date = :tgl AND ngt.visible = 1");
        return $comm->queryAll(true, array(':type' => JURNAL_UMUM, ':tgl' => $tgl));
    }

    public static function jurnal_umum_details($type_no)
    {
        $comm = Yii::app()->db->createCommand("SELECT
        ngt.type_no,ngt.tran_date,ngt.amount,
        if(ngt.amount >= 0,ngt.amount,0) debit,
        if(ngt.amount < 0,-ngt.amount,0) kredit,
        ngt.counter,ngt.memo_,ngt.account_code
        FROM nscc_gl_trans AS ngt
        WHERE ngt.type = :type AND ngt.type_no = :type_no AND ngt.visible = 1");
        return $comm->queryAll(true, array(':type' => JURNAL_UMUM, ':type_no' => $type_no));
    }



}